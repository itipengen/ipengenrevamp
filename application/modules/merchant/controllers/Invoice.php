<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Invoice extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->model(array('order_model','dashboard_model','Invoice_model'));
		$this->load->library('M_pdf'); 
	}
	public function index()
	{
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		$data['merchant_id'] = $merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		$data['getInvoice']=$this->Invoice_model->getInvoiceByMerchant($merchant_id);
		$data["page_title"] = "Invoice || iPengen";
		$data["page_desc"] = "Invoice";
		// print_r($data);die();
		$this->load->mtemplate('invoice/index',$data);
	}
	
	public function getFilteredInvoiceBymerchant()
	{
		if($this->input->is_ajax_request()){
			$post=$this->input->post();
			$getMerchantInvoiceDetails=$this->Invoice_model->getMerchantInvoice($post);
			echo json_encode($getMerchantInvoiceDetails);
		}
	}
	
	function details($mid,$invid)
	{
		$data["page_title"] = "Invoice Details | iPengen";
		
		//echo $mid.'---'.$invid;die();
		$data['getInvoiceProduct'] = $this->Invoice_model->invoiceProductDetails($mid,$invid);
		$inv_id=str_replace('IPGNINV', "", $invid);
		$data['getInvoiceDetails']=$this->Invoice_model->getInvoiceDetails($inv_id);
		//print_r($data);die();
		//die();
		$this->load->mtemplate("invoice/details", $data);
	}
	
	public function downloadpdf($inv_id){
		$data=array();

		if(!empty($inv_id)){
			$invid=str_replace('IPGNINV', "", $inv_id);
		   $data['getInvoiceDetails']=$this->Invoice_model->getInvoiceDetails($invid);
	        if(!empty($data['getInvoiceDetails'])){
				echo "ff";
				$mid=$data['getInvoiceDetails'][0]->merchant_id;
				$data['getInvoiceProduct'] = $this->Invoice_model->invoiceProductDetails($mid,$inv_id);
				$data['businessnamearray'] = $this->Invoice_model->getbusinessname($mid);
				//$data['currency']="RP ";
				$data['currency']=$this->config->item('currency');
				$html=$this->load->view("invoicepdfhtml/invoice_download", $data,true);
				$mpdf = new mPDF();
				$this->m_pdf->pdf->Bookmark('invoice ipengen');
				//$data['datetime']=date("d.m.Y h:i:sa");
				$this->m_pdf->pdf->WriteHTML($html);
				$this->m_pdf->pdf->Output($inv_id."-Ipegen", 'I');
			}
		}
	 }
  
	

}

?>