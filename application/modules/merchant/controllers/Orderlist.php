<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Orderlist extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->model(array('order_model','dashboard_model','Orderlist_model')); 
	}
	public function index()
	{
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		$data["page_title"] = "Report || iPengen";
		$data["page_desc"] = "Report";
		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		$data['getReport']=$this->Orderlist_model->getAllReportList($merchant_id);	
		// print_r($data);die();
		$this->load->mtemplate('order/orderlist',$data);
	}
	
	public function searchOrder()
	{
		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		if($this->input->is_ajax_request()){
			
			$post=$this->input->post();
			if($post['tag']=='between'){
				$my_orderList['listData']=$this->Orderlist_model->my_orderList($post,$merchant_id);	
				$my_orderTotalPrice['totalPrice']=$this->Orderlist_model->my_orderTotalPrice($post,$merchant_id);	
				$totalArray = array_merge($my_orderList, $my_orderTotalPrice);
			}
			if($post['tag']=='week'){
				$dayDifference=$post['weekSrch']*7;
				//echo date('Y-m-d', strtotime('-14 days'));
				$post['dateFrom']=date('Y-m-d', strtotime("-$dayDifference days"));
				$post['dateTo']=date('Y-m-d');
				//print_r($post);die();
				$my_orderList['listData']=$this->Orderlist_model->my_orderList($post,$merchant_id);	
				$my_orderTotalPrice['totalPrice']=$this->Orderlist_model->my_orderTotalPrice($post,$merchant_id);	
				$totalArray = array_merge($my_orderList, $my_orderTotalPrice);
			}
			//print_r($totalArray);die();
			echo json_encode($totalArray);	
			
			//print_r($my_orderList);die();
		}
	}
}

?>