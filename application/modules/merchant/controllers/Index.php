<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Index extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model(array('login_model','dashboard_model','product_model','order_model')); 
		$this->load->helper(array('ipengen_email_helper'));
		$this->load->library('email');
	}
	public function index()
	{
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		else{
			
			redirect('merchant/dashboard');}
	}

	function login()
	{ 
	$data["page_title"] = "Merchant Log-In || iPengen";

		if (!$this->session->userdata('merchant_logged_in'))
		{
			if(isset($_POST['submit']))
			{ 
				  $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	
				  $this->form_validation->set_rules('password', 'Password', 'required');
	
					if($this->form_validation->run() == TRUE)
					{
						$username = $this->input->post('email');
						$password = $this->input->post('password');
						$result   = $this->login_model->login($username, $password);
						if($result!='')
						{
							 if($result->marchent_status != 0)
							{
								$this->session->set_userdata('merchant_logged_in',$result->marchent_email);
								$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
								$merchant_name = $this->dashboard_model->getMerchantnameByEmail($this->session->userdata('merchant_logged_in'));
								$login_notification = array(
			                        'm_id' => $merchant_id,
									'type' => "login",
									'message'=> $merchant_name . " is login  on ". date('d-m-Y'),
									'raw' => '',
									'date' => date('Y-m-d'),
									'notification_for' => 1,
			                    );
								
					          $this->product_model->insert_admin_notification($login_notification);
							  
								$data["status"] = "login";
								$data["merchantNAme"] = $merchant_name;
								$data["productcount"] = '';
								$emailBody = $this->load->view("email_template/bulk_uplode.php",$data,true);
								$supportData = $this->order_model->support_data();
								if(!empty($supportData))
								{
								$emailTo = $supportData[0]->info_email; 
								$subject = $merchant_name . " is login on". date('d-m-Y');
								
								$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$this->session->userdata('merchant_logged_in'),
								"to"		=>	$emailTo,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
									);
								send_email($emailData);
								
								}
								//die('oooo');
								$merchant_Details=$this->dashboard_model->getDetailsById($merchant_id);
								//print_r($merchant_Details);die();
								if(isset($merchant_Details) && $merchant_Details!=''){
									$company_name = $merchant_Details->bname;
									$account_name = $merchant_Details->account_name;
									$account_number = $merchant_Details->account_number;
									$bank_name = $merchant_Details->bank_name;
									$store_name = $merchant_Details->sname;
									$brands = $merchant_Details->brands;		
									if($company_name=='' || $account_name=='' || $account_number='' || $bank_name=='' || $store_name=='' || $brands==''){
										//$res['status']=0;	
										redirect('merchant/profile');	
									}
									else{
										//$res['status']=1;
										redirect('merchant/dashboard');
									}
								}
								else{
									//$res['status']=0;
									redirect('merchant/login');
								}
								//redirect('merchant/dashboard');
							}
							else
							{
								$this->session->set_flashdata('msg','Please wait until your account is approved by Admin!');
								redirect("merchant/login");
							} 
						}
						else
						{
							$this->session->set_flashdata('msg','Email or Password is invalid!');
							$data["page_title"] = "Merchant Log-In || iPengen";
							$this->load->view('merchant/login',$data);	
						}
	
					}
					else
					{
						$this->session->set_flashdata('msg','Invalid Email or Password is required!');
						$data["page_title"] = "Merchant Log-In || iPengen";
						$this->load->view('merchant/login',$data);
					}
				}
			else
			{ 
				$data["page_title"] = "Merchant Log-In || iPengen";
				$this->load->view('merchant/login',$data);
			}
		}
		else
		{
			$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
			$merchant_Details=$this->dashboard_model->getDetailsById($merchant_id);
			//print_r($merchant_Details);die();
			if(isset($merchant_Details) && $merchant_Details!=''){
				$company_name = $merchant_Details->bname;
				$account_name = $merchant_Details->account_name;
				$account_number = $merchant_Details->account_number;
				$bank_name = $merchant_Details->bank_name;
				$store_name = $merchant_Details->sname;
				$brands = $merchant_Details->brands;		
				if($company_name=='' || $account_name=='' || $account_number='' || $bank_name=='' || $store_name=='' || $brands==''){
					//$res['status']=0;	
					redirect('merchant/profile');	
				}
				else{
					//$res['status']=1;
					redirect('merchant/dashboard');
				}
			}
			else{
				//$res['status']=0;
				redirect('merchant/login');
			}
			//$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));die('9999');
			//redirect('merchant/dashboard');
		}
	 }

	function logout()
	{
		$this->session->unset_userdata('merchant_logged_in');
		redirect('merchant/login');
	}

	function forget_password()
	{   
		$data["page_title"] = "Merchant Forget Password | iPengen";
		if(isset($_POST['email_submit']))
		{
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() == TRUE)
			{
				$cusd=$this->login_model->forgotemail($this->input->post('email'));
					if(!empty($cusd))
					{
						$email_to = $this->input->post('email');
						$subject="[Ipengen] Change Password";	//subject
						
						$data['username'] = ucfirst($cusd->marchent_first_name).' '.ucfirst($cusd->marchent_last_name);
						$data['imgURL'] = base_url('');
						$data['link'] = base_url('merchant/index/resetpassword/'.base64_encode($email_to));
						$message = $this->load->view('e_template/password-change',$data, true);
						$email= 'ipengen.com';
						$emailData = array(
								"title" 	=> "Ipengen-Forget Password",
								"from"		=> $email ,
								"to"		=>	$email_to,
								"subject"	=>	$subject,
								"message"	=>	$message,
			                  );
			          
						if(send_email($emailData)){
						   $this->session->set_flashdata('smsg','Thank You! Please check your mail to reset password.');
						   redirect('merchant/login');
						}else{
						  $this->session->set_flashdata('forgetpwd_ErrMsg','Oops..!!! Something Wrong..Please try again.');
						  redirect('merchant/forget_password');
						}
						$this->email->print_debugger();
	               }else{
						$this->session->set_flashdata('msg','Email is not registered!');
						redirect('merchant/login'); 
				}
			 }
			else
			{
				$data["page_title"] = "Merchant Forget Password | iPengen";
				$this->load->view('merchant/fotgotpassword',$data);
			}
		}
		else
		{
			$data["page_title"] = "Merchant Forget Password | iPengen";
			$this->load->view('merchant/fotgotpassword',$data);
		}
	}

    function resetpassword($log_email)
	{  
		$data["page_title"] = "Merchant Reset Password || iPengen";
		if(!empty($_POST))
		{
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('re_password', 'Re-enter password', 'required|matches[password]');
			if($this->form_validation->run() == TRUE)
			{
				$password=$this->input->post('password');
				$total_result=$this->login_model->admin_exist();
				if($total_result)
				{
					foreach($total_result as $result_singal)
					{
						if($log_email !== NULL || $log_email != '')
						 $this->db->update('ig_marchent_login',array('marchent_password'=>md5($this->config->item('email_verification_salt').$password)),array('marchent_email'=>base64_decode($log_email)));
						 $this->session->set_flashdata('msg','Password Change successfully');
							redirect('merchant/login');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('re-msg','Password is not matched.');
				redirect('merchant/resetpassword/'.$log_id);}
			}
		else
		{
			$this->load->view('merchant/resetpassword',array('id'=>$log_email,"page_title"=>"Merchant Reset Password || iPengen"));
		}

	}	

	public function dashboard()
	{
		if (!$this->session->userdata('merchant_logged_in')){
			redirect('merchant/login');
		}else{
			$saleDate=array();
			$saleTotal=array();
			$saleColor=array();
			$saleBorderColor=array();
			$data["page_title"] = "Merchant Dashboard || iPengen";
			$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
			$data["merchant_details"] = $this->dashboard_model->getDetailsById($merchant_id);
			$data["total_product"] = $this->dashboard_model->getTotalProduct($merchant_id);
			$data["total_order"] = $this->dashboard_model->getTotalorder($merchant_id);
			$data["products"] = $this->dashboard_model->list_orders($merchant_id);
			$data["approveRequest"] = $this->dashboard_model->send_approved_request($merchant_id);
			$data["getadminApproval"] = $this->dashboard_model->get_admin_approval($merchant_id);
			$data["getSaleStat"] =$getSaleStat = $this->dashboard_model->get_sale_stat($merchant_id);
			//print_r($getSaleStat);die();
			foreach($getSaleStat as $key=>$val){
				$saleDay=explode('-',$val['date']);
				$saleDate[$key]=$saleDay[2];
				$saleTotal[$key]=$val['total'];
				$saleColor[$key]="'rgba(54, 162, 235, 0.2)'";
				$saleBorderColor[$key]="'rgba(54, 162, 235, 1)'";
				
				//print_r($val['total']);
			}
			//$dateDay=explode('-',$saleDate);
			$data['saleDate']=$saleDate;
			$data['saleTotal']=$saleTotal;
			$data['saleColor']=$saleColor;
			$data['saleBorderColor']=$saleBorderColor;
			//echo implode(",",$saleDate);
			//print_r($saleColor);
			//print_r($saleBorderColor);
			//die();
			$data["merchantid"] = $merchant_id;
			$this->load->mtemplate('site/index',$data);
		}
	}
	
	public function profile()
	{
		if (!$this->session->userdata('merchant_logged_in')){
			redirect('merchant/login');
		}else{
			$data["page_title"] = "Merchant Dashboard || iPengen";
			$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
			$data["merchant_details"] = $this->dashboard_model->getDetailsById($merchant_id);
			$this->load->mtemplate('site/profile',$data);
		}
	}

	public function business_registration()
	{
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
	        $this->form_validation->set_error_delimiters( '<div class="error">','</div>' );
	        if($this->form_validation->run() == TRUE){ 
	            $data['cname'] = $this->input->post('cname');
	            $data['cdescription'] = $this->input->post('cdescription');
	            $data['cemail'] = $this->input->post('cemail');
	            $data['cphone'] = $this->input->post('cphone');
	            $data['merchant_id'] = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
	            
	            $details = $this->dashboard_model->getBusinessByMerchantId($data['merchant_id']);
	            if(isset($details) && $details!=''){
		            $businessId = $details->business_id;
		            $business = $this->dashboard_model->updateBusinessByBId($data,$businessId);
		            if($business){
		            	$result['status']  = 'success';
		                $result['message'] = 'Business Details updated successfully!';
		            }
		        }
		        else{
		        	$business_id = $this->dashboard_model->addBusiness($data); 
		            if($business_id != 0){
	            		$result['business_id'] = $business_id;
	            		$result['status'] = 'success';
	                    $result['message'] ='Business Details added successfully!';
		            }
		        }
       		}
	        else{
	            $result['status'] = 'error';
	        }
	         echo json_encode($result);
		}
	}

	public function approve_success()
	{
		$email =  $this->input->post('email');	
		$insert_success = $this->dashboard_model->approvesuccess($email);
		if($insert_success){
		            	$result['msg'] = "success";
						
						$supportData = $this->order_model->support_data();
						$data["merchant"] = $email;
						$emailBody = $this->load->view("email_template/merchnat_approve",$data,true);
						$emailto = $supportData[0]->info_email;
						$subject = 'New merchant send an approved request';
						$emailData = array(
							"title" 	=> "Ipengen",
							"from"		=>	$email,
							"to"		=>	$emailto,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
						);
						send_email($emailData);
						
						
		
		            }else{
						$result['msg'] = "error";
					}
					echo json_encode($result);
	
	}
	public function bank_registration()
	{
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('acc_name', 'Account Name', 'trim|required');
			$this->form_validation->set_rules('acc_number', 'Account Number', 'trim|required');
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required');

	        $this->form_validation->set_error_delimiters( '<div class="error">','</div>' );

	        if($this->form_validation->run() == TRUE){ 
	            $data['acc_name'] = $this->input->post('acc_name');
	            $data['acc_number'] = $this->input->post('acc_number');
	            $data['bank_name'] = $this->input->post('bank_name');
	            $data['branch_name'] = $this->input->post('branch_name');
	            $data['state'] = $this->input->post('state');
	            $data['city'] = $this->input->post('city');

	            $data['merchant_id'] = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));

	            $details = $this->dashboard_model->getBankByMerchantId($data['merchant_id']);
	            if(isset($details) && $details!=''){
		            $bankId = $details->bank_id;
		            $bank = $this->dashboard_model->updateBankByBankId($data,$bankId);
		            if($bank){
		            	$result['status']  =  'success';
		                $result['message'] = 'Bank Details updated successfully!';
		            }
		        }
		        else{
		        	$bank_id = $this->dashboard_model->addBank($data);  
		            if($bank_id != 0){
		            		$result['bank_id'] = $bank_id;
		            		$result['status'] = 'success';
		                    $result['message'] = 'Bank Details added successfully!';
		            }
		        }
	            
       		 }
	        else{
	            $result['status'] = 'error';
	        }
	         echo json_encode($result);
		}
	}

	public function store_registration()
	{
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('store_name', 'Store Name', 'trim|required');
			$this->form_validation->set_rules('brands', 'Brands', 'trim|required');

	        $this->form_validation->set_error_delimiters( '<div class="error">','</div>' );

	        if($this->form_validation->run() == TRUE){ 
	            $data['store_name'] = $this->input->post('store_name');
	            $data['sdescription'] = $this->input->post('sdescription');
	            $data['brands'] = $this->input->post('brands');
	            $data['merchant_id'] = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));

	            $details = $this->dashboard_model->getStoreByMerchantId($data['merchant_id']);
	            if(isset($details) && $details!=''){
		            $storeId = $details->store_id;
		            $store = $this->dashboard_model->updateStoreByStoreId($data,$storeId);
		            if($store){
		            	$result['status'] = 'success';
		                $result['message'] =  'Store Details updated successfully!';
		            }
		        }
		        else{
		        	$store_id = $this->dashboard_model->addStore($data);  
		            if($store_id != 0){
		            		$result['store_id'] = $store_id;
		            		$result['status'] = 'success';
		                    $result['message'] =  'Store Details added successfully!';
		             }
		        }
       		 }
	        else{
	            $result['status'] = 'error';
	        }
	         echo json_encode($result);
		}
	}
	
	 public function notifiction(){
	   
	   $data["page_title"] = "Notification";
	   $data['notificationresult'] = "";
	   $this->load->mtemplate('site/notification',$data);
	   
	   
	}
	
    public function ajaxnotification(){
		
		
		// storing  request (ie, get/post) global array to a variable  
		$requestData= $_REQUEST;
		
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'type',
			2=> 'message',
			3=> 'date',
			4=> 'status',
		);
		
		

		$result11 = $this->dashboard_model->getajaxnotification($requestData);
		$totalFiltered = $result11['totalFiltered'];
		
        $data = array();
		foreach($result11['queryresult'] as $row){  // preparing an array
		
		if($row['status'] == 1){
				$status = '<span class="label label-sm label-warning"> Read </span>';
			}
			else{
				$status = '<span class="label label-sm label-success"> Unread </span>';
				}
		
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["type"]);
			$nestedData[] = ucfirst($row["message"]);
			$nestedData[] = date('d/m/Y', strtotime($row["date"]));
			$nestedData[] = $status;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		  "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalFiltered ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
			echo json_encode($json_data);  // send data as json format
		
		}




}



?>