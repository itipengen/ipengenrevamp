<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Order extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','commerceapi'));
		$this->load->helper(array('url','form'));
		$this->load->model(array('order_model','dashboard_model','product_model')); 
		$this->load->helper(array('ipengen_email_helper'));
	}
	public function index()
	{
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		else{ 
		
		$data["page_title"] = "List Orders || iPengen";
		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		$data['order_list']=$this->order_model->get_orders($merchant_id);
		foreach($data['order_list'] as $list){
			$product_id = $list->product_id;
			$data['product'] = $this->product_model->getProductById($product_id);
		}
		$data['approved_list']=$this->order_model->get_approved_orders($merchant_id);
		$data['cancelled_list']=$this->order_model->get_cancelled_orders($merchant_id);
		$data['delivered_list']=$this->order_model->get_delivered_orders($merchant_id);
		$data['shipped']=$this->order_model->get_shipped_orders($merchant_id);
		$data['count_new_order']=$this->order_model->get_count_new_order($merchant_id);
		$mstore = $this->dashboard_model->getStoreByMerchantId($merchant_id);
		$data['store'] = $mstore->sname.' '.$mstore->sdescription;
		$this->load->mtemplate('merchant/order/index',$data);
		
		
		}
	}

	
	public function details($id,$pId)
	{
		//echo $pId;die();
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		else{ 
		$data["page_title"] = "Order Details || iPengen";
		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		$data['order_items_list']=$this->order_model->get_orders_items($id,$pId,$merchant_id); 
		$data['orderdetailshistory']= $this->order_model->getOrderDetailshistory($id,$pId);
		//print_r($data['orderdetailshistory']);die();
		$this->load->mtemplate('merchant/order/details',$data);
		
		
		}
	}

	public function status_change()
		{
			$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
			$orderId =  $this->input->post('orderid');
			$productId =  $this->input->post('productid');
			$status =  $this->input->post('status');
			$otid =  $this->input->post('otid');
			$suborder_id =  $this->input->post('suborder_id');
			$quantity = $this->input->post('quantity');
			$price = $this->input->post('price');
			$subtotal = $this->input->post('subtotal');
			$sku = $this->input->post('sku');
			$store = $this->input->post('store');
			$order_date = $this->input->post('order_date');
			$order_date = current(explode(' ',$order_date));
			$orderHeaders=array(
				'order_no'=>$suborder_id,
				'order_date'=> $order_date,
				'due_date'  => $order_date,
				'courier_id'=>$this->input->post('courier_id'),
			 	'cod'=>$this->input->post('cod'),
			 	'order_source'=>$this->input->post('order_source'),
			 	'dest_name'=>$this->input->post('dest_name'),
			 	'dest_address1'=>$this->input->post('dest_address1'),
			 	'dest_country'=>$this->input->post('dest_country'),
			 	'dest_province'=>$this->input->post('dest_province'),
			 	'dest_city'=>$this->input->post('dest_city'),
			 	'dest_postal_code'=>$this->input->post('dest_postal_code'),
			 	'fulfillment_center_id'=>$this->input->post('fulfillment_center_id'),
			 	'awb_no' => '',
			 	'insured' => 0,
			 	'dest_address2' => '',
			 	'dest_area' => '',
			 	'dest_sub_area' => '',
			 	'dest_village' => '',
			 	'dest_remarks' => '',
			 	'promo_code' => '',
			 	'dest_phone' => '',
			 	'dest_mobile' => '',
			 	'dest_email' => '',
			 	'special_packaging' => ''
			);

			$orderDetails=array(
				'sku_code'=> $sku,
				'sku_description'  => 'description',
				'qty_order'=> $quantity,
			 	'price'=> $price,
			 	'amount_order'=> $subtotal,
			 	'remarks'=>$store,
			 	'promo_code'=>'',
			 	'insured'=>0,
			 	'special_packaging'=>'',
			);
			if($status == 1){
				/** Checking if order header id exists in table **/
				$order_header_id_exists = $this->order_model->checkForOrderHeaderId($orderId,$productId);
				if($order_header_id_exists == 0){
					if(isset($orderHeaders)){
						/** Set Sales Order **/
						$header = $this->commerceapi->setOrder($orderHeaders);
						$headerArray = json_decode($header);
						$order_header_id = $headerArray->order_header_id;
						if($order_header_id > 0){
							$this->order_model->updateOrderHeaderId($orderId,$order_header_id,$productId);
							/** Order Details 
							$orderDetails['order_header_id'] = $order_header_id;
							$apiOrderDetails = $this->commerceapi->setDetails($orderDetails);
							**/
							/** Get Order Status **/
							$apiGetStatus = $this->commerceapi->getStatus($suborder_id);

							$json_array = json_decode($apiOrderDetails);
							print_r($json_array); 

							$json_status_array = json_decode($apiGetStatus);
							foreach ($json_status_array as $key => $value) {
								$statusArr = array(
									'p_status' =>$value->status,
									'awb_no' =>$value->awb_no,
									'courier_id' =>$value->courier_id
								);
								
								$ordHisstatusArr = array(
									'order_id' => $orderId,
									'order_product_id' =>$productId,
									'status' =>$value->status,
									'remark' =>$value->status,
									'orderchange_date' =>date('Y-m-d H:i:s')
								);
							}
							if(isset($statusArr) && !empty($statusArr)){
								$this->order_model->updateOrderItemStatus($statusArr,$orderId,$order_header_id,$productId);
							}
							
							if(isset($ordHisstatusArr) && !empty($ordHisstatusArr)){
								$this->order_model->insertOrderHistoryStatus($ordHisstatusArr);
							}
						}
					}
				}
			}

			$statuschange = array(
					'm_order_id' => $orderId,
					'm_product_id' => $productId,
					'm_status' => $status,					);
			$updatestatus = array('order_status' => $status);	
			$insert_order = $this->order_model->statuschange($statuschange);
			$status_order_product = $this->order_model->statuschangeonproduct($orderId,$otid,$updatestatus);
			$count =$this->order_model->get_count_new_order($merchant_id);
			if(!empty($insert_order)){
				
				if($status == 1)
				{ $stat = "Approved"; }
				elseif($status == 2)
				{$stat = "Cancelled";}
				
				$data["order_id"] = $orderId;
				$data["status"] = $stat;
			    $data["userData"] = $this->order_model->getUserData($orderId);
				$data['productdetails'] = $this->order_model->getproductDetails($productId,$orderId,$merchant_id);
				$emailBody = $this->load->view("email_template/order-status-delivery",$data,true);
				$supportData = $this->order_model->support_data();
				$emailTo = $data["userData"][0]->userEmail;
				 if(!empty($supportData))
			      {
					if(count($data['productdetails']) == 1)
					{ $productName = 'of "'.$data['productdetails'][0]->name.'"'; }
					if($status == 1){
						$var = "approved by Merchant";
					}elseif($status == 2){
						$var = "cancelled by Merchant";
					}elseif($status == 3){
						$var = "approved by Admin";
					}elseif($status == 4){
						$var = "cancelled by Admin";
					}

					$emailFrom = $supportData[0]->info_email;
					$subject = 'Your Ipengen order (#'.$orderId.') '.$productName.' has been '. $var;
					$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$emailFrom,
								"to"		=>	$emailTo,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					send_email($emailData);
			    }
				
				$result['msg'] = "success";
				$result['count'] = $count;
			}else{
			    $result['msg'] = "error";	
			}
			
			echo json_encode($result); 
		}
}

?>