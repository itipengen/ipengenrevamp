<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Product extends MX_Controller
	{
		var $extension_array;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->library('session'); 
		$this->load->library('imageresize');
		$this->load->library(array('form_validation','csvimport','imageresize','upload','unzip','excel'));
		$this->load->helper(array('ipengen_email_helper'));
		$this->load->model(array('product_model','dashboard_model','order_model')); 
		if (!$this->session->userdata('merchant_logged_in')){redirect('merchant/login');}
		$this->extension_array = array('jpg','png','jpeg','JPG','PNG','JPEG');
		
	}
	public function index()
	{
		$data["page_title"] = "Add Products || iPengen";
		$categories = $this->product_model->getCategories();
		foreach ($categories as $key => $value) {
			$category['cat_id'] = $value['cat_id'];
			$category['name'] = $value['name'];
			$newArr[] = $category;
		}
		
		
		require_once '/home/ipengenc/public_html/staging/application/third_party/PHPExcel/IOFactory.php';
				
		$data['categories'] = $newArr;
		$insert_data_text = array();
		$image = array();
		$inputFileType = 'Excel2007';
		if(isset($_FILES) && !empty($_FILES)){
				$category = $this->input->post('category');
				$subcategory = $this->input->post('subcategory');
				$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		        $data['error'] = '';    //initialize image upload error array to empty
		        if(isset($_FILES['product'])){
		        	$path = $this->config->item('upload_image_path').'csv/';
		        	$csv_files = $this->multiple_upload('product',$path,'xlsx','1000');
		        	if($csv_files == false){
		        		$data['error'] = "The filetype you are attempting to upload is not allowed!";
		        	}
		        	else{
						$file_path =  $path.$csv_files['file_name'];
						$inputFileName = $file_path;
		                $sheetnames = array("Products");
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objReader->setLoadSheetsOnly($sheetnames);
                        $objPHPExcel = $objReader->load($inputFileName);
						/**  Read the document's creator property  **/
                        $creator = $objPHPExcel->getProperties()->getCreator();
						$loadedSheetNames = $objPHPExcel->getSheetNames();
						
						$sheetData = $objPHPExcel->setActiveSheetIndexByName("Products")->toArray(null,true,true,true);
						$i=0;
						
			            if (!empty($sheetData)) {
			                foreach ($sheetData as $key=>$row) {
								if($key!=1){
								if($row['B']!='' || $row['C']){
			                	$brand = $row['F'];
			                	$brand_id = $this->product_model->searchBrand($brand);
			                    $insert_data = array(
			                        'sku'=>$row['B'],
									'stock_quantity'=>$row['C'],
			                        'price'=>$row['D'],
			                        'category_id'=>$subcategory,
			                        'brand_id'=>$brand_id,
			                        'sale_price'=>$row['E'],
			                        'sale_start_date'=>$row['G'],
			                        'sale_end_date'=>$row['H'],
			                        'create_date'=>date('Y-m-d H:i:s'),
			                        'merchant_id' => $merchant_id,
			                        'admin_status' => 0
			                        
			                    );
			                    $product_id = $this->product_model->insert_products($insert_data);
								$i++;
								$insert_id = $this->db->insert_id();//$this->db->insert_id();2284
								$productDetails = $this->product_model->getProductById($insert_id);
								//print_r($productDetails->category_id);die();
								$result_id=$productDetails->category_id;
								if($result_id!=""){
									$category = $this->product_model->category_id($result_id);
									//print_r($category);die();
									$cat_name = $category->cat_id;
									$parent_id = $this->product_model->getcatParentId($result_id);
									if($parent_id != 0){
										$catgry = $this->product_model->category_id($parent_id);
										$category_name = $catgry->cat_id;
										$parent_details = $this->product_model->getcategoryById($parent_id);
										foreach($parent_details as $parent){
											$category_id = $parent->parent_id;	
											if($category_id == 0){
												$string = ucfirst($category_name).','.ucfirst($cat_name);
											}
										}
									}else{
										$string = ucfirst($cat_name);
										
									}
										$updateProduct = $this->product_model->updateProduct($insert_id,$string);
									//echo $string;die();
							
								}
			                    $folder['product_id'] =  $product_id;
			                    $folder['name'] = $row['N'];
			                    array_push($image, $folder);
			                    $this->session->set_userdata('product_image_folder',$image);
			                    $insert_data_en_text = array(
			                        'product_id' => $product_id,
									'language_id' => 1,
									'product_name' => $row['I'],
									'short_description' => $row['J'],
									'message' => $row['K']
			                    );
			                    $insert_data_id_text = array(
			                        'product_id' => $product_id,
									'language_id' => 2,
									'product_name' => $row['L'],
									'short_description' => '',
									'message' => $row['M']
			                    );
			                    array_push($insert_data_text, $insert_data_en_text, $insert_data_id_text);
			                }
						  }
						}
			                 $this->product_model->insert_producttext($insert_data_text);
			                 $data['success'] = $i. ' Bulk Products Added Succesfully';
			            } else {
			                $data['error'] = 'Upload Failed!';
			            }
						
				    }
					if(isset($insert_id) && $insert_id !='')
					{
				    $merchant_name = $this->dashboard_model->getMerchantnameByEmail($this->session->userdata('merchant_logged_in'));		
					$bulk_uplode_status = array(
			                        'merchant_id' => $merchant_id,
									'total_product' => $i,
									'date' => date('Y-m-d'),
			                    );
					
					$bulk_uplode_notification = array(
			                        'm_id' => $merchant_id,
									'type' => "bulk uplode",
									'message'=> $merchant_name . " uploded ". $i . " bulk propduct on ". date('d-m-Y'),
									'raw' => '',
									'date' => date('Y-m-d'),
									'notification_for' => 1,
			                    );
								
					$this->product_model->insert_bulk_status($bulk_uplode_status);
					$this->product_model->insert_admin_notification($bulk_uplode_notification);
					
					$data["status"] = "Bulk Product Upload";
					$data["merchantNAme"] = $merchant_name;
					$data["productcount"] = $i;
					$emailBody = $this->load->view("email_template/bulk_uplode.php",$data,true);
					$supportData = $this->order_model->support_data();
					if(!empty($supportData))
			         {
						$emailTo = $supportData[0]->info_email; 
						$subject = $merchant_name . " uploded ". $i . " bulk product on ". date('d-m-Y');
						
						$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$this->session->userdata('merchant_logged_in'),
								"to"		=>	$emailTo,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					    send_email($emailData);
						 
					 }
					
					
					}
		        }
		        if(isset($_FILES['product_image'])){ //print_r($_FILES);
		        	$zpath = $this->config->item('upload_image_path').'zip/';
		        	$zip_files = $this->multiple_upload('product_image',$zpath,'zip','');
		        	$zip_name = $zip_files['file_name']; 
		        	$encryptName = str_replace('.zip','',$zip_name);
		        	if(!is_dir($zpath.$encryptName)){ mkdir($zpath.$encryptName); }
		        	$source = $zpath.$zip_name;
		        	$destination = $zpath.$encryptName.'/';
		        	$this->unzip->extract($source,$destination);
		        	$dir = $destination.'images/';
						// Open a directory, and read its contents
					if (is_dir($dir)){ 
						if ($dh = opendir($dir)){
						  	$variable = $this->session->userdata('product_image_folder');
						  	foreach ($variable as $key => $value) {
						  		$subDir = $dir.$value['name'];
						  		if(is_dir($subDir)){
						  			if ($sdh = opendir($subDir)){
						  				$main_dir = $subDir.'/main/';
						  				$gallery_dir = $subDir.'/gallery/';
						  				if(is_dir($main_dir)){
					  						if($main = opendir($main_dir)){
								    			while (($file = readdir($main)) !== false){
											      	if($file!='')
											      	{
												      	$img_path = $main_dir.$file;
												      	if(is_file($img_path))
												      	{
												      		$this->originalImgUpload($file,$value['product_id'],$main_dir);
												      	}
											    	}
										    	}						  				
											}
										}else{
											$data['error'] = "No main directory found!";
										}
										if(is_dir($gallery_dir)){
					  						if($gallery = opendir($gallery_dir)){
					  							$index = 0;
								    			while (($gfile = readdir($gallery)) !== false){
											      	if($gfile!='')
											      	{
												      	$gallery_img_path = $gallery_dir.$gfile;
												      	if(is_file($gallery_img_path))
												      	{
												      		$this->galleryImgUpload($gfile,$value['product_id'],$index,$gallery_dir);
												      	}
											    	}
											    	$index = $index + 1;
										    	}						  				
											}
										}
										else{
											$data['error'] = "No gallery directory found!";
										}
									}
						  		}}
				    	}
				    	$data['success_image'] = "Image uploaded successfully!";
					}else{
						$data['error'] = "No directory found!";
					}
		    	}
			}
			$this->load->mtemplate('merchant/product/index',$data);
	}
	public function subcategory(){
		if($this->input->is_ajax_request()){
			
	            $data['category_id'] = $this->input->post('catid');
	            $subcategories = $this->product_model->getSubcategoriesBycatId($data['category_id'] );
//print_r($subcategories);die();
	            if(!empty($subcategories)){
		            foreach ($subcategories as $key => $value) {
		            	$subcat['sub_id'] = $value['cat_id'];
		            	$subcat['name'] = $value['name'];
		            	$subArr [] = $subcat;
		            }
		        }else{
		        	$subArr = array();
		        }
	            echo json_encode($subArr);  
	            
		}
	}
	public function multiple_upload($name, $upload_dir, $allowed_types, $size)
    {
        $config['upload_path']   = realpath($upload_dir);
        $config['allowed_types'] = $allowed_types;
        $config['max_size']      = $size;
        $config['overwrite']     = FALSE;
        $config['encrypt_name']  = TRUE;
        $ffiles = $this->upload->data();
       
        $this->upload->initialize($config);
        $errors = FALSE;
        if(!$this->upload->do_upload($name))://I believe this is causing the problem but I'm new to codeigniter so no idea where to look for errors
           echo $errors = $this->upload->display_errors();
        else:
            // Build a file array from all uploaded files
            $files = $this->upload->data();
        endif;
        // There was errors, we have to delete the uploaded files
         if($errors):                   
            @unlink($files['full_path']);
            return false;
        else:
            return $files;
        endif;
    }//end of multiple_upload()
    public function originalImgUpload($product_image,$getProductId,$dir){ 
		if(!empty($product_image)) 
		{ 
			if (!is_dir($this->config->item('image_path').'product/')) { 
				mkdir($this->config->item('image_path').'product/', 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'), 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'), 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'), 0777, true); 
			}
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'), 0777, true); 
			}
		
			$productImageExtension= explode('.',$product_image); 
			if(in_array($productImageExtension[1], $this->extension_array)){
					$img_file_name = $getProductId."_".time().'.'.$productImageExtension[1]; 
					$config['image_library'] = 'gd2'; 
					$this->load->library('image_lib', $config); 
					list($width, $height) = getimagesize($dir.$product_image);
					if ($width>$height) {
					$orientation = "width";
					} else {
					$orientation = "height";
					}
					/*------------------------original Image Upload----------------------------*/
					$config = array( 
								'source_image'      => $dir.$product_image, 
								'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$img_file_name, 
								'maintain_ratio'    => FALSE, 
								'width'             => $width, 
								'height'            => $height, 
								'master_dim' =>'width' 
								); 
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
					/*--------------------Image Resize for 250*250------------------------------------------*/
					$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path250_250 = $dir.$product_image;
					$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
					$this->image_lib->resize();
					/*------------------------Image Resize for 250*250------------------------------------------*/
					/*-------------------------Image Resize for 400*310------------------------------------------*/
					
					$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path400_310 = $dir.$product_image;
					$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.
					$this->config->item('medium_thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
					/*----------------------------Image Resize for 400*310------------------------------------------*/
					/*--------------------------Image Resize for 650*500------------------------------------------*/
					
					$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path650_310 = $dir.$product_image;
					$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
					
					/*-----------------------Image Resize for 650*500------------------------------------------*/
					$file_name = $img_file_name;  
					$this->product_model->updateProductByImages($getProductId,$file_name); 
			}
			else{
				$data['error'] = "Invalid file extensions!";
			}
			
		} 
    }
    public function galleryImgUpload($productImageId,$getProductId,$index,$gallery_dir){
		$productImageExtension= explode('.',$productImageId);
		if(in_array($productImageExtension[1], $this->extension_array)){
			$additional_file_name = $getProductId.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
			$config['image_library'] = 'gd2'; 
			$this->load->library('image_lib', $config); 
			list($width, $height) = getimagesize($gallery_dir.$productImageId);
			if($width > $height){
			$orientation = "width";
			} else {
			$orientation = "height";
			}
			
			/*------------------------original Image Upload----------------------------*/
			$config = array( 
							'source_image'      => $gallery_dir.$productImageId, 
							'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$additional_file_name, 
							'maintain_ratio'    => FALSE, 
							'width'             => $width, 
							'height'            => $height, 
							'master_dim' =>'width' 
							); 
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			
			/*--------------------Image Resize for 250*250------------------------------------------*/
			/*-----------------------Image Resize for 250*250------------------------------------------*/
			$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path250_250 = $gallery_dir.$productImageId;
			$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
			
			/*---------------Image Resize for 250*250------------------------------------------*/
			/*-----------------Image Resize for 450*450------------------------------------------*/
			
			$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path400_310 = $gallery_dir.$productImageId;
			$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
			/*-----------------Image Resize for 450*450------------------------------------------*/
			/*-------------------Image Resize for 800*800------------------------------------------*/
			
			$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path650_310 = $gallery_dir.$productImageId;
			$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
			
			/*-------------------Image Resize for 800*800------------------------------------------*/
			
			$this->product_model->additionProductByImages($getProductId,$additional_file_name);
		}
		else{
			$data['error'] = "Invalid file extensions!";
		}
    }
    public function productlist(){
    	$data["page_title"] = "List Products || iPengen";
    	$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		$data['product_list']=$this->product_model->list_products($merchant_id);
		$this->load->mtemplate('merchant/product/list',$data);
    }
	
	public function add()
	{
		       $get_merchant=array();
				$data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$merchant = $this->product_model->get_merchant();
				if(!empty($merchant)){
					foreach($merchant as $val){
						$get_merchant[$val['marchent_id']]	= $val['marchent_name'];
					}	
				}
				$data['get_merchant'] = $get_merchant;
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->mtemplate('merchant/product/addproduct',$data); 
	}
	
	public function addproduct()	 
			{ 
			 $this->load->helper('directory');
			 $merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in')); 
			 if($this->input->post('price') >= $this->input->post('saleprice'))
			 {
					
					$data=array( 
								 'merchant_id'=>$merchant_id,
								 'sku'=>$this->input->post('SKU'), 
								 'stock_quantity'=>$this->input->post('stock_quantity'), 
								 'stock' => $this->input->post('stock'),
								 'price'=>$this->input->post('price'),
								 'category_id'=>$this->input->post('category'),
								 'brand_id'=>$this->input->post('brand'),
								 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
								 'sale_start_date'=>$this->input->post('startdate'),
								 'sale_end_date'=>$this->input->post('enddate'),
								 'create_date'=>date('Y-m-d H:i:s'), 
								 'shipping_weight'=>$this->input->post('shipping_weight'),
								 'shipping_dimension'=>$this->input->post('shipping_dimension'),
								 'admin_status'=>0,
								  ); 
								  //print_r($data);die();
							$getProductId=$this->product_model->insert_products($data); 
							$insert_id = $this->db->insert_id();//$this->db->insert_id();2284
							$productDetails = $this->product_model->getProductById($insert_id);
							//print_r($productDetails->category_id);die();
							$result_id=$productDetails->category_id;
							if($result_id!=""){
								$category = $this->product_model->category_id($result_id);
								$cat_name = $category->cat_id;
								$parent_id = $this->product_model->getcatParentId($result_id);
								if($parent_id != 0){
									$catgry = $this->product_model->category_id($parent_id);
									$category_name = $catgry->cat_id;
									$parent_details = $this->product_model->getcategoryById($parent_id);
									foreach($parent_details as $parent){
										$category_id = $parent->parent_id;	
										if($category_id == 0){
											$string = ucfirst($category_name).','.ucfirst($cat_name);
										}
									}
								}else{
									$string = ucfirst($cat_name);
									
								}
									$updateProduct = $this->product_model->updateProduct($insert_id,$string);
								//echo $string;die();
						
							}
				
				
							
							$row_ids = $this->input->post('row');
							$product_name = $this->input->post('product_name');
							$language_id = $this->input->post('language_id');
							$short_description = $this->input->post('s_description'); 
							$message = $this->input->post('editor'); 
							$datatext = array();
							//print_r($language_id); die();
							$post = $this->input->post(); 
							for ($i = 0; $i < ($this->input->post('row')); $i++)
							{
								$datatext[] = array(
								'product_id' => $getProductId,
								'language_id' => $language_id[$i],
								'product_name' => $product_name[$i],
								'short_description' => $short_description[$i],
								'message' => $message[$i],
								);
								
						
							}

							$this->product_model->insert_producttext($datatext);	

						 
						if(!empty($_POST['product_image'] )) 
						{ 
						$product_image = $_POST['product_image'];
						/*if (!is_dir('media/'.$getProductId)) { 
							mkdir('./media/'.$getProductId, 0777, true); 
						}*/ 
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'), 0777, true); 
						}
						
								$productImageExtension= explode('.',$product_image); 
								$img_file_name = $getProductId."_".time().'.'.$productImageExtension[1]; 

								$config['image_library'] = 'gd2'; 

								$this->load->library('image_lib', $config); 

						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
						if ($width>$height) {
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$img_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
								$this->image_lib->initialize($config); 
								$this->image_lib->resize();

							/*--------------------Image Resize for 250*250------------------------------------------*/
							$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
							$this->image_lib->resize();
							/*------------------------Image Resize for 250*250------------------------------------------*/
							/*-------------------------Image Resize for 400*310------------------------------------------*/
							
							$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.
$this->config->item('medium_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
							/*----------------------------Image Resize for 400*310------------------------------------------*/
							/*--------------------------Image Resize for 650*500------------------------------------------*/
							
							$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
							
							/*-----------------------Image Resize for 650*500------------------------------------------*/
							$file_name = $img_file_name;  
							$this->product_model->updateProductByImages($getProductId,$file_name); 
							
						} 
						/*additional images computer*/
						if(isset($_POST['serverhiddenid']))
						{
	
							$productImageIds = $_POST['serverhiddenid'];
	
							$index=0;
	
							foreach($productImageIds as $productImageId)
	
							{
	
								$productImageExtension= explode('.',$productImageId);
	
								$additional_file_name = $getProductId.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
	
								$config['image_library'] = 'gd2'; 
	
								$this->load->library('image_lib', $config); 
	
						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height){
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$additional_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						
						/*--------------------Image Resize for 250*250------------------------------------------*/
						/*-----------------------Image Resize for 250*250------------------------------------------*/
						$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
						
						/*---------------Image Resize for 250*250------------------------------------------*/
						/*-----------------Image Resize for 450*450------------------------------------------*/
						
						$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
						/*-----------------Image Resize for 450*450------------------------------------------*/
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
						
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$this->product_model->additionProductByImages($getProductId,$additional_file_name);
						$index = $index+1;
					  }
	
					}
					
					$bulk_uplode_notification = array(
			                        'm_id' => $merchant_id,
									'type' => "single uplode",
									'message'=> $merchant_name . " uploded a propduct on ". date('d-m-Y'),
									'raw' => '',
									'date' => date('Y-m-d'),
									'notification_for' => 1,
									
			                    );
								
					$this->product_model->insert_admin_notification($bulk_uplode_notification);
					$merchant_name = $this->dashboard_model->getMerchantnameByEmail($this->session->userdata('merchant_logged_in'));
					$data["status"] = "Single Product Upload";
					$data["merchantNAme"] = $merchant_name;
					$data["productcount"] = '1';
					$emailBody = $this->load->view("email_template/bulk_uplode.php",$data,true);
					$supportData = $this->order_model->support_data();
					if(!empty($supportData))
			         {
						$emailTo = $supportData[0]->info_email; 
						$subject = $merchant_name . " uploded a product on ". date('d-m-Y');
						
						$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$this->session->userdata('merchant_logged_in'),
								"to"		=>	$emailTo,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					    send_email($emailData);
						 
					 }
					
					    	
				//redirect('product/list');	
				redirect('merchant/product/productlist');
			 }
			 else
			 {
				 $this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
				 $data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->template('mtemplate/product/add',$data); 
			}

										

					} 
    
	
	public function productview($id=false)
	{
		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));
		
		if ($id!=false) {
			$data["page_title"] = "View Product | iPengen";
			$data['data'] = $this->product_model->getProductDataById($id,$merchant_id);
			$data['galary'] = $this->product_model->getProductImgGalaryById($id,$merchant_id);
			$data['product_id'] = $id;
			$this->load->mtemplate('merchant/product/productview',$data);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
		
	}
	
	public function edit($id = NULL) { 
		$data["page_title"] = "Edit Product | iPengen";
		$data['row'] = $this->product_model->edit_product($id); 
		$data['language'] = $this->product_model->get_editlanguage($id); 
		$data['galleries']=$this->product_model->exit_id($id);
		$data['cat_list'] = $this->product_model->get_category_list();
		$data['brands'] = $this->product_model->get_brand_list();
		$this->load->mtemplate('merchant/product/edit',$data); 
	
	}
	
	public function delete($pro_id = NULL) { 
	
	  $this->product_model->deleteProductById($pro_id); 
	
	  redirect('merchant/product-list'); 
	
	  }	
	  
	 public function deleteAll() { 
		$pro_id=$this->input->post('pid');
		$deleteAllProductById=$this->product_model->deleteAllProductById($pro_id); 
		if($deleteAllProductById['result']=='1'){
			$data['success']='1';
		}
		else{$data['success']='0';}
		//print_r($data);die();
		echo json_encode($data);
		//return $data;
		//redirect('merchant/product/product-list'); 
	
	}	
	
	public function editproduct($p_id = NULL)
	{ 
		if($this->input->post('price') >= $this->input->post('saleprice'))	
		{				
				$data=array( 
						 'sku'=>$this->input->post('SKU'), 
						 'stock_quantity'=>$this->input->post('stock_quantity'), 
						 'stock' => $this->input->post('stock'),
						 'price'=>$this->input->post('price'),
						 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
						 'category_id'=>$this->input->post('category'),
						 'brand_id'=>$this->input->post('brand'),
						 'sale_start_date'=>$this->input->post('startdate'),
						 'sale_end_date'=>$this->input->post('enddate'),
						 'create_date'=>date('Y-m-d H:i:s'), 
						 'shipping_weight'=>$this->input->post('shipping_weight'),
						 'shipping_dimension'=>$this->input->post('shipping_dimension'),
						  ); 
					$getProductId=$this->product_model->update_product($data,$p_id);
					
					$row_ids = $this->input->post('row');
					$product_name = $this->input->post('product_name');
					$language_id = $this->input->post('language_id');
					$short_description = $this->input->post('s_description'); 
					$message = $this->input->post('editor'); 
					$datatext = array();
					//print_r($language_id); die();
					$post = $this->input->post(); 
					for ($i = 0; $i < ($this->input->post('row')); $i++)
						{
							$datatext[] = array(
							'product_id' => $getProductId,
							'language_id' => $language_id[$i],
							'product_name' => $product_name[$i],
							'short_description' => $short_description[$i],
							'message' => $message[$i],
							);
						 }	
					$this->product_model->updet_producttext($datatext,$p_id);	
	
								
					if (!empty($_POST['product_image'] )) 
					{ 
	$product_image = $_POST['product_image'];
	$product_previous_image= $this->product_model->getProductImageByID($p_id);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image);
	
	
	
	if (!is_dir($this->config->item('image_path').'product/')) { 
		 mkdir($this->config->item('image_path').'product/', 0777, true); 
		} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
	}
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
	}
	
	$productImageExtension= explode('.',$product_image); 
    $img_file_name = $p_id."_".time().'.'.$productImageExtension[1]; 
	
	$config['image_library'] = 'gd2'; 
	$this->load->library('image_lib', $config); 
					
					/*$config = array( 
					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100-100/'.$img_file_name, 
					'maintain_ratio'    => FALSE, 
					'width'             => 250, 
					'height'            => 250, 
					'master_dim' 		=> 'width' 
					);  
				   $this->image_lib->initialize($config); 
					$this->image_lib->resize();*/ 
					
					/*$config1 = array( 
					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400-400/'.$img_file_name, 
					'maintain_ratio'    => TRUE, 
							
					'width'             => 250, 
				
					'height'             => 250, 
					'master_dim' 		=> 'width' 
					); 
					$this->image_lib->initialize($config1); 
					$this->image_lib->resize();	*/
		
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
					if ($width > $height) {
						 $orientation = "width";
					} else {
						 $orientation = "height";
					}
					
		/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$img_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
		/*----------------------------Image Resize for 250*250------------------------------------------*/
			$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
			$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
		
		/*--------------------------Image Resize for 250*250------------------------------------------*/
		/*-----------------------------Image Resize for 400*310------------------------------------------*/
			$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
			$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
		/*-------------------Image Resize for 400*310------------------------------------------*/
		/*---------------Image Resize for 650*500------------------------------------------*/
		
		$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
		$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
		$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$img_file_name;
		$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
	
		/*------------------------------Image Resize for 650*500------------------------------------------*/
					
					 
					/*$config = array( 
					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800-800/'.$img_file_name, 
					'maintain_ratio'    => FALSE, 
					'width'             => 650, 
					'height'            => 500, 
					'master_dim' 		=> 'width' 
					); 
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();	*/ 
					 
					 $file_name = $img_file_name;  
					 $this->product_model->updateProductByImages($p_id,$file_name); 
		} 
	
					if(isset($_POST['deleteproductsimages']))
					{
				 $deleteproductsimages = $_POST['deleteproductsimages'];
		 foreach($deleteproductsimages as $deleteproductsimage){
			  if($deleteproductsimage!=''){  
				 $getProductId=$this->product_model->deleteExistProductByID($deleteproductsimage,$p_id);
				}
			}
		}
					/*additional images computer*/
					if(isset($_POST['serverhiddenid']))
					{
						//echo "Starting Error";
						$productImageIds = $_POST['serverhiddenid'];
						$index=0;
						
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
						}
						/*---------------------------------------- End Create directory----------------------------------*/
						foreach($productImageIds as $productImageId)
						{
							$productImageExtension= explode('.',$productImageId);
							$additional_file_name = $p_id.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
							$config['image_library'] = 'gd2'; 
							$this->load->library('image_lib', $config); 
							
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height)
						{
						$orientation = "width";
						}
						else
						{
							$orientation = "height";
						}
					
					/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$additional_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
					
					
				$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
				
				/*------------------Image Resize for 250*250------------------------------------------*/
				/*---------------Image Resize for 400*310------------------------------------------*/
				
				$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
				/*----- ----------Image Resize for 400*310------------------------------------------*/
				/*----------------Image Resize for 650*500------------------------------------------*/
				
				$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
				
				/*-----------------------Image Resize for 650*500------------------------------------------*/
							$this->product_model->updationProductByImages($p_id,$additional_file_name);
							$index = $index+1;
						}
					}	
			redirect('merchant/product/productlist');	
		}
		else
		{
			$this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
			$data["page_title"] = "Edit Product | iPengen";
			$data['row'] = $this->product_model->edit_product($p_id); 
			$data['language'] = $this->product_model->get_editlanguage($p_id); 
			$data['galleries']=$this->product_model->exit_id($p_id);
			$data['cat_list'] = $this->product_model->get_category_list();
			$data['brands'] = $this->product_model->get_brand_list();
			$this->load->mtemplate('merchant/products/edit',$data);
		}
	
	}	    
	public function gallery($p_id = NULL ) { 
	
	$files = $_FILES;   
	$product_img_arr = $_FILES['pimages']['name']; 
	
	$p_name=$this->input->post('get_p_name'); 
	
	$p_img_id=$this->input->post('pimageId[]'); 
	
	for($i=0;$i<count($product_img_arr);$i++) 
	
	{ 
	
		$_FILES['pimages']['name']= $files['pimages']['name'][$i]; 
	
		$_FILES['pimages']['type']= $files['pimages']['type'][$i]; 
	
		$_FILES['pimages']['tmp_name']= $files['pimages']['tmp_name'][$i]; 
	
		$_FILES['pimages']['error']= $files['pimages']['error'][$i]; 
	
		$_FILES['pimages']['size']= $files['pimages']['size'][$i];     
	
					$config = array(); 
	
					$config['upload_path'] = $this->config->item('image_path').'product/'.$p_id.'/originalImages/'; 
	
					$config['allowed_types'] = 'jpg|jpeg|gif|png'; 
	
					$config['file_name'] = $p_id.'_gallery_'.date("Ymdhis"); 
	
					$config['overwrite']     = FALSE; 
	
					$this->upload->initialize($config); 
	
					 
	
					 if ($this->upload->do_upload('pimages')) 
	
						 { 
	
							$img = $this->upload->data(); 
	
							 
	
							 
	
							$this->load->library('image_lib', $config);  
	
							 $config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400*400/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 400, 
	
							'height'            => 300, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							 
	
							 
	
							 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800*800/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 650, 
	
							'height'            => 500, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100*100/', 
	
							'maintain_ratio'    => FALSE, 
	
							'width'             => 250, 
	
							'height'            => 250, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$file_name = $img['file_name']; 
	
							$get_product_gallery_id=$this->product_model->exit_id($p_id); 
	
							if(is_array($p_img_id) && isset($p_img_id[$i]) && $p_img_id[$i]!="" ) 
	
							{ 
	
								 
	
							$this->product_model->edit_gallery($file_name,$p_img_id[$i]); 
	
							} 
	
							else 
	
							{ 
	
							$this->product_model->insert_gallery($file_name,$p_id); 
	
							} 
	
						 } 
	
				} 
	
			$this->session->set_flashdata('message', 'Galleries updated successfully. '); 
	
				$data['row'] = $this->product_model->edit_product($p_id); 
	
				$data['gallery']=$this->product_model->exit_id($p_id); 
	
			   // $this->load->template('products/edit/'.$id.'#tab_1_2',$data); 
	
				redirect('merchant/product/edit/'.$p_id.'#productIMG',$data);	 
	
					exit(); 
	
	} 
	
	public function status($id=false,$value=NULL)
	{
		
		if($id != false && $value != NULL)
		{
			echo $result = $this->product_model->product_admin_status_change($id,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}  
	
		   	
  	
}
?>