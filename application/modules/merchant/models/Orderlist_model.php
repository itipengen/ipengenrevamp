<?php

//defined('BASEPATH') OR exit('No direct script access allowed');



class Orderlist_model extends CI_Model {



    function __construct()

    {

        parent::__construct();

		$this->load->database();

    }

	

	function my_orderList($post,$merchant_id)

	{

		$merchant_id = $this->dashboard_model->getMerchantByEmail($this->session->userdata('merchant_logged_in'));

		$this->db->select('*');

		$this->db->from('ig_order_items');

		$this->db->join('ig_order', 'ig_order_items.order_id = ig_order.order_id');

		//$this->db->join('ig_products', 'ig_order_items.product_id = ig_products.product_id');

		$this->db->where('ig_order_items.merchant_id', $merchant_id);

		//$this->db->where('ig_order.date_added >=', $post['dateFrom']);

		//$this->db->or_where('ig_order.date_added <=', $post['dateTo']);

		//$this->db->where('(ig_order.date_added >="'. $post['dateFrom'].'" OR ig_order.date_added <="'.$post['dateTo'].'")');

		$this->db->where('(ig_order.date_added BETWEEN "'. $post['dateFrom'].'" AND "'.$post['dateTo'].'")');

		$query = $this->db->get();

		//echo $this->db->last_query();

		//echo $query->num_rows();die();

		 return $query->num_rows() > 0 ? $query->result_array() : array();

	}
	
	function getAllReportList($merchant_id){
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order_items.order_id = ig_order.order_id');
		$this->db->where('ig_order_items.merchant_id', $merchant_id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result_array() : array();
	}

	function my_orderTotalPrice($post,$merchant_id)

	{

		

		$this->db->select('sum(ig_order_items.total) AS totalPrice');

		$this->db->from('ig_order_items');

		$this->db->join('ig_order', 'ig_order_items.order_id = ig_order.order_id');

		//$this->db->join('ig_products', 'ig_order_items.product_id = ig_products.product_id');

		$this->db->where('ig_order_items.merchant_id', $merchant_id);

		$this->db->where('ig_order_items.order_status', 'approved');

		//$this->db->where('ig_order.date_added >=', $post['dateFrom']);

		//$this->db->or_where('ig_order.date_added <=', $post['dateTo']);

		//$this->db->where('(ig_order.date_added >="'. $post['dateFrom'].'" OR ig_order.date_added <="'.$post['dateTo'].'")');

		$this->db->where('(ig_order.date_added BETWEEN "'. $post['dateFrom'].'" AND "'.$post['dateTo'].'")');

		

		$query = $this->db->get();

		//echo $this->db->last_query();die();

		//echo $query->num_rows();

		 return $query->num_rows() > 0 ? $query->result_array() : array();

	}

	

		

}		



?>