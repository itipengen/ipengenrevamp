<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	function get_orders($merchant_id)
    {
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id));
		$this->db->where(array('ig_order_items.p_status' => 'success'));
		$this->db->order_by("ig_order_items.order_id", "desc");
		$query = $this->db->get();
		//echo '-='.$this->db->last_query();
		 return $query->num_rows() > 0 ? $query->result() : array(); 

    }
	function get_count_new_order($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('merchant_id' => $merchant_id));
		$this->db->where(array('ig_order_items.p_status' => 'success'));
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	function get_approved_orders($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id));
		$where = "(ig_order_items.p_status='draft' or ig_order_items.p_status='packing' or ig_order_items.p_status='new')";
        $this->db->where($where);
		$this->db->order_by("ig_order_items.order_id", "desc");
		$query = $this->db->get();
		//echo '-='.$this->db->last_query();
		 return $query->num_rows() > 0 ? $query->result() : array(); 
		 

	}
	function get_cancelled_orders($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id,));
		$where = '(ig_order_items.order_status=2 or ig_order_items.order_status = 4 or ig_order.status = "canceled")';
        $this->db->where($where);
		$query = $this->db->get();
		//echo '-='.$this->db->last_query();
		//die();
		 return $query->num_rows() > 0 ? $query->result() : array(); 

	}
	
	function get_delivered_orders($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id,'ig_order.status'=>'delivered'));
		$this->db->order_by("ig_order_items.order_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die();
		 return $query->num_rows() > 0 ? $query->result() : array(); 

	}
	function get_shipped_orders($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id,'ig_order.status'=>'shipped'));
		$this->db->order_by("ig_order_items.order_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die();
		 return $query->num_rows() > 0 ? $query->result() : array(); 

	}
	
	public function statuschange($data)
	{  
		$this->db->insert('ig_marchent_product_history',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function statuschangeonproduct($Oid,$otid,$data)
	{  
		$this->db->where('order_id',$Oid);
		$this->db->where('order_item_id',$otid);
		$this->db->update('ig_order_items',$data);
		return true;
	}
	

	function get_orders_items($id,$pid,$merchant_id)
    {
		//echo $merchant_id;
		//echo '---'.$pid;
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		//$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id');
		$this->db->where(array('ig_order_items.order_id' => $id, 'ig_order_items.product_id' => $pid , 'ig_order_items.merchant_id' => $merchant_id));
		$query = $this->db->get();
		//echo $this->db->last_query();
		
		 return $query->num_rows() > 0 ? $query->result() : array(); 

    }
	
	public function getOrderDetailshistory($orderid,$pId){
		$this->db->where(array('order_id'=>$orderid,'order_product_id'=>$pId));
		$query=	$this->db->get('ig_order_status_history');
		return $query->num_rows() > 0 ? $query->result() : array(); 
		//return $query->result();
	}
	
	public function getUserData($orderId = NULL)
	{
		$query =	$this->db->select("
								ig_order.order_id as order_id,
								ig_order.payment_firstname as fname,
								ig_order.payment_lastname as lname,
								ig_user.mobile as mobile,
								ig_user.email as userEmail,
								ig_order.payment_address_1 as address,
								ig_order.payment_address_2 as address_1,
								ig_order.payment_city as city,
								ig_order.payment_postcode as postCode,
										")
							 ->from("ig_order")
							 ->where(array("ig_order.order_id"=>$orderId))
							 ->join("ig_user","ig_user.id = ig_order.user_id")
							 ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	public function getproductDetails($pid,$oid,$mid)
		{
			$this->db->select('ig_order.*,ig_order.status as stuts,ig_order_items.*,ig_products.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id', 'left');
			$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id', 'left');
			//$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_order_items.merchant_id',$mid);
			$query = $this->db->get();

			$result = $query->result();
			return $result;			
		}
		
		public function support_data()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}

	public function updateOrderHeaderId($order_id,$order_header_id,$product_id){
	   $array = array();
	   $array['order_header_id'] = $order_header_id;
	   $this->db->where(array('order_id'=>$order_id,'product_id'=>$product_id));
	   if($this->db->update('ig_order_items', $array)){
	   		return true;
	   }else{
	   		return false;
	   }
   }

   public function updateOrderItemStatus($statusArr,$order_id,$order_header_id,$product_id){
	   $this->db->where(array('order_id' => $order_id, 'order_header_id' => $order_header_id, 'product_id' => $product_id));
	   if($this->db->update('ig_order_items', $statusArr)){
	   		return true;
	   }else{
	   		return false;
	   }
   }

   public function insertOrderHistoryStatus($statusArr){
	   if (isset($statusArr)) {
	   		$this->db->insert('ig_order_status_history',$statusArr);
	   		return $this->db->insert_id();
	   }else{
	   		return false;
	   }
   }

   public function checkForOrderHeaderId($orderId,$productId){
	   $query = $this->db->get_where('ig_order_items',array('order_id'=>$orderId,'product_id'=>$productId))->row();
	   if($query){
	   		$header_id = $query->order_header_id;
	   		return $header_id;
	   }
	   return false;
   }
		
}		

?>