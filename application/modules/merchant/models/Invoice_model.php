<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function getInvoiceByMerchant($merchant_id){
		$this->db->select('ig_invoice.*');
		$this->db->from("ig_invoice");
		$this->db->where('ig_invoice.merchant_id', $merchant_id);
		$this->db->order_by('ig_invoice.id','DESC');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$res=$query->result();
		}  
		else{
			$res= array();	
		}
		return $res;
	}
	
	public function getMerchantInvoice($post){
		$this->db->select('ig_invoice.*');
		$this->db->from("ig_invoice");
		$this->db->where('ig_invoice.merchant_id', $post['mid']);
		$this->db->where('(ig_invoice.date BETWEEN "'. $post['dateFrom'].'" AND "'.$post['dateTo'].'")');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$res=$query->result();
		}  
		else{
			$res= array();	
		}
		return $res;
	}
	
	function invoiceProductDetails($mid,$invid){
		//echo '--'.$invid;
		$this->db->select('ig_order_items.*,ig_marchent_login.marchent_name');
		$this->db->from("ig_order_items");
		$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
		$this->db->where("ig_order_items.invoice_id", $invid);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$res['data']=$query->result_array();
			foreach($res['data'] as $key=>$val){
					$this->db->select('ig_products.product_image');
					$this->db->from("ig_products");
					$this->db->where("ig_products.product_id", $val['product_id']);
					$query = $this->db->get();
					
					if($query->num_rows() > 0){
						$result=$query->result_array();	
						$res['data'][$key]['product_image']=$result[0]['product_image'];
					}
					else{
						$res['data'][$key]['product_image']='';
					}
			}
		}
		else{
			$res['data']=array();	
		}
		return $res;
	}
	
	function getInvoiceDetails($inv_id)
	{
		$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
		$this->db->from("ig_invoice");
		$this->db->where('ig_invoice.invoice', $inv_id);
		$this->db->join('ig_marchent_login','ig_invoice.merchant_id=ig_marchent_login.marchent_id');
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	public function getbusinessname($mid){
		 if(!empty($mid)){
			$this->db->select('bname');
			$this->db->from("ig_merchant_business");
			$this->db->where('merchant_id',$mid);
			$query = $this->db->get();
		    return $query->num_rows() > 0 ? $query->result() : array();
			 
		 }
	 }
}
?>