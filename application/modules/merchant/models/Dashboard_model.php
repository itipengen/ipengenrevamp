<?php

class Dashboard_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function addBusiness($data){

		if($data['merchant_id'] != 0){
			$sql = $this->db->get_where('ig_merchant_business',array('merchant_id' => $data['merchant_id']));
			if($sql->num_rows() > 0){
				return false;
			}
			else{
				$result['merchant_id'] = $data['merchant_id'];
				$result['bname'] = $data['cname'];
				$result['bdescription'] = $data['cdescription'];
				$result['email'] = $data['cemail'];
				$result['phone'] = $data['cphone'];

				if($this->db->insert('ig_merchant_business',$result)){
					return $bid = $this->db->insert_id();
				}
			}
		}

		return false;
	}

	function addBank($data){
		if($data['merchant_id'] != 0){
			$sql = $this->db->get_where('ig_merchant_bank',array('merchant_id' => $data['merchant_id']));
			if($sql->num_rows() > 0){
				return false;
			}
			else{
				$result['merchant_id'] = $data['merchant_id'];
				$result['account_name'] = $data['acc_name'];
				$result['account_number'] = $data['acc_number'];
				$result['bank_name'] = $data['bank_name'];
				$result['branch_name'] = $data['branch_name'];
				$result['state'] = $data['state'];
				$result['city'] = $data['city'];

				if($this->db->insert('ig_merchant_bank',$result)){
					return $bank_id = $this->db->insert_id();
				}
			}
		}

		return false;
	}

	function addStore($data){

		if($data['merchant_id'] != 0){
			$sql = $this->db->get_where('ig_merchant_store',array('merchant_id' => $data['merchant_id']));
			if($sql->num_rows() > 0){
				return false;
			}
			else{

				$result['merchant_id'] = $data['merchant_id'];
				$result['sname'] = $data['store_name'];
				$result['sdescription'] = $data['sdescription'];
				$result['brands'] = $data['brands'];

				if($this->db->insert('ig_merchant_store',$result)){
					return $sid = $this->db->insert_id();
				}
			}
		}

		return false;
	}

	function getMerchantByEmail($email){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_email' => $email));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->marchent_id;
		}
		return false;
	}
	
	function getMerchantnameByEmail($email){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_email' => $email));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->marchent_name;
		}
		return false;
	}
	
	function send_approved_request($id){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_id' => $id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->send_approved_request;
		}
		return false;
	}
	
	function get_admin_approval($id){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_id' => $id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->merchant_admin_status;
		}
		return false;
	}
	
	function getTotalProduct($id){
		$query = $this->db->get_where('ig_products',array('merchant_id' => $id));
	    return $query->num_rows();
	}
	function getTotalorder($id){
		$query = $this->db->get_where('ig_order_items',array('merchant_id' => $id));
	    return $query->num_rows();
	}
	function list_orders($merchant_id)
    {
		$this->db->select('*');
		$this->db->from('ig_order_items');
		$this->db->join('ig_order', 'ig_order.order_id =  ig_order_items.order_id');
		$this->db->where(array('ig_order_items.merchant_id' => $merchant_id));
		$this->db->limit(5);
		$query = $this->db->get();
		 return $query->num_rows() > 0 ? $query->result() : array(); 
    }

	function getDetailsById($merchant_id){
		$this->db->select('*');
		$this->db->from('ig_marchent_login');
		$this->db->join('ig_merchant_business', 'ig_merchant_business.merchant_id = ig_marchent_login.marchent_id','left');
		$this->db->join('ig_merchant_bank', 'ig_merchant_bank.merchant_id = ig_marchent_login.marchent_id','left');
		$this->db->join('ig_merchant_store', 'ig_merchant_store.merchant_id = ig_marchent_login.marchent_id','left'); 
		$this->db->where('ig_marchent_login.marchent_id',$merchant_id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->row() : '';
	}

	function getBusinessByMerchantId($merchant_id){
		$query = $this->db->get_where('ig_merchant_business',array('merchant_id' => $merchant_id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row;
		}
		return false;
	}

	function updateBusinessByBId($data,$bid){

		$businessArr=array(
			'merchant_id' => $data['merchant_id'],
			'bname' => $data['cname'],
			'bdescription' => $data['cdescription'],
			'email' => $data['cemail'],
			'phone' => $data['cphone'],

		);

		$this->db->where('business_id',$bid);
		if($this->db->update('ig_merchant_business',$businessArr))
			return true;
		else
			return false;
	}
	
	function approvesuccess($mail){
		
		$successArr=array(
			'send_approved_request' => 1,

		);

		$this->db->where('marchent_email',$mail);
		if($this->db->update('ig_marchent_login',$successArr))
			return true;
		else
			return false;
		
	}

	function getBankByMerchantId($merchant_id){
		$query = $this->db->get_where('ig_merchant_bank',array('merchant_id' => $merchant_id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row;
		}
		return false;
	}

	function updateBankByBankId($data,$bankId){

		$bankArr=array(
			'merchant_id' => $data['merchant_id'],
			'account_name'=>$data['acc_name'],
			'account_number'=>$data['acc_number'],
			'bank_name'=>$data['bank_name'],
			'branch_name'=> $data['branch_name'],
			'state'=> $data['state'],
			'city'=> $data['city']
		);

		$this->db->where('bank_id',$bankId);
		if($this->db->update('ig_merchant_bank',$bankArr))
			return true;
		else
			return false;
	}

	function getStoreByMerchantId($merchant_id){
		$query = $this->db->get_where('ig_merchant_store',array('merchant_id' => $merchant_id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row;
		}
		return false;
	}

	function updateStoreByStoreId($data,$storeId){

		$storeArr=array(
			'merchant_id' => $data['merchant_id'],
			'sname' => $data['store_name'],
			'sdescription' => $data['sdescription'],
			'brands' => $data['brands']

		);

		$this->db->where('store_id',$storeId);
		if($this->db->update('ig_merchant_store',$storeArr))
			return true;
		else
			return false;
	}
	public function getProductUrl_name($id,$name){
		
		$slag=$this->slugify($name);
		return  base_url().'p/'.$id.'-'.$slag;
	}
	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
	
	public function getajaxnotification($requestData){
		
		$query = $this->db->get_where('ig_marchent_login',array('marchent_email'=>$this->session->userdata('merchant_logged_in'))); 
			$result = $query->result();
			$full_name = "";
			$position = "";
			if (!empty($result)) {
			foreach ($result as $value) {
				$full_name = ucwords($value->marchent_name);
				$approved = $value->merchant_admin_status;
				$merchantid = $value->marchent_id;
			}
		}
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'type',
			2=> 'message',
			3=> 'date',
			4=> 'status',
		);
		
		// getting total number records without any search
		$this->db->select('*');
		$this->db->from('ig_admin_notification');
		$this->db->where('m_id',$merchantid);
		$this->db->where('notification_for',2);
		$query = $this->db->get();
		$totalData = $query->num_rows();
		$data['totalFiltered'] = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		
		
		
		
		
		
		
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('*');
			$this->db->from('ig_admin_notification');
			$this->db->where('m_id',$merchantid);
			$this->db->where('notification_for',2);
			$this->db->like('message', $requestData['search']['value']);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$queryresult = $this->db->get();
			$data['queryresult']= $queryresult->result_array();
			
		}
		else{
			$this->db->select('*');
			$this->db->from('ig_admin_notification');
			$this->db->where('m_id',$merchantid);
			$this->db->where('notification_for',2);
			$this->db->like('message', $requestData['search']['value']);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$queryresult = $this->db->get();
			$data['queryresult']= $queryresult->result_array();
			
			}
			
			
			return $data;
	}

	public function get_sale_stat($merchant_id){
		$sumArr = array();
		$this->db->select('ig_order.*,DATE_FORMAT(date_added, "%Y-%m-%d") AS order_date');
		$this->db->from("ig_order");
		
		$this->db->join('ig_order_items', 'ig_order_items.order_id = ig_order.order_id');
		//$this->db->join('ig_marchent_login', 'ig_order_items.merchant_id = ig_marchent_login.marchent_id');
		$this->db->where('(ig_order.date_added BETWEEN "'. date('Y-m-d', strtotime('today - 30 days')).'" AND "'.date('Y-m-d').'")');
		$this->db->where('ig_order_items.merchant_id',$merchant_id);
		//$this->db->group_by('ig_order.date_added');
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();
			//print_r($res); die();
			foreach($res as $key=>$val){
			//	echo $key;
				if(array_key_exists($val->order_date,$sumArr)){
					$sumArr[$val->order_date]['total'] += $val->total;
				}else{
					$sumArr[$val->order_date]['total'] = $val->total;
				}
				$sumArr[$val->order_date]['date']=$val->order_date;
			}
			//print_r($sumArr);
		}  
		else{
			$sumArr= array();	
		}
		//die('00');
	return $sumArr;
		//echo $merchant_id;die();
	}


}
?>