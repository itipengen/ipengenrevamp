<?php
class Product_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function getCategories(){
		$this->db->select('*');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ipengen_category.parent_id'=>0,'ig_category_details.lang'=>1)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array();
	}
	function getSubcategoriesBycatId($catId){
		$this->db->select('*');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ipengen_category.parent_id'=>$catId,'ig_category_details.lang'=>1)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array();
	}
	
	function insert_products($data)
	{  
		$this->db->insert('ig_products',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function insert_bulk_status($data)
	{  
		$this->db->insert('ig_bulk_puload_history',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function insert_admin_notification($data)
	{  
		$this->db->insert('ig_admin_notification',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
		
	
	function insert_producttext($data)
    {  
     	if($this->db->insert_batch('ig_products_text',$data))
			return true; 
		else
			return false;
	}
	function searchBrand($brand){
		$query = $this->db->get_where('ig_brand',array('brand_name' => $brand));
		if($query->num_rows() > 0){
			$result = $query->row();
			return $brand_id = $result->brand_id;
		}else{
			$data = array('brand_name'=>$brand);
			$this->db->insert('ig_brand',$data);
			return $this->db->insert_id();
		}
	}
	function updateProductByImages($p_id,$images)
    {  
       $this->db->where('product_id',$p_id);
	   $data=array('product_image' => $images);
	   $this->db->update('ig_products',$data);
	} 
	function additionProductByImages($p_id,$product_name)
	{
		$data = array('product_id' => $p_id, 'image_name' => $product_name);
		$this->db->insert('ig_product_img_gallery',$data);
	}
	function list_products($merchant_id)
    {
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
		//$this->db->join('ig_category_details', 'ig_category_details.cat_id =  ig_products.category_id');
		$this->db->where(array('ig_products_text.language_id' => 1,'ig_products.merchant_id' => $merchant_id));
		$this->db->order_by("ig_products.product_id","desc");
		$query = $this->db->get();
		 return $query->num_rows() > 0 ? $query->result() : array(); 
    }
	
	public function getProductDataById($id,$merchant_id)
		{
			
			$db_query = $this->db->select('*')
								 ->from('ig_products')
								 ->where(array('ig_products.product_id'=>$id,'ig_products.merchant_id' => $merchant_id,'ig_category_details.lang'=>1))
								 ->join('ig_products_text','ig_products_text.product_id = ig_products.product_id')
								 ->join('ig_brand','ig_brand.brand_id = ig_products.brand_id')
								 ->join('ig_category_details','ig_category_details.cat_id = ig_products.category_id')
								 ->get();
			
			
			return $db_query->num_rows() > 0 ? $db_query->result() : array();
		}
		public function getProductImgGalaryById($id)
		{
			$db_query = $this->db->select('
									ig_products.product_image,
									ig_product_img_gallery.image_name
										')
								 ->from('ig_products')
								 ->where(array('ig_products.product_id'=>$id))
								 ->join('ig_product_img_gallery','ig_product_img_gallery.product_id = ig_products.product_id')
								 ->get();
			$all_image = $db_query->result_array();
			foreach($all_image as $list)
			{
				$mainArr[] = $list['product_image'];
				$mainArr[] = $list['image_name'];
				
			}
			return $db_query->num_rows() > 0 ? array_unique($mainArr) : $this->_get_product_image($id);
		}
		
		private function _get_product_image($id) 
		{
			$db_query = $this->db->get_where("ig_products",array("product_id"=>$id));
			$productData = $db_query->result();
			foreach($productData as $data)
			{
				$productArr[] = $data->product_image;
			}
			return $db_query->num_rows() > 0 ? $productArr : array();
			
		}
		
		public function edit_product($p_id)
	  { 
	  
	    
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->where('ig_products.product_id',$p_id);
			$query = $this->db->get();
			return $query->row();
		
		}
		
		public function get_editlanguage($p_id)
	   {
		/*$this->db->select('*'); 
		$query  = $this->db->get('ig_language');
        return $query->result();*/
	
		$this->db->select('*');
		$this->db->from('ig_products_text');
		$this->db->join('ig_products',' ig_products.product_id=ig_products_text.product_id');
		$this->db->join('ig_language',' ig_products_text.language_id=ig_language.language_id');
		$this->db->where(array('ig_products_text.product_id'=>$p_id));
		$query=$this->db->get(); 
		if($query){
		return $query->result();
		}else{
		return FALSE;
		}
		
	   }  	
	   
	public function update_product($all_data,$p_id)
	  { 
		  $this->db->update('ig_products',$all_data,array('product_id'=>$p_id));
		  //echo $this->db->last_query();
		  //die();
		return $this->db->affected_rows();}	
		
	public function updet_producttext($all_data,$p_id)
	  { 
	
	 foreach($all_data as $val)
	 {
		 $dataArr=array(
		                'product_name'=>$val['product_name'],
						'short_description'=>$val['short_description'],
						'message'=>$val['message']		 				
		 );
		 $this->db->where(array('language_id'=>$val['language_id'],'product_id'=>$p_id));
		 $query=$this->db->update('ig_products_text', $dataArr); 
		 
	 }
		  
	}		
		
	public function insert_gallery($file_name,$p_id)
	  {   
	  $data=array('product_id' => $p_id,'image_name' => $file_name);
		  $this->db->insert('ig_product_img_gallery',$data);
		  }	
	   public function exit_id($id)
	  { 
		 $this->db->from('ig_product_img_gallery'); 
		 $this->db->where('product_id',$id);
		$query  = $this->db->get();
		
		if($query->num_rows()>0)
		{
        return $query->result();
		  }	 
		
	  }
	  
	   public function deleteExistProductByID($existProductID, $p_id)

	   {

		   $this->db->where('img_id',$existProductID);

		   $query =  $this->db->get('ig_product_img_gallery')->row();

		   $this->db->where('img_id',$existProductID);

		   $delQuery = $this->db->delete('ig_product_img_gallery'); 

			/*if($delQuery){

				if(is_file('./uploads/productImages/'.$p_id.'/100*100/'.$query->image_name))

					unlink('./uploads/productImages/'.$p_id.'/100*100/'.$query->image_name);

				if(is_file('./uploads/productImages/'.$p_id.'/400*400/'.$query->image_name))

					unlink('./uploads/productImages/'.$p_id.'/400*400/'.$query->image_name);

				if(is_file('./uploads/productImages/'.$p_id.'/800*800/'.$query->image_name))

					unlink('./uploads/productImages/'.$p_id.'/800*800/'.$query->image_name);

			}*/

		}
		
		
	  
	  public function updationProductByImages($g_id,$file_name)

		{

			//echo $g_id.'----'.$file_name;die();

			$data = array('product_id' => $g_id, 'image_name' => $file_name);

          $this->db->where('img_id',$g_id);

		  $this->db->insert('ig_product_img_gallery',$data);

			

		}   

		public function getProductImageByID($p_id)

		{

			$this->db->where('product_id',$p_id);

			$query =  $this->db->get('ig_products')->row();

		   if($query)

		   {return $query;}

		}
		
		
	  public function get_category_list()
		{
			$q = $this->db->select('
							ig_category_details.cat_id as categoryID,
							ig_category_details.name as categoryName,
									')
							->from('ig_category_details')
							->where(array('ig_category_details.lang'=>'1'))
							->order_by("ig_category_details.name","asc")
							->get();
			return ($q->num_rows() > 0) ? $q->result() : array();
		}
		
		
		
		public function deleteProductById($ProductId)
	 {
		//$this->db->where('product_id',$ProductId);
		//$this->db->delete('ig_products');
		$this->db->delete('ig_products', array('product_id' => $ProductId));
		$this->db->delete('ig_products_text', array('product_id' => $ProductId));
		
        rmdir('uploads/productImages/'.$ProductId);
		rmdir($this->config->item('image_path').'product/'.$ProductId);
		 }	
		
   		public function deleteAllProductById($ProductId){
			
			//$this->db->where('product_id',$ProductId);
			//$this->db->delete('ig_products');
			$this->db->delete('ig_products', array('product_id' => $ProductId));
			$this->db->delete('ig_products_text', array('product_id' => $ProductId));
			if (!$this->db->affected_rows()) {
				$ret['result']='0';
			}
			else{
				$ret['result']='1' ;	
			}
			//print_r($ret);
			return $ret;
			//$ret['result']= ($this->db->affected_rows() != 1) ? '0' : '1';
			//if (file_exists('uploads/productImages/'.$filename)) {
			//}
			// rmdir('uploads/productImages/'.$ProductId);
			//rmdir($this->config->item('image_path').'product/'.$ProductId);
			//return $ret;
		}
		
		public function getProductById($id){
        $q = $this->db->get_where('ig_products', array('product_id' => $id), 1);
        if ($q->num_rows() > 0) {
			foreach($q->result() as $row){
				$data = $row;
			}
			return $data;
        }
        return FALSE;
	   }
	   
	   public function category_id($id){
			$this->db->select('cat_id');
			$this->db->from('ig_category_details');
			$this->db->where(array('cat_id'=>$id,'lang'=>1)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->row() : '';
		}
		
		public function getcatParentId($id){
			$this->db->select('parent_id');
			$this->db->from('ipengen_category');
			$this->db->where(array('id'=>$id)); 
			$query=$this->db->get();
			if($query->num_rows() > 0)
			{
				$category = $query->row();
				$parent_id = $category->parent_id;
			}
			return $parent_id;
			
		}	


		public function getcategoryById($id){
			
			//$query=$this->db->get_where('ipengen_category', array('id'=>$id)); 
			$this->db->select('ipengen_category.id,ipengen_category.banner,ipengen_category.status,ipengen_category.parent_id,ig_category_details.name,ig_category_details.description,ig_language.language_id,ig_language.name as lang_name');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id','right');
			$this->db->join('ig_language','ig_category_details.lang=ig_language.language_id');
			$this->db->where(array('ig_category_details.cat_id'=>$id));
			$query=$this->db->get(); 
			if($query){
					return $query->result();
				}else{
					return FALSE;
				}
			
		}
		
		
		
		public function get_brand_list()
		{
			$this->db->order_by("ig_brand.brand_name","asc");
			$db_query = $this->db->get('ig_brand');
			return $db_query->num_rows() > 0 ? $db_query->result() : array();
		} 
		
		public function updateProduct($insert_id,$string){
			//	echo $insert_id;
			//echo $string;die();
			//$this->db->where('id', $insert_id);
			$this->db->update('ig_products',array('category_path'=>$string),array('product_id'=>$insert_id));
			if($this->db->affected_rows() >=0){
			// echo '1';
			return 1; 
			}else{
			//echo "0";
			return 0; 
			}
			//echo $this->db->last_query();die();	
		}
		
		public function get_merchant(){
			$this->db->select('*');
			$this->db->from('ig_marchent_login');
			$query = $this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array(); 
	    }
		
		 public function get_language()
	   {
		$this->db->select('*'); 
		$query  = $this->db->get('ig_language');
        return $query->result();
	   } 
	   
	   public function getProductUrl_name($id,$name){
		
		$slag=$this->slugify($name);
		return  base_url().'p/'.$id.'-'.$slag;
	}
	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
	public function product_admin_status_change($id,$value)
		{
			$this->db->update('ig_products',array('admin_status'=>$value),array('product_id'=>$id));
			return $this->db->affected_rows();
		}
		
}
?>