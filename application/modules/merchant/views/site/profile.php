<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url();?>assets/merchant/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<?php $class = array('class'=>'control-label'); ?>
<!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Profile
                                
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container-fluid">
                       <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <div class="profile-sidebar">
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light profile-sidebar-portlet ">
                                            <!-- SIDEBAR USERPIC -->
                                            <div class="profile-userpic">
                                                <img src="<?=base_url();?>assets/merchant/img/no-user-image.png" class="img-responsive" alt=""> </div>
                                            <!-- END SIDEBAR USERPIC -->
                                            <!-- SIDEBAR USER TITLE -->
                                            <div class="profile-usertitle">
                                                <!--<div class="profile-usertitle-name"> Marcus Doe </div>-->
                                                <div class="profile-usertitle-job"> Merchant </div>
                                                <div style="margin-bottom:10px"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- END SIDEBAR USER TITLE -->
                                            
                                        </div>
                                        <!-- END PORTLET MAIN -->
                                    </div>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title tabbable-line">
                                                        <div class="caption caption-md">
                                                            <i class="icon-globe theme-font hide"></i>
                                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#tab_1_1" data-toggle="tab">Business Details</a>
                                                            </li>
                                                            <li>
                                                                <a href="#tab_1_2" data-toggle="tab">Bank Details</a>
                                                            </li>
                                                            <li>
                                                                <a href="#tab_1_3" data-toggle="tab">Store Details</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="tab-content">
                                                            <!-- PERSONAL INFO TAB -->
                                                            <div class="alert alert-success successDiv" style="display:none;"></div>
                                                             <div class="alert alert-danger errorDiv" style="display:none;"></div>
                                                            <div class="tab-pane active" id="tab_1_1">
                                                                
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                        <?php  echo  form_label('Company Name','cname',$class); ?>
                                                                        <?php
                                                                            echo form_input(array('name' => 'cname','class' => 'form-control', 'id'=>'cname', 'placeholder' => 'Company Name','required'=>'required','value' => isset($merchant_details->bname) ? $merchant_details->bname : set_value("cname"),'readonly' => isset($merchant_details->bname) ? true : false)); 
                                                                            echo form_error('cname'); 
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->bname) && $merchant_details->bname!=''){ ?>
                                                                        <div class="col-md-1 custompencil">
                                                                        <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                        </div>
                                                                        <?php } ?>
                                                                         <div class="clearfix"></div>
																	</div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-11">
                                                                        <?php  echo  form_label('Description','cdescription',$class); ?>
                                                                        <?php
                                                                            echo form_input(array('name' => 'cdescription','class' => 'form-control', 'id'=>'cdescription', 'placeholder' => 'Description','value' => isset($merchant_details->bdescription) ? $merchant_details->bdescription : set_value("cdescription"),'readonly' => isset($merchant_details->bdescription) ? true : false)); 
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->bdescription) && $merchant_details->bdescription!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                       <?php echo  form_label('Email Address','cemail',$class); ?>
                                                                       <?php
																	   
																	         if(isset($merchant_details->email) && $merchant_details->email !='')
																			 {
																				 $email = $merchant_details->email;
																			 }else { $email = $merchant_details->marchent_email;}
                                                                                echo form_input(array('name' => 'cemail','class' => 'form-control', 'id'=>'cemail', 'placeholder' => 'Email Address','value' => isset($email) ? $email : set_value("cemail"),'readonly' => isset($merchant_details->email) ? true : false)); 
                                                                            ?>
                                                                        </div>
																		<?php if(isset($email) && $email!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        </div>
                                                                    <div class="form-group">
                                                                         <div class="col-md-11">
                                                                       <?php  echo  form_label('Phone/Mobile No.','cphone',$class); ?>
                                                                       <?php
																	        if(isset($merchant_details->phone) && $merchant_details->phone !='')
																			 {
																				 $phone = $merchant_details->phone;
																			 }else { $phone = $merchant_details->merchant_phone;}
																			 
                                                                            echo form_input(array('name' => 'cphone','class' => 'form-control', 'id'=>'cphone', 'placeholder' => 'Phone/Mobile No.','value' => isset($phone) ? $phone : set_value("cphone"),'readonly' => isset($merchant_details->phone) ? true : false)); 
                                                                        ?>
                                                                        </div>
																		<?php if(isset($phone) && $phone!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        
                                                                        </div>
                                                                   
                                                                    <div class="margiv-top-10">
                                                                     <button type="submit" class="btn green" id="businessSubmit">Save Changes</button>
                                                                        <button type="button" class="btn default" id="businessCancel">Cancel</button>

                                                                    </div>
                                                              
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                            <!-- CHANGE AVATAR TAB -->
                                                            <div class="tab-pane" id="tab_1_2">
                                                            
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                        <?php  echo  form_label('Account Name','acc_name',$class); ?>
                                                                        <?php
                                                                            echo form_input(array('name' => 'acc_name','class' => 'form-control', 'id'=>'acc_name', 'placeholder' => 'Account Name','required' => 'required','value' => isset($merchant_details->account_name) ? $merchant_details->account_name : set_value("acc_name"),'readonly' => isset($merchant_details->account_name) ? true : false)); 
                                                                            echo form_error('acc_name'); 
                                                                        ?>
                                                                        </div>
                                                                        <?php if(isset($merchant_details->account_number) && $merchant_details->account_number!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                         <div class="clearfix"></div>
																	</div>
                                                                    
                                                                    <div class="form-group">
                                                                        <div class="col-md-11">
                                                                        <?php  echo  form_label('Account Number','acc_number',$class); ?>
                                                                        <?php
                                                                            echo form_input(array('name' => 'acc_number','class' => 'form-control', 'id'=>'acc_number', 'placeholder' => 'Account Number','required' => 'required','value' => isset($merchant_details->account_number) ? $merchant_details->account_number : set_value("acc_number"),'readonly' => isset($merchant_details->account_number) ? true : false)); 
                                                                            echo form_error('acc_number');
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->account_name) && $merchant_details->account_name!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                       <?php  echo form_label('Bank Name','bank_name',$class); ?>
                                                                      <?php
                                                                            echo form_input(array('name' => 'bank_name','class' => 'form-control', 'id'=>'bank_name', 'placeholder' => 'Bank Name','required' => 'required','value' => isset($merchant_details->bank_name) ? $merchant_details->bank_name : set_value("bank_name"),'readonly' => isset($merchant_details->bank_name) ? true : false)); 
                                                                            echo form_error('bank_name');
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->bank_name) && $merchant_details->bank_name!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        </div>
                                                                        
                                                                    <div class="form-group">
                                                                         <div class="col-md-11">
                                                                       <?php  echo form_label('Branch Name','branch_name',$class); ?>
                                                                       <?php
                                                                            echo form_input(array('name' => 'branch_name','class' => 'form-control', 'id'=>'branch_name', 'placeholder' => 'Branch Name','value' => isset($merchant_details->branch_name) ? $merchant_details->branch_name : set_value("branch_name"),'readonly' => isset($merchant_details->branch_name) ? true : false)); 
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->branch_name) && $merchant_details->branch_name!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        
                                                                        </div>
                                                                        
                                                                    <div class="form-group">
                                                                         <div class="col-md-11">
                                                                       <?php  echo form_label('State','state',$class); ?>
                                                                       <?php
                                                                            echo form_input(array('name' => 'state','class' => 'form-control', 'id'=>'state', 'placeholder' => 'State','value' => isset($merchant_details->state) ? $merchant_details->state : set_value("state"),'readonly' => isset($merchant_details->state) ? true : false)); 
                                                                        ?>
                                                                        </div>
																		 <?php if(isset($merchant_details->state) && $merchant_details->state!=''){ ?>
                                                                    <div class="col-md-1 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        
                                                                        </div> 
                                                                        
                                                                    <div class="form-group">
                                                                         <div class="col-md-11">
                                                                        <?php  echo  form_label('City','city',$class); ?>
                                                                      <?php
                                                                            echo form_input(array('name' => 'city','class' => 'form-control', 'id'=>'city', 'placeholder' => 'City','value' => isset($merchant_details->city) ? $merchant_details->city : set_value("city"),'readonly' => isset($merchant_details->city) ? true : false)); 
                                                                        ?>
                                                                        </div>
																		<?php if(isset($merchant_details->city) && $merchant_details->city!=''){ ?>
                                                                    <div class="col-md-2 custompencil">
                                                                         <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        
                                                                        </div>       
                                                                   
                                                                    <div class="margiv-top-10">
                                                                    <button type="submit" class="btn green" id="bankSubmit">Submit</button>
                                                                        <button type="button" class="btn default" id="bankCancel">Cancel</button>
                                       
                                                                    </div>
                                                                    
                                                            </div>
                                                            <!-- END CHANGE AVATAR TAB -->
                                                            <!-- CHANGE PASSWORD TAB -->
                                                            <div class="tab-pane" id="tab_1_3">
                                                            
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                         <?php  echo  form_label('Store Name','store_name',$class); ?>
                                                                        <?php
                                                                                echo form_input(array('name' => 'store_name','class' => 'form-control', 'id'=>'store_name', 'placeholder' => 'Store Name','required' => 'required','value' => isset($merchant_details->sname) ? $merchant_details->sname : set_value("store_name"), 'readonly' =>isset($merchant_details->sname) ? true : false)); 
                                                                                echo form_error('store_name');
                                                                            ?>
                                                                        </div>
																		<?php if(isset($merchant_details->sname) && $merchant_details->sname!=''){ ?>
                                                                        <div class="col-md-1 custompencil">
                                                                             <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                        </div>
                                                                        <?php } ?>
                                                                         <div class="clearfix"></div>
																	</div>
                                                                    
                                                                    <div class="form-group">
                                                                        <div class="col-md-11">
                                                                        <?php  echo  form_label('Store Description','sdescription',$class); ?>
                                                                        <?php
                                                                                echo form_input(array('name' => 'sdescription','class' => 'form-control', 'id'=>'sdescription', 'placeholder' => 'Store Description','value' => isset($merchant_details->sdescription) ? $merchant_details->sdescription : set_value("sdescription"),'readonly' => isset($merchant_details->sdescription) ? true : false)); 
                                                                            ?>
                                                                        </div>
																		<?php if(isset($merchant_details->sdescription) && $merchant_details->sdescription!=''){ ?>
                                                                        <div class="col-md-1 custompencil">
                                                                             <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                        </div>
                                                                        <?php } ?>
                                                                    <div class="clearfix"></div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        
                                                                        <div class="col-md-11">
                                                                       <?php  echo  form_label('Brands','brands',$class); ?>
                                                                       <?php
                                                                                echo form_input(array('name' => 'brands','class' => 'form-control', 'id'=>'brands', 'placeholder' => 'Brands','required' => 'required','value' => isset($merchant_details->brands) ? $merchant_details->brands : set_value("brands"),'readonly' => isset($merchant_details->brands) ? true : false)); 
                                                                                echo form_error('brands');
                                                                            ?>
                                                                        </div>
																		<?php if(isset($merchant_details->brands) && $merchant_details->brands!=''){ ?>
                                                                        <div class="col-md-1 custompencil">
                                                                             <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                                                        </div>
                                                                        <?php } ?>
                                                                    <div class="clearfix"></div> 
                                                                        </div>
                                                                        
                                                                    
                                                                   
                                                                    <div class="margiv-top-10">
                                                                     <button type="submit" class="btn green" id="storeSubmit"> Submit</button>
                                                                            <button type="button" class="btn default" id="storeCancel">Cancel</button>

                                                                    </div>
                                                                    
                                                            </div>
                                                            <!-- END CHANGE PASSWORD TAB -->
                                               <input type="hidden" value="<?php echo (isset($merchant_details) && $merchant_details->bname!='') ? 1 : 0 ?>" id="hidTab1" />
                                                <input type="hidden" value="<?php echo (isset($merchant_details) && $merchant_details->account_name!='') ? 1 : 0 ?>" id="hidTab2" />
                                                <input type="hidden" value="<?php echo (isset($merchant_details) && $merchant_details->sname!='') ? 1 : 0 ?>" id="hidTab3" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/custom_dashboard.js"></script>