<link rel="stylesheet" type="text/css" href="http://staging.ipengen.com/assets/css/style.css">
<div class="page-container">
<div class="page-content-wrapper">
<div class="page-head">
      <div class="container" style="padding-left: 34px;"> 
        
        <!-- BEGIN PAGE TITLE -->
        <div class="row">
        <div class="page-title">
          <h1>Notifications</h1>
        </div>
        </div>
        <div class="row">
        <div class="bredcum">
         <ol class="breadcrumb">
            <li> <a href="<?php echo base_url("merchant");?>">Home</a>/ </li>
            <li class="active"> Notifications</li>
        </ol>
        </div>
        </div>
        
       
      </div>
    </div>
    
            <div class="page-content">
             <div class="container">
             <div class="page-content-inner">
                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">

                        </div>
                        <div class="ibox-content">
    <table class="table table-striped table-bordered table-hover " id="notificationtable" >
        <thead>
            <tr>
                <th>Serial No</th>
                <th>Type</th>
                <th>Message</th>
                <th>Date</th> 
                <th>Status</th>
            </tr>
        </thead>
    </table>
 </div>
                        
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>





<!-- ................................. datatable js url ............... -->         
	<script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
		var base_url = window.location.origin;
		
		/* Init DataTables */
		var dataTable = $('#notificationtable').DataTable( {
					"processing": true,
					"serverSide": true,
					"aaSorting": [[0,'DESC']],
					"ajax":{
						url :base_url+'/merchant/index/ajaxnotification', // json datasource
						type: "post",  // method  , by default get
						error: function (){ 
						
							$(".employee-grid-error").html("");
							$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
						
						
						
						 }
				
					},
					"fnRowCallback" : function(nRow, aData, iDisplayIndex){
						$("td:first", nRow).html(iDisplayIndex +1);
					   return nRow;
					}
				} );
		
		
	});
    
    </script>