<link rel="stylesheet" type="text/css" href="http://staging.ipengen.com/assets/css/style.css">
<div class="page-container">
  <div class="page-content-wrapper">
    <div class="page-head">
      <div class="container"> 
        
        <!-- BEGIN PAGE TITLE -->
        
        <div class="page-title">
          <h1>Order Details </h1>
        </div>
      </div>
    </div>
    <div class="page-content">
      <div class="container">
        <div class="page-content-inner">
          <div class="row">
            <div class="col-lg-12">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <div class="row">
                    <div class="col-md-9"><span style='font-size:20px;'><b> INVOICE ID - <?php echo isset($getInvoiceDetails[0]->invoice)? $getInvoiceDetails[0]->invoice : ''; ?></b> 
                    
                     <a href="<?php echo base_url(); ?>admin/invoice/downloadpdf/<?php echo $getInvoiceDetails[0]->invoice; ?>" title="Download pdf">
			<i class="fa fa-file-pdf-o" style="color:#f00"></i> </a>
            
              </span>
            
                 </div>
                    
                    <div class="col-md-3"> <span style='font-size:20px;'><b>TOTAL - <?php echo strtoupper($this->config->item('currency')).' '.number_format((int)$getInvoiceDetails[0]->total_amount) ?></b></span> </div>
                  </div>
                </div>
                <div class="ibox-content">
                  <?php  $totalPrice=array();
						if(!empty($getInvoiceProduct['data'])){ ?>
                  <div class='productList'>
                    <?php foreach($getInvoiceProduct['data'] as $order){
							//$tprice=$order->sale_price*$order->quantity;
							$totalPrice[]=(int)$order['total']-(int)$order['transaction_amt'];
							$full_name = ucfirst($order['marchent_name']);
							?>
                    <div class="row">
                      <div class="col-md-2">
                        <?php 
							if(is_file($this->config->item('image_path').'/product/'.$order['product_id']."/450X450/".$order['product_image']))
							{ $imageURL = base_url()."photo/product/".$order['product_id']."/450X450/".$order['product_image']; } 
							else
							{$imageURL = base_url('photo/product/no_product.jpg');}
						?>
                        <img src="<?php echo $imageURL; ?>"  class="img-responsive" width="100px" height="100px"/> </div>
                      <div class="col-md-5">
                        <div><?php echo $order['name'];?></div>
                        <?php if($order['item_type']!='cash_gift'){ ?>
                        <div>Quantity : <?php echo $order['quantity']; ?></div>
                        <?php } ?>
                        <div> Fee :
                          <?php  echo strtoupper($this->config->item('currency')).' '.$order['transaction_amt'];?>
                        </div>
                        <div> Order Id :
                          <?php  echo $order['order_id']; ?>
                        </div>
                        <div class="statusmsg" style="display:none"></div>
                        <input type="hidden" value="<?php echo $order['order_id']?>" name="productid" id="oID" />
                        <input type="hidden" value="<?php echo $order['product_id']?>" name="productid" id="pID" />
                      </div>
                      <div class="col-md-3"> </div>
                      <div class="col-md-2"><?php echo strtoupper($this->config->item('currency')).' '.number_format($order['total']); ?></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <?php } ?>
                  </div>
                  <div class="row">
                    <div class="col-md-9"><b> Total Fee</b> </div>
                    <div class="col-md-3"> <?php echo '-'.strtoupper($this->config->item('currency')).' '.$getInvoiceDetails[0]->total_transaction_fee; ?> </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-md-9"> <b>Invoice Date</b> </div>
                    <div class="col-md-3"><?php echo $getInvoiceDetails[0]->date; ?> </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-md-9"> <b>Invoice Status</b> </div>
                    <div class="col-md-3"> <?php echo $getInvoiceDetails[0]->is_paid==0 ? 'Pending' : 'Paid';  ?> </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-md-9"> <b>Payment Date</b> </div>
                    <div class="col-md-3"><?php echo $getInvoiceDetails[0]->payment_date; ?> </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="row">
                    <div class="col-md-9"> <b>Remarks</b> </div>
                    <div class="col-md-3"><?php echo $getInvoiceDetails[0]->remarks=='' ? '' : $getInvoiceDetails[0]->remarks; ?> </div>
                  </div>
                 <!-- <div class="hr-line-dashed"></div>
                  <div class="row">
                    <button id="generatePdf"  style="  background-color: #1a7bb9;
    border-color: #1a7bb9;
    color: #ffffff; 
          onclick="#">Generate PDF </button>
                  </div>-->
                  <?php }else{
								echo "No result found.";	
							}?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
