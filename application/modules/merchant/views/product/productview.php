 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Productview
                               
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
        <div class="row">
          <div class="col-md-5">
            <div class="product-images">
				<?php if (!empty($galary)) {foreach ($galary as $value) { ?>
                    <div>
                        <div class="image-imitation">
                        	<?php if(is_file($this->config->item('image_path').'/product/'. $product_id .
$this->config->item('large_thumb_size').$value)){
								
								$imagepath = site_url('photo/product/'. $product_id .
$this->config->item('large_thumb_size').$value);
								
							}
							else{
								$imagepath = site_url('photo/product/no_product.jpg');
								
								 }?>
                        	<img src="<?php echo $imagepath; ?>" width="387" height="430">
                        </div>
                    </div>
                <?php }}else{ ?>
                	<div>
                        <div class="image-imitation">
                        	<img src="<?= site_url('photo/product/no_product.jpg'); ?>" width="387" height="430">
                        </div>
                    </div>
                <?php } ?>
            </div>
          </div>
          <div class="col-md-7">
		
          <?php if(!empty($data)){
					foreach($data as $list){if($list->language_id == '1'){ ?>
            <h2 class="font-bold m-b-xs"> <?= ucfirst($list->product_name) ?> </h2>
            <small><!--Many desktop publishing packages and web page editors now.--></small>
            <div class="m-t-md">
              <h2 class="product-main-price"><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?> <small class="text-muted"><!--Exclude Tax--> Price</small> </h2>
            </div>
            <hr>
            <h4>Product Brand Name</h4>
            <div class="small text-muted"> <?= ucfirst($list->brand_name) ?> </div>
            
            <h4>Product Category Name</h4>
            <div class="small text-muted"> <?= ucfirst($list->name) ?> </div>
            
            <h4>Short Description</h4>
            <div class="small text-muted"> <?= ucfirst($list->short_description) ?> </div>
            <h4>Sale Start Date</h4>
            <div class="small text-muted"> <?= $list->sale_start_date ?> </div>
            <h4>Sale End Date</h4>
            <div class="small text-muted"> <?= $list->sale_end_date ?> </div>
            <h4>Price</h4>
            <div class="small text-muted"> <?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?> </div>
            <h4>Sale Price</h4>
            <div class="small text-muted"> <?= strtoupper($this->config->item('currency')).' '.number_format($list->sale_price) ?> </div>
            
            <h4>Product Description</h4>
            <div class="small text-muted"> <?= ucfirst($list->message) ?> </div>
            
            
            
            <hr>
          <?php }}}else{ ?>
            <div class="col-md-12"> No Data Available </div>
          <?php } ?>
          </div>
        </div>
        <div style="text-align: right;"> <a href="<?= base_url('merchant/product/edit/'.$this->uri->segment(4)); ?>" class="btn btn-success">Edit</a> </div>
        <div class="clearfix"></div>
      </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        
        <script>
    $(document).ready(function(){
        $('.product-images').slick({
            dots: true
        });
    });

</script>