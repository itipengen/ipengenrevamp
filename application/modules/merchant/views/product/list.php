<link href="<?=base_url();?>assets/merchant/css/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>assets/merchant/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
	#sample_1_wrapper .col-md-12 {margin-bottom: 1%;}
	.dt-buttons a {margin-right: 5px;}
	.dt-buttons {float: right !important;}
	.xlsBtn{background-color: #1a7bb9;border-color: #1a7bb9;color: #fff}
</style>
<?php $class = array('class'=>'col-md-3 control-label'); ?>
<!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>List of Products</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container-fluid">
                        
                        <!-- BEGIN PAGE CONTENT INNER -->
                        	<div class="page-content-inner">
                            
	                            <?php if (isset($error) && $error!=''): ?>
	                                <div class="alert alert-danger"><?php echo $error; ?></div>
	                            <?php endif; ?>
	                            <?php if (isset($success) && $success!=''): ?>
	                                <div class="alert alert-success"><?php echo $success; ?></div>
	                            <?php endif; ?>
                                
                                
                            
								<div class="row">
								  <div class="col-lg-12">
								    <div class="ibox float-e-margins">
								      <div class="ibox-title"></div>
								      <div class="ibox-content"> <a href="<?= base_url('merchant/product') ?>" class="btn btn-success">Add New</a>
                                      <button class='btn btn-success' id='deleteSelected'>Delete</button>
                                      <button class='btn btn-success btn-transparent btn-outline btn-sm active xlsBtn' id='excelBtn'>Excel</button>
                                      <?php /*?><button id="excelBtn" class="btn btn-transparent btn-outline btn-sm active xlsBtn"  style="margin-bottom:10px;">Excel</button><?php */?>
                                        
								        <br/><br/>
                                        <div class="row admin-list-msg" style="display:none">
                                <div class="col-lg-12">
                                <div class="ibox float-e-margins admin-flash-msg"> </div>
                                </div>
                                </div>
								        <table class="table table-striped table-bordered table-hover text-center" id="sample_1" >
								          <thead>
								            <tr>
                                              <th class="text-center"><div class="checker"><span><input class="group-checkable" type="checkbox"></span></div></th>
                                              <th class="text-center">Product ID</th>
								              <th class="text-center">Product Name</th>
								              <th class="text-center">Stock Quantity</th>
								              <th class="text-center">Product Price</th>
								              <th class="text-center">Sale Price</th>
								              <th class="text-center">Image</th>
								              <th class="text-center">Admin Status</th>
                                              <th class="text-center">Product Status</th>
                                              <th class="text-center">View</th>
								              <th class="text-center">Actions</th>
								            </tr>
								          </thead>
								          <tbody>
								            <?php if(!empty($product_list)){foreach($product_list as $list){ ?>
								            <tr>
											  <td><div class="checker"><span><input class="group-checkable-sub" type="checkbox"></span></div></td>
								              <td><?= $list->product_id ?></td>
								              <td><?= $list->product_name ?></td>
								              <td><?= $list->stock_quantity ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->sale_price) ?></td>
								              <td><?php
											   if(is_file($this->config->item('image_path').'/product/'.$list->product_id.$this->config->item('thumb_size').$list->product_image)){ ?>
								                <img src="<?= base_url('photo/product/'.$list->product_id.$this->config->item('thumb_size').$list->product_image); ?>" width="36" height="36" class="img-circle imageZoom" id="<?= base_url('photo/product/'.$list->product_id.$this->config->item('medium_thumb_size').$list->product_image); ?>"> <img src="" style="display:none;"/>
								                <?php }else{ ?>
								                <img src="<?= base_url('photo/product/no_product.jpg'); ?>" width="36" height="36" class="img-circle imageZoom">
								                <?php } ?></td>
                                                
								              <td><?php
								                  if ($list->admin_status == 0) { ?>
								                     <span class="label label-sm label-danger">Not Approved</span>
								                  <?php }else{ ?>
								                    <span class="label label-sm label-success">Active</span>
								                 <?php } ?>
                                               
								               </td>
                                               
                                               <?php if ($list->is_approve == 1){?>
                                                <td>
                                                <?php 
                                                if ($list->admin_status == 0) {
                                                $checkad = '';
                                                }else{
                                                $checkad = 'checked';
                                                }
                                                ?>
                                                 <input type='checkbox' value='<?php echo $list->product_id; ?>' class='js-switch1' <?php echo $checkad ?> />
                                               </td>
                                               <?php } else {?>
                                              <td> wait for admin approve</td>
                                               <?php } ?>
                                               <td>
                                               <div style="margin-top: 10px;"><a class="overlay" target="_blank" href="<?php echo $this->dashboard_model->getProductUrl_name($list->product_id,$list->product_name)?>" title="Front View"><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div></td>
								              <td><div style="margin-top: 10px;"> <a href="<?= site_url('merchant/product/productview/'.$list->product_id); ?>" title="Product View"> <i class="fa fa-eye fa-fw" style="color:#508DF2"></i> </a> <a href="<?php echo site_url('merchant/product/edit/'.$list->product_id);?>" title="Product Edit"> <i class="fa fa-pencil-square-o fa-fw action-btn"></i> </a> <a href="javascript:void(0)" class="product_delete" id="<?php echo $list->product_id; ?>" title="Product Delete"> <i class="fa fa-trash-o fa-fw action-btn text-danger"></i> </a> </div></td>
								            </tr>
								            <?php }} ?>
								          </tbody>
								        </table>
								      </div>
								    </div>
								  </div>
								</div>
                            </div>
                            
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/custom_product.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/datatable.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/datatables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/datatables.bootstrap.js"></script>
<?php /*?><script type="text/javascript" src="<?=base_url();?>assets/merchant/js/table-datatables-colreorder.min.js"></script><?php */?>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.table2excel.js"></script>
<script>
$(document).ready(function(){
	var base_url = window.location.origin;
		var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	    var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';

	$(document).on('click','#excelBtn',function(){
		$("#sample_1").table2excel({
			name: "Product List",
			filename: "Product",
			fileext: ".xls",
			exclude_img: true,
			exclude_links: true,
			exclude_inputs: true
		});	
	});


	$(document).on('click','.group-checkable',function(){
		//var checkboxes = $(this).closest('form').find(':checkbox');
		if($(this).is(':checked')) {
			alert('checked');
			$('.group-checkable-sub').prop('checked', true);
		} else {
			alert('not checked');
			$('.group-checkable-sub').prop('checked', false);
		}
	});	
	
	$(document).on('click','#deleteSelected',function(){
		jQuery.each( $('.group-checkable-sub'), function( i, val ) {
			if($(this).is(':checked')){
				var pid=$(this).parent().parent().parent().parent().find('td:nth-child(2)').html();
				$.ajax({
						type: "POST",
						//url: "http://localhost/ipengen/wishlist/create_wishlist",
						url: base_url+'merchant/product/deleteAll',
						data: { pid: pid },
						cache: false,
						dataType: 'json',
						success: function(result){
							console.log(result);
							if(result.success=='1'){
								window.location.replace(base_url+'merchant/product-list');	
							}
							else{
								alert('Deletion Problem. Please try again. ');
							}
						}	
				});	
			}
		});
	});
	
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch1'));
	// console.log(elems);
	$(".switchery").remove();
	elems.forEach(function(data) {
	
	var switchery = new Switchery(data,{ color: '#1AB394' });
	});
	
	/***after Click on Pagination Link*******/
	$(document).on('click', '.paginate_button a', function(){
	$( "#admintable tbody tr" ).each(function() {
	 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){
		var elem = this.querySelector('.js-switch1');
		Switchery(elem, { color: '#1AB394' });
		var elem_new = this.querySelector('.js_switch_feature');
		Switchery(elem_new, { color: '#1AB394' });	
		$(".imageZoom").imageTooltip();
	 }
	});
	
	setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch1,.js_switch_feature'));
	$(".imageZoom").imageTooltip();
	
	$(".switchery").remove();
	elems.forEach(function(data) {
	
	var switchery = new Switchery(data,{ color: '#1AB394' });
	}); }, 3000);
	
	});
	
	/*****Product Status Change*****/
    $(document).on("change",".js-switch1",function(e){
	  var productId = $(this).attr("value");
	  var ProName = $(this).closest("tr").find("td:eq(1)").html();
	  if (this.checked) {
		  $.ajax({
			  url: base_url+"/merchant/product/status/" + productId + "/1",
			  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
			  success: function(result){
				  	$('#loaderBg2').remove();
					if($.trim(result) != 0)
					{
						$(this).attr("checked", "checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(ProName +" activated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).removeAttr("checked");}
				  }
			  });
	  }else{
		  $.ajax({
			  url: base_url+"/merchant/product/status/" + + productId + "/0",
			   beforeSend: function(){
					  	$("body").append(lhtml);
					  },
			  success: function(result){
				  	$('#loaderBg2').remove();
					if($.trim(result) != 0)
					{
						$(this).removeAttr("checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(ProName +" deactivated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).attr("checked", "checked");}
				  }
			  });
	  };
    });
	
	$( ".dt-buttons a:nth-child(1)" ).css('display','none');
	$( ".dt-buttons a:nth-child(2)" ).removeClass('green').addClass('label-success');
	$( ".dt-buttons a:nth-child(3)" ).removeClass('purple').addClass('label-success');
});
</script>
<style>
.admin-flash-msg {
    background-color: #daefda;
    padding: 12px;
}
.switchery {
    border-radius: 15px !important;
}
</style>