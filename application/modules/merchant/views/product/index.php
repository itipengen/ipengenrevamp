<?php $class = array('class'=>'col-md-5 control-label'); ?>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Upload Bulk Products
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-6 ">
                                    <!-- BEGIN SAMPLE FORM PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-red-sunglo">
                                                <i class="icon-settings font-red-sunglo"></i>
                                                <span class="caption-subject font-dark bold uppercase">upload product</span>
                                            </div>
  
                                        </div>
										<?php
                                                $attributes = array('class' => 'form', 'id' => 'merchantform');
                                                echo form_open_multipart('merchant/product', $attributes); 
                                                ?>
                                        <div class="portlet-body form">
                                        
									<?php if (isset($error) && $error!=''): ?>
                                    <div class="alert alert-danger"><?php echo $error; ?></div>
                                    <?php endif; ?>
                                    <?php if (isset($success) && $success!=''): ?>
                                    <div class="alert alert-success"><?php echo $success; ?></div>
                                    <?php endif; ?>
                                    <?php if (isset($success_image) && $success_image!=''): ?>
                                    <div class="alert alert-success"><?php echo $success_image; ?></div>
                                    <?php endif; ?>
												
                                                <div class="form-body">
                                                    <div class="form-group">
                                                    
                                                        <label for="single" class="control-label">Categories</label>
                                                            <select required id="singleCat" class="form-control select2" name="category">
                                                            <option></option>
                                                            <?php foreach ($categories as $key => $category) { ?>
                                                            
                                                            <option value="<?php echo $category['cat_id'] ?>"><?php echo strtoupper($category['name']); ?></option>
                                                            
                                                            <?php } ?>
                                                            </select>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="multiple" class="control-label">Subcategory</label>
                                                         <select required id="singleSub" name="subcategory" class="form-control select2"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <?php  echo  form_label('Upload bulk Products (.xlsx)','product',$class); ?>
                                                       
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <span class="btn green btn-file">
                                                                    <span class="fileinput-new"> Select file </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="product" id="product" required='required'> </span>
                                                                <span class="fileinput-filename"> </span> &nbsp;
                                                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                            </div>
                                                            
                                                        
                                                        </div>
                                                    <div class="form-group">
													<?php  echo  form_label('Upload Image folder (.zip)','image',$class); ?>
                                                                                                           
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <span class="btn green btn-file">
                                                                    <span class="fileinput-new"> Select file </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="product_image" id="image" required='required'> </span>
                                                                <span class="fileinput-filename"> </span> &nbsp;
                                                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                            </div>
                                                            
                                                        
                                                        </div>    
                                                    </div>
                                                 </div>
                                                <div class="form-actions">
                                                <button type="submit" class="btn green" id="productSubmit" name="submit">Upload</button>
                                                 </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- END SAMPLE FORM PORTLET-->
                                
                                <div class="col-md-6 ">
                                    <!-- BEGIN SAMPLE FORM PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-settings font-dark"></i>
                                                <span class="caption-subject font-dark sbold uppercase">guidelines</span>
                                            </div>
                                            
                                        </div>
                                        <div class="portlet-body form">
                                                <div class="form-group">
                                                
                                                This is the guidelines for uplode bulk product, please read this before 
                                                you upload the product.
                                                
                                                <br /><br />
                                                
                                                First you need to choose category and then there will display subcategory 
                                                with respect to category. Then choose subcategory and after that you can uplode
                                                a (.xlsx) file of product list and upload product image with a .zip folder.
                                                
                                                
                                                <br /><br />
                                                
                                                ** Please view the sample file before upload. 
                                                
                                                
                                                </div>   
                                                   
                                                <div class="form-actions">
                                                    <div class="actions">
                                                <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                    <a id="sample_csv_file" href="javascript:void(0);" class="btn"><label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                        <input type="radio" name="options" class="toggle" id="option2">Sample File (.xlsx)</label></a>
                                                </div>
                                                
                                                <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                    <a id="sample_image_folder" href="javascript:void(0);" class="btn"><label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                        <input type="radio" name="options" class="toggle" id="option2">Sample image folder</label></a>
                                                </div>
                                                
                                            </div>
                                                </div>
                                           
                                        </div>
                                    </div>
                                    <!-- END SAMPLE FORM PORTLET-->
                                                                        
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/custom_product.js"></script>

