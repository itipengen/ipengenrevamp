<?php
/// check is invoice is paid or not 
if($getInvoiceDetails[0]->is_paid == 0){
  	$balance_amount_due =  $currency .$getInvoiceDetails[0]->total_amount;
	$balance_amount_paid =  0;//$currency .$getInvoiceDetails[0]->total_amount;
}else{
	if($getInvoiceDetails[0]->is_paid == 0){
		$balance_amount_due =  0;
		$balance_amount_paid =  $currency .$getInvoiceDetails[0]->total_amount;
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<title>Invoice</title>
<link href="<?php echo base_url();?>assets/admin/invoice/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/invoice/css/print.css" rel="stylesheet">
</head>

<body>
<div id="page-wrap">
  <div align="center" class="invoice_header">Invoice</div>
  <div id="logo"> <img  src="<?php echo base_url();?>assets/admin/invoice/images/logo.png" alt="logo" /> <br />
  </div>
  <div id="identity"> <?php echo $getInvoiceDetails[0]->marchent_name;?> ( #<?php echo $getInvoiceDetails[0]->merchant_id;?> ) <br />
    <!--123 Appleseed Street<br />
Appleville, WI 53719<br />--> 
    <?php echo $getInvoiceDetails[0]->marchent_email;?> <br />
    Phone: <?php echo $getInvoiceDetails[0]->merchant_phone;?> <br />
    <?php if(!empty($businessnamearray)){echo $businessnamearray[0]->bname;}?>
  </div>
  <div style="clear:both"></div>
  <div id="customer">
    <?php // if(!empty($businessnamearray)){echo $businessnamearray[0]->bname;}?>
    <table id="meta">
      <tr>
        <td class="meta-head">Invoice #</td>
        <td><?php echo $getInvoiceDetails[0]->invoice;?></td>
      </tr>
      <tr>
        <td class="meta-head">Date</td>
        <td><?php  echo date("jS F, Y", strtotime($getInvoiceDetails[0]->date)); ?></td>
      </tr>
      <tr>
        <td class="meta-head">Amount</td>
        <td><div class="due"><?php echo $currency .$getInvoiceDetails[0]->total_amount;?></div></td>
      </tr>
    </table>
  </div>
  <table id="items">
    <tr>
      <th>Order id</th>
      <th>Description</th>
      <th>Unit Cost</th>
      <th>Quantity</th>
      <th>Price</th>
    </tr>
    <?php
		  
		  $productArry = $getInvoiceProduct['data'];
		  
		 
		   if(!empty($productArry))
		       {
				   
				  foreach($productArry as $getInvoiceProduct)
				  
				  {  
				   print_r($getInvoiceProduct['order_id']);
				  ?>
    <tr class="item-row">
      <td class="item-name"><div class="delete-wpr"><?php echo $getInvoiceProduct['order_id'] ;?></div></td>
      <td class="description"><?php echo $getInvoiceProduct['name'] ;?></td>
      <td><?php echo $currency.$getInvoiceProduct['price'] ;?></td>
      <td><?php echo $getInvoiceProduct['quantity'] ;?></td>
      <td><span class="price"><?php echo $currency.$getInvoiceProduct['total'];?></span></td>
    </tr>
    <?php 	}
		 		 }
		    else { 
			echo "No product found"; 
			
			} ?>
    <?php 
		  
		  if(isset($getInvoiceDetails[0]->total_transaction_fee) && $getInvoiceDetails[0]->total_transaction_fee>0)
		  {
			  ?>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Total Transaction Fees</td>
      <td class="total-value"><div id="subtotal">- <?php echo $currency .$getInvoiceDetails[0]->total_transaction_fee;?></div></td>
    </tr>
    <?php 
					  
		  }
		  ?>
    <tr id="hiderow">
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Subtotal</td>
      <td class="total-value"><div id="subtotal"><?php echo $currency .$getInvoiceDetails[0]->total_amount;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Total</td>
      <td class="total-value"><div id="total"><?php echo $currency .$getInvoiceDetails[0]->total_amount;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Amount Paid</td>
      <td class="total-value"><?php echo $balance_amount_paid?></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line balance">Balance Due</td>
      <td class="total-value balance"><div class="due"><?php echo $balance_amount_due;?></div></td>
    </tr>
  </table>
</div>
</body>
</html>