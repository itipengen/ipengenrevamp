 <!-- BEGIN FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
<!-- Mainly scripts --> 
 
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/plugins/jeditable/jquery.jeditable.js"></script> 
<!-- jQuery UI --> 
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!-- Jvectormap --> 
<script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 

<!-- Sparkline --> 
<script src="<?php echo base_url();?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script> 
<!-- Sparkline demo data  --> 
<script src="<?php echo base_url();?>assets/js/demo/sparkline-demo.js"></script> 
<!-- ChartJS--> 
<script src="<?php echo base_url();?>assets/js/plugins/chartJs/Chart.min.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/bootstrap_confirm.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/bootbox.js"></script> 
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/js/plugins/select2/select2.full.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/merchant/js/app.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/merchant/js/components-select2.min.js" type="text/javascript"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->

<script src="<?php echo base_url();?>assets/merchant/js/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/merchant/js/demo.min.js" type="text/javascript"></script>

<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>