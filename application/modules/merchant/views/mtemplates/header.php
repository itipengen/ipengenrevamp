<?php 



  $query = $this->db->get_where('ig_marchent_login',array('marchent_email'=>$this->session->userdata('merchant_logged_in'))); 
  $result = $query->result();
  $full_name = "";
  $position = "";
  if (!empty($result)) {
    foreach ($result as $value) {
      $full_name = ucwords($value->marchent_name);
      $approved = $value->merchant_admin_status;
	  $merchantid = $value->marchent_id;
    }
  }
?>

<?php
$CI =& get_instance();
 if($this->input->is_ajax_request() && isset($_POST['status']) && !empty($_POST['status']) && $_POST['status'] == "1"){
		
		//count not read notification....
		$data= array( 'status' => 1);
		
		$CI->db->where('m_id', $merchantid);
		$CI->db->update('ig_admin_notification', $data); 
		
		$notiquerycheck = $CI->db->affected_rows();
		if ($notiquerycheck == '1') {
		return TRUE;
		} 
		
		exit(0);
		
	  
	  }

?>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo (isset($page_title)) ? $page_title : $this->lang->line('admin_tiltle');?>
<?php //echo $this->lang->line('dashboard');?>
</title>
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/merchant/css/components.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/merchant/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<!-- Jquery User Interface-->

<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<!-- Loader CSS-->

<link href="<?php echo base_url();?>assets/css/loader.css" rel="stylesheet" type="text/css" />

<!--Sumer Notes-->

<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

<!-- Morris -->

<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">

<!-- slick carousel-->

<link href="<?php echo base_url();?>assets/css/plugins/slick/slick.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">

<!-- Switchery -->

<link href="<?php echo base_url();?>assets/css/plugins/switchery/switchery.css" rel="stylesheet">

<!-- Select2 -->

<link href="<?php echo base_url();?>assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/select2/select2-bootstrap.min.css" rel="stylesheet">

<!-- Layout Style -->

<link href="<?php echo base_url();?>assets/css/layout/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/layout/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/merchant/css/merchant.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/merchant/css/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

<!--********************************Js File Start***************************************-->

<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/merchant/js/bootstrap-fileinput.js"></script>

<!-- Data picker -->

<script src="<?php echo base_url('assets/js/plugins/datapicker/bootstrap-datepicker.js');?>"></script>
<script src="http://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slick/slick.min.js"></script>

<!-- Switchery -->

<script src="<?php echo base_url();?>assets/js/plugins/switchery/switchery.js"></script>

<!--Image Zoom-->

<script src="<?php echo base_url();?>assets/js/plugins/image-tooltip/image-tooltip.js"></script>


<style>
.colorchange {
    background: #e8e7e7 none repeat scroll 0 0;
}
</style>

</head>


<?php
			$CI->db->select('*');
			$CI->db->from('ig_admin_notification');
			$CI->db->where('notification_for',2);
			$CI->db->where('status',0);
			$CI->db->where('m_id',$merchantid);

			$notiquerycheck = $CI->db->get();
			$notiquerycount =  $notiquerycheck->num_rows();
			
			
			//shows result last 10 notification....
			$CI->db->select('*');
			$CI->db->from('ig_admin_notification');
			$CI->db->limit(10);
			$CI->db->where('notification_for',2);
			$CI->db->where('m_id',$merchantid);
			$this->db->order_by('date','DESC');

			$notiquery = $CI->db->get();
			$countnoti =  $notiquery->num_rows();
			$notiresult = $notiquery->result();

?>

<body class="page-container-bg-solid page-boxed">

<!-- BEGIN HEADER -->

<div class="page-header"> 
  
  <!-- BEGIN HEADER TOP -->
  
  <div class="page-header-top">
    <div class="container-fluid"> 
      
      <!-- BEGIN LOGO -->
      
      <div class="page-logo"> <a href="<?= base_url('merchant'); ?>"> <img src="<?php echo base_url();?>assets/frontend/img/logo.png" alt="logo" class="logo-default"> </a> </div>
      
      <!-- END LOGO --> 
      
      <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
      
      <a href="javascript:;" class="menu-toggler"></a> 
      
      <!-- END RESPONSIVE MENU TOGGLER -->
      
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
        
         <li id="notification_li"> 
         <?php if($notiquerycount >0){ ?>
            <span id="notification_count"><?php echo $notiquerycount; ?></span> 
            <?php } ?>
            <a href="javascript:void(0)" id="notificationLink">
            <i class="glyphicon glyphicon-globe" id="notificationOpen" style="position: relative; top: 21px; color: rgb(255, 248, 248); font-size: 20px; left: -18px;">
            </i>
            </a>
            <div id="notificationContainer" style="display: none;z-index:10000">
            <div id="notificationTitle">Notifications</div>
            <div id="notificationsBody" class="notifications">
            <ul class="dropdown-menu-list scroller" style="height: 250px; overflow-y: scroll; margin: 0px; padding: 0px;" data-handle-color="#637283">
                   <?php
				   if(!empty($notiresult)){
					   foreach($notiresult as $notiresults){
						   $date = new DateTime($notiresults->date);
							$now = new DateTime();
							
							$curdate = date('Y-m-d',strtotime($notiresults->date));
							$curnow = date('Y-m-d');
							$status = $notiresults->status;
							if($status == 0){
								$class= "colorchange";
								}else{
									$class= "";}

							if($curdate == $curnow){
								$datetime = $date->diff($now)->format("%h hours");
							}
							else{
								$datetime = $date->diff($now)->format("%d days");
								}
				   
				   
				   ?>
                   
                    <li class="<?php echo $class; ?>" onClick="window.location.href='<?= base_url('merchant'); ?>/notifications'">
                        <a href="<?= base_url('merchant'); ?>/notifications">
                        <span class="time"><?php echo $datetime; ?></span>
                        <span class="details">
                        <span class="label label-sm label-icon label-success">
                        <i class="fa fa-plus"></i>
                        </span><?php echo $notiresults->message; ?></span>
                        </a>
                    </li>
                   <?php }} ?>
                    
                    </ul>
            
            
            
            </div>
            <div id="notificationFooter" onClick="window.location.href='<?= base_url('merchant'); ?>/notifications'"><a href="<?= base_url('merchant'); ?>/notifications">See All</a></div>
            </div>
         </li>
          
          <!-- BEGIN USER LOGIN DROPDOWN -->
          
          <li class="dropdown dropdown-user dropdown-dark"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <img alt="" class="img-circle" src="<?php echo base_url();?>assets/img/avatar.png"> <span class="username username-hide-mobile"><?php echo $full_name; ?></span> </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li> <a href="<?php echo base_url()?>merchant/profile"> <i class="icon-user"></i> My Profile </a> </li>
              <li> <a href="<?php echo base_url()?>merchant/logout"> <i class="icon-key"></i> Log Out </a> </li>
            </ul>
          </li>
          
          <!-- END USER LOGIN DROPDOWN --> 
          
          <!-- BEGIN QUICK SIDEBAR TOGGLER -->
          
          <li class="dropdown dropdown-extended quick-sidebar-toggler"> <span class="sr-only">Toggle Quick Sidebar</span> <i class="icon-logout"></i> </li>
          
          <!-- END QUICK SIDEBAR TOGGLER -->
          
        </ul>
      </div>
      
      <!-- END TOP NAVIGATION MENU --> 
      
    </div>
  </div>
  
  <!-- END HEADER TOP --> 
  
  <!-- BEGIN HEADER MENU -->
  
  <div class="page-header-menu">
    <div class="container-fluid"> 
      
      <!-- BEGIN MEGA MENU --> 
      
      <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background --> 
      
      <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
      
      <div class="hor-menu  ">
        <ul class="nav navbar-nav">
          <li class="menu-dropdown classic-menu-dropdown active"> <a href="<?php echo base_url('merchant/dashboard'); ?>"> Dashboard <span class="arrow"></span> </a> 
           
            
          </li>
          <?php if($approved == 1) { ?>
          <li class="menu-dropdown mega-menu-dropdown  "> <a href="javascript:void(0);"> Products</a>
            <ul class="dropdown-menu" style="min-width:185px;" >
              <li>
                <div class="mega-menu-content">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="mega-menu-submenu">
                        <li> <a href="<?php echo base_url('merchant/product'); ?>"> Add bulk Products </a> </li>
                        <li> <a href="<?php echo base_url('merchant/product/add'); ?>"> Add Products </a> </li>
                        <li> <a href="<?php echo base_url('merchant/product-list'); ?>"> List Products </a> </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown mega-menu-dropdown  "> <a href="<?php echo base_url('merchant/order'); ?>"> Orders</a> </li>
          <li class="menu-dropdown mega-menu-dropdown  "> <a href="<?php echo base_url('merchant/Orderlist'); ?>"> Report</a> </li>
          <li class="menu-dropdown mega-menu-dropdown  "> <a href="<?php echo base_url('merchant/invoice'); ?>"> Invoice</a> </li>
          <?php } ?>
        </ul>
      </div>
      
      <!-- END MEGA MENU --> 
      
    </div>
  </div>
  
  <!-- END HEADER MENU --> 
  
</div>

<!-- END HEADER -->
<script type="text/javascript" >
$(document).ready(function()
{
	var current = location.pathname;
$(".menu-dropdown" ).removeClass('active');
   $('.nav li a').each(function(){
       var $this = $(this);
       // if the current path is like this link, make it active
       if($this.attr('href').indexOf(current) !== -1){
		   $this.closest("li" ).addClass('active');
           $this.closest(".mega-menu-dropdown" ).addClass('active');
       }
   });
	
	
$("#notificationLink").click(function()
{
$("#notificationContainer").fadeToggle(300);
$("#notification_count").fadeOut("slow");
return false;
});

//Document Click
$(document).click(function()
{
$("#notificationContainer").hide();
});
//Popup Click
$("#notificationContainer").click(function()
{
return false
});

$('.glyphicon.glyphicon-globe').click(function(){
	
			
			$.ajax({
            type: "POST",
            data: {status:1},
            url: "",
            success: function(data){
               

                }
            });
			
			});



});
</script> 
<style>
#notification_li {
    background: transparent none repeat scroll 0 0;
    border: medium none;
}
#notificationLink:hover{
	background: transparent;
	
	
	}
	#notificationLink:focus,#notificationLink:hover,#notificationLink:active{
		background: transparent;
	
	
	}
		
		
		
#notification_li > a {
    margin-left: 0;
    top: -17px;
    width: 46px;
}
#notification_li {
    top: 6px;
}
#notificationContainer {
background-color: #fff;
border: 1px solid rgba(100, 100, 100, .4);
-webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
overflow: visible;
position: absolute;
top: 44px;
margin-left: -170px;
width: 400px;
z-index: -1;
display: none;
}
#notificationContainer:before {
content: '';
display: block;
position: absolute;
width: 0;
height: 0;
color: transparent;
border: 10px solid black;
border-color: transparent transparent white;
margin-top: -20px;
margin-left: 170px;
}
#notificationTitle {
z-index: 1000;
font-weight: bold;
padding: 8px;
font-size: 13px;
background-color: #ffffff;
width: 384px;
border-bottom: 1px solid #dddddd;
}
#notificationsBody {

min-height:300px;
}
#notificationFooter {
background-color: #e9eaed;
text-align: center;
font-weight: bold;
padding: 8px;
font-size: 12px;
border-top: 1px solid #dddddd;
}
#notification_count {
    background: #cc0000 none repeat scroll 0 0;
    border-radius: 9px !important;
    color: #ffffff;
    font-size: 11px;
    font-weight: bold;
    padding: 3px 7px;
    position: absolute;
}
.time {
    float: right;
}
.dropdown-menu-list.scroller > li {
    border-bottom: 1px solid #ebdddd;
    list-style: outside none none;
    padding: 15px;
    width: 100%;
}
</style>

</body>
