<style>
.nav-pills, .nav-tabs {
    margin-bottom: 0px;
}
.badge.badge-default {
    background-color: #f36a5a;
}
</style>
<?php $class = array('class'=>'col-md-3 control-label');?>
 
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Order Details
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container-fluid">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-social-dribbble font-purple-soft"></i>
                                                <span class="caption-subject font-purple-soft bold uppercase">Orders</span>
                                            </div>
                                            
                                        </div>
                                        <div class="portlet-body">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_1_1" data-toggle="tab">
													<span class="badge badge-default"><?php echo $count_new_order; ?></span> New Order </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_2" data-toggle="tab"> Approved </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_5" data-toggle="tab"> shipped </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_4" data-toggle="tab"> Delivered </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_3" data-toggle="tab"> Canceled </a>
                                                </li>
                                                 
                                                
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane fade active in" id="tab_1_1">
                                                <div style="margin-top:20px;"></div>
                                                <div class="msg"></div>
                                                <div class="errmsg"></div>
                                     <table class="table table-striped table-bordered table-hover text-center" id="orderlist" >
								          <thead>
								            <tr>
                                              <th class="text-center">Sl.no</th>
                                              <th class="text-center">Order ID</th>
								              <th class="text-center">Product ID</th>
                                              <th class="text-center">date</th>
								              <th class="text-center">Product Name</th>
								              <th class="text-center">Quantity</th>
								              <th class="text-center">Price</th>
	                                          <th class="text-center">O_status</th>
                                              <th class="text-center">P_status</th>
								              <th class="text-center">Action</th>
                                              <th class="text-center">View</th>
								            </tr>
								          </thead>
								          <tbody>
								            <?php $i=1; if(!empty($order_list)){foreach($order_list as $list){
												      //print_r($order_list);
											      ?>
								            <tr>
                                              <td><?= $i ?></td>
                                              <td><?= $list->order_id ?></td>
								                              <td><?= $list->product_id ?></td>
                                              <td><?= $list->date_added ?></td> 
                								              <td><?= $list->name ?></td>
                								              <td><?= $list->quantity ?></td>
                								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
                								              <td><?= $list->status ?></td>
                                                              <td><?= $list->p_status ?></td>
                                              <td>
                                              <div style="margin-top: 10px;">
                                              <input type="hidden" value="<?= $list->order_id ?>" id="hid" />
                                              <input type="hidden" value="<?= $list->product_id ?>" id="pid" />
                                              <input type="hidden" value="<?= $list->order_item_id ?>" id="otid" />
                                              <input type="hidden" value="<?= $list->suborder_id ?>" id="soid" />
                                              <input type="hidden" value="<?= $list->date_added ?>" id="order_created" />
                                              <input type="hidden" value="<?= $list->shipping_firstname.' '.$list->shipping_lastname ?>" id="shipping_name" />
                                              <input type="hidden" value="<?= $list->shipping_address ?>" id="shipping_address" /> 
                                              <input type="hidden" value="<?= $list->shipping_kecamatan ?>" id="shipping_kecamatan" />
                                              <input type="hidden" value="<?= $list->shipping_city ?>" id="shipping_city" />
                                              <input type="hidden" value="<?= $list->shipping_postcode ?>" id="shipping_postcode" />

                                              <input type="hidden" value="<?= $list->quantity ?>" id="prodQty" />
                                              <input type="hidden" value="<?= $list->price ?>" id="prodPrice" /> 
                                              <input type="hidden" value="<?= $list->subtotal ?>" id="prodAmt" />
                                              <input type="hidden" value="<?= $product->sku ?>" id="sku" />

                                              <input type="hidden" value="<?= $store ?>" id="store" />

                                                <select class="form-control dropdown drop" id="">
                                                <option value="">select</option>
                                                <option value="1">Approved</option>
                                                <option value="2">Cancelled</option>
                                                
                                                </select> 
                                              </div>
                                              
                                              </td>
                                              <td><a href="order/details/<?php echo $list->order_id; ?>/<?= $list->product_id ?>" title="Order Details">
														<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
											  </a></td>
								            </tr>
								            <?php $i++;}}?>
								          </tbody>
								        </table>
                                                </div>
                                                <div class="tab-pane fade" id="tab_1_2">
                                                    <div style="margin-top:20px;"></div>
                                                    <div class="sucmsg"></div>
                                                    <table class="table table-striped table-bordered table-hover text-center" id="activeorder" >
								          <thead>
								            <tr>
                                              <th class="text-center">Sl.no.</th>
                                              <th class="text-center">Order ID</th>
								              <th class="text-center">Product ID</th>
                                              <th class="text-center">date</th>
								              <th class="text-center">Product Name</th>
								              <th class="text-center">Quantity</th>
								              <th class="text-center">Price</th>
                                              <th class="text-center">O_status</th>
								              <th class="text-center">M_Status</th>
                                              <th class="text-center">View</th>
								            </tr>
								          </thead>
								          <tbody>
								            <?php $i=1;if(!empty($approved_list)){foreach($approved_list as $list){
												
											?>
								            <tr>
                                             <td><?= $i ?></td>
                                              <td><?= $list->order_id ?></td>
								              <td><?= $list->product_id ?></td>
                                              <td><?= $list->date_added ?></td>
								              <td><?= $list->name ?></td>
								              <td><?= $list->quantity ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
								              <td>
                                              <?= $list->status ?>
                                              </td>
								             
								              <td>
                                              <?php if($list->order_status == 1)
											  { 
											  $astatus = "Approved by merchant";
											  } elseif($list->order_status == 3)
											  { $astatus = "Approved by admin";}
											  ?>
                                              <?= $astatus; ?>
                                              </td>
                                              <td><a href="order/details/<?php echo $list->order_id; ?>/<?= $list->product_id ?>" title="Order Details">
														<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
											  </a></td>
								            </tr>
								            <?php $i++;}}?>
								          </tbody>
								        </table>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="tab_1_3">
                                                      <div style="margin-top:20px;"></div>
                                                      <div class="errmsg"></div>
                                                    <table class="table table-striped table-bordered table-hover text-center" id="cancelorder" >
                                                      <thead>
                                                        <tr>
                                                          <th class="text-center">Order ID</th>
                                                          <th class="text-center">Product ID</th>
                                                          <th class="text-center">date</th>
                                                          <th class="text-center">Product Name</th>
                                                          <th class="text-center">Quantity</th>
                                                          <th class="text-center">Price</th>
                                                          <th class="text-center">O_status</th>
                                                          <th class="text-center">Status</th>
                                                          <th class="text-center">View</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
								            <?php if(!empty($cancelled_list)){foreach($cancelled_list as $list){
											
											 ?>
								            <tr>
                                              <td><?= $list->order_id ?></td>
								              <td><?= $list->product_id ?></td>
                                              <td><?= $list->date_added ?></td>
								              <td><?= $list->name ?></td>
								              <td><?= $list->quantity ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
								              <td><?= $list->status ?></td>
                                              <td>
                                              <?php if($list->order_status == 2)
											  { 
											  $cstatus = "Cancelled by merchant";
											  } elseif($list->order_status == 4)
											  { $cstatus = "Cancelled by admin";}
											  ?>
                                              <?= $cstatus ?>
                                              </td>
                                              <td><a href="order/details/<?php echo $list->order_id; ?>/<?= $list->product_id ?>" title="Order Details">
														<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
											  </a></td>
								            </tr>
								            <?php }}?>
								          </tbody>
								        </table>
                                                </div>
                                                
                                                
                                                <div class="tab-pane fade" id="tab_1_4">
                                                    <div style="margin-top:20px;"></div>
                                                    <div class="sucmsg"></div>
                                                    <table class="table table-striped table-bordered table-hover text-center" id="delivered" >
								          <thead>
								            <tr>
                                               <th class="text-center">Sl.no</th>
                                              <th class="text-center">Order ID</th>
								              <th class="text-center">Product ID</th>
                                              <th class="text-center">date</th>
								              <th class="text-center">Product Name</th>
								              <th class="text-center">Quantity</th>
								              <th class="text-center">Price</th>
                                              <th class="text-center">O_status</th>
                                              <th class="text-center">View</th>
								            </tr>
								          </thead>
								          <tbody>
								            <?php $i=1;if(!empty($delivered_list)){foreach($delivered_list as $list){
												
											?>
								            <tr>
                                              <td><?= $i ?></td>
                                              <td><?= $list->order_id ?></td>
								              <td><?= $list->product_id ?></td>
                                              <td><?= $list->date_added ?></td>
								              <td><?= $list->name ?></td>
								              <td><?= $list->quantity ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
								              <td>
                                              <?= $list->status ?>
                                              </td>
                                              <td><a href="order/details/<?php echo $list->order_id; ?>/<?= $list->product_id ?>" title="Order Details">
														<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
											  </a></td>
								            </tr>
								            <?php $i++;}}?>
								          </tbody>
								        </table>
                                                </div>
                                                
                                                <div class="tab-pane fade" id="tab_1_5">
                                                    <div style="margin-top:20px;"></div>
                                                    <div class="sucmsg"></div>
                                                    <table class="table table-striped table-bordered table-hover text-center" id="shipped" >
								          <thead>
								            <tr>
                                              <th class="text-center">Sl.no</th>
                                              <th class="text-center">Order ID</th>
								              <th class="text-center">Product ID</th>
                                              <th class="text-center">date</th>
								              <th class="text-center">Product Name</th>
								              <th class="text-center">Quantity</th>
								              <th class="text-center">Price</th>
                                              <th class="text-center">O_status</th>
                                              <th class="text-center">View</th>
								            </tr>
								          </thead>
								          <tbody>
								            <?php $i=1; if(!empty($shipped)){foreach($shipped as $list){
												
											?>
								            <tr>
                                              <td><?= $i ?></td>
                                              <td><?= $list->order_id ?></td>
								              <td><?= $list->product_id ?></td>
                                              <td><?= $list->date_added ?></td>
								              <td><?= $list->name ?></td>
								              <td><?= $list->quantity ?></td>
								              <td><?= strtoupper($this->config->item('currency')).' '.number_format($list->price) ?></td>
								              <td>
                                              <?= $list->status ?>
                                              </td>
								             
                                              <td><a href="order/details/<?php echo $list->order_id; ?>" title="Order Details">
														<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
											  </a></td>
								            </tr>
								            <?php $i++;}}?>
								          </tbody>
								        </table>
                                                </div>
                                                
                                            </div>
                                            <div class="clearfix margin-bottom-20"> </div>
                                            
                                        </div>
                                    </div>
                                     
                                </div>
                                
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->
<script type="text/javascript" src="<?=base_url();?>assets/merchant/js/custom_dashboard.js"></script>