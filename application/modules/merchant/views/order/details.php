<link rel="stylesheet" type="text/css" href="http://staging.ipengen.com/assets/css/style.css">
<div class="page-container">
<div class="page-content-wrapper">
<div class="page-head">
      <div class="container"> 
        
        <!-- BEGIN PAGE TITLE -->
        
        <div class="page-title">
          <h1>Order Details </h1>
        </div>
      </div>
    </div>
    
            <div class="page-content">
             <div class="container">
             <div class="page-content-inner">
                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">

                        </div>
                        <div class="ibox-content">
                        
                        <?php  $tprice=0;
						if(!empty($order_items_list)){
							foreach($order_items_list as $order){
							//$tprice=$order->sale_price*$order->quantity;
							?>
                        <div class="row">
                        <div class="col-md-2">
                        <?php 
						//echo $this->config->item('image_path').'/product/'.$order->product_id."/450X450/".$order->product_image;
							//if(is_file($this->config->item('image_path').'/product/'.$order->product_id."/450X450/".$order->product_image))
							//{ $imageURL = base_url()."photo/product/".$order->product_id."/450X450/".$order->product_image; } 
							//else
							//{$imageURL = base_url('photo/product/no_product.jpg');}
							$imageURL = base_url('photo/product/no_product.jpg');
						?> 
                        <img src="<?php echo $imageURL; ?>"  class="img-responsive" width="100px" height="100px"/>
                        </div>
                        <div class="col-md-5 textbold">
                        <div><?php echo $order->item_description;?></div>
                        <?php if($order->item_type!='cash_gift'){?>
                        <div>Quantity : <?php echo $order->quantity;?></div>
                        <?php }?>
                        <div>
                        <?php if($order->item_type=='contributed_cash_gift'){?>
                        Contributed amount for this product (<?php echo strtoupper($this->config->item('currency')).' '.number_format($order->price);?>) :
                        <?php }else{?>
                        Price : 
                        <?php }?>
                         <?php
						 switch($order->item_type){
							 case 'contributed_cash_gift':
							            $prce= $order->contribute_amt; 
										break;                  
							 
							 case 'cash_gift':
							 $prce= $order->cash_amt+$order->transaction_amt;
							 break;
							 case 'buy_gift':$prce= $order->price;
							 break;
						 }
						  echo $prce;
						  
						  ?>
                         
                         
                         
                         </div>
                         <?php 
						 if($order->order_status == 1){ $astatus = 'Approved by merchant';}
						 elseif($order->order_status == 2){ $astatus = 'Cancell by merchant';}
						 elseif($order->order_status == 3){ $astatus = 'Approved by admin';}
						 elseif($order->order_status == 4){ $astatus = 'Cancell by admin';}
						 else {$astatus = 'process';}
						 ?>   
                         <div><strong>Status : </strong><span class="ostatus" style="color:#f36a5a;"><?php echo $astatus;?></span></div>
                         <div><strong>Awb id : </strong><span class="ostatus" style="color:#f36a5a;"><?php echo $order->awb_no;?></span></div>

                        </div>
                        
                        <div class="col-md-3 text-center textbold"><?php echo strtoupper($this->config->item('currency')).' '.number_format($order->subtotal); ?></div>
                        
                        <div class="col-md-2 viewstatus">
                        <div class="statuschange"> <?php echo $order->p_status; ?></div>
                        </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row billingad">
                        <div class="col-md-3 address">
                        <div class="billadd"> Billing Address </div>
                        <?php 
								if($order->wishlist_id == 0)
								{
									$address = "";
									if($order->shipping_firstname != "" && $order->shipping_lastname != "")
									{$address .= ucfirst($order->shipping_firstname)." ".ucfirst($order->shipping_lastname);}
									if($order->shipping_address != "")
									{$address .= "<br>".$order->shipping_address;}
									if($order->shipping_city != "")
									{$address .= ",".$order->shipping_city;}
									if($order->shipping_kecamatan != "")
									{$address .= ",".$order->shipping_kecamatan;}
									if($order->shipping_postcode != "")
									{$address .= ",".$order->shipping_postcode;}
									if($order->shipping_contact != "")
									{$address .= "<br> T - ".$order->shipping_contact;}
									echo $address;
								}
                            ?>                       
                        </div>
                        <div class="col-md-3 address">
                        <div class="billadd"> Shipping Address </div>
                        <?php 
								if($order->wishlist_id == 0)
								{
									$address = "";
									if($order->shipping_firstname != "" && $order->shipping_lastname != "")
									{$address .= ucfirst($order->shipping_firstname)." ".ucfirst($order->shipping_lastname);}
									if($order->shipping_address != "")
									{$address .= "<br>".$order->shipping_address;}
									if($order->shipping_city != "")
									{$address .= ",".$order->shipping_city;}
									if($order->shipping_kecamatan != "")
									{$address .= ",".$order->shipping_kecamatan;}
									if($order->shipping_postcode != "")
									{$address .= ",".$order->shipping_postcode;}
									if($order->shipping_contact != "")
									{$address .= "<br> T - ".$order->shipping_contact;}
									echo $address;
								}
                            ?>                        </div>
                        
                        </div>

                        <?php } ?>
                         
                      
                       <div class="alert alert-success" id="statusmsg" style="display:none;"></div>
                       
                         <input type="hidden" value="<?php echo isset($order_items_list[0]->total) ? $order_items_list[0]->order_id : '0' ?>" name="orderid" id="oID" />
                        
                                
                        <div class="clearfix"></div>
                      
                        <div class="hr-line-dashed"></div><!--<h4>Order Transaction Status</h4>
                       <div class="hr-line-dashed"></div>-->
                       
                            <div class="alert alert-success" id="succmsg" style="display:none;">
                           
                           
                            </div>
                        <!--<form class="form-horizontal" id="orderst">
                                <div class="form-group"><label class="col-sm-2 control-label">Remarks </label>
                                         <input type="hidden" value="<?php echo $order_items_list[0]->order_id?>" name="orderid" />
                                         <input type="hidden" value="<?php echo $order_items_list[0]->id?>" name="userid" />
                                    <div class="col-sm-10"><input type="text" name="remark" class="form-control" value="" required="required" /></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>

                                    <div class="col-sm-10"><select name="status" id="changestatus" class="form-control" required >
                                    <option <?php if($order_items_list[0]->stuts=="processing"){echo 'selected="selected"';} ?> value="processing">Processing</option>
                                    <option <?php if($order_items_list[0]->stuts=="approved"){echo 'selected="selected"';} ?> value="approved">Approved</option>
                                    <option <?php if($order_items_list[0]->stuts=="ready to ship"){echo 'selected="selected"';} ?> value="ready to ship">Ready to Ship</option>
                                    <option <?php if($order_items_list[0]->stuts=="shipping"){echo 'selected="selected"';} ?> value="shipping">Shipping</option>
                                    <option <?php if($order_items_list[0]->stuts=="shipped"){echo 'selected="selected"';} ?> value="shipped">Shipped</option>
                                    <option <?php if($order_items_list[0]->stuts=="canceled"){echo 'selected="selected"';} ?> value="canceled">canceled</option>
                                    <option <?php if($order_items_list[0]->stuts=="failed"){echo 'selected="selected"';} ?> value="failed">Failed payment</option>
                                    </select></div>
                                </div>
                                 <div class="hr-line-dashed"></div>
 
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        
                                        <button type="button" id="changeBtn" class="btn btn-primary">Change status</button>
                                    </div>
                                </div>
                            </form>-->

                            <h4>Product Transaction Status History</h4>
                            
                            <div class="hr-line-dashed"></div>
                            <?php if(!empty($orderdetailshistory)){?>
                            <div class="row">
                            <div class="col-md-4"><b>Date</b></div>
                             <div class="col-md-4"><b>Remark</b></div>
                              <div class="col-md-4"><b>Status</b></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <?php foreach($orderdetailshistory as $Hval){?>
                            
                            <div class="row">
                            <div class="col-md-4"><?php echo $Hval->orderchange_date?></div>
                             <div class="col-md-4"><?php echo $Hval->remark?></div>
                              <div class="col-md-4"><?php echo $Hval->status?></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <?php }}
							else{
								echo "No result found.";	
							}
							
							}else{
								echo "No result found.";	
							}?>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>


<style>
.col-md-2.viewstatus {
    border-left: 1px solid #bbb;
    min-height: 170px;
}
.billadd {
    font-weight: bold;
}
.col-md-3.address {
    border-right: 1px dotted;
}
.textbold {
    font-size: 18px;
}
</style>