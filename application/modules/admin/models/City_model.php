<?php
/**
* 
*/
class City_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function add_city($data)
	{
		$this->db->insert("ig_city",$data);
		return $this->db->affected_rows();
	}
	function get_all_city()
	{
		$query = $this->db->get("ig_city");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function city_data_for_edit($cityCode)
	{
		$query = $this->db->get_where("ig_city",array("city_id"=>$cityCode));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function update_city($data,$cityCode)
	{
		$this->db->update("ig_city",$data,array("city_id"=>$cityCode));
		return $this->db->affected_rows();
	}
	function deleteCityById($cityCode)
	{
		$this->db->delete("ig_city",array("city_id"=>$cityCode));
		return $this->db->affected_rows();
	}
	function adminStatusChange($cityCode, $value)
	{
		$this->db->update("ig_city",array("status"=>$value),array("city_id"=>$cityCode));
		return $this->db->affected_rows();
	}
}
?>