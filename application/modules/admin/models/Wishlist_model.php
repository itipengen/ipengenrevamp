<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wishlist_model extends CI_Model
{
	function __construct()
		 { parent::__construct();
		   $this->load->database();
		   $this->load->model('user_model');
		 }
	private function _get_userFullName_By_email($data)
	{
		foreach($data as $row)
		{
			$fullName = ucfirst($row->fname)." ".ucfirst($row->lname);
		}
		return $fullName;
	} 
	public function wishlistaddress($data)
	{  
		$this->db->insert('ig_address_book',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function getpassword($id)
	{  
		$this->db->select('wishlist_password');
		$this->db->from('ig_wishlist');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function sendmailurl($id)
	{  
		$this->db->select('url');
		$this->db->from('ig_wishlist');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	public function insertwishlist($data)
	{  
		$this->db->insert('ig_wishlist',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	public function editwishlist($data,$id)
	{  
		
		$this->db->where('id',$id);
		$this->db->update('ig_wishlist',$data);
		return true; 
	}
	/*public function checkContributionByProductId($id)
	{  

		$this->db->select('*,SUM(total_amt_received) as total'); 
		$this->db->from('ig_contribution');
		$this->db->where('product_id',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}*/
	public function checkContributionByWishlistId($product_id,$id)
	{  
		$this->db->select('*'); 
		$this->db->from('ig_contribution');
		$this->db->where(array('product_id' => $product_id,'wishlist_id'=>$id));
		$query = $this->db->get();
		if($query->num_rows() > 0 ){
			$this->db->select('SUM(total_amt_received) as total'); 
		    $this->db->from('ig_contribution');
		    $this->db->where(array('product_id' => $product_id,'wishlist_id'=>$id));
		    $q = $this->db->get();
			$res = $q->result();
			$total_amt_recvd = $res[0]->total;
			foreach($query->result() as $r){
			     $r->total = $total_amt_recvd;
				 $result[] = $r;	
			}			
		}else{
		     return array();	
		}
		return $result;
		
	}
	public function getContributionUser($id,$wid)
	{  

		$this->db->select('*');
		$this->db->from('ig_user');
		$this->db->join(' ig_contribution', 'ig_user.id = ig_contribution.user_id');
		//$this->db->where('ig_contribution.product_id',$id);
		$this->db->where(array('ig_contribution.wishlist_id'=>$wid,'ig_contribution.product_id'=>$id));
		$this->db->order_by("ig_contribution.contributor_id","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	
	
	public function getContributionUsercount($pid,$wshid,$type)
	{  
			$cart=array();
			 $uid=0;
			if(!empty($_SESSION['log_in'])){
          $uid = $this->session->userdata['log_in']['user_id'];
			}
		  if(!empty($_SESSION['cart_contents'])){
		  $cart = $this->session->userdata['cart_contents'];
		  }
		  $k=0;
		  if( !empty($cart) ){
			   foreach($cart as $val){
				   if(is_array($val)){
					   
					 if(($val['id']==$pid) && ($val['wishlist_id']==$wshid) &&(($val['type']==$type)||($val['type']==$type))){
						 $k= 1;
						 break;
					   }
				   }
				   
			   }
			   return $k;
			  
		  }else{
				$this->db->select('*');
				$this->db->from('ig_order_items');
				
				$this->db->where(array('wishlist_id'=>$wshid,'user_id'=>$uid,'product_id'=>$pid,'item_type'=>$type));
				$query = $this->db->get();
				$result = $count = $query->num_rows();
				return $result;
		  }
		
	}
	public function shippingddress($data)
	{  
		$this->db->insert('ig_wishlist_shipping',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	public function editshippingddress($data,$id)
	{  
	     $this->db->where('wid',$id);
		$this->db->update('ig_wishlist_shipping',$data);
		//return $id;
	}
	
			
	public function get_dropdown_list($uid)
	{ 
	    $result = array();
	    $this->db->select('*'); 
		$this->db->from('ig_wishlist');
		$this->db->where('uid',$uid);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
		
	public function get_my_wishlist($uid,$limit,$offset)
	{ 
	   
		$this->db->select('ig_wishlist.*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wishlist_shipping', 'ig_wishlist.id =  ig_wishlist_shipping.wid');
		$this->db->limit($limit,$offset);
		$this->db->where('ig_wishlist.uid',$uid);
		$this->db->order_by("ig_wishlist.id","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
		
	}
	public function get_my_wishlist5($uid)
	{ 
	   
		$this->db->select('ig_wishlist.*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wishlist_shipping', 'ig_wishlist.id =  ig_wishlist_shipping.wid');
		$this->db->limit(5,0);
		$this->db->where('ig_wishlist.uid',$uid);
		$this->db->order_by("ig_wishlist.id","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
		
	}
	public function row_count($uid)
	{
			$this->db->select('*');
			$this->db->from('ig_wishlist');
			$this->db->where('uid',$uid);
			$result = $this->db->get()->num_rows();
			
			return $result;
	}
	public function totalWishlistCount($uid)
	{
			$this->db->select('*');
			$this->db->from('ig_wishlist');
			$this->db->where('uid',$uid);
			$result = $this->db->get()->num_rows();
			return $result;
			
	}
	 public function getimageid($id)
	{ 
	   
		$this->db->select('wishlist_image');
		$this->db->from('ig_wishlist');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
		
	}
	
	 public function getCashGiftTransactionFee()
	{
		$this->db->select('transaction_fees');
		$this->db->from('ig_transaction_fees');
		$this->db->where('gift_type','cash_gift');
		return $this->db->get();
	}
	public function getBuyGiftTransactionFee()
	{
		$this->db->select('transaction_fees');
		$this->db->from('ig_transaction_fees');
		$this->db->where('gift_type','buy_gift');
		return $this->db->get();
	}
	public function getaddress($addname)
	{ 
	   
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wishlist_shipping', 'ig_wishlist.id =  ig_wishlist_shipping.wid');
		$this->db->where('ig_wishlist.title',$addname);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
		
	}
	
	public function checkContribution($wid,$uid){
		
		$this->db->select('count(order_item_id)');
		$this->db->from('ig_order_items');
		$this->db->where('wishlist_id',$wid);
		$this->db->where('user_id',$uid);
		$this->db->where('item_type','contributed_cash_gift');
		$query = $this->db->get();

		$result = $query->result();
		return $result;
		
	}
	public function getAllcategory()
	{ 
	    $lanuagearray=$this->session->userdata('site_lang');
			if(!empty($lanuagearray)){
				$language_id=$lanuagearray['laguage_id'];
					$language_name=$lanuagearray['laguage_name'];
				}else{
			$language_id=1;
		}

	
	    $this->db->select('ig_event_category_text.event_name as name');
		$this->db->from('ig_event_category');
		$this->db->join('ig_event_category_text', 'ig_event_category.id = ig_event_category_text.event_category_id');
		//$this->db->where('lang_id',$language_id);
		$this->db->where(array('ig_event_category_text.lang_id'=>$language_id,'ig_event_category.status'=>1));
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	   
		//$query = $this->db->query('SELECT name FROM ig_event_category');
       // return $query->result();
		
		
	}
	public function getAllmaincategory()
	{ 
	   
		$query = $this->db->query('SELECT name FROM ig_category_details');
        return $query->result();
		
		
	}
	public function getid($id)
	{
		
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('uid',$id);
		$query = $this->db->get();
		$count = $query->num_rows();
		if($count!=0){
		  return true;
		} else {
			return false;
		}
		
		
	}
	public function checkUrl($name,$id)
	{
		
		$this->db->select('url');
		$this->db->from('ig_wishlist');
		$this->db->where('url',$name);
		$query = $this->db->get();
		$count = $query->num_rows();
		if($count!=0){
		  return $count;
		} else {
			return $count;
		}
		
		
	}
	public function getwishlistByid($id)
	{
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}
	public function getwishlistByUrl($url)
	{
		$query = $this->db->get_where("ig_wishlist",array("url" => $url));
		//echo $this->db->last_query();
		//die();
		/*$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('url',$url);
		$query = $this->db->get();*/
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}else{
			return false;
		}
	}
	
	
	
	public function wishlistpersonalize($deta,$id)
	{
		$this->db->where('id',$id);
		$query = $this->db->update('ig_wishlist',$deta);
		echo $this->db->last_query();
		die();
		return true;
		
	}
	public function updateprivacy($deta,$id)
	{
		$this->db->where('id',$id);
		$query = $this->db->update('ig_wishlist',$deta);
		return true;
		
	}
	
	public function editwishlistpersonalize($deta,$id)
	{
		$this->db->where('id',$id);
		$query = $this->db->update('ig_wishlist',$deta);

		return true;
		
	}
	public function changeprivacy()
	{
		  $wishlistid= $this->input->post('wishlistid');
		  $is_public= $this->input->post('is_public');
		 
		$data=array('is_public'=>$is_public,
					'wishlist_password'=>$this->input->post('password')
		
		);
		$this->db->where(array('id'=>$wishlistid));
		$query=$this->db->update('ig_wishlist',$data);
		
		
		if($query){
		  return true;
		} else {
			return false;
		}
		
		
	}
	
	public function getWishlistShippingById($id){
		$this->db->select('*');
		$this->db->from(' ig_wishlist_shipping');
		$this->db->where('wid',$id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;	
	}
	
	public function getdetailwishlistByid($id){
		
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wishlist_shipping', 'ig_wishlist.id =  ig_wishlist_shipping.wid');
		$this->db->where('ig_wishlist.id',$id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function getUserDetailsById($id){
		
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->join('ig_wishlist_shipping', 'ig_wishlist.id =  ig_wishlist_shipping.wid');
		$this->db->where('ig_wishlist.id',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	public function getProduct($id,$Langid){
		//echo $id;
		$data=array();
		$this->db->select('ig_wishlist_product.*,ig_products.*,ig_products_text.*');
		$this->db->from('ig_wishlist_product');
		$this->db->join('ig_products', 'ig_wishlist_product.product_id = ig_products.product_id');
		$this->db->join('ig_products_text', 'ig_products.product_id = ig_products_text.product_id');
		$this->db->where('ig_wishlist_product.wishlist_id',$id);
		$this->db->where('ig_products_text.language_id',$Langid);
		$selectQuery = $this->db->get();
		//echo $this->db->last_query();die();
		if($selectQuery->num_rows() > 0){
			$queryData=$selectQuery->result();
			foreach($queryData as $value){
				$slug=$this->Product_model->getProductUrl_name($value->product_id,$value->product_name);
				$value->slug=$slug;
				//$value->contribution = $this->checkContributionByProductId($value->product_id);
				$value->contribution = $this->checkContributionByWishlistId($value->product_id, $value->wishlist_id);
				$value->user = $this->getContributionUser($value->product_id,$value->wishlist_id);
				$value->cnbtn_count = $this->getContributionUsercount($value->product_id,$id,'contributed_cash_gift');
				$value->gift_count = $this->getContributionUsercount($value->product_id,$id,'buy_gift');
				$data[]=$value;
				
			}
			//print_r($data);die();
			return $data;
		}
		else{
			return false;	
		}
		//die();
		//print_r($data);die();
		
	}
	
	
	
	public function addRecipentModel($uid,$wid,$wishUid){
		//echo $uid.$wid.$wishUid;
		//echo $id;die();
		$this->db->select('*');
		$this->db->from('ig_wishlist_recipient');
		$this->db->where('uuid',$uid);
		$this->db->where('wishlist_id',$wid);
		$this->db->where('wishlist_uid',$wishUid);
		$selectQuery = $this->db->get();
		//$result = $query->row();
		if($selectQuery->num_rows() < 1){
			$data = array(
			'uuid' => $uid,
			'wishlist_id' => $wid,
			'wishlist_uid' => $wishUid
			);
			$this->db->insert('ig_wishlist_recipient', $data);
			return ($this->db->affected_rows() != 1) ? "error" : "success";
			//return $result;
	    }
		else{
			return "error";
		}	
	}
	
	public function removeRecipentModel($uid,$wid,$wishUid){
		//echo $uid.$wid.$wishUid;
		//echo $id;die();
		$this->db->select('*');
		$this->db->from('ig_wishlist_recipient');
		$this->db->where('uuid',$uid);
		$this->db->where('wishlist_id',$wid);
		$this->db->where('wishlist_uid',$wishUid);
		$selectQuery = $this->db->get();
		//$result = $query->row();
		if($selectQuery->num_rows() > 0){
			$this -> db -> where('uuid', $uid);
			$this -> db -> where('wishlist_id', $wid);
			$this -> db -> where('wishlist_uid', $wishUid);
  			$this -> db -> delete('ig_wishlist_recipient');
			return ($this->db->affected_rows() != 1) ? "error" : "success";
			//return $result;
	    }
		else{
			return "error";
		}	
	}
	
	public function checkHasRecipent($uid,$wid,$wishUid){
	//echo $uid,$wid,$wishUid;die();
		$this->db->select('*');
		$this->db->from('ig_wishlist_recipient');
		$this->db->where('uuid',$uid);
		$this->db->where('wishlist_id',$wid);
		$this->db->where('wishlist_uid',$wishUid);
		$selectQuery = $this->db->get();
		if($selectQuery->num_rows() < 1){
			return '0';
		}
		else{
			return '1';
		}
	}
	
	public function getWishIdByUrl($segment_1){
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('url',$segment_1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->row();
			return $result;
		}
	}
	
	public function checkWishlistPass($wishPass,$id){
		//echo $id.$wishPass;die();
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('wishlist_password',$wishPass);
		$this->db->where('id',$id);
		$selectQuery = $this->db->get();
		//print_r($selectQuery);die();
		if($selectQuery->num_rows() > 0){
			return true;
		}
		else{
			return false;	
		}
	}
	public function checkWishlistPasslasturl($url){
		//echo $id.$wishPass;
		$this->db->select('*');
		$this->db->from('ig_wishlist');
		$this->db->where('url',$url);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}
	}
	
	
	public function recipient_list($uid){
		
		$this->db->select('ig_wishlist_recipient.*,ig_wishlist.title,ig_wishlist.category,ig_wishlist.url,ig_wishlist.e_startdate,ig_wishlist.e_enddate,ig_wishlist.wishlist_image,ig_user.fname,ig_user.lname');
		$this->db->from('ig_wishlist_recipient');
		$this->db->join('ig_wishlist', 'ig_wishlist.id = ig_wishlist_recipient.wishlist_id');
		$this->db->join('ig_user', 'ig_wishlist_recipient.wishlist_uid = ig_user.id');
		$this->db->where('ig_wishlist_recipient.uuid',$uid);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	public function delete_recipient($wid)
	{
		$this->db->where('id',$wid);
		$query = $this->db->delete('ig_wishlist_recipient');
		if($query){return TRUE;}
		else {return false;}

	}
	public function allWishlistId($like)
	{
		$this->db->select('ig_wishlist.id,ig_wishlist.title,ig_wishlist.url,ig_wishlist.wishlist_image as image, ig_user.fname, ig_user.lname');
	    $this->db->from('ig_wishlist');
		$this->db->join('ig_user', 'ig_wishlist.uid = ig_user.id');
		//$this->db->like('ig_wishlist.title',$like);
		$this->db->like('ig_user.email',$like);
		$this->db->or_like('ig_user.fname',$like);
		$this->db->or_like('ig_user.lname',$like);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $this->allwishlist($like);
		}

	}
	public function wishlist_notification($uid, $langId){
		$notify = array();
		$this->db->select('
							ig_notification.*,
							ig_user.id,
							ig_user.user_id,
							ig_user.fname,
							ig_user.lname,
							ig_wishlist.id as wshid,
							ig_wishlist.title,
							ig_wishlist.url
						');
	    $this->db->from('ig_notification');
		$this->db->join('ig_user', 'ig_notification.sender_uid = ig_user.id', 'left');
		$this->db->join('ig_wishlist', 'ig_notification.wishlistid = ig_wishlist.id', 'left');
		$this->db->where("(ig_notification.uuid = '$uid' OR ig_notification.sender_uid = '$uid') 
                   AND ig_notification.notification_status = 1");
		$this->db->order_by("ig_notification.notification_date","desc");
		 
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$i = 0;
			foreach($query->result() as $row)
			{
				$notify[$i] = (array)$row;
				$fetchId = ($row->product_id != 0) ? $row->product_id : $row->order_id;
				$notify[$i]["product_title"] = $this->_order_notification_product($fetchId,$langId);
				$notify[$i]["getuserprofileurl"] = base_url().'u/'.$row->user_id;
				$notify[$i]["receiver_name"] = $this->getUserFullName($row->uuid);
				$notify[$i]["receiver_url"] = $this->user_model->getuserUrl($row->uuid);
				$i++;
			}
			foreach($notify as $data)
			{
				$result[] = (object)$data;
			}
			//print_r($result);
			//die();
			return $result;
		}else{
			return $notify;
		}
		
	}
	public function get_user_noification($nid){
		$query = $this->db->get_where('ig_notification',array('notification_id'=>$nid));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function add_user_noification($data){
		
		$this->db->insert('ig_notification',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
		
	}
	public function update_user_noification($nid){
		$this->db->where(array('notification_id'=>$nid));
		$this->db->update('ig_notification',array('notification_status'=>0));
		return true;
	}
	public function getDetailWishlistAndProductByWishlistid($wishlistID , $productID, $langId)
	{
		$query =	$this->db->select("
								ig_wishlist.title as wishlistName,
								ig_wishlist.category as wishlistCategory,
								ig_wishlist.event_date as wishlistEvent_date,
								ig_wishlist.wishlist_address as wishlistAddress,
								ig_wishlist.url as wishlistURL,
								ig_products.product_id as product_id,
								ig_products.product_image as product_image,
								ig_products.sale_price as product_sale_price,
								ig_products.sku as product_sku,
								ig_products_text.product_name as product_name,
								
										")
							 ->from("ig_wishlist_product")
							 ->where(array(
							 				"ig_wishlist_product.wishlist_id" => $wishlistID , 
											"ig_wishlist_product.product_id" => $productID,
											"ig_products_text.language_id" => $langId,
											)
									)
							 ->join("ig_user" , "ig_user.id = ig_wishlist_product.uid")
							 ->join("ig_wishlist" , "ig_wishlist.id = ig_wishlist_product.wishlist_id")
							 ->join("ig_products" , "ig_products.product_id = ig_wishlist_product.product_id")
							 ->join("ig_products_text" , "ig_products_text.product_id = ig_wishlist_product.product_id")
							 ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function getUserdataUsingId($userId)
	{
		$query = $this->db->get_where("ig_user", array("ig_user.id" => $userId));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function receiverDataByEmailId($email)
	{
		$query = $this->db->get_where("ig_user", array("ig_user.email" => $email));
		return $query->num_rows() > 0 ? $this->_get_userFullName_By_email($query->result()) : $email;
	}
	public function getAllCashGift($wishlistid,$Langid)
	{
		$query = $this->db->select("
								ig_order_items.*, 
								ig_wishlist.title, 
								ig_wishlist.url, 
								ig_user.fname, 
								ig_user.lname,
								ig_user.user_id,
								ig_transaction.*
								  ")
						  ->from("ig_order_items")
						  ->join("ig_wishlist", "ig_wishlist.id = ig_order_items.wishlist_id")
						  ->join("ig_user", "ig_user.id = ig_order_items.user_id")
						  ->join("ig_transaction", "ig_transaction.order_id = ig_order_items.order_id")
						  ->where(array("ig_order_items.wishlist_id" => $wishlistid , "ig_order_items.item_type" => "cash_gift"))
						  ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	private function allwishlist($text)
	{
		$keyword = explode(" ",$text);
		$mainArr = array();
		if(!empty($keyword))
		{
				if($keyword[0] != "" )
				{
					$this->db->select('ig_wishlist.id,ig_wishlist.title,ig_wishlist.url,ig_wishlist.wishlist_image as image, ig_user.fname, ig_user.lname');
					$this->db->from('ig_wishlist');
					$this->db->join('ig_user', 'ig_wishlist.uid = ig_user.id');
					$this->db->like('ig_user.fname',$keyword[0]);
					$this->db->or_like('ig_user.lname',$keyword[0]);
					$query = $this->db->get();
					if($query->num_rows() > 0){
						$mainArr[] = $query->result();
					}else{
						$mainArr[] = array();
					}
					if(empty($mainArr[0]))
					{
						return $mainArr;
					}
				}
			return $mainArr;
		}
	}
	public function getwishlistProduct($wishlistId,$Langid,$shortVal)
	{
		if($shortVal == "")
		{ $val = "'ig_wishlist_product.id', 'asc'"; }
		elseif($shortVal == "latest")
		{ $val = "'ig_products.create_date', 'desc'"; }
		elseif($shortVal == "natz")
		{ $val = "'ig_products_text.product_name', 'asc'"; }
		elseif($shortVal == "nzta")
		{ $val = "'ig_products_text.product_name', 'desc'"; }
		elseif($shortVal == "phtl")
		{ $val = "'ig_products.sale_price', 'desc'"; }
		elseif($shortVal == "plth")
		{ $val = "'ig_products.sale_price', 'asc'"; }
		$data=array();
		$this->db->select('ig_wishlist_product.*,ig_products.*,ig_products_text.*');
		$this->db->from('ig_wishlist_product');
		$this->db->join('ig_products', 'ig_wishlist_product.product_id = ig_products.product_id');
		$this->db->join('ig_products_text', 'ig_products.product_id = ig_products_text.product_id');
		$this->db->where('ig_wishlist_product.wishlist_id',$wishlistId);
		$this->db->where('ig_products_text.language_id',$Langid);
		$this->db->order_by($val);
		$selectQuery = $this->db->get();
		//echo $this->db->last_query();die();
		if($selectQuery->num_rows() > 0){
			$queryData=$selectQuery->result();
			foreach($queryData as $value){
				$slug=$this->Product_model->getProductUrl_name($value->product_id,$value->product_name);
				$value->slug=$slug;
				$value->contribution = $this->checkContributionByWishlistId($value->product_id, $value->wishlist_id);
				$value->user = $this->getContributionUser($value->product_id,$wishlistId);
				$value->cnbtn_count = $this->getContributionUsercount($value->product_id,$wishlistId,'contributed_cash_gift');
				$value->gift_count = $this->getContributionUsercount($value->product_id,$wishlistId,'buy_gift');
				$data[]=$value;
				
			}
			//print_r($data);die();
			return $data;
		}
		else{
			return false;	
		}
		//die();
		//print_r($data);die();
		
	}
	
	private function _order_notification_product($fetchId = NULL, $langID = NULL)
	{
		$producturl = "";
		if($fetchId != NULL && $langID != NULL)
		{
			
			$query = $this->db->select("ig_products_text.*")
							  ->from("ig_notification")
							  ->where(array("ig_notification.order_id" => $fetchId, "ig_products_text.language_id" => $langID))
							  ->join("ig_order_items","ig_order_items.order_id = ig_notification.order_id")
							  ->join("ig_products_text","ig_products_text.product_id = ig_order_items.product_id")
							  ->get();
			$queryData = $query->result();
			if($query->num_rows() > 0)
			{
				$productId = $queryData[0]->product_id;
				$productName = $queryData[0]->product_name;
				$producturl = getproducturl($productId, $productName);
				return $producturl;
			}
			else
			{
				$dquery = $this->db->select("ig_products_text.*")
							  ->from("ig_notification")
							  ->where(array("ig_notification.product_id" => $fetchId, "ig_products_text.language_id" => $langID))
							  ->join("ig_products","ig_products.product_id = ig_notification.product_id")
							  ->join("ig_products_text","ig_products_text.product_id = ig_products.product_id")
							  ->get();
				$dqueryData = $dquery->result();
				if($dquery->num_rows() > 0)
				{
					$productId = $dqueryData[0]->product_id;
					$productName = $dqueryData[0]->product_name;
					$producturl = getproducturl($productId, $productName);
					return $producturl;
				}
				else
				{
					return $producturl;
				}
			}
		}
		else
		{
			return $producturl;;
		}
	}
	
	public function getUserFullName($uid){
		$result = $this->user_model->userdeatails($uid);
		foreach($result as $res){
			
			$name = ucwords($res->fname.' '.$res->lname);	
		}
		
		return $name;	
	}
	
}