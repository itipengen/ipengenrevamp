<?php
class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	

		public function getOrderDetails($oid)
		{
			$this->db->select('ig_order.*,ig_order.status as stuts,ig_order_items.*,ig_products.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id', 'left');
			$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id', 'left');
			//$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
			$this->db->where('ig_order.order_id',$oid);
			$query = $this->db->get();

			$result = $query->result();
			return $result;			
		}
		public function getOrderDetailsNew($oid,$pid)
		{
			$this->db->select('ig_order.*,ig_order.status as stuts,ig_order_items.*,ig_user.*,ig_marchent_login.marchent_name as merchant');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id', 'left');
			$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_order_items.product_id',$pid);
			$query = $this->db->get();
            //echo $this->db->last_query();
			$result = $query->result();
			return $result;			
		}
		
		
		public function change_status($orderid,$status,$remark){
			
			$this->db->select('status');
			$this->db->from('ig_order');
			$this->db->where(array('order_id'=>$orderid));
			$query = $this->db->get();
			$result = $query->row();
		
			if($status!=$result->status){			
			$this->db->where(array('order_id'=>$orderid));
			$this->db->update('ig_order',array('status'=>$status));
			
			$date=date_create(date('Y-m-d H:i:s', time()));
        $date_time= date_format($date,"Y/m/d H:i:s");
		$data=array(
		'order_id'=>$orderid,
		'status'=>$status,
		'remark'=>$remark,
		'orderchange_date'=>$date_time
		
		);
		$this->db->insert('ig_order_status_history',$data);
	
			}
		return true;	
		}
		
		public function change_productstatus($orderid,$status,$remark,$productid){
			
			$this->db->select('p_status');
			$this->db->from('ig_order_items');
			$this->db->where(array('order_id'=>$orderid));
			$query = $this->db->get();
			$result = $query->row();
		
			if($status!=$result->p_status){			
			$this->db->where(array('order_id'=>$orderid));
			$this->db->where(array('product_id'=>$productid));
			$this->db->update('ig_order_items',array('p_status'=>$status));
			
		$date=date_create(date('Y-m-d H:i:s', time()));
        $date_time= date_format($date,"Y/m/d H:i:s");
		$data=array(
		'order_id'=>$orderid,
		'order_product_id'=>$productid,
		'status'=>$status,
		'remark'=>$remark,
		'orderchange_date'=>$date_time
		
		);
		$this->db->insert('ig_order_status_history',$data);
	
			}
		return true;	
		}
		
		
		
		
	public function getOrderDetailshistory($orderid){
			$this->db->where(array('order_id'=>$orderid));
			$this->db->order_by("id","desc");
		    $query=	$this->db->get('ig_order_status_history');
			
			return $query->result();
		}
		
	public function getProductDetailshistory($orderid,$pid){
		$this->db->where(array('order_id'=>$orderid));
		$this->db->where(array('order_product_id'=>$pid));
		$this->db->order_by("id","desc");
		$query=	$this->db->get('ig_order_status_history');
		
		return $query->result();
	}

	function get_all_orders()
	{
		$query = $this->db->select("
							ig_order.order_id as orderID,
							ig_user.fname as client_fname,
							ig_user.lname as client_lname,
							ig_order.total as total_amount,
							ig_order.date_added as order_date,
							ig_transaction.transaction_status as transaction_status,
							ig_order.status as order_status,
									")
						  ->from("ig_order")
						  ->join("ig_transaction","ig_transaction.order_id = ig_order.order_id",'left')
						  ->join("ig_user","ig_user.id = ig_order.user_id",'left')
						   ->where(array("ig_order.language_id"=>"1"))
						  ->get();
		//echo $this->db->last_query();
		//die();
		return $query->num_rows() > 0 ? $query->result() : array();
		//print_r($query->result());
		//die();
	}
	
	public function getUserData($orderId = NULL)
	{
		$query =	$this->db->select("
								ig_order.order_id as order_id,
								ig_order.payment_firstname as fname,
								ig_order.payment_lastname as lname,
								ig_user.mobile as mobile,
								ig_user.email as userEmail,
								ig_order.payment_address_1 as address,
								ig_order.payment_address_2 as address_1,
								ig_order.payment_city as city,
								ig_order.payment_postcode as postCode,
										")
							 ->from("ig_order")
							 ->where(array("ig_order.order_id"=>$orderId))
							 ->join("ig_user","ig_user.id = ig_order.user_id")
							 ->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function support_data()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function changeorderstatus($status,$orderID,$productID,$data)
	{
		$this->db->where('product_id',$productID);
		$this->db->where('order_id',$orderID);
		$this->db->update('ig_order_items',$data);
		//echo $this->db->last_query();
		return true;
	}
	public function changeorderstatusupdate($data,$orderID)
	{
		
				$this->db->from('ig_admin_order_status');
				$this->db->where('aos_order_id', $orderID); 
				$query = $this->db->get();
		         if($query->num_rows() != 0)
				 {
					$query=$this->db->where('aos_order_id', $orderID)
			        ->update('ig_admin_order_status',$data);	
					 
				  }else{
					  
					$this->db->insert('ig_admin_order_status',$data);
					$insert_id = $this->db->insert_id();
					return $insert_id;	 
					  
				  }
			
	}
	function get_merchant($id){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_id' => $id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->marchent_email;
		}
		return false;
	}
	function insert_merchant_notification($data)
	{  
		$this->db->insert('ig_admin_notification',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function orderPaymentsDetails($orderId)
	{  
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->where('ig_transaction.order_id',$orderId);
		$query=$this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();	
		}
		else{
			$res=array();	
		}
		return $res;
	}
	
	function productPaymentsDetails($orderId,$pid)
	{  
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->where('ig_transaction.order_id',$orderId);
		$query=$this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();	
		}
		else{
			$res=array();	
		}
		return $res;
	}
	
	
	public function getProductInOrder($OID,$PID)
		{
			$this->db->select('*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'inner');
			$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
			$this->db->join('ig_merchant_store', 'ig_merchant_store.merchant_id = ig_order_items.merchant_id','left');
			
			$this->db->where('ig_order_items.order_id',$OID);
			$this->db->where('ig_order_items.product_id',$PID);
			
			$query = $this->db->get();
            //echo $this->db->last_query();die();
			$result = $query->result();
			return $result;			
		}
	
}
?>