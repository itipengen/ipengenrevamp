<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function insert_invoice()
	{
		$this->db->select('ig_order_items.*');
		$this->db->from("ig_order");
		//$this->db->order_by("marchent_id", "desc");
		$this->db->join('ig_order_items', 'ig_order_items.order_id = ig_order.order_id');
		$this->db->where("ig_order.status", "delivered");
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();
			$invoice_data = array(
				'prefix' => 'IPGNINV',
				//'amount' => $this->input->post('demail'),
				'status' => 0,
				//'date' => $res[0]->date_added,
				'merchant_id' => 5,
				);
				$this->db->insert('ig_invoice', $invoice_data);
				$insert_id = $this->db->insert_id();
			//echo $this->db->last_query();die();
			foreach($res as $val){
				$itemData=array(
				'invoice_id' => $insert_id,
				//'amount' => $this->input->post('demail'),
				'product' => $val->product_id,
				'product_name' =>$val->name ,
				'price' => $val->total,
				//'date' => $res[0]->date_added,
				);
				$this->db->insert('ig_invoice_details', $itemData);
				//print_r($itemData);	
			}	
		}  
		else{
			return array();	
		}
	die('cc');
	}
	
	
	
	function get_all_invoice()
	{
		
		$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
		$this->db->from("ig_invoice");
		$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
		$this->db->order_by('ig_invoice.id','DESC');
		$query = $this->db->get();
		//echo $this->db->last_query();die('ll');
		if($query->num_rows() > 0){
			$res=$query->result();
		}  
		else{
			$res= array();	
		}
	return $res;
	}
	
	function get_all_invoice_bydate($post)
	{
		
		$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
		$this->db->from("ig_invoice");
		$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
		$this->db->where('(ig_invoice.date BETWEEN "'. $post['dateFrom'].'" AND "'.$post['dateTo'].'")');
		$this->db->order_by('ig_invoice.id','DESC');
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();
			
		}  
		else{
			$res= array();	
		}
	return $res;
	}
	
	function get_all_merchant()
	{
		$this->db->select('*');
		$this->db->from("ig_marchent_login");
		//$this->db->order_by("marchent_id", "desc");
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	
	function generateInvoice($post)
	{
		$from=$post['fromDate'];
		$to=$post['toDate'];
		$merchantId=$post['merchantId'];
		$this->db->select('*,ig_order_items.total AS TOTALPRICE,ig_order_items.transaction_amt AS TotalTransactionAmt');
		$this->db->from("ig_order");
		//$this->db->order_by("marchent_id", "desc");
		$this->db->join('ig_order_items', 'ig_order_items.order_id = ig_order.order_id','right');
		$this->db->where('(ig_order.date_added BETWEEN "'. $post['fromDate'].'" AND "'.$post['toDate'].'")');
		$this->db->where("ig_order_items.merchant_id", $merchantId);
		$this->db->where("ig_order_items.invoice_id", '');
		$this->db->where("ig_order.status", "success");
		$this->db->where("(ig_order_items.order_status=1 or  ig_order_items.order_status = 3)");
		$this->db->where("ig_order_items.p_status","delivered");
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$res['data']=$query->result_array();
			$totalPrice=0;
			$TotalTransactionAmt=0;
			foreach($res['data'] as $key=>$val){
				$totalPrice=$totalPrice+$val['TOTALPRICE'];
				$TotalTransactionAmt=$TotalTransactionAmt+$val['TotalTransactionAmt'];
			}
			$res['totalPrice']=$totalPrice;
			$res['TotalTransactionAmt']=$TotalTransactionAmt;
			//print_r($res);die();
			$invoice_data = array(
				'prefix' => 'IPGNINV',
				'invoice' => rand(100000000,1000000000),
				'status' => 0,
				'date' => date('Y-m-d h:i:s'),
				'merchant_id' => $merchantId,
				'amount' => $totalPrice,
				'total_transaction_fee' => $TotalTransactionAmt,
				'total_amount' => (int)$totalPrice-(int)$TotalTransactionAmt,
			);
			//print_r($invoice_data);die();
			$invoiceid=$invoice_data['prefix'].$invoice_data['invoice'];
			$this->db->insert('ig_invoice', $invoice_data);
			$insert_id = $this->db->insert_id();
			foreach($res['data'] as $val1){
				//print_r($val1);
				$itemData=array(
					'invoice_id' => $insert_id,
					'product' => $val1['product_id'],
					'product_name' =>$val1['name'] ,
					'price' => $val1['TOTALPRICE']
				);
				$this->db->insert('ig_invoice_details', $itemData);
				$this->db->where('product_id', $val1['product_id']);
				$this->db->where('order_id', $val1['order_id']);
    			$this->db->update('ig_order_items', array('invoice_id' => $invoiceid));
			}
			$notify_data=array(
				'm_id'=>$merchantId,
				'type'=>'New Invoice',
				'message'=>"New Invoice from $from to $to has sent by Admin.",
				'raw'=>0,
				'date'=>date('Y-m-d h:i:s'),
				'status'=>0,
				'notification_for'=>2
			);
			$this->db->insert('ig_admin_notification', $notify_data);
			
			$res['msg']='Invoice generation is successful';
			//print_r($itemData);	
			//echo $totalPrice;die();
		}
		else{
			$res['data']=array();
			$res['msg']='No Order found for the date range.';
		}
		return $res;
		//print_r($res);die();
	}
	
	function invoiceProductDetails($mid,$invid){
		//echo $mid;die();
		$this->db->select('ig_order_items.*,ig_marchent_login.marchent_name');
		$this->db->from("ig_order_items");
		//$this->db->order_by("marchent_id", "desc");
		//$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id');
		$this->db->join('ig_marchent_login', 'ig_marchent_login.marchent_id = ig_order_items.merchant_id');
		$this->db->where("ig_order_items.invoice_id", 'IPGNINV'.$invid);
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res['data']=$query->result_array();
			//print_r($res['data']);die();
			foreach($res['data'] as $key=>$val){
			$this->db->select('ig_products.product_image');
			$this->db->from("ig_products");
			$this->db->where("ig_products.product_id", $val['product_id']);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				$result=$query->result_array();	
				$res['data'][$key]['product_image']=$result[0]['product_image'];
			}
			else{
				$res['data'][$key]['product_image']='';
			}
			//$result=$query->result_array();
			//print_r($result);
			//$res['data'][$key]['image']=$result[0]['product_image'];
			
			
		}
		}
		else{
			$res['data']=array();	
		}
		//print_r($res);
		//die();
		return $res;
		//echo $this->db->last_query();die('fghh');
	}
	
	function getInvoiceDetails($inv_id)
	{
		$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name,ig_marchent_login.merchant_phone,ig_marchent_login.marchent_email');
		$this->db->from("ig_invoice");
		$this->db->where('ig_invoice.invoice', $inv_id);
		$this->db->join('ig_marchent_login','ig_invoice.merchant_id=ig_marchent_login.marchent_id');
		//$this->db->order_by("marchent_id", "desc");
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}

	function invoice_pay()
	{
		$post=$this->input->post();
		$remarks=preg_replace('/[^A-Za-z0-9\-.=: \n]/', '', $post['remarks']); 
		$this->db->select('ig_invoice.is_paid');
		$this->db->from("ig_invoice");
		$this->db->where('ig_invoice.invoice', $post['invoiceId']);
		$this->db->where('ig_invoice.merchant_id', $post['merchantId']);
		$queryInv = $this->db->get();
		//echo $this->db->last_query();
		$queryInv->num_rows(); 
		if($queryInv->num_rows() > 0){
			$res=$queryInv->result_array();
			if($res[0]['is_paid']==0){
				//echo 'checkec';
				$data = array(
					   'is_paid' => 1,
					   'status' => 1,
					   'remarks' => $remarks,
					   'payment_date' => date('Y-m-d h:i:s'),
					);
		
				$this->db->where('invoice', $post['invoiceId']);
				$this->db->where('merchant_id', $post['merchantId']);
				$this->db->update('ig_invoice', $data);
				$response['message']='Payment Successful.'; 	
			}
			else{
				//echo 'not';
				$response['message']="Already Paid";		
			}
			
		}
		else{
			$response['message']="Invoice not found";
		}
		//die('jj');
		return $response;
		//print_r($post);die();
		
		//echo $this->db->last_query();die();
	}
	
	 
	 /*method to get bussiness name (bname) here $mid='merchent id'*/
	 
	 public function getbusinessname($mid){
		 if(!empty($mid)){
			$this->db->select('bname');
			$this->db->from("ig_merchant_business");
			$this->db->where('merchant_id',$mid);
			$query = $this->db->get();
		    return $query->num_rows() > 0 ? $query->result() : array();
			 
		 }
	 }


}