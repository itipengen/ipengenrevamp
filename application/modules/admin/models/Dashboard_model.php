<?php

class Dashboard_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function registered_user_count()
	{
		$db_query = $this->db->get("ig_user");
		return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
	}
	
	function wishlist_create_count()
	{
		$db_query = $this->db->get("ig_wishlist");
		return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
	}
	function total_order_count()
	{
		$db_query = $this->db->get("ig_order");
		return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
	}
	function best_selling_product()
	{
		$first_day_of_this_month = date('Y-m-01');
		$last_day_of_this_month = date('Y-m-t');
		$query =	$this->db->select("
								ig_order_items.product_id, 
								ig_order_items.name, 
								ig_order_items.price, 
								ig_products.sku,
								ig_products.sale_price,
								ig_products.product_image,
								count(ig_order_items.product_id) as product_count
									")
							 ->from("ig_order_items")
							 ->where(array(
							 		"ig_order.date_added >=" => $first_day_of_this_month,
									"ig_order.date_added <=" => $last_day_of_this_month,
									"ig_order_items.product_id !=" => 0,
							 				))
							 ->join("ig_order", "ig_order.order_id = ig_order_items.order_id","right")
							 ->join("ig_products", "ig_products.product_id = ig_order_items.product_id","left")
							 //->limit("5","0")
							 ->group_by("ig_order_items.product_id")
							 ->order_by("ig_order.date_added","desc")
							 ->get();		
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function latest_order()
	{
		
		$this->db->select('*');
		$this->db->from("ig_order");
		$this->db->limit(10,0);
		$this->db->order_by('order_id','DESC');
		$query = $this->db->get();
		//return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
		
		return $query->result();
	}
	function latest_gift()
	{
		
		$this->db->select('ig_order_items.*,ig_user.fname,ig_user.lname,ig_wishlist.title,ig_wishlist.url');
		$this->db->from("ig_order_items");
		$this->db->join("ig_wishlist",'ig_order_items.wishlist_id = ig_wishlist.id');
		$this->db->join("ig_user",'ig_order_items.user_id = ig_user.id');
		$this->db->where(array('ig_order_items.item_type'=>'cash_gift'));
		$this->db->limit(10,0);
		$this->db->order_by('order_id','DESC');
		$query = $this->db->get();
		//return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
		
		return $query->result();
	}
	
	/*function latest_gift()
	{
		
		$this->db->select('*');
		$this->db->from("ig_order_items");
		$this->db->where(array('item_type'=>'cash_gift'));
		$this->db->limit(10,0);
		$this->db->order_by('order_id','DESC');
		$query = $this->db->get();
		//return $db_query->num_rows() > 0 ? $db_query->num_rows() : 0;
		
		return $query->result();
	}*/
	function revenue_count()
	{
		 $sql = $this->db->select("oi.order_id,w.title,SUM(oi.transaction_amt) AS amount")
				  ->from("ig_order_items oi")
				  ->join('ig_wishlist w', 'oi.wishlist_id = w.id')
				  ->where(array("oi.item_type"=>"cash_gift"))
				  ->group_by("oi.order_id")
				  ->get();
		  $res = $sql->result();
		  $sum = 0;
		  foreach($res as $r){
			$sum = $sum + $r->amount;  
		  }
		  return $sql->num_rows() > 0 ? $sum : 0;
	}
	public function getProductUrl_name($id,$name){
		
		$slag=$this->slugify($name);
		return  base_url().'p/'.$id.'-'.$slag;
	}
	public function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
		return 'n-a';
		}
		
		return $text;
	}
	
	function get_merchant($id){
		$query = $this->db->get_where('ig_marchent_login',array('marchent_id' => $id));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $id = $row->marchent_email;
		}
		return false;
	}
	public function support_data()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	public function getDetailsById($merchant_id)
	{
		$this->db->select('*');
		$this->db->from('ig_marchent_login');
		$this->db->join('ig_merchant_business', 'ig_merchant_business.merchant_id = ig_marchent_login.marchent_id','inner');
		$this->db->join('ig_merchant_bank', 'ig_merchant_bank.merchant_id = ig_marchent_login.marchent_id','inner');
		$this->db->join('ig_merchant_store', 'ig_merchant_store.merchant_id = ig_marchent_login.marchent_id','inner'); 
		$this->db->where('ig_marchent_login.marchent_id',$merchant_id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->row() : '';
	}
}
?>