<?php
class Setting_model extends CI_Model
{
	function add_fees($fees_value)
	{
		$this->db->insert("ig_transaction_fees",$fees_value);
		return $this->db->affected_rows();
	}
	
	function getAllTransaction()
	{
		$query = $this->db->get("ig_transaction_fees");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	function update_fees($fees_value, $fees_id)
	{
		//print_r($fees_value);
		if($fees_value['fee_type']==1){
			$transactionFee=$fees_value['transaction_fees']." %";
		}
		if($fees_value['fee_type']==2){
			$transactionFee="RP ".$fees_value['transaction_fees'];
		}
		//echo $transactionFee;die();
		$this->db->select('*');
		$this->db->from('ig_transaction_fees');
		$this->db->where('transaction_fees_id',$fees_value['gift_type']);
		$query=$this->db->get();
		//$res=$query->result_array();
		if($query->num_rows() > 0){
			//die('update');
			
			//$res=$query->result_array();
			$fees_value1=$fees_value['gift_type'];
			if($fees_value['gift_type']==1){
				$data=array(
					'transaction_fees_id'=>1,
					'transaction_fees_type'=>'Cash Gift',
					'transaction_fees'=>$transactionFee,
					'admin_status'=>1,
					'gift_type'=>'cash_gift'
				);
			}
			if($fees_value['gift_type']==2){
				$data=array(
					'transaction_fees_id'=>2,
					'transaction_fees_type'=>'Buy Gift',
					'transaction_fees'=>$transactionFee,
					'admin_status'=>1,
					'gift_type'=>'buy_gift'
				);	
			}
			$this->db->update("ig_transaction_fees",$data, array("transaction_fees_id"=>$fees_value1));
		}
		else{
			//die('insert');
			if($fees_value['gift_type']==2){
				$data=array(
					'transaction_fees_id'=>2,
					'transaction_fees_type'=>'Buy Gift',
					'transaction_fees'=>$transactionFee,
					'admin_status'=>1,
					'gift_type'=>'buy_gift'
				);	
			}
			if($fees_value['gift_type']==1){
				$data=array(
					'transaction_fees_id'=>1,
					'transaction_fees_type'=>'Cash Gift',
					'transaction_fees'=>$transactionFee,
					'admin_status'=>1,
					'gift_type'=>'cash_gift'
				);	
			}
			$this->db->insert('ig_transaction_fees', $data); 
				
		}
		//print_r($res);die();
		
		//echo $this->db->last_query();die();
		return $this->db->affected_rows();
	}
	
	function delete_fees($fees_id)
	{
		$this->db->delete("ig_transaction_fees", array("transaction_fees_id"=>$fees_id));
		return $this->db->affected_rows();
	}
	
	function add_config($data)
	{
		$this->db->insert("ig_setting",$data);
		return $this->db->affected_rows();
	}
	
	function getConfigData()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function getEmailDataById($configID)
	{
		$query = $this->db->get_where("ig_setting",array("setting_id" => $configID));
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function update_config($data, $configID)
	{
		$this->db->update("ig_setting",$data,array("setting_id" => $configID));
		return $this->db->affected_rows();
	}
	function delete_config($configID)
	{
		$this->db->delete("ig_setting",array("setting_id" => $configID));
		return $this->db->affected_rows();
	}
	function add_shippingData($db_data)
	{
		$this->db->insert("ig_shipping",$db_data);
		return $this->db->affected_rows();
	}
	function get_shippingData()
	{
		$query = $this->db->get("ig_shipping");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function update_shippingData($db_data, $shippingID)
	{
		$this->db->update("ig_shipping",$db_data,array("shipping_id" => $shippingID));
		return $this->db->affected_rows();
	}
	function delete_shipping_charge($shippingID)
	{
		$this->db->delete("ig_shipping",array("shipping_id" => $shippingID));
		return $this->db->affected_rows();
	}
}
?>