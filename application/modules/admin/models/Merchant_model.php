<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function get_all_merchant()
	{
		$this->db->select('*');
		$this->db->from("ig_marchent_login");
		$this->db->order_by("marchent_id", "desc");
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	function isApproved($mid){
	    $this->db->where('marchent_id', $mid);
		$db_query=$this->db->update('ig_marchent_login', array('merchant_admin_status' => 0)); 
		if($db_query)
			return TRUE;
		else
			return FALSE;
	}
	function isnotApproved($mid){
		$result = $this->getDetailsById($mid);
		$data = array();
		$array = array();
		if(isset($result) && $result!=''){
			$data['company_name'] = $result->bname;
			$data['account_name'] = $result->account_name;
			$data['account_number'] = $result->account_number;
			$data['bank_name'] = $result->bank_name;
			$data['store_name'] = $result->sname;
			$data['brands'] = $result->brands;		
		}
		foreach($data as $key=>$value){
			$value = trim($value);
			if(!empty($value)){
				array_push($array, 1);
			}
		}
		/*if (count(array_unique($array)) === 1) { */
			$this->db->where('marchent_id', $mid);
			$db_query=$this->db->update('ig_marchent_login', array('merchant_admin_status' => 1)); 
			if($db_query){
				return TRUE;
			}
		/*}*/else{
			return FALSE;
		}
	}
	
	function insertTransection($mid,$tfees,$ttype){
		
			$this->db->where('marchent_id', $mid);
			$db_query=$this->db->update('ig_marchent_login', array('transaction_fee' => $tfees,'transaction_fee_type' => $ttype)); 
			if($db_query){
			return TRUE;
			}else{
			return FALSE;
			}
		
	}
	
	function isVerified($mid){
	    $this->db->where('marchent_id', $mid);
		$db_query=$this->db->update('ig_marchent_login', array('marchent_status' => 0)); 
		if($db_query)
			return TRUE;
		else
			return FALSE;
	}
	function isnotVerified($mid){
	    $this->db->where('marchent_id', $mid);
		$db_query=$this->db->update('ig_marchent_login', array('marchent_status' => 1)); 
		if($db_query)
			return TRUE;
		else
			return FALSE;
	}
	function getMerchantById($id)
	{
		$query = $this->db->get_where('ig_marchent_login',array('marchent_id' => $id));
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
   function getTransection()
	{
		$query = $this->db->get_where('ig_transaction_fees',array('gift_type' => 'buy_gift'));
		return $query->num_rows() > 0 ? $query->row() : false;
	}
    
	function getDetailsById($merchant_id){
		$this->db->select('*');
		$this->db->from('ig_marchent_login');
		$this->db->join('ig_merchant_business', 'ig_merchant_business.merchant_id = ig_marchent_login.marchent_id','inner');
		$this->db->join('ig_merchant_bank', 'ig_merchant_bank.merchant_id = ig_marchent_login.marchent_id','inner');
		$this->db->join('ig_merchant_store', 'ig_merchant_store.merchant_id = ig_marchent_login.marchent_id','inner'); 
		$this->db->where('ig_marchent_login.marchent_id',$merchant_id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->row() : '';
	}
	
	function get_product($mid){
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
		$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
		$this->db->where('ig_products.merchant_id',$mid);
		$this->db->where('ig_products_text.language_id', 1);
		$this->db->where('ig_products.admin_status', 0);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : '';
	}
	
	function get_all_product(){
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
		$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
		$this->db->where('ig_products_text.language_id', 1);
		$this->db->where('ig_products.admin_status', 0);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : '';
	}
	
	function get_merchant($id=NULL){
		$this->db->select('*');
		$this->db->from('ig_marchent_login');
		$this->db->join('ig_merchant_bank', 'ig_merchant_bank.merchant_id = ig_marchent_login.marchent_id','left');
		$this->db->join('ig_merchant_business', 'ig_merchant_business.merchant_id =  ig_marchent_login.marchent_id','left');
		$this->db->join('ig_merchant_store', 'ig_merchant_store.merchant_id =  ig_marchent_login.marchent_id','left');
		$this->db->where('ig_marchent_login.marchent_id', $id);
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : '';
	}
	
	function get_all_merchantdetails()
	{
		$this->db->select('*');
		$this->db->from("ig_marchent_login");
		$this->db->order_by("marchent_id", "desc");
		$query = $this->db->get();
		/*if($query->num_rows() > 0){
			$res=$query->result_array();
			//print_r($res);
			foreach($res as $key=>$val){
				$mid=$val['marchent_id'];
				$result = $this->checkProfile($mid);
				$res[$key]['profile']=$result['status'];
				
			}
			$result=$res;
		}
		else{
			$result=array();
		}
		return $result;*/
         	return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	function checkProfile($mid){
		//$mid='5';
		$result = $this->getDetailsById($mid);
		//print_r($result);die();
		
		if(isset($result) && $result!=''){
			$company_name = $result->bname;
			$account_name = $result->account_name;
			$account_number = $result->account_number;
			$bank_name = $result->bank_name;
			$store_name = $result->sname;
			$brands = $result->brands;		
			if($company_name=='' || $account_name=='' || $account_number='' || $bank_name=='' || $store_name=='' || $brands==''){
				$res['status']=0;		
			}
			else{
				$res['status']=1;
			}
		}
		else{
			$res['status']=0;
		}
		return $res;
	}
	
	public function merchantadd(){
		
		$basicdata = array(
				'marchent_name' => $this->input->post('name'),
				'marchent_email' => $this->input->post('email') ,
				'marchent_password' => md5($this->config->item('email_verification_salt').$this->input->post('password')),
				'merchant_phone' => $this->input->post('phonenumber') 
			);
			$this->db->insert('ig_marchent_login', $basicdata); 
			$merchantid=  $this->db->insert_id();
			$merchantinsert = $this->db->affected_rows();
			if($merchantinsert > 0 && !empty($merchantid)){
				//bank data inserted if ig_marchent_login add....
				$bankdata = array(
								'merchant_id' => $merchantid,
								'account_name' => $this->input->post('accountname') ,
								'account_number' => $this->input->post('accountnumber'),
								'bank_name' => $this->input->post('bankname'),
								'branch_name' => $this->input->post('branchname') ,
								'state' => $this->input->post('state'),
								'city' => $this->input->post('city')  
								);
								
				$this->db->insert('ig_merchant_bank', $bankdata);
				
				//business data inserted if ig_marchent_login add....
				$businessdata = array(
								'merchant_id' => $merchantid,
								'bname' => $this->input->post('companyname') ,
								'bdescription' => $this->input->post('companydesc'),
								'email' => $this->input->post('businessemail'),
								'phone' => $this->input->post('businessphone')
								);
								
				$this->db->insert('ig_merchant_business', $businessdata);
				
				//Store data inserted if ig_marchent_login add....
				$storedata = array(
								'merchant_id' => $merchantid,
								'sname' => $this->input->post('storename') ,
								'sdescription' => $this->input->post('storedesc'),
								'brands' => $this->input->post('brands')
								
								);
								
				$this->db->insert('ig_merchant_store', $storedata);
				$storeinsert = $this->db->affected_rows();
				if($storeinsert > 0){
					
					return true;
					
				}
			}
		
		}
	function getSubcategoriesBycatId($catId){
		    $this->db->select('*');
			$this->db->from('ipengen_category');
			$this->db->join('ig_category_details','ipengen_category.id=ig_category_details.cat_id');
			$this->db->where(array('ipengen_category.parent_id'=>$catId,'ig_category_details.lang'=>1)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array();
	}
	
	// merchant fees details----
    public function getfees($mid){ 
		
		 	$this->db->select('*');
			$this->db->from('ig_marchent_login');
			$this->db->where(array('marchent_id'=>$mid)); 
			$query=$this->db->get();
			return $query->num_rows() > 0 ? $query->result_array() : array();
		
		
	}
  // merchant fees update -----
  
   function merchantfeesupdate($updatearray,$mid){
	    $this->db->where('marchent_id', $mid);
		$db_query=$this->db->update('ig_marchent_login',$updatearray); 
		if($db_query)
			return TRUE;
		else
			return FALSE;
	}
	
	public function get_ajax_merchant_product($columns,$requestData){
		
		if(isset($_POST['merchantId']) && !empty($_POST['merchantId'])){
			$mid = $_POST['merchantId'];
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
			$this->db->where('ig_products.merchant_id',$mid);
			$this->db->where('ig_products_text.language_id', 1);
			$this->db->where('ig_products.admin_status', 0);
			$query = $this->db->get();
			$totalData = $query->num_rows();
			$data['totalData'] = $totalData;
			
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
				$this->db->select('*');
				$this->db->from('ig_products');
				$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
				$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
				$this->db->where('ig_products.merchant_id',$mid);
				$this->db->where('ig_products_text.language_id', 1);
				$this->db->where('ig_products.admin_status', 0);
				
				$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
				$this->db->limit($requestData['start'],$requestData['length']);
				$queryresult = $this->db->get();
				$queryCOUNT = $queryresult->num_rows();
				
			}
				else{
				$this->db->select('*');
				$this->db->from('ig_products');
				$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
				$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
				$this->db->where('ig_products.merchant_id',$mid);
				$this->db->where('ig_products_text.language_id', 1);
				$this->db->where('ig_products.admin_status', 0);
				
				
				$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
				$this->db->limit($requestData['length'],$requestData['start']);
				$queryresult = $this->db->get();
				$queryCOUNT = $queryresult->num_rows();
				
				
				
				}
				
				$data['queryresult'] = $queryresult->result_array();
				$data['FILTERCOUNT'] = $queryCOUNT;
			
			return $data;
			
			
			
			}
		
		
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
		$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
		$this->db->where('ig_products_text.language_id', 1);
		$this->db->where('ig_products.admin_status', 0);
		$query = $this->db->get();
		$totalData = $query->num_rows();
		$data['totalData'] = $totalData;
		
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
			$this->db->where('ig_products_text.language_id', 1);
			$this->db->where('ig_products.admin_status', 0);
			
			$this->db->like('ig_products_text.product_name', $requestData['search']['value']);
			$this->db->or_like('ig_marchent_login.marchent_name', $requestData['search']['value']);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$queryresult = $this->db->get();
			$queryCOUNT = $queryresult->num_rows();
			
		}
			else{
			$this->db->select('*');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
			$this->db->where('ig_products_text.language_id', 1);
			$this->db->where('ig_products.admin_status', 0);
			
			$this->db->like('ig_products_text.product_name', $requestData['search']['value']);
			$this->db->or_like('ig_marchent_login.marchent_name', $requestData['search']['value']);
			
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$queryresult = $this->db->get();
			$queryCOUNT = $queryresult->num_rows();
			
			
			
			}
			
			$data['queryresult'] = $queryresult->result_array();
			$data['FILTERCOUNT'] = $queryCOUNT;
		
		return $data;
		
		
		}
 
}