<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	public function language_list(){
		$query=$this->db->get('ig_language'); 
		if($query){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	public function insert()
	{  
	  $data=array('page_status'=>1);
	  $sql=$this->db->insert('ig_pages', $data); 
	  $last_id=$this->db->insert_id();
	  
	  $i=0;
	  $nameArr=$this->input->post('page_name');
	  $page_descriptionArr=$this->input->post('page_description');
	  $langArr=$this->input->post('langId');
	   
	  foreach($nameArr as $val)
	  {
		 $dataArr=array(
						'page_id'=>$last_id,
						'lang_id'=>$langArr[$i],
						'page_name'=>$val,
						'page_slug'=>strtolower(str_replace(' ','-',$val)),
						'page_description'=>$page_descriptionArr[$i],
		 );
		 $query=$this->db->insert('ig_page_text', $dataArr); 
		 $i++;
	  }
	  if($query){
		return $last_id;
	  }else{
		return FALSE;
	  }
	}
	
	public function listing()
	{
		$this->db->select('*');
		$this->db->from('ig_pages');
		$this->db->join('ig_page_text','ig_pages.page_id=ig_page_text.page_id');
		$this->db->where(array('ig_page_text.lang_id'=>1));
		$query=$this->db->get(); 
		if($query){
			return $query->num_rows() > 0 ? $query->result_array() : array();;
		}else{
			return FALSE;
		}
	}
	
	public function get_details($id)
	{
		$this->db->select('*');
	   $this->db->from('ig_pages');
	   $this->db->where(array('ig_pages.page_id'=>$id, "ig_page_text.lang_id" => 1));
	   $this->db->join("ig_page_text","ig_page_text.page_id = ig_pages.page_id");
	   $query = $this->db->get();
	   return $query->result();
	}
	
	public function update_details($id)
	{
		$data = array('page_name' => $this->input->post('page_name'),
		              'page_slug' => $this->input->post('slug'),
					  'page_description' => $this->input->post('content')
					  );
					  
		$slug = $this->input->post('slug');
		if(!empty($slug))
		{
			
		    $this->db->where('page_id',$id);
			$this->db->update('ig_page_text',$data);	
		}
		else
		{
			$config = array(
                                      'table' => 'ig_page_text',
                                      'id' => 'page_id',
                           	          'field' => 'page_name',
                                      'title' => 'page_name',
                                      'replacement' => 'dash' // Either dash or underscore
                                   );
			$this->load->library('slug',$config);
			$data_slug = array('page_name' => $this->input->post('page_name'));
			$data['page_slug'] = $this->slug->create_uri($data_slug);
			
			$this->db->where('page_id',$id);
			$this->db->update('ig_page_text',$data);
		}
	
	}
	
	public function delete_pages($id)
	{
		$this->db->delete("ig_pages",array("page_id" => $id));
		$this->db->delete("ig_page_text",array("page_id" => $id));
	}
}
?>