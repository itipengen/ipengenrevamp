<?php
/**
* 
*/
class Contribute_model extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	function getContributelist_bak()
	{
		$query = $this->db->select("
						ig_contribution.order_id as order_id,
						ig_contribution.wishlist_id as wishlist_id,
						ig_wishlist.title as wishlist_name,
						ig_contribution.total_amt_received as contribute_amount,
						ig_contribution.date_added as contribute_date,
						ig_user.fname as w_owner_fname,
						ig_user.lname as w_owner_lname,
						ig_products_text.language_id as language_id,
						ig_products_text.product_name as product_name,
						ig_products.product_id as product_id,
						ig_products.price as product_price,
						ig_products.product_image as product_image,
									")
						  ->from("ig_contribution")
						  ->join("ig_wishlist","ig_wishlist.id = ig_contribution.wishlist_id")
						  ->join("ig_user","ig_user.id = ig_wishlist.uid")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_products_text","ig_products_text.product_id = ig_contribution.product_id")
						  ->get();
		$db_query['arr1'] = $query->result_array();
		$query = $this->db->select("
							ig_user.fname as contributer_fname,
							ig_user.lname as contributer_lname,
							ig_category_details.name as product_cat_name,
		 						  ")
						  ->from("ig_contribution")
						  ->join("ig_user","ig_user.id = ig_contribution.user_id")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_category_details","ig_category_details.cat_id = ig_products.category_id")
						  ->get();
		$db_query['arr2'] = $query->result_array();
		if (!empty($db_query['arr1']) && !empty($db_query['arr2'])) {
			$i=0;
			foreach ($db_query['arr1'] as $value) {
				$alternative_arr = $db_query['arr2'][$i];
				$main_array[$i] = array_merge($value,$alternative_arr);
				$i++;
			}
			return $main_array;
		}
		else
		{
			return array();
		}
	}
	function getContributelist()
	{
		$query = $this->db->select("
								ig_contribution.order_id as order_id,
								ig_contribution.wishlist_id as wishlist_id,
								ig_contribution.user_id as user_id,
								ig_wishlist.title as wishlist_name,
								ig_contribution.date_added as contribute_date,
								ig_user.fname as w_owner_fname,
								ig_user.lname as w_owner_lname,
								ig_products_text.language_id as language_id,
								ig_products_text.product_name as product_name,
								ig_products.product_id as product_id,
								ig_products.price as product_price,
								ig_products.product_image as product_image,
								sum(ig_contribution.total_amt_received) as contribute_amount
								")
						  ->from("ig_contribution")
						  ->where(array("ig_products_text.language_id"=>1))
						  ->join("ig_wishlist","ig_wishlist.id = ig_contribution.wishlist_id")
						  ->join("ig_user","ig_user.id = ig_wishlist.uid")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_products_text","ig_products_text.product_id = ig_contribution.product_id")
						  ->group_by(array("ig_contribution.wishlist_id","ig_contribution.user_id"))
						  ->get();
		$db_query['arr1'] = $query->result_array();
		
		
		
		$query = $this->db->select("
							ig_user.fname as contributer_fname,
							ig_user.lname as contributer_lname,
							ig_category_details.name as product_cat_name,
		 						  ")
						  ->from("ig_contribution")
						  ->join("ig_user","ig_user.id = ig_contribution.user_id")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_category_details","ig_category_details.cat_id = ig_products.category_id")
						  ->get();
		$db_query['arr2'] = $query->result_array();
		
		
		
		if (!empty($db_query['arr1']) && !empty($db_query['arr2'])) {
			$i=0;
			foreach ($db_query['arr1'] as $value) {
				$alternative_arr = $db_query['arr2'][$i];
				$main_array[$i] = array_merge($value,$alternative_arr);
				$i++;
			}
			//print_r($main_array);
			//die();
			return $main_array;
		}
		else
		{
			return array();
		}
	}
	
	public function ajax_contribution_list($columns,$requestData){
		//count array...
		$query = $this->db->select("
								ig_contribution.order_id as order_id,
								ig_contribution.contributor_id as contributor_id,
								ig_contribution.order_id as order_id,
								ig_contribution.wishlist_id as wishlist_id,
								ig_contribution.user_id as user_id,
								ig_wishlist.title as wishlist_name,
								ig_contribution.date_added as contribute_date,
								ig_user.fname as w_owner_fname,
								ig_user.lname as w_owner_lname,
								ig_products_text.language_id as language_id,
								ig_products_text.product_name as product_name,
								ig_products.product_id as product_id,
								ig_products.price as product_price,
								ig_products.product_image as product_image,
								sum(ig_contribution.total_amt_received) as contribute_amount
								")
						  ->from("ig_contribution")
						  ->where(array("ig_products_text.language_id"=>1))
						  ->join("ig_wishlist","ig_wishlist.id = ig_contribution.wishlist_id")
						  ->join("ig_user","ig_user.id = ig_wishlist.uid")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_products_text","ig_products_text.product_id = ig_contribution.product_id")
						  ->group_by(array("ig_contribution.wishlist_id","ig_contribution.user_id"))
						  ->get();
		$queryCOUNT = $query->num_rows();
		
		
		
		
		
		
		
		
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
				
				
				
	    $query = $this->db->select("
								ig_contribution.order_id as order_id,
								ig_contribution.contributor_id as contributor_id,
								ig_contribution.wishlist_id as wishlist_id,
								ig_contribution.user_id as user_id,
								ig_wishlist.title as wishlist_name,
								ig_contribution.date_added as contribute_date,
								ig_user.fname as w_owner_fname,
								ig_user.lname as w_owner_lname,
								ig_products_text.language_id as language_id,
								ig_products_text.product_name as product_name,
								ig_products.product_id as product_id,
								ig_products.price as product_price,
								ig_products.product_image as product_image,
								sum(ig_contribution.total_amt_received) as contribute_amount
								")
						  ->from("ig_contribution")
						  ->where(array("ig_products_text.language_id"=>1))
						  ->join("ig_wishlist","ig_wishlist.id = ig_contribution.wishlist_id")
						  ->join("ig_user","ig_user.id = ig_wishlist.uid")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_products_text","ig_products_text.product_id = ig_contribution.product_id")
						  ->like('ig_products_text.product_name', $requestData['search']['value'])
						  
		
			               ->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."")
						   ->limit($requestData['start'],$requestData['length'])
						  ->group_by(array("ig_contribution.wishlist_id","ig_contribution.user_id"))
						  
						 
						  ->get();
		$queryresult = $query->result_array();
		
	
		
		
		}
		else{
			
			$query = $this->db->select("
								ig_contribution.order_id as order_id,
								ig_contribution.contributor_id as contributor_id,
								ig_contribution.wishlist_id as wishlist_id,
								ig_contribution.user_id as user_id,
								ig_wishlist.title as wishlist_name,
								ig_contribution.date_added as contribute_date,
								ig_user.fname as w_owner_fname,
								ig_user.lname as w_owner_lname,
								ig_products_text.language_id as language_id,
								ig_products_text.product_name as product_name,
								ig_products.product_id as product_id,
								ig_products.price as product_price,
								ig_products.product_image as product_image,
								sum(ig_contribution.total_amt_received) as contribute_amount
								")
						  ->from("ig_contribution")
						  ->where(array("ig_products_text.language_id"=>1))
						  ->join("ig_wishlist","ig_wishlist.id = ig_contribution.wishlist_id")
						  ->join("ig_user","ig_user.id = ig_wishlist.uid")
						  ->join("ig_products","ig_products.product_id = ig_contribution.product_id")
						  ->join("ig_products_text","ig_products_text.product_id = ig_contribution.product_id")
						  
						  ->like('ig_products_text.product_name', $requestData['search']['value'])
						 
		
			               ->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."")
						  ->group_by(array("ig_contribution.wishlist_id","ig_contribution.user_id"))
						  
						 
						  ->limit($requestData['length'],$requestData['start'])
						  ->get();
		$queryresult = $query->result_array();
		
	
		
			
			
			}
		
				
				$data['queryresult'] = $queryresult;
				$data['FILTERCOUNT'] = $queryCOUNT;
			
			return $data;
			
		
		
		
		}
	
	
}
?>