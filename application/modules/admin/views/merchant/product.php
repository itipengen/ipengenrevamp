<style> 
	.xlsBtn{margin-left:5px;background-color: #1a7bb9;border-color: #1a7bb9;color: #fff;}
</style>
<div class="form-group"> <label for="for=" stock""="">Stock</label>
	<select name="merchantId" class="form-control" id='merchantId'>
    	<option value="">Select Merchant</option>
		<?php  
			if(!empty($merchant)){
				foreach($merchant as $key=>$val){
		?>
				<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
		<?php		
				}
			}
		?>
        
	</select>
    <br/><br/>
    <button id="merchantBtn" class="btn btn-success btn-transparent btn-outline active">Search</button>
    
    <div class="row admin-list-msg" style="display:none">
      <div class="col-lg-12">
        <div class="ibox float-e-margins admin-flash-msg"> </div>
      </div>
    </div>
    <br/><br/>
    <div class="ibox-content">
    <div>
        	<span><button class='btn btn-success xlsBtn' id='excelBtn'>Excel</button></span>
        </div>
        <table class="table table-striped table-bordered table-hover " id="merchantptable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Product ID</th>
              <th>Product Name</th>
              <th>Merchant Name</th>
              <th>Stock Quantity</th>
              <th>Product Price</th>
              <th>Sale Price</th>
              <th>Image</th>
              <th>Feature</th>
              <th>Status</th>
            </tr>
          </thead>
          
       </table>
     </div>
 </div>
 <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.table2excel.js"></script>
 <script>
	$(document).ready(function(){
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature')); //console.log();
		elems.forEach(function(data) {
			var switchery = new Switchery(data,{ color: '#1AB394' });
		});	
		
		
		
		//$('#merchantptable').DataTable();
		var base_url = window.location.origin;
        //merchant products datatable...................................
		$('#merchantptable').dataTable({
		"processing": true,
		 "scrollCollapse": false,
		"serverSide": true,
		"aaSorting": [[ 0, "desc" ]],
		
		"ajax":{
								url :base_url+'/admin/merchant/ajax_merchant_product_list', // json datasource
								//var data = {date1 : date1, date2 : date2};
								type: "post",  // method  , by default get
								
								
								error: function (){ 
								
									$(".employee-grid-error").html("");
									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
									$("#employee-grid_processing").css("display","none");
								
								
								
								 }
		
								
							}
		
		
		
		
		});
		setTimeout(function(){
				$(".switchery").remove();
				 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
			elems.forEach(function(html) {
			var switchery = new Switchery(html,{ color: '#1AB394' });
		  });
		   var elems = Array.prototype.slice.call(document.querySelectorAll('.js_switch_feature'));
				elems.forEach(function(html) {
				var switchery = new Switchery(html,{ color: '#1AB394' });
			  });
			  $(".imageZoom").imageTooltip();
		  },3000);
  
	  setInterval(function(){
					$(".switchery").remove();
					 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
				elems.forEach(function(html) {
				var switchery = new Switchery(html,{ color: '#1AB394' });
			  });
			  
			         var elems = Array.prototype.slice.call(document.querySelectorAll('.js_switch_feature'));
				elems.forEach(function(html) {
				var switchery = new Switchery(html,{ color: '#1AB394' });
			  });
			  $(".imageZoom").imageTooltip();
			  },10000);
		
		
		
		
		//var base_url='http://staging.ipengen.com/';
		var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
		var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
		$("#merchantId").select2({
			placeholder:"Type Name..",
		});
		
		
		$(document).on('click','#excelBtn',function(){
		$("#merchantptable").table2excel({
			name: "Product List",
			filename: "Product",
			fileext: ".xls",
			exclude_img: true,
			exclude_links: true,
			exclude_inputs: true
		});	
	});
		
		$(document).on('click','#merchantBtn',function(){
		
			var merchantId = $('#merchantId').val();
			
			$('#merchantptable').dataTable().fnDestroy();
			$('#merchantptable').dataTable({
					"processing": true,
					"serverSide": true,
					"aaSorting": [[ 0, "desc" ]],
			
					"ajax":{
					url :base_url+'/admin/merchant/ajax_merchant_product_list', // json datasource
					//var data = {date1 : date1, date2 : date2};
					type: "post",  // method  , by default get
					data: {
						'merchantId': merchantId,
					},
					
					
					error: function (){ 
					
					$(".employee-grid-error").html("");
					$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
					$("#employee-grid_processing").css("display","none");
					
					
					
					}
					
					
					}
		
		
		
		
		});
		setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));
  $(".imageZoom").imageTooltip();

 $(".switchery").remove();
  elems.forEach(function(data) {
   
    var switchery = new Switchery(data,{ color: '#1AB394' });
  }); }, 3000);



			
	
			
			
				
		})
		
		$(document).on("change",".js-switch",function(e){
	  var productId = $(this).attr("value");
	  var ProName = $(this).closest("tr").find("td:eq(1)").html();
	  if (this.checked) {
		  $.ajax({
			  url:"<?php echo base_url('admin/product/status/'); ?>" + productId + "/1",
			  beforeSend: function(){
					  	$("body").append(lhtml);
					  },
			  success: function(result){
				  	$('#loaderBg2').remove();
					if($.trim(result) != 0)
					{
						$(this).attr("checked", "checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(ProName +" activated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).removeAttr("checked");}
				  }
			  });
	  }else{
		  $.ajax({
			  url:"<?php echo base_url('admin/product/status/'); ?>" + productId + "/0",
			   beforeSend: function(){
					  	$("body").append(lhtml);
					  },
			  success: function(result){
				  	$('#loaderBg2').remove();
					if($.trim(result) != 0)
					{
						$(this).removeAttr("checked");
						$(".admin-list-msg").css("display","");
						$(".admin-flash-msg").html(ProName +" deactivated.");
						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
					}
					else
					{$(this).attr("checked", "checked");}
				  }
			  });
	  };
		});
		
		/*****Product Feature Status Change*****/
		$(document).on("change",".js_switch_feature",function(e){
			  var productId = $(this).attr("value");
			  var ProName = $(this).closest("tr").find("td:eq(1)").html();
			  if (this.checked) {
				  $.ajax({
					  url:"<?php echo base_url('admin/product/feature_status/'); ?>" + productId + "/1",
					  beforeSend: function(){
								$("body").append(lhtml);
							  },
					  success: function(result){
							$('#loaderBg2').remove();
							if($.trim(result) != 0)
							{
								$(this).attr("checked", "checked");
								$(".admin-list-msg").css("display","");
								$(".admin-flash-msg").html(ProName +" will display on homepage.");
								$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
							}
							else
							{$(this).removeAttr("checked");}
						  }
					  });
			  }else{
				  $.ajax({
					  url:"<?php echo base_url('admin/product/feature_status/'); ?>" + productId + "/0",
					  beforeSend: function(){
								$("body").append(lhtml);
							  },
					  success: function(result){
							$('#loaderBg2').remove();
							if($.trim(result) != 0)
							{
								$(this).removeAttr("checked");
								$(".admin-list-msg").css("display","");
								$(".admin-flash-msg").html(ProName +" will hide from homepage.");
								$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");
							}
							else
							{$(this).attr("checked", "checked");}
						  }
					  });
			  };
		});
	
	});
 </script>