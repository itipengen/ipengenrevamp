

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>View Merchant</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a>Merchant</a>
                        </li>
                        <li class="active">
                            <strong>View</strong>
                        </li>
                    </ol>
                </div>

            </div>
            <div class="wrapper wrapper-content animated fadeInRight">

                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                           
                            <!--<div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>-->
                        </div>
                        <?php foreach ($merchant as $merchants){?>
                        <div class="ibox-content">
          
        
            
            <div class="row">
            <div class="col-md-12"><h3> Transaction Fee</h3></div>
            
            <div class="col-md-6">
                  <div class="form-group">
                    <label for="name"><strong>Transaction Fee : </strong></label>
                    <?php echo $merchants->	transaction_fee .' '. $merchants->transaction_fee_type;?>
                  </div>
              </div>
              
            
            <div class="col-md-12"><h3> Basic Details</h3></div>
             <div class="col-md-6">
                  <div class="form-group">
                    <label for="name"><strong>Name : </strong></label>
                    <?php echo $merchants->marchent_name;?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="phonenumber"><strong>Phone : </strong></label>
                    <?php echo $merchants->merchant_phone;?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="email"><strong>Email : </strong></label>
                   <?php echo $merchants->marchent_email;?>
                  </div>
              </div>
              </div>
              
             
            <div class="row">
            
            <div class="col-md-12"><h3>Business Details</h3></div>
             
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="companyname"><strong>Company Name : </strong></label>
					<?php echo $merchants->bname;?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="companydesc"><strong>Company Description : </strong></label>
					<?php echo $merchants->bdescription;?>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="companyname"><strong>usiness Email : </strong></label>
					<?php echo $merchants->email;?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="companydesc"><strong>Business Phone/Mobile No : </strong></label>
					<?php echo $merchants->phone;?>
                  </div>
              </div>
            
              </div> 
              
            <div class="row">
            
            <div class="col-md-12"><h3>Bank Details</h3></div>
             <div class="col-md-6">
                  <div class="form-group">
                    <label for="accountname"><strong>Account Name : </strong></label>
					<?php echo $merchants->account_name;?>
                  </div>
              </div>
              
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="accountnumber"><strong>Account Number : </strong></label>
					<?php echo $merchants->account_number;?>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="bankname"><strong>Bank Name : </strong></label>
					<?php echo $merchants->bank_name;?>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="branchname"><strong>Branch Name : </strong></label>
					<?php echo $merchants->branch_name;?>
                  </div>
              </div>
               
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="State"><strong>State : </strong></label>
					<?php echo $merchants->state;?>
                  </div>
              </div>
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="City"><strong>City : </strong></label>
                    <?php echo $merchants->city;?>
                  </div>
              </div>
              
            
              </div>
              
            <div class="row">
            
            <div class="col-md-12"><h3> Store Details</h3></div>
              <div class="col-md-6">
                  <div class="form-group">
                   <label for="storename"><strong>Store Name : </strong></label>
				   <?php echo $merchants->sname;?>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="storedesc"><strong>Store Description : </strong></label>
					<?php echo $merchants->sdescription;?>
                  </div>
              </div>
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="Brands"><strong>Brands : </strong></label>
					<?php echo $merchants->brands;?>
                  </div>
              </div>
              
              
              </div>  
        
      </div>
                        <?php } ?>
                    </div>
                </div>
<style>
.col-md-12 > h3 {
    color: #19aa8d;
}
</style>                