<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Merchant Add</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Merchant</a> </li>
      <li class="active"> <strong>Merchant Add</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php /*?><?php if($this->session->userdata('approval_failed') == 0){ ?>
<div class="row danger">
  <div class="col-lg-12">
    <div class="admin-error-msg alert alert-danger"> All mandatory informations are incomplete related to the merchant.Unable to approve! </div>
  </div>
</div>
<?php } ?><?php */?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title"> </div>
      <div class="ibox-content">
       <?php echo form_open('admin/merchant/merchantAdd', 'class="myFormclass" id="myform"'); ?>
        
            
            <div class="row">
            
            <div class="col-md-12"><h3> Basic Details</h3></div>
             <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="name">Name:</label>
                    <input type="text"  required="required" name="name" class="form-control" id="name" value="<?php echo set_value('name'); ?>">
                    <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="phonenumber">Phone/Mobile #:</label>
                    <input type="text"  required="required" name="phonenumber" class="form-control" id="phonenumber" value="<?php echo set_value('phonenumber'); ?>">
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="email">Email address:</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?php echo set_value('email'); ?>">
                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="pwd">Password:</label>
                    <input type="password" name="password" class="form-control" id="pwd" value="<?php echo set_value('password'); ?>">
                    <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                  </div>
              </div>
              
              
              
              
              
              
              </div>
              
             
            <div class="row">
            
            <div class="col-md-12"><h3>Business Details</h3></div>
             
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="companyname">Company Name:</label>
                    <input type="text"  required="required" name="companyname" class="form-control" id="companyname" value="<?php echo set_value('companyname'); ?>">
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="companydesc">Company Description:</label>
                    <input type="text"  required="required" name="companydesc" class="form-control" id="companydesc" value="<?php echo set_value('companydesc'); ?>">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="companyname">Business Email:</label>
                    <input type="text"  required="required" name="businessemail" class="form-control" id="businessemail" value="<?php echo set_value('businessemail'); ?>">
                  </div>
              </div>
              
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="companydesc">Business Phone/Mobile No:</label>
                    <input type="text"  required="required" name="businessphone" class="form-control" id="businessphone" value="<?php echo set_value('businessphone'); ?>">
                  </div>
              </div>
              
               
               
               
               
              
              
               
              
            
              </div> 
              
            <div class="row">
            
            <div class="col-md-12"><h3>Bank Details</h3></div>
             <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="accountname">Account Name:</label>
                    <input type="text"  required="required" name="accountname" class="form-control" id="accountname" value="<?php echo set_value('accountname'); ?>">
                  </div>
              </div>
              
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="accountnumber">Account Number:</label>
                    <input type="text"  required="required" name="accountnumber" class="form-control" id="accountnumber" value="<?php echo set_value('accountnumber'); ?>">
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="bankname">Bank Name:</label>
                    <input type="text"  required="required" name="bankname" class="form-control" id="bankname" value="<?php echo set_value('bankname'); ?>">
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="branchname">Branch Name:</label>
                    <input type="text"  required="required" name="branchname" class="form-control" id="branchname" value="<?php echo set_value('branchname'); ?>">
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="Country">Country:</label>
                <input type="text"  required="required" name="country" class="form-control" id="Country" value="<?php echo set_value('country'); ?>">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="State">State:</label>
                    <input type="text"  required="required" name="state" class="form-control" id="state" value="<?php echo set_value('state'); ?>">
                  </div>
              </div>
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="City">City:</label>
                    <input type="text"  required="required" name="city" class="form-control" id="city" value="<?php echo set_value('city'); ?>">
                  </div>
              </div>
              
            
              </div>
              
            <div class="row">
            
            <div class="col-md-12"><h3> Store Details</h3></div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="storename">Store Name:</label>
                    <input type="text"  required="required" name="storename" class="form-control" id="storename" value="<?php echo set_value('storename'); ?>">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="storedesc">Store Description:</label>
                    <input type="text"  required="required" name="storedesc" class="form-control" id="storedesc" value="<?php echo set_value('storedesc'); ?>">
                  </div>
              </div>
              
               <div class="col-md-6">
                  <div class="form-group">
                    <label for="required" class="required">*</label><label for="Brands">Brands:</label>
                    <input type="text"  required="required" name="brands" class="form-control" id="brands" value="<?php echo set_value('brands'); ?>">
                  </div>
              </div>
              
              
              </div>  
          
          
          
          
          
          <button type="submit" class="btn btn-success">Submit</button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<style>
.col-md-12 > h3 {
   /* background: #19aa8d none repeat scroll 0 0;
      padding: 10px;
   */
    color: #19aa8d;
    
}
.required {
    color: #f40e0e;
}

</style>
