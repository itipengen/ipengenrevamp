<style>

.alert.alert-success.alert-dismissable {

    margin-top: 18px;

}

</style>

<div class="row wrapper border-bottom white-bg page-heading">

  <div class="col-lg-10">

    <h2>Merchant List</h2>

    <ol class="breadcrumb">

      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>

      <li> <a>Merchant</a> </li>

      <li class="active"> <strong>Merchant List</strong> </li>

    </ol>

  </div>

  <div class="col-lg-2"> </div>

</div>

<?php /*?><?php if($this->session->userdata('approval_failed') == 0){ ?>

<div class="row danger">

  <div class="col-lg-12">

    <div class="admin-error-msg alert alert-danger"> All mandatory informations are incomplete related to the merchant.Unable to approve! </div>

  </div>

</div>

<?php } ?><?php */?>

<div class="row admin-list-msg" style="display:none">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg">

     

    

     </div>

  </div>

</div>

<div class="row">

<div class="col-lg-12">

<?php if($this->session->flashdata('merchant_add_success_msg')) { ?>



    <div class="alert alert-success alert-dismissable">

    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

    <strong>Success!</strong>Merchant Account successfully Created.

    </div>

<?php } ?>



<?php if($this->session->flashdata('merchant_update_success_msg')) { ?>



    <div class="alert alert-success alert-dismissable">

    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

    <strong>Success!</strong>Merchant Fee updated succefully.

    </div>

<?php } ?>

     </div>

</div>





<div class="row">

  <div class="col-lg-12">

    <div class="ibox float-e-margins">

      <div class="ibox-title"> </div>

      <div class="ibox-content"> <a href="add" class="btn btn-success">Add New</a>

        <div class="table-responsive">

                    <table class="table table-bordered" id="merchantTbl" style="width:100%;">

                      <thead>

                        <tr>

                            <th>Id</th>

                            <th>Name</th>

                            <th>Profile</th>

                            <th>Email</th>

                            <th>Verified</th>

                            <th>Status</th>

                            <th>Edit Fees</th>

                            <th>Action</th>

                        </tr>

                      </thead>

                      

                </table>

          </div>

      </div>

    </div>

  </div>

</div>



<!-- Modal -->

    <div id="editFees" class="modal fade" role="dialog">

        <div class="modal-dialog">

        

        <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title merchantmodaltitle"></h4>

                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="<?php echo base_url()?>admin/merchant/feesupdate" method="post" name="feesupdate">

                    <input type="hidden" class="hiddenmid" name="hideenmid" value="" />

                    <div class="form-group">

                    <label class="control-label col-sm-3" for="email">Fees Value:</label>

                    <div class="col-sm-8">

                    <input type="text" name="feeamt" class="form-control" id="feeamt" placeholder="Enter Fee" value="">

                    </div>

                    </div>

                    <div class="form-group">

                    <label class="control-label col-sm-3" for="pwd">Fee Type:</label>

                    <div class="col-sm-8">

                    <select class="form-control" name="feetype" id="feetype">

                    <option value="">Select</option>

                    <option value="%">Percent</option>

                    <option value="RP">Fixed</option>

                    </select>



                    </div>

                    </div>

                    

                    <div class="form-group">

                    <div class="col-sm-offset-3 col-sm-8">

                    <button type="submit" class="btn btn-info btn-lg">Update</button>

                    </div>

                    </div>

                    </form>

               

                </div>

                <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

            </div>

        

        </div>

    </div>



<script type="text/javascript">

$(document).ready(function(){

	

	var base_url = window.location.origin;



$('#merchantTbl').dataTable({

		"processing": true,

		 "scrollCollapse": false,

		"serverSide": true,

		"aaSorting": [[ 0, "desc" ]],

		

		"ajax":{

								url :base_url+'/admin/merchant/ajax_merchant_list', // json datasource

								//var data = {date1 : date1, date2 : date2};

								type: "post",  // method  , by default get

								

								

								error: function (){ 

								

									$(".employee-grid-error").html("");

									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

									$("#employee-grid_processing").css("display","none");

								

								

								

								 }

		

								

							}

		

		

		

		

		});



  var loader = "<?php echo base_url('assets/frontend/img/ripple.gif'); ?>";

  var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';

  /*var message = <?php echo $this->session->userdata('approval_failed') ?>;

  if(message == 0){

     $(".danger").delay(2000).fadeTo(4000).slideUp("slow");

  }*/

  /****Switcher****/

  // For Single Checkbox

  // var elem = document.querySelector('.js-switch');

  // Switchery(elem, { color: '#1AB394' });

  // For multiple Checkbox

  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {

    var switchery = new Switchery(html,{ color: '#1AB394' });

  });

  $(document).on('click', '.paginate_button a', function(){

    $( "#admintable tbody tr" ).each(function(){

       if(!$(this).find('td:eq(6) span').hasClass('switchery')){

        var elem = this.querySelector('.js-switch');

        Switchery(elem, { color: '#1AB394' });

        /*Image Tooltips*/

        $(".imageZoom").imageTooltip();

		

      }

    });

	setTimeout(function(){

		$(".switchery").remove();

		 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {

    var switchery = new Switchery(html,{ color: '#1AB394' });

  });

  },3000);

  });

  setTimeout(function(){

		$(".switchery").remove();

		 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {

    var switchery = new Switchery(html,{ color: '#1AB394' });

  });

  },3000);

  

  setInterval(function(){

		$(".switchery").remove();

		 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {

    var switchery = new Switchery(html,{ color: '#1AB394' });

  });

  },10000);



  

  

  

  $(document).on("keyup",".dataTables_filter input", function(){

    $( "#admintable tbody tr" ).each(function(){

       if(!$(this).find('td:eq(6) span').hasClass('switchery')){

        var elem = this.querySelector('.js-switch');

        Switchery(elem, { color: '#1AB394' });

        /*Image Tooltips*/

        $(".imageZoom").imageTooltip();

      }

    });

  });

  $(document).on("change","#admintable_length select", function(){

    $( "#admintable tbody tr" ).each(function(){

       if(!$(this).find('td:eq(6) span').hasClass('switchery')){

        var elem = this.querySelector('.js-switch');

        Switchery(elem, { color: '#1AB394' });

        /*Image Tooltips*/

        $(".imageZoom").imageTooltip();

      }

    });

  });

  

  /*******Switcher End********/

  /*****Status Change*****/

  $(document).on("change",".adminStatus",function(e){

      var mid = $(this).attr("value");

      var mname = $(this).closest("tr").find("td:eq(1)").html();

      var email = $(this).closest("tr").find("td:eq(2)").html();

      if (this.checked) {

        $.ajax({

          url:"<?php echo  base_url('admin/merchant/isnotApproved/'); ?>" + mid,

          beforeSend: function(){

            $("body").append(lhtml);

            },

          success: function(result){ 

              $('#loaderBg2').remove();

              if($.trim(result) != 0)

              { 

                  

                      $(this).attr("checked", "checked");

                      $(".admin-list-msg").css("display","");

                      $(".admin-flash-msg").html(mname +" now has been activated.");

                      $(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

                   

              }

              else

              {

                  $(this).removeAttr("checked");

              }

            }

          });

      }else{

        $.ajax({

          url:"<?php echo base_url('admin/merchant/isApproved/'); ?>" + mid ,

          beforeSend: function(){

            $("body").append(lhtml);

            },

          success: function(result){

            $('#loaderBg2').remove();

            if($.trim(result) != 0)

            {

              $(this).removeAttr("checked");

              $(".admin-list-msg").css("display","");

              $(".admin-flash-msg").html(mname +" now has been deactivated.");

              $(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

            }

            else

            {$(this).attr("checked", "checked");}

            }

          });

      };

  });



  $(document).on("change",".verifyStatus",function(e){

      var mid = $(this).attr("value");

      var mname = $(this).closest("tr").find("td:eq(1)").html();

      if (this.checked) {

        $.ajax({

          url:"<?php echo  base_url('admin/merchant/isnotVerified/'); ?>" + mid,

          beforeSend: function(){

            $("body").append(lhtml);

            },

          success: function(result){

            $('#loaderBg2').remove();

            if($.trim(result) != 0)

            {

              $(this).attr("checked", "checked");

              $(".admin-list-msg").css("display","");

              $(".admin-flash-msg").html(email +" has been verified.");

              $(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

            }

            else

            {$(this).removeAttr("checked");}

            }

          });

      }else{

        $.ajax({

          url:"<?php echo base_url('admin/merchant/isVerified/'); ?>" + mid ,

          beforeSend: function(){

            $("body").append(lhtml);

            },

          success: function(result){

            $('#loaderBg2').remove();

            if($.trim(result) != 0)

            {

              $(this).removeAttr("checked");

              $(".admin-list-msg").css("display","");

              $(".admin-flash-msg").html(email +" has not yet verified.");

              $(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

            }

            else

            {$(this).attr("checked", "checked");}

            }

          });

      };

  });

  

  //-------------------Merchant fees modal open and merchant data fetch using ajax--------------------------------------------

  $(document).on("click","#merchantfeeedit",function(e){

      var mid = $(this).attr("data-id");



        $.ajax({

		   dataType:'json',

          url:"<?php echo base_url('admin/merchant/getfees/'); ?>" + mid ,

          beforeSend: function(){

            $("body").append(lhtml);

            },

          success: function(result){

			

			

			

            $('#loaderBg2').remove();

			$('.merchantmodaltitle').empty();

			$('.feeamt').val('');

			

			$('.merchantmodaltitle').append(result[0].marchent_name);

			$('#feeamt').val(result[0].transaction_fee);

			$( "#feetype" ).val(result[0].transaction_fee_type);

			$( ".hiddenmid" ).val(result[0].marchent_id);

			//$( "#feetype" ).text(result[0].transaction_fee_type);

			

			

		  }

          });

      

  });

  

  

  

});

</script>