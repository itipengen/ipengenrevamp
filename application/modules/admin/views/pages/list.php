<?php if($this->session->flashdata('pagesEditMsg')){?>

<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('pagesEditMsg');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('pagesDeleteMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('pagesDeleteMsg');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('pagesInsertMsg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('pagesInsertMsg');?> </div>
  </div>
</div>
<?php }?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Page List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Pages</a> </li>
      <li class="active"> <strong>Page List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <button class="btn btn-primary" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/pages/add';"> Add New Page</button>
          <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> <a class="dropdown-toggle" data-toggle="dropdown" href="#"></a> <a class="close-link"> <i class="fa fa-times"></i> </a> </div>
        </div>
        <div class="ibox-content">
          <table class="table table-striped table-bordered table-hover " id="admintable" >
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>SLUG</th>
                <th>DESCRIPTION</th>
                <th class="center">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($result as $row) {?>
              <tr>
                <td><?php echo $row['page_id']; ?></td>
                <td><?php echo $row['page_name']; ?></td>
                <td><?php echo $row['page_slug']; ?></td>
                <td><?php echo $row['page_description']; ?></td>
                <td class="center"><a href="<?php echo base_url()?>admin/pages/edit/<?php echo $row['page_id']; ?>" title="Edit"><i class="fa fa-pencil-square-o action-btn"></i></a>&nbsp; <a href="javascript:void(0)" id="<?php echo $row['page_id']; ?>" class="pageDelete" title="Delete"><i class="fa fa-trash-o action-btn text-danger" ></i></a></td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(document).ready(function(){
	 /* Init DataTables */
	var oTable = $('#admintable').DataTable({
		"order": [[ 0, "desc" ]],
		"pageLength": 50,});
	
	});
	/*****Page Delete******/
	$(document).on('click','.pageDelete',function(){
		var pageId = $(this).attr('id');
			 if(pageId!=""){
		  bootbox.confirm("Are you sure want delete ?", function(result) {
			if(result){
			  window.location.href = "<?php echo base_url('admin/pages/delete/')?>"+ pageId ; 
			}
		  }); 
		 }
	  });
  </script>