        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Edit Pages</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li>
                            <a>admin</a>
                        </li>
                        <li class="active">
                            <strong>Edit Pages</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
       
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit Pages </h5>
                        </div>
                        <?php foreach($result as $row) {?>
                        <div class="ibox-content">
                            <?php $attributes = array('class' => 'form-horizontal', 'id' => 'adminform','method' => 'post' ,'role' =>'form');

				echo form_open('admin/pages/edit/'.$row->page_id, $attributes);?><strong></strong>
                            
                                <div class="form-group">
                                 <?php  $page_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Page Name','page_name',$page_name_label);?>

                                    <div class="col-sm-10">
                                     <?php $page_name = array('name' => 'page_name','class' => 'form-control','placeholder' => 'Page name','required'=>'required','value' => $row->page_name );?>
                    <?= form_input($page_name); ?>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                <?php  $slug_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Slug','slug',$slug_label);?>
                                    <div class="col-sm-10">
                                     <?php $slug = array('name' => 'slug','class' => 'form-control','placeholder' => 'Slug','disabled'=>'""','value' => $row->page_slug); ?>
                    <?= form_input($slug); ?>
                                    </div>
                                </div>
                                
                                <div class="hr-line-dashed"></div>
                                <div class="">
                           <h5><font size="2.75">Description</font></h5>
                        </div>
                        <div class="ibox-content no-padding">

                              <div class="pagedescription">
                               
                              </div>
                         </div>
                             
                                <!--<div class="form-group"><label class="col-lg-2 control-label">Disabled</label>

                                    <div class="col-lg-10"><input type="text" disabled="" placeholder="Disabled input here..." class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-lg-2 control-label">Static control</label>

                                    <div class="col-lg-10"><p class="form-control-static">email@example.com</p></div>
                                </div>
                                <div class="hr-line-dashed"></div>-->
                                 <?php echo $this->ckeditor->editor('content',$row->page_description);?> <font color="red"> <?php echo $this->session->flashdata('msg1');?></font>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="<?php echo base_url()?>admin/pages/pagelist"><button type="button" class="btn btn-white">Cancel</button></a>
                                         <?php echo form_submit('editsubmit','Save changes','class="btn btn-primary"');?>
                                    </div>
                                </div>
                           <?php echo form_close();?>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            
        </div>
