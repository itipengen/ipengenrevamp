<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Coupon List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Coupon</a> </li>
      <li class="active"> <strong>Coupon List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Coupon List</h5>
      </div>
      <?php if($this->session->flashdata('successMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <?php if($this->session->flashdata('errorMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <div class="ibox-content">
        <div class=""> <a href="<?= base_url('admin/coupon/add'); ?>" class="btn btn-success ">Add New</a> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Coupon Code</th>
              <th>Coupon Discount</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th class="center">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($couponlist)){ $i=0; foreach($couponlist as $list){ 
					if($list->coupon_fixed != '')
					{$value = $list->coupon_fixed;}else{$value = $list->coupon_percentage." %";}
			?>
            <tr>
              <td><?php echo ++$i; ?></td>
              <td><?php echo $list->coupon_code; ?></td>
              <td><?php echo $value; ?></td>
              <td><?php echo date("d-M-Y",strtotime($list->c_start_date)); ?></td>
              <td><?php echo date("d-M-Y",strtotime($list->c_end_date)); ?></td>
              <td><a href="<?php echo base_url("admin/coupon/edit/".$list->coupon_id); ?>" title="Coupon Edit"><i class="fa fa-pencil-square-o action-btn fa-fw"></i></a> <a href="javascript:void(0);" id="<?php echo $list->coupon_id; ?>" class="deleteCoupon" title="Coupon Delete"><i class="fa fa-trash-o fa-fw action-btn text-danger" ></i></a></td>
            </tr>
            <?php  } }else{ ?>
            <tr>
              <td colspan="6" align="center"><h2>No data available</h2></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
   	$('#admintable').DataTable({
		order:[[0,"desc"]],
		pageLength: 25,
	});
	$(document).on('click','.deleteCoupon',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want to delete?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/coupon/delete/"+catid; 
				}
			}); 
		 }
	});

});
</script>