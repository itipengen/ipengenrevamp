<style>
.fileinput-button {
	display: inline-block;
	overflow: hidden;
	position: relative;
}
input[type="file"] {
	display: none;
}
</style>
<?php
	$optionCat[""] = "Choose Category";
	foreach ($result1 as $value) {
	 	$optionCat[$value->id] = ucfirst($value->name);
	}
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Edit Category</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Category</a> </li>
      <li class="active"> <strong>Edit category</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('successMsg')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
    </div>
  </div>
  <?php }?>
  <?php $attributes = array('class' => 'form-horizontal', 'id' => 'categoryform','method' => 'post' ,'role' =>'form');
				echo form_open_multipart('', $attributes);?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#genaral">Genaral</a></li>
            <li><a data-toggle="tab" href="#data">Data</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div id="genaral" class="tab-pane fade in active">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <ul class="nav nav-tabs">
                <?php $classAc="";$j=0;
                     if(!empty($result)){
						
					 foreach($result as $val){
						 
						 if($j==0){$classAc="active";}else{$classAc="";}?>
                <li class="<?php echo $classAc; ?>"><a data-toggle="tab" href="#<?php echo $val->lang_name;?>"><?php echo ucfirst($val->lang_name);?></a></li>
                <?php $j++;}} ?>
                <!--<li><a data-toggle="tab" href="#barsha">Barsha</a></li>-->
              </ul>
            </div>
          </div>
          <div class="tab-content">
            <?php $class="";$i=0;
					if(!empty($result)){
						
					 foreach($result as $val){?>
            <?php 
					if($i==0){$class=" in active";}else{$class="";}
					
					?>
            <div id="<?php echo $val->lang_name;?>" class="tab-pane fade <?php echo $class;?>">
              <input type="hidden" value="<?php echo $val->language_id;?>" name="language[]" />
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h5>Edit Category </h5>
                </div>
                <div class="ibox-content"> <strong></strong>
                  <div class="form-group">
                    <?php  $category_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Category Name','category_name',$category_name_label);?>
                    <div class="col-sm-10">
                      <?php $category_name = array('name' => 'name[]','class' => 'form-control','placeholder' => 'Category name','required'=>'required','value' => $val->name); ?>
                      <?= form_input($category_name); ?>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <?php  $category_des_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Category Description','last_name',$category_des_label);?>
                    <div class="col-sm-10">
                      <?php $category_des = array('name' => 'description[]','class' => 'form-control','placeholder' => 'Category description','required'=>'required');
                    
                    echo form_textarea('description[]', $val->description,  $category_des); ?>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                </div>
              </div>
            </div>
            <?php $i++; }} ?>
          </div>
        </div>
        <div id="data" class="tab-pane fade">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>edit Category data</h5>
            </div>
            <div class="ibox-content">
              <div class="form-group">
                <div class="col-md-3">
                  <?php  $category_parent_label = array('class'=>'control-label');
                                        echo  form_label('Parent Category','last_name',$category_parent_label);?>
                </div>
                <div class="col-md-9">
                  <?php echo form_dropdown('parent_id',$optionCat,$parentId,['class'=>'form-control categorySelect']); ?>
                  </datalist>
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <div class="form-group">
                <div class="col-md-8">
                  <div class="col-md-3">
                    <?php  $category_banner_label = array('class'=>'control-label');
                                        echo  form_label('Category Image','',$category_banner_label);?>
                  </div>
                  <div class="col-md-9"> <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i>
                    <label for="category-banner" style="cursor: pointer;">Browse...</label>
                    <input type="file" multiple name="banner" id="category-banner">
                    </span> 
                    <!--<input type="file" name="banner" id="category-banner" class="form-control" />--> 
                  </div>
                </div>
                <div class="col-md-4">
                  <?php 
                                            if (!empty($result)) {
                                                foreach ($result as $value) {
                                                    $imageName = $value->banner;
                                                    $categoryId = $value->id;
                                                }
                                            }
                                            if ($imageName != '') { ?>
                  <img src="<?= base_url('photo/category/'.$categoryId.'/'.$imageName); ?>" width="100" height="100" class="img-circle"/>
                  <?php   } ?>
                </div>
              </div>
              <div class="hr-line-dashed"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
      <?php echo form_submit('submit','Save changes','class="btn btn-primary"');?> </div>
  </div>
  <?php echo form_close();?> </div>
<script>
$(document).ready(function() {
	$(".categorySelect").select2({
			placeholder:"Type Name..",
		});
    $('.catnameid').on('click',function(){
    	$('#dLabel').html($(this).text()+'<span class="caret"></span>');
    	$('#parent_id').val($(this).attr('id'));
    	});
    $('#category_data').focusout(function(){
        var val = $('#category_data').val();
        var cat = $("#categoryList option").filter(function() {return this.value == val;}).data('xyz');
        $('#parent_id').val(cat);
    });
});
		</script> 
