<style>
.wrapper-content {
    background: #fcfcfc none repeat scroll 0 0;
    margin-top: 11px;
    padding: 10px;
}
.wrapper-content {
    background: #fcfcfc none repeat scroll 0 0;
    margin-top: 11px;
    padding: 10px;
}
.textheader {
    background: #18a78a none repeat scroll 0 0;
    color: #fff;
    font-size: 21px;
    font-weight: 600;
    text-align: center;
    width: 100%;
}
.noimageupload {
    background: #edebeb none repeat scroll 0 0;
    padding: 11px;
    text-align: center;
}
img{
	width:100%;
	height:250px !important;
	
	}
	input[type="file"] {
  
    float: left;
    width: 70%;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add No Images</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>

      <li class="active"> <strong>No Image</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('error_msg')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('error_msg');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->userdata('error_message')){ ?>
	<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <?php print_r($this->session->userdata('error_message')); ?>
</div>
	
<?php } ?>

<?php if($this->session->userdata('message_success')){ ?>
<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Image Uploaded.
</div>

	
	
<?php } ?>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="">
<div class="row">
<div class="col-md-4">
<div class="textheader">Event</div>
<div id="noimageupload1" class="noimageupload">
<img src="<?php echo base_url().'photo/eventcategory/no-event.png'; ?>" class="img-thumbnail" />
	<?php 
		$uploaddata = array('name'=>'addimage','id'=>'addimage');
		echo form_open_multipart('admin/Noimage/imageupload',$changepasswordform);
	
	 ?>
  <input type="hidden" name="image" value="1" />
  <input type="file" name="userfile" id="imgInp1">
  <input type="submit" class="btn btn-primary" value="Upload">
<?php echo form_close(); ?>
</div>
</div>

<div class="col-md-4">
<div class="textheader">Wishlist</div>
<div id="noimageupload2" class="noimageupload">
<img src="<?php echo base_url().'photo/wishlist/no-event.png'; ?>" class="img-thumbnail" />
<?php 
		$uploaddata = array('name'=>'addimage','id'=>'addimage');
		echo form_open_multipart('admin/Noimage/imageupload',$changepasswordform);
	
	 ?>
<input type="hidden" name="image" value="2" />
  <input type="file" name="userfile" id="imgInp2">
   <input type="submit" class="btn btn-primary" value="Upload">
<?php echo form_close(); ?>
</div>
</div>

<div class="col-md-4">
<div class="textheader">Product</div>
<div id="noimageupload3" class="noimageupload">
<img src="<?php echo base_url().'photo/product/no_product.jpg'; ?>" class="img-thumbnail" />
<?php 
		$uploaddata = array('name'=>'addimage','id'=>'addimage');
		echo form_open_multipart('admin/Noimage/imageupload',$changepasswordform);
	
	 ?>
   <input type="hidden" name="image" value="3" />
  <input type="file" name="userfile" id="imgInp3">
  <input type="submit" class="btn btn-primary" value="Upload">
<?php echo form_close(); ?>

</div>
</div>
</div>


</div>


</div>
<script type="text/javascript">

	$("#imgInp1").change(function(){
	if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

			$("#noimageupload1 img").attr('src', e.target.result);
		}

        reader.readAsDataURL(this.files[0]);
    }});
	$("#imgInp2").change(function(){
	if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

			$("#noimageupload2 img").attr('src', e.target.result);
		}

        reader.readAsDataURL(this.files[0]);
    }});
	
	
	$("#imgInp3").change(function(){
	if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

			$("#noimageupload3 img").attr('src', e.target.result);
		}

        reader.readAsDataURL(this.files[0]);
    }});
</script>