<style>
textarea#remarks {
	width: 420px;
	height: 100px;
	border: 3px solid #cccccc;
	padding: 5px;
	font-family: Tahoma, sans-serif;
	background-position: bottom right;
	background-repeat: no-repeat;
}
.genPDF {
	background-color: #1a7bb9;
	border-color: #1a7bb9;
	color: #fff;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Order Details</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a href="<?php echo base_url();?>admin/order/list">Order</a> </li>
      <li class="active"> <strong>Details</strong> </li>
    </ol>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <div class="row">
          <div class="col-md-9"><span style='font-size:20px;'><b> INVOICE ID - <?php echo $getInvoiceDetails[0]->invoice; ?></b> <a href="<?php echo base_url();?>admin/invoice/downloadpdf/<?php echo $getInvoiceDetails[0]->invoice; ?>" title="Download invoice" target="_blank"> <i class="fa fa-file-pdf-o" style="color:#f00"></i> </a> </span> </div>
          <div class="col-md-3"> <span style='font-size:20px;'><b>TOTAL - <?php echo strtoupper($this->config->item('currency')).' '.number_format($getInvoiceDetails[0]->total_amount) ?></b></span> </div>
        </div>
      </div>
      <div class="ibox-content">
        <?php  
						$totalPrice=array();
						if(!empty($getInvoiceProduct['data'])){
							foreach($getInvoiceProduct['data'] as $order){
								//$query = $this->db->get_where('ig_marchent_login',array('marchent_id'=>$order->merchant_id)); 
								//$result = $query->result();
								/*$full_name = "";
								if (!empty($result)) {
								foreach ($result as $value) {*/
								$totalPrice[]=(int)$order['total']-(int)$order['transaction_amt'];
								$full_name = ucfirst($order['marchent_name']);
								/*
								}
								}*/
							?>
        <div class="row">
          <div class="col-md-2">
            <?php 
							if(is_file($this->config->item('image_path').'/product/'.$order['product_id']."/450X450/".$order['product_image']))
							{ $imageURL = base_url()."photo/product/".$order['product_id']."/450X450/".$order['product_image']; } 
							else
							{$imageURL = base_url('photo/product/no_product.jpg');}
						?>
            <img src="<?php echo $imageURL; ?>"  class="img-responsive" width="100px" height="100px"/> </div>
          <div class="col-md-5">
            <div><?php echo $order['name'];?></div>
            <?php if($order['item_type']!='cash_gift'){ ?>
            <div>Quantity : <?php echo $order['quantity']; ?></div>
            <?php } ?>
            <div> Fee :
              <?php  echo strtoupper($this->config->item('currency')).' '.$order['transaction_amt'];?>
            </div>
            <div> Order Id :
              <?php  echo $order['order_id']; ?>
            </div>
            <div class="statusmsg" style="display:none"></div>
            <input type="hidden" value="<?php echo $order['order_id']?>" name="productid" id="oID" />
            <input type="hidden" value="<?php echo $order['product_id']?>" name="productid" id="pID" />
          </div>
          <div class="col-md-3"> </div>
          <div class="col-md-2"><?php echo strtoupper($this->config->item('currency')).' '.number_format($order['total']); ?></div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php }
						
						
						
						} 
						$priceSum=array_sum($totalPrice);
						//var_dump($summ);
						
						?>
        <div class="row">
          <div class="col-md-10"> <b>Total Fee</b> </div>
          <div class="col-md-2"> -<?php echo strtoupper($this->config->item('currency')).' '.$getInvoiceDetails[0]->total_transaction_fee; ?> </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="row">
          <div class="col-md-3"><b> Merchant Name</b> </div>
          <div class="col-md-9"> <?php echo $getInvoiceDetails[0]->marchent_name; ?> (#<?php echo $getInvoiceDetails[0]->merchant_id; ?>) </div>
        </div>
        <div class="hr-line-dashed"></div>
        
        <!-- <div class="row">
                        
                        <div class="col-md-3"><b> Total Amount</b> </div>
                        <div class="col-md-9"> <?php echo strtoupper($this->config->item('currency')).' '.number_format($getInvoiceDetails[0]->total_amount) ?> </div>
                        </div>-->
        <div class="hr-line-dashed"></div>
        <div class="row">
          <div class="col-md-3"><b> Invoice Date</b> </div>
          <div class="col-md-9"> <?php echo $getInvoiceDetails[0]->date; ?> </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="row">
          <div class="col-md-3"> <b>Invoice Status</b> </div>
          <div class="col-md-9"> <?php echo $getInvoiceDetails[0]->is_paid==0 ? 'Pending' : 'Paid';  ?> </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php 
							if($getInvoiceDetails[0]->is_paid==1){
						?>
        <div class="row">
          <div class="col-md-3"> <b>Payment Date</b> </div>
          <div class="col-md-9"> <?php echo $getInvoiceDetails[0]->payment_date; ?></div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php 
							}
						?>
        <div class="row">
          <div class="col-md-3"> <b>Remarks</b> </div>
          <div class="col-md-9">
            <textarea placeholder="Type Payment Remarks here" id="remarks" class="" rows="4" cols="50" maxlength="200"><?php echo $getInvoiceDetails[0]->remarks=='' ? '' : $getInvoiceDetails[0]->remarks; ?></textarea>
          </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="row">
          <div class="col-md-3">
            <button class='btn btn-primary' id='invoicePay'>Pay</button>
          </div>
          <div class="col-md-9"></div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins admin-flash-msg" id='msgDiv' style='display:none;'> </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      
      <!-- 
                        <div style='background-color: #ffffff;border-color: #e7eaec;border-image: none;border-style: solid solid none;border-width: 1px 0;color: inherit;padding: 15px 20px 20px;'><button class='btn btn-transparent btn-outline btn-sm active genPDF' id='generatePdf' onclick="javascript:demoFromHTML();">Generate PDF</button></div>-->
      
      <?php /*?><div id='generatePdfDiv'>
                            <div class="container">
    	                        <p><h2><b>Ipengen</b></h2></p><hr>
	                            <p><h4>INVOICE ID - <?php echo $getInvoiceDetails[0]->invoice; ?></h4></p>
                                   
                            </div>
							<div class="container">
                            	<div class="row">
                            	<div class="col-md-12">
                                		<table id="tab_customers" class="table table-striped">
                                    	        <thead>
                                        			<tr class=''>
                                            			<th>i</th>
                                            			<th>k</th>
                                                    </tr>
            			                        </thead>
                        			            <tbody>
                                                <?php  
													$totalPrice=array();
													if(!empty($getInvoiceProduct['data'])){
														foreach($getInvoiceProduct['data'] as $order){
															$totalPrice[]=(int)$order['total']-(int)$order['transaction_amt'];
															$full_name = ucfirst($order['marchent_name']);
												?>
                                    			    <tr>
                                            			<td class="text-left">
                                                        <?php 
															if(is_file($this->config->item('image_path').'/product/'.$order['product_id']."/450X450/".$order['product_image']))
															{ $imageURL = base_url()."photo/product/".$order['product_id']."/450X450/".$order['product_image']; } 
															else
															{$imageURL = base_url('photo/product/no_product.jpg');}
														?>
                                                        <img src="http://staging.ipengen.com/photo/product/3049/450X450/3049_1483422499.jpeg"  width="100px" height="100px"/>
                                            			</td>
                                            			<td class="text-left">
                                           	                <div style="float:left; width:100%;"><?php echo $order['name'];?></div>
															<?php if($order['item_type']!='cash_gift'){ ?>
                                                            <div>Quantity : <?php echo $order['quantity']; ?></div>
                                                            <?php } ?>
                                                            <div>
                                                                Fee : 
                                                                <?php  echo strtoupper($this->config->item('currency')).' '.$order['transaction_amt'];?>
                                                            </div>
                                                             <div>
                                                                Order Id : 
                                                                <?php  echo $order['order_id']; ?>
                                                            </div>
                                                            <div>
                                                                Price : 
                                                                <?php echo strtoupper($this->config->item('currency')).' '.number_format($order['total']); ?>
                                                            </div>
			                                            </td> 
                                                                                      
                                        			</tr>
                                                    <?php } } ?>
                                    			</tbody>
                                			</table>
                                		<?php
										
										?>
                            		</div>
                            	</div>
                                <div class="row">
                            	<div class="col-md-12">
                                <p><h4>TOTAL - <?php echo strtoupper($this->config->item('currency')).' '.number_format($getInvoiceDetails[0]->total_amount) ?></h4></p>
                                </div>
								</div>
                            </div>
                                
                        	           
                        </div><?php */?>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.debug.js"></script> 
<script type="text/javascript">
function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter','div');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#generatePdfDiv')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 50,
        bottom: 60,
        left: 40,
        width: 800
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers 
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);
}
$(document).ready(function(){
	var base_url='http://ipengen.com/';
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
  	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
	$('#invoicePay').on('click',function(){
		$('#msgDiv').css('display','none');
		var remarks=$('#remarks').val()
		var invoiceId = <?php echo $getInvoiceDetails[0]->invoice; ?>;
		var merchantId = <?php echo $getInvoiceDetails[0]->merchant_id; ?>;
		if(remarks=='' || invoiceId=='' || merchantId==''){
			$('#msgDiv').css('display','block');
			$('#msgDiv').html('Remarks should not be empty.');
		}else{
			$.ajax({
			type: "POST",
			//url: "http://localhost/ipengen/wishlist/create_wishlist",
			url: base_url+'admin/invoice/invoicePay',
			data: { invoiceId: invoiceId, merchantId: merchantId,remarks:remarks },
			cache: false,
			dataType: 'json',
			beforeSend: function(){
            	$("body").append(lhtml);
            },              
			success: function(res){
				$('#loaderBg2').remove();
				var resmessage=res.result.message;
				$('#msgDiv').css('display','block').html('').html(resmessage);
				
				//console.log(res);
			}	
		});
		}
	});	
});

</script> 
