<style>
#searchBtn{background-color: #1a7bb9;border-color: #1a7bb9;color: #fff;}
.invBtn,.xlsBtn{background-color: #1a7bb9;border-color: #1a7bb9;color: #fff}

.spanSpace {padding-right: 10%;}
.dt-buttons a {margin-right: 5px;}
.dt-buttons {padding-bottom: 1%;}
</style>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Invoice</h1>
                </div>
                <!-- END PAGE TITLE -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row admin-list-msg">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins admin-flash-msg" id='msgDiv' style='display:none;'>  </div>
                    </div>
                </div>      
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div>
                            <h4>Generate Invoice</h4><br/>
                            <div class="row" style="margin:15px 0;">
                                <div class="form-group">
                                    <label class="col-xs-1">   Merchant:</label>
                                    <div class="col-xs-5">
                                        <select id='merchantList'>
                                            <option value=''>Select Merchant</option>
                                            <?php 
											//print_r($merchant);die();
											if(!empty($merchant)){
                                                foreach($merchant as $key=>$val){	
                                            ?>
                                            <option value='<?php echo $key;?>'><?php echo $val;?></option>
                                            <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" style="margin:15px 0;">                 
                                <div class="form-group">
                                    <label class="col-xs-1">   From:</label>
                                    <div class="col-xs-2"><input type="text" id="datepickerFrom" class='form-control input-sm input-inline'></div>       
                                    <label class="col-xs-1">To: </label>
                                    <div class="col-xs-2">  <input type="text" id="datepickerTo" class='form-control input-xs input-sm input-inline '></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" style="margin:15px 0;">
                                <div class="form-group"><div class="col-xs-1">
                                    <button class='btn btn-transparent btn-outline  btn-sm active invBtn' id='InvoiceGenerate'>Generate</button>
                                </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
 	<div class="row">
    	<div class="col-md-12">
        	<!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet light ">
            	<div class="portlet-title">
					<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
    <thead>
        <tr role="row" class="heading">
            <th width="15%"> Start Date </th>
            <th width="15%"> End Date </th>
            <th width="10%"> Search </th>
        </tr>
        <tr role="row" class="filter">
            
            <td>
                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control form-filter input-sm date1" readonly name="date1"  id='orderFrom' placeholder="From">
                    <!--<span class="input-group-btn">
                        <button class="btn btn-sm default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>-->
                </div>
            </td><td>    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control form-filter input-sm date2" readonly name="date2" id='orderTo' placeholder="To">
                    <!--<span class="input-group-btn">
                        <button class="btn btn-sm default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>-->
                </div>
            </td>
            
            <td>
                <div class="margin-bottom-5">
                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom" id='searchBtn'>
                        <i class="fa fa-search"></i> Search</button>
             </td>
        </tr>
    </thead>
    <tbody> </tbody>
</table>
				</div>
            </div>
        </div>
    </div>
	<div class="ibox-content">
    <div><button class='btn btn-transparent btn-outline  btn-sm active xlsBtn' id='excelBtn' style='margin-bottom:10px;'>Excel</button></div>
        <table class="table table-striped table-bordered table-hover colreorder_1" id="invoicelistTable111" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Invoice ID</th>
              <th>Merchant Name</th>
              <th>Date</th>
              <th>Status</th>
              <th>Total Price</th>
              <th>View</th>
               <td>Remarks</td>
            </tr>
          </thead>
          
       </table>
     </div> 
	</div>
</div>                                       
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/datatable.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/datatables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/datatables.bootstrap.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.table2excel.js"></script>
<script>
$(document).ready(function(){
	//var  base_url='http://staging.ipengen.com/';
	$( ".dt-buttons a:nth-child(1)" ).css('display','none');
	$( ".dt-buttons a:nth-child(2)" ).removeClass('green').addClass('label-success');
	$( ".dt-buttons a:nth-child(3)" ).removeClass('purple').addClass('label-success');
	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';
	$("#merchantList").select2({
			placeholder:"Type Name..",
		});
	$('#orderFrom,#orderTo,#datepickerFrom,#datepickerTo').datepicker({ dateFormat: 'yy-mm-dd' });
	//$('#invoicelistTable').DataTable().destroy();
	
	
	var base_url = window.location.origin;
	
	
	$('#invoicelistTable111').dataTable({
		"processing": true,
		"serverSide": true,
		"aaSorting": [[ 0, "desc" ]],
		"buttons": [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
		
		"ajax":{
								url :base_url+'/admin/invoice/ajax_invoice_list', // json datasource
								type: "post",  // method  , by default get
								
								
								error: function (){ 
								
									$(".employee-grid-error").html("");
									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
									$("#employee-grid_processing").css("display","none");
								
								
								
								 }
		
								
							}
		
		
		
		
		});
	
	$('#excelBtn').on('click',function(){
		$("#invoicelistTable111").table2excel({
					name: "Invoice List",
					filename: "Invoice",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
	});
	$('#searchBtn').on( 'click', function () { 
		
		var date1 = $('.date1').val();
		var date2 = $('.date2').val();
		
		var data = {date1 : date1, date2 : date2};
		$('#invoicelistTable111').dataTable().fnDestroy();
		$('#invoicelistTable111').dataTable({
		"processing": true,
		"serverSide": true,
		"aaSorting": [[ 0, "desc" ]],
		
		"ajax":{
								url :base_url+'/admin/invoice/ajax_invoice_list', // json datasource
								//var data = {date1 : date1, date2 : date2};
								type: "post",  // method  , by default get
								data: {
									'date1': date1,
									'date2': date2
									
									},
								
								
								error: function (){ 
								
									$(".employee-grid-error").html("");
									$("#invoicelistTable111").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
									$("#employee-grid_processing").css("display","none");
								
								
								
								 }
		
								
							}
		
		
		
		
		});
		setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));
  $(".imageZoom").imageTooltip();

 $(".switchery").remove();
  elems.forEach(function(data) {
   
    var switchery = new Switchery(data,{ color: '#1AB394' });
  }); }, 3000);



			/* $.ajax({
				  type: 'POST',
				  url: base_url+'/admin/wishlist/ajaxwishlist',
				  data: data,
				  dataType: "json",
				  success: function(resultData) {
						Table.fnClearTable();
						Table.fnDraw(resultData);
						
					   //	Table.fnDraw(resultData); // Add new data
					  //Table.columns.adjust().draw(); // Redraw the DataTable
					
					  
				  }
			})*/
		
        /*
 
        counter++;*/
	});
	
	
	// invoice datatable assign..................
	//$('#invoicelistTable').DataTable();
	
	// filter from date1 to date2 ........................................
	$(document).on('click','#searchBtn',function(){/*
		$('#invoicelistTable').DataTable().destroy();
		var dateFrom=$('#orderFrom').val();
		var dateTo=$('#orderTo').val();
		$('#orderFrom,#orderTo').css('border-color','#eee');
		if(dateFrom=='' || dateTo==''){
			if(dateFrom==''){
				$('#orderFrom').css('border-color','red');
			}
			if(dateTo==''){
				$('#orderTo').css('border-color','red');
			}	
		}
		else{
			$("body").append(lhtml);
			//var data = {date1 : dateFrom, date2 : dateTo};
			$.ajax({
			type: "POST",
			//url: "http://localhost/ipengen/wishlist/create_wishlist",
			url: base_url+'admin/invoice/getFilteredInvoice/',
			data: { dateFrom: dateFrom, dateTo: dateTo },
			cache: false,
			dataType: 'json',
			success: function(result){
				//console.log(result);
				$('#loaderBg2').remove();
				
				$('#invoicelistTablebody').html('');
				
				var i=1;
				$.each(result,function(key,val){
					if(val.status==0){
						var status='Pending';	
					}
					else{
						var status='Paid';	
					}
					$('#invoicelistTablebody').append('<tr><td>'+i+'</td><td>'+val.invoice+'</td><td>'+val.marchent_name+'</td><td>'+val.date+'</td><td>'+val.amount+'</td><td>'+status+'</td><td><a href="invoice/details/'+val.id+'/'+val.prefix+val.invoice+'" title="Order Details"><i class="fa fa-eye fa-fw " style="color:#508DF2"></i></a></td><td>'+val.remarks+'</td></tr>');
					i++;
				})
				//$('#invoicelistTable').DataTable();
				
			}	
		});	
		}
		
		
	*/});
	
	$(document).on('click','#InvoiceGenerate',function(){
		
		var fromDate=$('#datepickerFrom').val();
		var toDate=$('#datepickerTo').val();
		var merchantId=$('#merchantList').val();
		$('#merchantList,#datepickerTo,#datepickerFrom').css('border-color','#676a6c')
		if(merchantId=='' || toDate==''||fromDate==''){
			if(merchantId==''){$('#merchantList').css('border-color','red')}
			if(toDate==''){$('#datepickerTo').css('border-color','red')}
			if(fromDate==''){$('#datepickerFrom').css('border-color','red')}
		}
		else{
			$("body").append(lhtml);
			$.ajax({
			type: "POST",
			//url: "http://localhost/ipengen/wishlist/create_wishlist",
			url: base_url+'/admin/invoice/generateInvoice/',
			data: { fromDate: fromDate, toDate: toDate,merchantId: merchantId },
			cache: false,
			dataType: 'json',
			success: function(result){
				
				$('#msgDiv').css('display','block');
				$('#msgDiv').html(result.msg);
				$('#loaderBg2').remove();
				console.log(result);
				
				
			}	
		});	
		}
		
		
	});
});
</script>