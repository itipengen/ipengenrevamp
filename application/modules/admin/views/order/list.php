<style> 
	.xlsBtn{margin-left:5px;background-color: #1a7bb9;border-color: #1a7bb9;color: #fff;}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Order List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin")?>">Home</a> </li>
      <li> <a>Order</a> </li>
      <li class="active"> <strong>Order List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('sussess_message')!=""){ ?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('sussess_message');?> </div>
  </div>
</div>
<?php } if($this->session->flashdata('error_message')!=""){ ?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
  </div>
</div>
<?php	}	?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class=" float-e-margins admin-flash-msg"> </div>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title"> </div>
        <div class="ibox-content">
        <div>
        	<!--<span><button class='btn btn-success xlsBtn' id='excelBtn'>Excel</button></span>-->
        </div>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover " id="admintable">
              <thead>
                <tr>
                  <th>Order Id</th>
                  <th>Client Name</th>
                  <th>Total Amount</th>
                  <th>Order Date</th>
                 
                  <th>Order Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.table2excel.js"></script>
<script>
$(document).ready(function(e) {
    /*var oTable = $('#admintable').DataTable({
			order:[[0,"desc"]],
			pageLength: 50,
		});
		*/
	var base_url = window.location.origin;
	var dataTable = $('#admintable').DataTable( {
					"processing": true,
					"serverSide": true,
					"aaSorting": [[0,'DESC']],
					"ajax":{
						url :base_url+'/admin/order/ajax_order_list', // json datasource
						type: "post",  // method  , by default get
						error: function (){ 
						
							$(".employee-grid-error").html("");
							$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
						
						
						
						 }

						
					}
				} );
	$(document).on('click','#excelBtn',function(){
		$("#admintable").table2excel({
			name: "Product List",
			filename: "Product",
			fileext: ".xls",
			exclude_img: true,
			exclude_links: true,
			exclude_inputs: true
		});	
	});
				
});
</script> 
