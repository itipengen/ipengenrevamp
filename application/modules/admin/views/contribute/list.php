<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Contribution List Panel</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Contribution</a> </li>
      <li class="active"> <strong>Contribution List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Contribution List</h5>
      </div>
      <?php if($this->session->flashdata('successMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('successMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <?php if($this->session->flashdata('errorMsg')){?>
      <div class="row admin-list-msg">
        <div class="col-lg-12">
          <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('errorMsg');?> </div>
        </div>
      </div>
      <?php }?>
      <div class="ibox-content">
        <div class=""> <!--<a href="<? //= base_url('admin/coupon/add'); ?>" class="btn btn-success ">Add New</a>--> </div>
        <table class="table table-striped table-bordered table-hover " id="admintable" >
          <thead>
            <tr>
				<th>SL. NO.</th>
                <th>Order No</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Wishlist Name</th>
                <th>Wishlist Owner</th>
                <th>Total Amount</th>
				<th class="no-sort">Cont. Amount</th>
				
            </tr>
          </thead>
          
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
	var base_url = window.location.origin;
	$('#admintable').dataTable({
		"processing": true,
		
		"serverSide": true,
		"aaSorting": [[ 0, "desc" ]],
		"columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
   		 } ],
		
		"ajax":{
								url :base_url+'/admin/contribute/ajax_contribution_list', // json datasource
								//var data = {date1 : date1, date2 : date2};
								type: "post",  // method  , by default get
								
								
								error: function (){ 
								
									$(".employee-grid-error").html("");
									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
									$("#employee-grid_processing").css("display","none");
								
								
								
								 }
		
								
							}
		
		
		
		
		});
	
  
	/*$(document).on('click','.deleteCoupon',function(){
		var catid=$(this).attr('id');
		 if(catid!=""){
			bootbox.confirm("Are you sure you want delete this Category?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url()?>admin/coupon/delete/"+catid; 
				}
			}); 
		 }
	});*/
	$(".imageZoom").imageTooltip();

});
</script>