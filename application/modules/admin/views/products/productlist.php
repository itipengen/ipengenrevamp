<style> 

	.xlsBtn{margin-left:5px;}

</style>



<div class="row wrapper border-bottom white-bg page-heading">

  <div class="col-lg-10">

    <h2>Product List</h2>

    <ol class="breadcrumb">

      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>

      <li> <a>Products</a> </li>

      <li class="active"> <strong>Products List</strong> </li>

    </ol>

  </div>

  <div class="col-lg-2"> </div>

</div>

<?php if($this->session->flashdata('productsuccessMsg')){?>

<div class="row admin-list-msg">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('productsuccessMsg');?> </div>

  </div>

</div>

<?php }?>

<?php if($this->session->flashdata('producterrorMsg')){?>

<div class="row admin-list-msg">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('producterrorMsg');?> </div>

  </div>

</div>

<?php }?>

<div class="row admin-list-msg" style="display:none">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg"> </div>

  </div>

</div>

<div class="row">

  <div class="col-lg-12">

    <div class="ibox float-e-margins">

      <div class="ibox-title">

        <h5>Product List </h5>

      </div>

      <div class="ibox-content"> <span><a href="<?= base_url('admin/product') ?>" class="btn btn-success">Add New</a></span>

      <span><button class='btn btn-success xlsBtn' id='excelBtn'>Excel</button></span>

        <table class="table table-striped table-bordered table-hover " id="admintable" >

          <thead>

            <tr>

               <th>Product ID</th>

              <th>Product Name</th>

              <th>Merchant Name</th>

              <th>Stock Quantity</th> 

              <th>Product Price</th>

              <th>Sale Price</th>

              <th>Image</th>

              <th>Feature</th>

              <th>Status</th>

              <th>View</th>

              <th class="center">Actions</th>

             

            </tr>

          </thead>

          

        </table>

      </div>

    </div>

  </div>

</div>

<script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.table2excel.js"></script>

<script>

$(document).ready(function(){

	var base_url = window.location.origin;

	var loader = "<?php echo base_url('assets/frontend/img/ripple.gif'); ?>";

	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';

	/* Init DataTables */

	var dataTable = $('#admintable').DataTable( {

					"processing": true,

					"serverSide": true,

					"aaSorting": [[0,'DESC']],

					"ajax":{

						url :base_url+'/admin/product/ajaxproductlist', // json datasource

						type: "post",  // method  , by default get

						error: function (){ 

						

							$(".employee-grid-error").html("");

							$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

							$("#employee-grid_processing").css("display","none");

						

						

						

						 }

				

					}

				} );

	

	$(document).on('click','#excelBtn',function(){

		$("#admintable").table2excel({

			name: "Product List",

			filename: "Product",

			fileext: ".xls",

			exclude_img: true,

			exclude_links: true,

			exclude_inputs: true

		});	

	});

	 

	

	

	

	

	

	

	$(".imageZoom").imageTooltip();

  /****Switcher****/

  // For Single Checkbox

  // var elem = document.querySelector('.js-switch');

  // Switchery(elem, { color: '#1AB394' });

  // For multiple Checkbox

 var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

  // console.log(elems);

 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  });

  /***after Click on Pagination Link*******/

$(document).on('click', '.paginate_button a', function(){

	$( "#admintable tbody tr" ).each(function() {

		 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){

			var elem = this.querySelector('.js-switch');

		 	Switchery(elem, { color: '#1AB394' });

			var elem_new = this.querySelector('.js_switch_feature');

		 	Switchery(elem_new, { color: '#1AB394' });	

			$(".imageZoom").imageTooltip();

		 }

	});

	

	setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

  $(".imageZoom").imageTooltip();



 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  }); }, 3000);

	

});



$(document).on("keyup",".dataTables_filter input", function(){

	$( "#admintable tbody tr" ).each(function() {

		 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){

			var elem = this.querySelector('.js-switch');

		 	Switchery(elem, { color: '#1AB394' });

			var elem_new = this.querySelector('.js_switch_feature');

		 	Switchery(elem_new, { color: '#1AB394' });	

			$(".imageZoom").imageTooltip();

		 }

	});

});



$(document).on("change","#admintable_length select", function(){

	$( "#admintable tbody tr" ).each(function() {

		 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){

			var elem = this.querySelector('.js-switch');

		 	Switchery(elem, { color: '#1AB394' });

			var elem_new = this.querySelector('.js_switch_feature');

		 	Switchery(elem_new, { color: '#1AB394' });	

			$(".imageZoom").imageTooltip();

		 }

	});

});

	



  /*****Product Delete******/

$(document).on('click','.product_delete',function(){

    var product_id = $(this).attr('id');

         if(product_id!=""){

      bootbox.confirm("Are you sure want delete ?", function(result) {

        if(result){

          window.location.href = '<?php echo site_url("admin/product/delete/")?>'+product_id ; 

        }

      }); 

     }

  });

 /*****Product Status Change*****/

$(document).on("change",".js-switch",function(e){

	  var productId = $(this).attr("value");

	  var ProName = $(this).closest("tr").find("td:eq(1)").html();

	  if (this.checked) {

		  $.ajax({

			  url:"<?php echo base_url('admin/product/status/'); ?>" + productId + "/1",

			  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).attr("checked", "checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" activated.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).removeAttr("checked");}

				  }

			  });

	  }else{

		  $.ajax({

			  url:"<?php echo base_url('admin/product/status/'); ?>" + productId + "/0",

			   beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).removeAttr("checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" deactivated.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).attr("checked", "checked");}

				  }

			  });

	  };

});



/*****Product Feature Status Change*****/

$(document).on("change",".js_switch_feature",function(e){

	  var productId = $(this).attr("value");

	  var ProName = $(this).closest("tr").find("td:eq(1)").html();

	  if (this.checked) {

		  $.ajax({

			  url:"<?php echo base_url('admin/product/feature_status/'); ?>" + productId + "/1",

			  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).attr("checked", "checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" will display on homepage.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).removeAttr("checked");}

				  }

			  });

	  }else{

		  $.ajax({

			  url:"<?php echo base_url('admin/product/feature_status/'); ?>" + productId + "/0",

			  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).removeAttr("checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" will hide from homepage.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).attr("checked", "checked");}

				  }

			  });

	  };

});

setTimeout(function(){ 

 $(".imageZoom").imageTooltip();



var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

   console.log(elems);

 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  }); }, 6000);



});

</script> 

