<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Server Image List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li class="active"> <strong>Media Images</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<?php if($this->session->flashdata('success_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('success_message');?> </div>
  </div>
</div>
<?php }?>
<?php if($this->session->flashdata('error_message')){?>
<div class="row admin-list-msg">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
  </div>
</div>
<?php }?>
<div class="row admin-list-msg" style="display:none">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> </div>
  </div>
</div>
<div class="row" style="margin-top:20px;">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-content">
        <form  action="<?php echo base_url()?>admin/media" method="post" name="deletemedia" onSubmit="return ValidateForm();" >
          <div class="row">
            <div class="col-md-6"> <span class="caption-subject font-red sbold uppercase">Media Lists</span> </div>
            <div class="col-md-6 text-right">
              <input type="submit" class="btn btn-danger" name="delsubmit" value="Delete Images" id="deleteMediaImages">
              <a href="<?= base_url()?>admin/media/add" class="btn btn-success">Add New</a> </div>
          </div>
          
          <!-------------------------------------------------------Image list--------------------------------------->
          <div class="row">
            <div class="col-md-12">
            <div class="row">
            <?php if(!empty($mediaThumbnailImages)){
                foreach($mediaThumbnailImages as $mediaThumbnailImage){
                    $mediasingleinagename=  explode('/',$mediaThumbnailImage); ?>
            <div class="col-md-3 mediaimagediv">
            <div class="row">
                  <div class="col-sm-12">
                  <img src="<?= base_url().$mediaThumbnailImage?>" alt="" />
                  </div>
                  <div class="col-sm-12">
                  <input type="checkbox" name="mediafiles[]" value="<?php echo $mediasingleinagename[3];?>"/>
                  </div>
                  </div>
            </div>
            <?php }} 
                else {
                    echo " No media files found";  
                 }?>
            
            </div>
              
                
               
             
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style>
.mediaimagediv {
    background: #e7e7e7 none repeat scroll 0 0;
    margin: 21px 8px 0 1px;
    padding: 26px;
    width: 24%;
}
.mediaImages img {

    height: 150px;

    width: 150px;

}

.mediaImages{

	padding:1px;

	 margin: 0 0 10px 15px;

	}

.mediaImages li {
    background: #f1eeee none repeat scroll 0 0;
    margin: 28px 3px 11px;
    padding: 18px 6px 14px;
    position: relative;
}

.mediaImages div {

    left: 0;

    position: absolute;

    top: 1px;

}	

</style>
<script>

function ValidateForm()

{

alert("Are you sure to delete images?");	

	}



</script> 
