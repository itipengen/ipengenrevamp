<div class="row wrapper border-bottom white-bg page-heading">

  <div class="col-lg-10">

    <h2>Multi-Admin List</h2>

    <ol class="breadcrumb">

      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>

      <li> <a>Admin</a> </li>

      <li class="active"> <strong>Multi-Admin List</strong> </li>

    </ol>

  </div>

  <div class="col-lg-2"> </div>

</div>

<?php if($this->session->flashdata('adminEditMsg')){?>

<div class="row admin-list-msg">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('adminEditMsg');?> </div>

  </div>

</div>

<?php }?>

<?php if($this->session->flashdata('adminDeleteMsg')){?>

<div class="row admin-list-msg">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('adminDeleteMsg');?> </div>

  </div>

</div>

<?php }?>

<div class="row admin-list-msg" style="display:none">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg">  </div>

  </div>

</div>

<div class="row">

  <div class="col-lg-12">

    <div class="ibox float-e-margins">

      <div class="ibox-title">

        <h5>Admin List</h5>

      </div>

      <div class="ibox-content">

        <div class=""> <a href="<?php echo base_url("admin/add");?>" class="btn btn-primary ">Add New</a> </div>

        <table class="table table-striped table-bordered table-hover " id="admintable" >

          <thead>

            <tr>

              <th>First Name</th>

              <th>Last Name</th>

              <th>Email</th>

              <th>Position</th>

              <th>Status</th>

              <th class="center">Actions</th>

            </tr>

          </thead>

          <tbody>

            <?php foreach($result as $row) {?>

            <tr class="gradeA">

              <td><?php echo ucfirst($row->first_name) ?></td>

              <td><?php echo ucfirst($row->last_name) ?></td>

              <td><?php echo $row->email ?></td>

              <td><?php echo ucfirst($row->position) ?></td>

              <td>

              	<?php

                  if($row->status == 0) {

                    $check = '';

                  }else{

                    $check = 'checked';

                  }

                ?>

			  	<input type="checkbox" value="<?= $row->log_id ?>" class="js-switch" <?= $check; ?> />

              </td>

              <td class="center">

              <a href="<?php echo base_url()?>admin/edit/<?php echo $row->log_id; ?>" title="Admin Edit"><i class="fa fa-pencil-square-o fa-fw action-btn"></i></a>

              <a href="javascript:void(0)" id="<?= $row->log_id ?>" class="adminDelete" title="Admin Delete"><i class="fa fa-trash-o fa-fw action-btn text-danger" ></i></a>

              </td>

            </tr>

            <?php }?>

          </tbody>

        </table>

      </div>

    </div>

  </div>

</div>

<script>

$(document).ready(function(){

	var loader = "<?php echo base_url('assets/frontend/img/ripple.gif'); ?>";

	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';

			 /* Init DataTables */

	var oTable = $('#admintable').DataTable({

			//order:[[0,"desc"]],

			pageLength: 25,

		});

	$(document).on('click','.adminDelete',function(){

		var ecatid=$(this).attr('id');

		 if(ecatid!=""){

		bootbox.confirm("Are you sure want to delete?", function(result) {

				if(result){

					window.location.href = "<?php echo base_url()?>admin/delete/"+ecatid; 

				}

			}); 

		 }

	});

		/****Switcher****/

		// For Single Checkbox

		// var elem = document.querySelector('.js-switch');

		// Switchery(elem, { color: '#1AB394' });

		// For multiple Checkbox

		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

			elems.forEach(function(html) {

			var switchery = new Switchery(html,{ color: '#1AB394' });

		});

		$(document).on('click', '.paginate_button a', function(){

			$( "#admintable tbody tr" ).each(function(){

				 if(!$(this).find('td:eq(4) span').hasClass('switchery')){

					var elem = this.querySelector('.js-switch');

					Switchery(elem, { color: '#1AB394' });

				}

			});

		});

		

		$(document).on("keyup",".dataTables_filter input", function(){

			$( "#admintable tbody tr" ).each(function(){

				 if(!$(this).find('td:eq(4) span').hasClass('switchery')){

					var elem = this.querySelector('.js-switch');

					Switchery(elem, { color: '#1AB394' });

				}

			});

		});

		

		$(document).on("change","#admintable_length select", function(){

			$( "#admintable tbody tr" ).each(function(){

				 if(!$(this).find('td:eq(4) span').hasClass('switchery')){

					var elem = this.querySelector('.js-switch');

					Switchery(elem, { color: '#1AB394' });

				}

			});

		});

		

	

		/*****Status Change*****/

		$(document).on("change",".js-switch",function(e){

			  var eventId = $(this).attr("value");

			  var fname = $(this).closest("tr").find("td:eq(0)").html();

			  var lname = $(this).closest("tr").find("td:eq(1)").html();

			  var fullname = fname +" "+ lname;

			  if (this.checked) {

				  $.ajax({

					  url:"<?php echo  base_url('admin/enablestatus/'); ?>" + eventId,

					  beforeSend: function(){

							$("body").append(lhtml);

						  },

					  success: function(result){

						  $('#loaderBg2').remove();

							if($.trim(result) != 0)

							{

								$(this).attr("checked", "checked");

								$(".admin-list-msg").css("display","");

								$(".admin-flash-msg").html(fullname + " now has been active.");

								$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

							}

							else

							{$(this).removeAttr("checked");}

						  }

					  });

			  }else{

				  $.ajax({

					  url:"<?php echo base_url('admin/disablestatus/'); ?>" + eventId ,

					  beforeSend: function(){

							$("body").append(lhtml);

						  },

					  success: function(result){

						  $('#loaderBg2').remove();

							if($.trim(result) != 0)

							{

								$(this).removeAttr("checked");

								$(".admin-list-msg").css("display","");

								$(".admin-flash-msg").html(fullname + " now has been deactive.");

								$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

							}

							else

							{$(this).attr("checked", "checked");}

						  }

					  });

			  };

		});



	});

</script>