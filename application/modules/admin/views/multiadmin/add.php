<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Add New Admin</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Admin</a> </li>
      <li class="active"> <strong>Add New Admin</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('adminInsertMsg')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('adminInsertMsg');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add Admin </h5>
        </div>
        <div class="ibox-content">
          <?php $attributes = array('class' => 'form-horizontal', 'id' => 'adminform','method' => 'post' ,'role' =>'form');

				echo form_open('admin/add', $attributes);?>
          <strong></strong>
          <div class="form-group">
            <?php  $first_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('First Name','first_name',$first_name_label);?>
            <div class="col-sm-10">
              <?php $first_name = array('name' => 'first_name','class' => 'form-control','placeholder' => 'First name','required'=>'required','value' => set_value('first_name')); ?>
              <?= form_input($first_name); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <?php  $last_name_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Last Name','last_name',$last_name_label);?>
            <div class="col-sm-10">
              <?php $last_name = array('name' => 'last_name','class' => 'form-control','placeholder' => 'Last name','required'=>'required','value' => set_value('last_name')); ?>
              <?= form_input($last_name); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <?php  $pass_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Password','password',$pass_label);?>
            <div class="col-sm-10">
              <?php $pass = array('name' => 'password','class' => 'form-control','placeholder' => 'Password','required'=>'required','value' => set_value('password')); ?>
              <?= form_password($pass); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <?php  $re_pass_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Re-enter Password','re_password',$re_pass_label);?>
            <div class="col-sm-10">
              <?php $re_pass = array('name' => 're_password','class' => 'form-control','placeholder' => 'Enter Password again','required'=>'required','value' => set_value('re_password')); ?>
              <?= form_password($re_pass); ?>
            </div>
            <span class="alert-danger form-admin-error"><?php echo  form_error('re_password');?></span> </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <?php  $email_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Email','email',$email_label);?>
            <div class="col-sm-10">
              <?php $email = array('name' => 'email','class' => 'form-control','placeholder' => 'Email Address','required'=>'required','value' => set_value('email')); ?>
              <?= form_input($email); ?>
            </div>
            <span class="alert-danger form-admin-error"><?php echo  form_error('email');?></span> </div>
          <div class="hr-line-dashed"></div>
          <!--<div class="form-group"><label class="col-lg-2 control-label">Disabled</label>

                                    <div class="col-lg-10"><input type="text" disabled="" placeholder="Disabled input here..." class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-lg-2 control-label">Static control</label>

                                    <div class="col-lg-10"><p class="form-control-static">email@example.com</p></div>
                                </div>
                                <div class="hr-line-dashed"></div>-->
          <div class="form-group">
            <?php  $Position_label = array('class'=>'col-sm-2 control-label');
								echo  form_label('Position','position',$Position_label);?>
            <div class="col-sm-10">
              <?php $position = array('name' => 'position','class' => 'form-control','placeholder' => 'Admin position','required'=>'required','value' => set_value('position')); ?>
              <?= form_input($position); ?>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('submit','Save changes','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
