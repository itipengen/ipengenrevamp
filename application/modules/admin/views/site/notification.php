<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Notfication List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      
      <li class="active"> <strong>Notification List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>

<!-- ................................. Notificationb datatable ............... -->
    <div class="ibox-content">
    <table class="table table-striped table-bordered table-hover " id="notificationtable" >
        <thead>
            <tr>
                <th>Serial No</th>
                <th>Type</th>
                <th>Message</th>
                <th>Date</th> 
                <th>Status</th>
            </tr>
        </thead>
    </table>
 </div>
 
<!-- ................................. datatable js url ............... -->         
	<script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
		var base_url = window.location.origin;
		
		/* Init DataTables */
		var dataTable = $('#notificationtable').DataTable( {
					"processing": true,
					"serverSide": true,
					"aaSorting": [[0,'DESC']],
					"ajax":{
						url :base_url+'/admin/index/ajaxnotification', // json datasource
						type: "post",  // method  , by default get
						error: function (){ 
						
							$(".employee-grid-error").html("");
							$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
						
						
						
						 }
				
					}
				} );
		
		
	});
    
    </script>