<style>

.externalFilter {

	background: #fff;

}

.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all {

	z-index: 1000 !important;

}

.table-responsive {

	min-height: 0.01%;

	overflow-x: -moz-hidden-unscrollable;

}

.search-section {

    background: #f1f1f1 none repeat scroll 0 0;

    height: 51px;

    margin: auto 15px 14px;

    padding: 12px;

    width: 97%;

}

.seachdiv {

    float: left;

    margin-left: 12px;

}

</style>

<div class="row wrapper border-bottom white-bg page-heading">

  <div class="col-lg-10">

    <h2>Wishlist</h2>

    <ol class="breadcrumb">

      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>

      <li> <a>Admin</a> </li>

      <li class="active"> <strong>Wishlist</strong> </li>

    </ol>

  </div>

  <div class="col-lg-2"> </div>

</div>

<div class="row admin-list-msg" style="display:none">

  <div class="col-lg-12">

    <div class="ibox float-e-margins admin-flash-msg"></div>

  </div>

</div>

<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">

  <div class="row">

    <div class="col-lg-12">

      <div class="ibox float-e-margins">

        <div class="ibox-title">

          <div class="ibox-content" style="padding:0px">

            <div class="row"> 

            <div class="search-section">

            <div class="seachdiv">

            <label class="date1label">

            Event Start Date

            </label>

            <input type="text" class="date1" id="datepicker1" />

            </div>

             <div class="seachdiv">

             <label class="date1label">

            Event End Date

            </label>

            <input type="text" class="date2" id="datepicker2"/>

            </div>

             <div class="seachdiv">

            <button class="serachdata btn btn-primary">Search</button>

            </div>

            

            

            </div>

            

            

            </div>

            <div class="table-responsive">

              <table class="table table-striped table-bordered table-hover dataTables-example" >

                <thead>

                 

                  <tr>

                    <th>Wishlist ID.</th>

                    <th>Owner Name</th>

                    <th>Title</th>

                    <th>Category</th>

                    <th>Event Start Date</th>

                    <th>Event End Date</th>

                    <th>Event Date</th>

                    <th>Wishlist Image</th>

                    <th>Event Status</th>

                    <th>Action</th>

                    <th>Action</th>

                  </tr>

                </thead>

                

                

              </table>

             

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<script src="<?php echo base_url()?>assets/datatable/js/jquery.jeditable.js"></script> 

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script> 

<script src="<?php echo base_url()?>assets/bootstrapDatepicker/jquery.dataTables.columnFilter.js"></script> 

<script>

$(document).ready(function(){

	

	var base_url = window.location.origin;

	$('.dataTables-example').dataTable({

		"processing": true,

		"serverSide": true,

		"aaSorting": [[ 0, "desc" ]],

		

		"ajax":{

								url :base_url+'/admin/wishlist/ajaxwishlist', // json datasource

								//var data = {date1 : date1, date2 : date2};

								type: "post",  // method  , by default get

								

								

								error: function (){ 

								

									$(".employee-grid-error").html("");

									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

									$("#employee-grid_processing").css("display","none");

								

								

								

								 }

		

								

							}

		
		

		

		

		});

	

	/*var Table = $('.dataTables-example').dataTable({

		"processing": true,

		"serverSide": true,

		"aaSorting": [[ 0, "desc" ]],

		

		"ajax":{

								url :base_url+'/admin/wishlist/ajaxwishlist', // json datasource

								//var data = {date1 : date1, date2 : date2};

								type: "post",  // method  , by default get

								

								

								error: function (){ 

								

									$(".employee-grid-error").html("");

									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

									$("#employee-grid_processing").css("display","none");

								

								

								

								 }

		

								

							}

		

		

		

		

		});*/

		setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

  $(".imageZoom").imageTooltip();



 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  }); }, 3000);

	//Serach filter for start and end event date

	

	$('.serachdata').on( 'click', function () { 

		

		var date1 = $('.date1').val();

		var date2 = $('.date2').val();

		

		var data = {date1 : date1, date2 : date2};

		$('.dataTables-example').dataTable().fnDestroy();

		$('.dataTables-example').dataTable({

		"processing": true,

		"serverSide": true,

		"aaSorting": [[ 0, "desc" ]],

		

		"ajax":{

								url :base_url+'/admin/wishlist/ajaxwishlist', // json datasource

								//var data = {date1 : date1, date2 : date2};

								type: "post",  // method  , by default get

								data: {

									'date1': date1,

									'date2': date2

									

									},

								

								

								error: function (){ 

								

									$(".employee-grid-error").html("");

									$("#admintable").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');

									$("#employee-grid_processing").css("display","none");

								

								

								

								 }

		

								

							}

		

		

		

		

		});

		setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

  $(".imageZoom").imageTooltip();



 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  }); }, 3000);







			/* $.ajax({

				  type: 'POST',

				  url: base_url+'/admin/wishlist/ajaxwishlist',

				  data: data,

				  dataType: "json",

				  success: function(resultData) {

						Table.fnClearTable();

						Table.fnDraw(resultData);

						

					   //	Table.fnDraw(resultData); // Add new data

					  //Table.columns.adjust().draw(); // Redraw the DataTable

					

					  

				  }

			})*/

		

        /*

 

        counter++;*/

	});

 

	

	

	

	 



    $( "#datepicker1" ).datepicker({dateFormat: 'yy-mm-dd' });

	$( "#datepicker2" ).datepicker({dateFormat: 'yy-mm-dd' });





	var base_url = window.location.origin;

	var loader = "<?php echo base_url('assets/frontend/img/ripple.gif'); ?>";

	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';

	$.datepicker.regional[""].dateFormat = 'dd/mm/yy';

	$.datepicker.setDefaults($.datepicker.regional['']);

	

	

	 

		

		

		

		

	$(".imageZoom").imageTooltip();

	/*******Switcher On-Off*************/

	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();

		elems.forEach(function(data) {

		var switchery = new Switchery(data,{ color: '#1AB394' });

	});

	/***after Click on Pagination Link*******/

	$(document).on('click', '.paginate_button a', function(){

		$( ".dataTables-example tbody tr" ).each(function() {

			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){

				var elem = this.querySelector('.js-switch');

				Switchery(elem, { color: '#1AB394' });

				$(".imageZoom").imageTooltip();

			 }

		});

		setTimeout(function(){ var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch,.js_switch_feature'));

  $(".imageZoom").imageTooltip();



 $(".switchery").remove();

  elems.forEach(function(data) {

   

    var switchery = new Switchery(data,{ color: '#1AB394' });

  }); }, 3000);

	});

	$(document).on("keyup",".dataTables_filter input", function(){

		$( ".dataTables-example tbody tr" ).each(function() {

			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){

				var elem = this.querySelector('.js-switch');

				Switchery(elem, { color: '#1AB394' });

				$(".imageZoom").imageTooltip();

			 }

		});

	});

	$(document).on("change",".dataTables_length select", function(){

		$( ".dataTables-example tbody tr" ).each(function() {

			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){

				var elem = this.querySelector('.js-switch');

				Switchery(elem, { color: '#1AB394' });

				$(".imageZoom").imageTooltip();

			 }

		});

	});

	$(document).on("change",".dandelion_column_filter", function(){

		$( ".dataTables-example tbody tr" ).each(function() {

			 if(!$(this).find('td:eq(10) span').hasClass('switchery')){

				var elem = this.querySelector('.js-switch');

				Switchery(elem, { color: '#1AB394' });

				$(".imageZoom").imageTooltip();

			 }

		});

	});

	

	$(document).on("change",".js-switch",function(e){

		  var wishlistId = $(this).attr("value");

		  var w_name = $(this).closest("tr").find("td:eq(2)").html();

		  if (this.checked) {

			  $.ajax({

				  url:"<?php echo base_url('admin/wishlist/status/'); ?>" + wishlistId + "/1" ,

				  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

				  success: function(result){

					  	$('#loaderBg2').remove();

					  	if($.trim(result) != 0)

						{

							$(this).attr("checked", "checked");

							$(".admin-list-msg").css("display","");

							$(".admin-flash-msg").html(w_name +" wishlist now has been Activate.");

							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

						}

						else

						{$(this).removeAttr("checked");}

					  }

				  });

		  }else{

			  $.ajax({

				  url:"<?php echo base_url('admin/wishlist/status/'); ?>"  + wishlistId + "/0" ,

				  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

				  success: function(result){

					  	$('#loaderBg2').remove();

					  	if($.trim(result) != 0)

						{

							$(this).removeAttr("checked");

							$(".admin-list-msg").css("display","");

							$(".admin-flash-msg").html(w_name +" wishlist now has been Deactivate.");

							$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

						}

						else



						{$(this).attr("checked", "checked");}

					  }

				  });

		  };

	});

});

</script>