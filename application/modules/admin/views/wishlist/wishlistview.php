<style type="text/css">
#ui-datepicker-div {
	z-index: 100 !important;
}
.image-imitation {
    background-color: #f8f8f9;
    padding: 0 0 !important;
    text-align: center;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>View Wishlist</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Admin</a> </li>
      <li class="active"> <strong>View Wishlist</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" style="z-index: 0;">
<?php if($this->session->flashdata('succeess_message')){?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
  </div>
</div>
<?php }?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><?php echo ucwords($wishlistData[0]->title); ?> Wishlist</h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="col-md-5">
            <div class="product-images">
              <?php 
					if(is_file("photo/wishlist/".$wishlistData[0]->id."/700-700/".$wishlistData[0]->wishlist_image))
					{$imageURL = base_url("photo/wishlist/".$wishlistData[0]->id."/700-700/".$wishlistData[0]->wishlist_image);}
					else
					{$imageURL = base_url("photo/wishlist/no-event.png");} 
			?>
              <div>
                <div class="image-imitation"> <img src="<?= $imageURL; ?>" width="387" height="387" alt="<?php echo ucwords($wishlistData[0]->fname." ".$wishlistData[0]->lname)."'s event photo"; ?>"> </div>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h2 class="font-bold m-b-xs">
              <?= ucwords($wishlistData[0]->title) ?>
            </h2>
            <small><!--Many desktop publishing packages and web page editors now.--></small> 
            <!--<div class="m-t-md">
              <h2 class="product-main-price"><? //= $list->price ?> <small class="text-muted"> Price</small> </h2>
            </div>-->
            <hr>
            <h4>Category</h4>
            <div class="small text-muted">
              <?= ucwords($wishlistData[0]->category) ?>
            </div>
            <h4>Owner Name</h4>
            <div class="small text-muted">
              <?= ucwords($wishlistData[0]->fname." ".$wishlistData[0]->lname) ?>
            </div>
            <h4>Date</h4>
            <div class="small text-muted">
              <?= date("d/m/Y",strtotime($wishlistData[0]->event_date)) ?>
            </div>
            <h4>Status</h4>
            <div class="small text-muted">
              <?= ucwords($wishlistData[0]->is_public); ?>
            </div>
            <hr>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
          <div class="col-sm-12">
            <table class="table table-striped table-bordered table-hover " id="admintable" >
              <thead>
              <th>Sl No.</th>
                <th>Product Name</th>
                <th>Product Stock</th>
                <th>Product Price</th>
                <th>Product Sale Price</th>
                <th>Start Sale Date</th>
                <th>End Sale Date</th>
                <th>Image</th>
                <th>Action</th>
                  </thead>
              <tbody>
                <?php 
						if(!empty($wishlistProduct)){ $i=0;
							foreach($wishlistProduct as $row){
								if(is_file($this->config->item('image_path').'/product/'.$row->productID.$this->config->item('thumb_size').$row->product_image))
								{$productImg = base_url('photo/product/'.$row->productID.$this->config->item('thumb_size').$row->product_image); }
								else
								{$productImg = $this->config->item('image_display_path')."/product/no_product.jpg";}
					?>
                <tr>
                  <td><?php echo ++$i; ?></td>
                  <td><?php echo $row->product_name; ?></td>
                  <td><?php echo $row->product_stock; ?></td>
                  <td><?php echo strtoupper($this->config->item('currency')).' '.number_format($row->product_price); ?></td>
                  <td><?php echo strtoupper($this->config->item('currency')).' '.number_format($row->product_sale_price); ?></td>
                  <td><?php echo $row->product_sale_start_date; ?></td>
                  <td><?php echo $row->product_sale_end_date; ?></td>
                  <td><img class="img-circle imageZoom" width="36" height="36" src="<?php echo $productImg; ?>"></td>
                  <td><a href="<?php echo base_url("admin/product/productview/".$row->productID); ?>" title="Product View"><i class="fa fa-eye fa-fw fa-2x" style="color:#508DF2"></i></a></td>
                </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<script>
$(document).ready(function(e) {
	$('#admintable').DataTable({
		order:[[0,"desc"]],
	});
	$(".imageZoom").imageTooltip();
});
</script> 
