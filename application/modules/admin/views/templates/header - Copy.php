<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $this->lang->line('admin_tiltle');?>|<?php echo $this->lang->line('dashboard');?></title>
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/components.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<!-- Jquery User Interface-->
<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<!--Sumer Notes-->
<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
<!-- Morris -->
<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- slick carousel-->
<link href="<?php echo base_url();?>assets/css/plugins/slick/slick.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">

<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Data picker -->
<script src="<?php echo base_url('assets/js/plugins/datapicker/bootstrap-datepicker.js');?>"></script>

<script src="http://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
<!-- slick carousel-->
<script src="<?php echo base_url();?>assets/js/plugins/slick/slick.min.js"></script>
</head>

<body>
<div id="wrapper">
<?php 
	$query = $this->db->get_where('admin_login',array('email'=>$this->session->userdata('logged_in'))); 
  $result = $query->result();
  $full_name = "";
  $position = "";
  if (!empty($result)) {
    foreach ($result as $value) {
      $full_name = ucfirst($value->first_name).' '.ucfirst($value->last_name);
      $position = ucfirst($value->position);
    }
  }
?>
<nav class="navbar-default navbar-static-side" role="navigation">
  <div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
      <li class="nav-header">
        <div class="dropdown profile-element"> 
         <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $full_name; ?></strong> </span> <span class="text-muted text-xs block"><?php echo $position; ?> <b class="caret"></b></span> </span> </a>
          <!--<ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><a href="#">Profile</a></li>
            <li><a href="#">Contacts</a></li>
            <li><a href="#">Mailbox</a></li>
            <li class="divider"></li>
            <li><a href="<?php //echo base_url()?>admin/logout">Logout</a></li>
          </ul>-->
        </div>
        <div class="logo-element"> IP+ </div>
      </li>
      <li class="active"> <a href="<?= base_url('admin'); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a> </li>
      <li class="adminList"> <a href="#"><i class="fa fa-user-secret"></i> <span class="nav-label">Admin</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="adminlist">
          <li><a href="<?php echo base_url();?>admin/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/list">List</a></li>
        </ul>
      </li>
      <li class="userlist"> <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Users</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="userlist">

          <li><a href="<?php echo base_url();?>admin/user/userlist">List</a></li>

        </ul>

      </li>

     <?php /*?>  <li class="contractorlist"> <a href="#"><i class="fa fa-user-plus"></i> <span class="nav-label">Contractor</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="contractorlist">

          <li><a href="<?php echo base_url();?>admin/contractor/contractorlist">List</a></li>

          <li><a href="<?php echo base_url();?>admin/contractor/verifyrequest">Verify Request</a></li>

        </ul>

      </li><?php */?>
      <li class="categorymenu"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Category</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="categorymenu">
          <li><a href="<?php echo base_url();?>admin/category/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/category/list">List</a></li>
        </ul>
      </li>
       <li class="eventmenu"> <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Event Category</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="eventmenu">
          <li><a href="<?php echo base_url();?>admin/event/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/event/list">List</a></li>
        </ul>
      </li>
      <li class="brandList"> <a href="#"><i class="fa fa-minus-square"></i> <span class="nav-label">Product Brand</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="brandList">
          <li><a href="<?php echo base_url();?>admin/brand/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/brand/listing">List</a></li>
        </ul>
      </li>
      <li class="productmenu"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Products</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="productmenu">
          <li><a href="<?php echo base_url();?>admin/product/">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/product/list">List</a></li>
        </ul>
      </li>
      <li class="bannertmenu"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Banner</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="bannertmenu">
          <li><a href="<?php echo base_url();?>admin/banner/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/banner/list">List</a></li>
        </ul>
      </li>
      <?php /*?><li class="staticmenu"> <a href="#"><i class="fa fa-outdent" aria-hidden="true"></i><span class="nav-label">Static Block</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="staticmenu">
          <li><a href="<?php echo base_url();?>admin/block/add">Add</a></li>
          <li><a href="<?php echo base_url();?>admin/block/list">List</a></li>
        </ul>
      </li><?php */?>
      
      <!-- <li>







                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Forms</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="form_basic.html">Basic form</a></li>







                        <li><a href="form_advanced.html">Advanced Plugins</a></li>







                        <li><a href="form_wizard.html">Wizard</a></li>







                        <li><a href="form_file_upload.html">File Upload</a></li>







                        <li><a href="form_editors.html">Text Editor</a></li>







                        <li><a href="form_markdown.html">Markdown</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">App Views</span>  <span class="pull-right label label-primary">SPECIAL</span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="contacts.html">Contacts</a></li>







                        <li><a href="profile.html">Profile</a></li>







                        <li><a href="profile_2.html">Profile v.2</a></li>







                        <li><a href="contacts_2.html">Contacts v.2</a></li>







                        <li><a href="projects.html">Projects</a></li>







                        <li><a href="project_detail.html">Project detail</a></li>







                        <li><a href="teams_board.html">Teams board</a></li>







                        <li><a href="social_feed.html">Social feed</a></li>







                        <li><a href="clients.html">Clients</a></li>







                        <li><a href="full_height.html">Outlook view</a></li>







                        <li><a href="vote_list.html">Vote list</a></li>







                        <li><a href="file_manager.html">File manager</a></li>







                        <li><a href="calendar.html">Calendar</a></li>







                        <li><a href="issue_tracker.html">Issue tracker</a></li>







                        <li><a href="blog.html">Blog</a></li>







                        <li><a href="article.html">Article</a></li>







                        <li><a href="faq.html">FAQ</a></li>







                        <li><a href="timeline.html">Timeline</a></li>







                        <li><a href="pin_board.html">Pin board</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Other Pages</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="search_results.html">Search results</a></li>







                        <li><a href="lockscreen.html">Lockscreen</a></li>







                        <li><a href="invoice.html">Invoice</a></li>







                        <li><a href="login.html">Login</a></li>







                        <li><a href="login_two_columns.html">Login v.2</a></li>







                        <li><a href="forgot_password.html">Forget password</a></li>







                        <li><a href="register.html">Register</a></li>







                        <li><a href="404.html">404 Page</a></li>







                        <li><a href="500.html">500 Page</a></li>







                        <li><a href="empty_page.html">Empty page</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Miscellaneous</span><span class="label label-info pull-right">NEW</span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="toastr_notifications.html">Notification</a></li>







                        <li><a href="nestable_list.html">Nestable list</a></li>







                        <li><a href="agile_board.html">Agile board</a></li>







                        <li><a href="timeline_2.html">Timeline v.2</a></li>







                        <li><a href="diff.html">Diff</a></li>







                        <li><a href="i18support.html">i18 support</a></li>







                        <li><a href="sweetalert.html">Sweet alert</a></li>







                        <li><a href="idle_timer.html">Idle timer</a></li>







                        <li><a href="truncate.html">Truncate</a></li>







                        <li><a href="spinners.html">Spinners</a></li>







                        <li><a href="tinycon.html">Live favicon</a></li>







                        <li><a href="google_maps.html">Google maps</a></li>







                        <li><a href="code_editor.html">Code editor</a></li>







                        <li><a href="modal_window.html">Modal window</a></li>







                        <li><a href="clipboard.html">Clipboard</a></li>







                        <li><a href="forum_main.html">Forum view</a></li>







                        <li><a href="validation.html">Validation</a></li>







                        <li><a href="tree_view.html">Tree view</a></li>







                        <li><a href="loading_buttons.html">Loading buttons</a></li>







                        <li><a href="chat_view.html">Chat view</a></li>







                        <li><a href="masonry.html">Masonry</a></li>







                        <li><a href="tour.html">Tour</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-flask"></i> <span class="nav-label">UI Elements</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="typography.html">Typography</a></li>







                        <li><a href="icons.html">Icons</a></li>







                        <li><a href="draggable_panels.html">Draggable Panels</a></li> <li><a href="resizeable_panels.html">Resizeable Panels</a></li>







                        <li><a href="buttons.html">Buttons</a></li>







                        <li><a href="video.html">Video</a></li>







                        <li><a href="tabs_panels.html">Panels</a></li>







                        <li><a href="tabs.html">Tabs</a></li>







                        <li><a href="notifications.html">Notifications & Tooltips</a></li>







                        <li><a href="badges_labels.html">Badges, Labels, Progress</a></li>







                    </ul>







                </li>















                <li>







                    <a href="grid_options.html"><i class="fa fa-laptop"></i> <span class="nav-label">Grid options</span></a>







                </li>







                <li>







                    <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Tables</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="table_basic.html">Static Tables</a></li>







                        <li><a href="table_data_tables.html">Data Tables</a></li>







                        <li><a href="table_foo_table.html">Foo Tables</a></li>







                        <li><a href="jq_grid.html">jqGrid</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">E-commerce</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="ecommerce_products_grid.html">Products grid</a></li>







                        <li><a href="ecommerce_product_list.html">Products list</a></li>







                        <li><a href="ecommerce_product.html">Product edit</a></li>







                        <li><a href="ecommerce_product_detail.html">Product detail</a></li>







                        <li><a href="ecommerce-cart.html">Cart</a></li>







                        <li><a href="ecommerce-orders.html">Orders</a></li>







                        <li><a href="ecommerce_payments.html">Credit Card form</a></li>







                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">Gallery</span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li><a href="basic_gallery.html">Lightbox Gallery</a></li>







                        <li><a href="slick_carousel.html">Slick Carousel</a></li>







                        <li><a href="carousel.html">Bootstrap Carousel</a></li>















                    </ul>







                </li>







                <li>







                    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Menu Levels </span><span class="fa arrow"></span></a>







                    <ul class="nav nav-second-level collapse">







                        <li>







                            <a href="#">Third Level <span class="fa arrow"></span></a>







                            <ul class="nav nav-third-level">







                                <li>







                                    <a href="#">Third Level Item</a>







                                </li>







                                <li>







                                    <a href="#">Third Level Item</a>







                                </li>







                                <li>







                                    <a href="#">Third Level Item</a>







                                </li>















                            </ul>







                        </li>







                        <li><a href="#">Second Level Item</a></li>







                        <li>







                            <a href="#">Second Level Item</a></li>







                        <li>







                            <a href="#">Second Level Item</a></li>







                    </ul>







                </li>--> 
      
      <!--  <li>







                    <a href="#"><i class="fa fa-magic"></i> <span class="nav-label">Group task category create</span></a>







                </li>-->
      
      <?php /*?><li class="cities"> <a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">Cities</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="cities">

          <li><a href="<?php echo base_url();?>admin/city/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/city/citylist">List</a></li>

        </ul>

      </li>

      <li class="joblist"> 

      <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Jobs</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="joblist">

          <li><a href="<?php echo base_url();?>admin/job/list">List</a></li>

        </ul>

      </li>

      <li> <a href="#"><i class="fa fa-gift"></i> <span class="nav-label">Offer</span></a> </li>

      <li> <a href="<?php echo base_url('admin/transaction/pending/payment');?>"><i class="fa fa-money"></i> <span class="nav-label">Pending Payment</span></a> </li>

      <li> <a href="<?php echo base_url('admin/transaction');?>"><i class="fa fa-table"></i> <span class="nav-label">Admin Transaction</span></a> </li>

      

      <li class="transaction"> <a href="#"><i class="fa fa-map-marker"></i> <span class="nav-label">All Transaction</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="transaction">

          <li><a href="<?php echo base_url();?>admin/transaction/reports">Reports</a></li>

          <li><a href="<?php echo base_url();?>admin/transaction/pending">Pending</a></li>

          <li><a href="<?php echo base_url();?>admin/transaction/failed">Failed</a></li>

        </ul>

      </li>

      

      

      

      <li> <a href="<?php echo base_url('admin/revenue');?>"><i class="fa fa-money"></i> <span class="nav-label">Revenue</span></a> </li>

      <li class="pages"> <a href="#"><i class="fa fa-file"></i> <span class="nav-label">Pages</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="pages">

          <li><a href="<?php echo base_url();?>admin/pages/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/pages/pagelist">List</a></li>

        </ul>

      </li>

      <li class="settings"> <a href="#"><i class="fa fa-wrench"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="settings">

          <li><a href="<?php echo base_url();?>admin/site_control_info">Site Settings</a></li>

        </ul>

      </li>

      <li> <a href="#"><i class="fa fa-bell-o"></i> <span class="nav-label">Notification</span></a> </li>

      <li class="bannerSetting"> <a href="#"><i class="fa fa-magic"></i> <span class="nav-label">Banner Setting</span> <span class="fa arrow"></span></a>

        <ul class="nav nav-second-level collapse" id="bannerSetting">

          <li><a href="<?php echo base_url();?>admin/banner/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/banner/bannerlist">List</a></li>

        </ul>

      </li>

      

      <li class="static_block"> <a href="#"><i class="fa fa-square" aria-hidden="true"></i><span class="nav-label">Static block</span> <span class="fa arrow"></span></a>

                <ul class="nav nav-second-level collapse" id="block">

          <li><a href="<?php echo base_url();?>admin/static_block/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/static_block/list_block">List</a></li>

        </ul>

      </li>

      

      <li class="FAQ"> <a href="#"><i class="fa fa-question" aria-hidden="true"></i><span class="nav-label">FAQ</span> <span class="fa arrow"></span></a>

                <ul class="nav nav-second-level collapse" id="FAQ">

          <li><a href="<?php echo base_url();?>admin/FAQ/add">Add</a></li>

          <li><a href="<?php echo base_url();?>admin/FAQ/list_FAQ">List</a></li>

        </ul>

      </li>
<?php */?>
      
      <!--<li class="landing_link">







                    <a target="_blank" href="#"><i class="fa fa-star"></i> <span class="nav-label">Landing Page</span> <span class="label label-warning pull-right">NEW</span></a>







                </li>







                <li class="special_link">







                    <a href="package.html"><i class="fa fa-database"></i> <span class="nav-label">Package</span></a>







                </li>-->
      
    </ul>
  </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
  <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> </div>
    <ul class="nav navbar-top-links navbar-right">
      <li> <span class="m-r-sm text-muted welcome-message"><?php echo $this->lang->line('welcome_message');?>.</span> </li>
      
      <!--<li class="dropdown">







                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">







                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>







                    </a>







                    <ul class="dropdown-menu dropdown-messages">







                        <li>







                            <div class="dropdown-messages-box">







                                <a href="profile.html" class="pull-left">







                                    <img alt="image" class="img-circle" src="img/a7.jpg">







                                </a>







                                <div>







                                    <small class="pull-right">46h ago</small>







                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>







                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>







                                </div>







                            </div>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <div class="dropdown-messages-box">







                                <a href="profile.html" class="pull-left">







                                    <img alt="image" class="img-circle" src="img/a4.jpg">







                                </a>







                                <div>







                                    <small class="pull-right text-navy">5h ago</small>







                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>







                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>







                                </div>







                            </div>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <div class="dropdown-messages-box">







                                <a href="profile.html" class="pull-left">







                                    <img alt="image" class="img-circle" src="img/profile.jpg">







                                </a>







                                <div>







                                    <small class="pull-right">23h ago</small>







                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>







                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>







                                </div>







                            </div>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <div class="text-center link-block">







                                <a href="mailbox.html">







                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>







                                </a>







                            </div>







                        </li>







                    </ul>







                </li>--> 
      
      <!--<li class="dropdown">







                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">







                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>







                    </a>







                    <ul class="dropdown-menu dropdown-alerts">







                        <li>







                            <a href="mailbox.html">







                                <div>







                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages







                                    <span class="pull-right text-muted small">4 minutes ago</span>







                                </div>







                            </a>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <a href="profile.html">







                                <div>







                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers







                                    <span class="pull-right text-muted small">12 minutes ago</span>







                                </div>







                            </a>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <a href="grid_options.html">







                                <div>







                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted







                                    <span class="pull-right text-muted small">4 minutes ago</span>







                                </div>







                            </a>







                        </li>







                        <li class="divider"></li>







                        <li>







                            <div class="text-center link-block">







                                <a href="notifications.html">







                                    <strong>See All Alerts</strong>







                                    <i class="fa fa-angle-right"></i>







                                </a>







                            </div>







                        </li>







                    </ul>







                </li>-->
      
      <li> <a href="<?php echo base_url()?>admin/logout"> <i class="fa fa-sign-out"></i> Log out </a> </li>
      
      <!--<li>







                    <a class="right-sidebar-toggle">







                        <i class="fa fa-tasks"></i>







                    </a>







                </li>-->
      
    </ul>
  </nav>
</div>
<script type="text/javascript">

				$(document).ready(function(){

				var currentUrl= window.location.href;

				if(currentUrl=="<?php echo base_url().'admin/job/list'; ?>"){

					$("#joblist").addClass('in');

					$(".joblist").addClass('active');

					

				}

				

			   if(currentUrl=="<?php echo base_url().'admin/list'; ?>"){

				   $("#adminlist").addClass('in');

				  $(".adminList").addClass('active'); 

			   }

			   

			   if(currentUrl=="<?php echo base_url().'admin/add'; ?>"){

				   $("#adminlist").addClass('in');

				  $(".adminList").addClass('active'); 

			   }

			   

			    if(currentUrl=="<?php echo base_url().'admin/user/userlist'; ?>"){

				   $("#userlist").addClass('in');

				  $(".userlist").addClass('active'); 

			   }

			  if((currentUrl=="<?php echo base_url().'admin/category/add'; ?>") ||(currentUrl=="<?php echo base_url().'admin/category/list'; ?>")){

				   $("#categorymenu").addClass('in');

				  $(".categorymenu").addClass('active'); 

			   }
if((currentUrl=="<?php echo base_url().'admin/event/add'; ?>") ||(currentUrl=="<?php echo base_url().'admin/event/list'; ?>")){

				   $("#ecategorymenu").addClass('in');

				  $(".ecategorymenu").addClass('active'); 

			   }
			   if((currentUrl=="<?php echo base_url().'admin/product/'; ?>") ||(currentUrl=="<?php echo base_url().'admin/product/list'; ?>")){

				   $("#productmenu").addClass('in');

				  $(".productmenu").addClass('active'); 

			   }
			   if((currentUrl=="<?php echo base_url().'admin/banner/add'; ?>") ||(currentUrl=="<?php echo base_url().'admin/banner/list'; ?>")){

				   $("#bannertmenu").addClass('in');

				  $(".bannertmenu").addClass('active'); 

			   }
			   

			   if(currentUrl=="<?php echo base_url().'admin/contractor/verifyrequest'; ?>"){

				   $("#contractorlist").addClass('in');

				  $(".contractorlist").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/group/create'; ?>"){

				   $("#groups").addClass('in');

				  $(".groups").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/group/grouplist'; ?>"){

				   $("#groups").addClass('in');

				  $(".groups").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/group/subgroupcreate'; ?>"){

				   $("#groups").addClass('in');

				  $(".groups").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/group/subgrouplist'; ?>"){

				   $("#groups").addClass('in');

				  $(".groups").addClass('active'); 

			   }

			   

			   if(currentUrl=="<?php echo base_url().'admin/skill/skilladd'; ?>"){

				   $("#skills").addClass('in');

				  $(".skills").addClass('active'); 

			   }

			   

			   if(currentUrl=="<?php echo base_url().'admin/skill/listskill'; ?>"){

				   $("#skills").addClass('in');

				  $(".skills").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/city/add'; ?>"){

				   $("#cities").addClass('in');

				  $(".cities").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/city/citylist'; ?>"){

				   $("#cities").addClass('in');

				  $(".cities").addClass('active'); 

			   }

			   

			   if(currentUrl=="<?php echo base_url().'admin/transaction/reports'; ?>"){

				   $("#transaction").addClass('in');

				  $(".transaction").addClass('active'); 

			   }

			    if(currentUrl=="<?php echo base_url().'admin/transaction/pending'; ?>"){

				   $("#transaction").addClass('in');

				  $(".transaction").addClass('active'); 

			   }

			    if(currentUrl=="<?php echo base_url().'admin/transaction/failed'; ?>"){

				   $("#transaction").addClass('in');

				  $(".transaction").addClass('active'); 

			   }

			   

			    if(currentUrl=="<?php echo base_url().'admin/pages/add'; ?>"){

				   $("#pages").addClass('in');

				  $(".pages").addClass('active'); 

			   }

			    if(currentUrl=="<?php echo base_url().'admin/pages/pagelist'; ?>"){

				   $("#pages").addClass('in');

				  $(".pages").addClass('active'); 

			   }

			   

			    if(currentUrl=="<?php echo base_url().'admin/site_control_info'; ?>"){

				   $("#settings").addClass('in');

				  $(".settings").addClass('active'); 

			   }

			   if(currentUrl=="<?php echo base_url().'admin/banner/add'; ?>"){

				   $("#bannerSetting").addClass('in');

				  $(".bannerSetting").addClass('active'); 

			   }

			    if(currentUrl=="<?php echo base_url().'admin/banner/bannerlist'; ?>"){

				   $("#bannerSetting").addClass('in');

				  $(".bannerSetting").addClass('active'); 

			   }

				

				});

</script>