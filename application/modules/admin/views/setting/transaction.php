<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Transaction Fee</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url();?>">Home</a> </li>
      <li> <a>Setting</a> </li>
      <li class="active"> <strong>Transaction Fee</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
    <?php if($this->session->flashdata('error_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-delete-falsh-msg"> <?php echo $this->session->flashdata('error_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
        <?php if(count($transactiondata) == 0) { ?>
          <h5>New Transaction Fee</h5>
        <?php }else{ ?>
          <h5>Update Transaction Fee</h5>
        <?php } ?>
        </div>
        <div class="ibox-content">
        <div>
        <?php if(count($transactiondata) == 0) { ?>
         	<div class="col-md-6 col-md-offset-3">
            	<?php echo form_open('admin/setting/add', array('class' => 'form-horizontal','id' => 'transactionform'));?>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Fees Value','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                      <?php echo form_input(array('name' => 'transaction_fees','class' => 'form-control feeinsert','placeholder' => 'Enter fees value with % or Rp sign','required'=>'required','value' => set_value('transaction_fees'))); ?>
                      <div class="text-danger" id="errorVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  
                  <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-2">
                      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                      <?php echo form_submit('transactionSubmit','Save',array("class"=>"btn btn-primary", "id"=>"transactionSubmit"));?> </div>
                  </div>
                  <?php echo form_close();?> 
            </div>
        <?php }else{ ?>
         	<div class="col-md-6 col-md-offset-3">
            	<?php echo form_open('admin/setting/add', array('class' => 'form-horizontal','id' => 'transactionupdate'));?>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Fees Value','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                    	<?php 
							if(isset($fees_id) != ''){$hideVal = $fees_id;}else{$hideVal = '';}
							//if(set_value('transaction_fees') && isset($transaction_fees) == ""){$feesval = set_value('transaction_fees');}else{$feesval = strtoupper($transaction_fees);}
							echo form_input(array("name"=>"feesID", "type"=>"hidden", "value"=>$hideVal));
                      		echo form_input(array('name' => 'transaction_fees','class' => 'form-control feeupdate','placeholder' => 'Enter fees value. example: 100','required'=>'required','value' => '')); ?>
                      <div class="text-danger" id="errorUpdateVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Fee Type','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                    	<?php 
							$ftype = array(
								'' => 'Select Fee Type',
								'1' => 'Percent',
								'2' => 'Fixed'
							);
							$selected = array('');
							echo form_dropdown('Fee_Type', $ftype, $selected, 'class="dropdown_box2" id="feeType"');
							 ?>
                      <div class="text-danger" id="errorFeesUpdateVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <div class="col-md-3">
                        <?php echo  form_label('Gift Type','',array('class'=>'control-label')); ?>
                    </div>
                    <div class="col-md-9">
                    	<?php 
							$options = array(
								'' => 'Select Gift Type',
								'1' => 'Cash Gift',
								'2' => 'Buy Gift'
							);
							$selected = array('');
							echo form_dropdown('Gift_Type[]', $options, $selected, 'class="dropdown_box2" id="giftType"');
							 ?>
                      <div class="text-danger" id="errorUpdateVal"></div>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-2">
                      <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
                      <?php echo form_submit('transactionUpdate','Update',array("class"=>"btn btn-primary", "id"=>"transactionUpdate"));?> </div>
                  </div>
                  <?php echo form_close();?> 
            </div>
        <?php } ?>
            <div class="clearfix"></div>
        </div>
        <div>
        	<table class="table table-striped table-bordered table-hover " id="admintable_bak" >
            	<thead>
                	<th>ID</th>
                	<th>Type</th>
                	<th>Transaction Fee</th>
                	<th>Action</th>
                </thead>
                <tbody>
                <?php if(!empty($transactiondata)){ $i = 0; foreach($transactiondata as $row) { ?>
                	<tr>
                        <td><?php echo ++$i; ?></td>
                        <td><?php echo $row->transaction_fees_type; ?></td>
                        <td><?php echo strtoupper($row->transaction_fees); ?></td>
                        <td><a href="javascript:void(0)" id="<?php echo $row->transaction_fees_id; ?>" class="fees-delete">
                        		<i class="fa fa-trash-o action-btn text-danger"></i>
                            </a>
                        </td>
                    </tr>
                 <?php } } ?>
                </tbody>
            </table>
        </div>
         </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
    $('#admintable').DataTable({});
	
	$('#giftType,#feeType').select2();
	$(document).on("click","#transactionSubmit",function(){
			var shipping_val = $.trim($(".feeinsert").val()).split(" ");
			var shipping_val_length = shipping_val.length;
			var inputFirstValue = shipping_val[0];
			var inputlastValue = shipping_val[1];
			
			if(shipping_val_length == 2)
			{
				if(inputFirstValue == "Rp")
				{
					var inputFirstValueCharecter = inputlastValue[0];
					if($.isNumeric(inputlastValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							$("#errorVal").html("");
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
				}
				else if(inputlastValue == "%")
				{
					var inputFirstValueCharecter = inputFirstValue[0];
					if($.isNumeric(inputFirstValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							if(inputFirstValue <= 100)
							{
								$("#errorVal").html("");
							}
							else
							{
								$("#errorVal").html("Percentage value not more than 100.");
								return false;
							}
						}
						else
						{
							$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Entire value must be numeric.");
						return false;
					}
					
				}
				else
				{
					$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
					return false;
				}
			}
			else
			{
				$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
				return false;
			}
			//return false;
		});
		
	<?php /*?>$(document).on("click","#transactionUpdate",function(){
			var shipping_val = $.trim($(".feeupdate").val()).split(" ");
			var shipping_val_length = shipping_val.length;
			var inputFirstValue = shipping_val[0];
			var inputlastValue = shipping_val[1];
			var gift_type=$('#giftType').val();
			var fee_type=$('#feeType').val();
			if(shipping_val_length == 2)
			{
				if(inputFirstValue == "Rp" || inputFirstValue == "rp" || inputFirstValue == "RP")
				{
					var inputFirstValueCharecter = inputlastValue[0];
					if($.isNumeric(inputlastValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							$("#errorUpdateVal").html("");
						}
						else
						{
							$("#errorUpdateVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorUpdateVal").html("Entire value must be numeric.");
						return false;
					}
				}
				else if(inputlastValue == "%")
				{
					var inputFirstValueCharecter = inputFirstValue[0];
					if($.isNumeric(inputFirstValue))
					{
						if(inputFirstValueCharecter != 0)
						{
							if(inputFirstValue <= 100)
							{
								$("#errorUpdateVal").html("");
							}
							else
							{
								$("#errorUpdateVal").html("Percentage value not more than 100.");
								return false;
							}
						}
						else
						{
							$("#errorUpdateVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first of Value.");
							return false;
						}
					}
					else
					{
						$("#errorUpdateVal").html("Entire value must be numeric.");
						return false;
					}
					
				}
				else
				{
					$("#errorUpdateVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
					return false;
				}
			}
			else
			{
				$("#errorUpdateVal").html("Enter value with Rp or % sign. <br>E.g - Rp&lt; space &gt;000.<br>E.g - 000&lt; space &gt;%.");
				return false;
			}
			//return false;
		});<?php */?>
		
	/*$(document).on("click","#transactionSubmit",function(){
			var trans_val = $.trim($(".feeinsert").val()).split(" ");
			var trans_val_length = trans_val.length;
			var inputValue = trans_val[0];
			var inputFirstValueCharecter = inputValue[0];
			if(trans_val_length == 2)
			{
				if($.isNumeric(inputValue))
				{
					if(inputFirstValueCharecter != 0)
					{
						if(trans_val[1] == "Rp" || trans_val[1] == "%")
						{
							$("#errorVal").html("");
						}
						else
						{
							$("#errorVal").html("Enter Rp or % sign after value.");
							return false;
						}
					}
					else
					{
						$("#errorVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first.");
						return false;
					}
				}
				else
				{
					$("#errorVal").html("Entire value must be numeric.");
					return false;
				}
			}
			else
			{
				$("#errorVal").html("Enter value with Rp or % sign. <br>E.g - 000&lt; space &gt;Rp.");
				return false;
			}
			//return false;
		});*/
	/*$(document).on("click","#transactionUpdate",function(){
			var trans_val = $.trim($(".feeupdate").val()).split(" ");
			var trans_val_length = trans_val.length;
			var inputValue = trans_val[0];
			var inputFirstValueCharecter = inputValue[0];
			if(trans_val_length == 2)
			{
				if($.isNumeric(inputValue))
				{
					if(inputFirstValueCharecter != 0)
					{
						if(trans_val[1] == "Rp" || trans_val[1] == "%")
						{
							$("#errorUpdateVal").html("");
							//$("#transactionform").submit();
						}
						else
						{
							$("#errorUpdateVal").html("Enter Rp or % sign after value.");
							return false;
						}
					}
					else
					{
						$("#errorUpdateVal").html("Enter Valid Value. "+ inputFirstValueCharecter +" is prohibited at the first.");
						return false;
					}
				}
				else
				{
					$("#errorUpdateVal").html("Entire value must be numeric.");
					return false;
				}
			}
			else
			{
				$("#errorUpdateVal").html("Enter value with Rp or % sign. <br>E.g - 000&lt; space &gt;Rp.");
				return false;
			}
			//return false;
		});*/
	$(document).on('click','.fees-delete',function(){
		var feesid=$(this).attr('id');
		 if(feesid!=""){
		bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					window.location.href = "<?php echo base_url(); ?>admin/transaction/delete/"+feesid; 
				}
			}); 
		 }
	});
});
</script>
