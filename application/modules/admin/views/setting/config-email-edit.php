<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Edit Email Configuration</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Setting</a> </li>
      <li class="active"> <strong>Edit Email Configuration</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php if($this->session->flashdata('succeess_message')){?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins admin-flash-msg"> <?php echo $this->session->flashdata('succeess_message');?> </div>
    </div>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edit Email Configuration</h5>
        </div>
        <div class="ibox-content">
          <?php 
		  		$attributes = array('class' => 'form-horizontal','id' => 'brandform');
				echo form_open('admin/setting/config-email-edit/'.$config_id, $attributes);?>
          <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Admin Email','admin_email',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
            	<?php if(set_value('admin_email')){$adminEmail = set_value('admin_email');}else{$adminEmail = $configArray[0]->admin_email;} ?>
              <?= form_input(array('name' => 'admin_email','type' => 'email','class' => 'form-control','placeholder' => 'Admin Email','required'=>'required','value' => $adminEmail)); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">
            	<?php echo  form_label('Support Email','support_email',array('class'=>'control-label')); ?>
            </div>
            <div class="col-md-9">
              <?php if(set_value('admin_email')){$supportEmail = set_value('admin_email');}else{$supportEmail = $configArray[0]->info_email;} ?>
              <?= form_input(array('name' => 'support_email','type' => 'email','class' => 'form-control','placeholder' => 'Support Email','required'=>'required','value' => $supportEmail )); ?>
            </div>
          </div>

          <div class="hr-line-dashed"></div>
         
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button class="btn btn-white" type="submit" onclick="window.location.href='<?php echo base_url()?>admin/dashboard';">Cancel</button>
              <?php echo form_submit('configUpdateSubmit','Update','class="btn btn-primary"');?> </div>
          </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
