<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Banner List</h2>
    <ol class="breadcrumb">
      <li> <a href="<?php echo base_url("admin");?>">Home</a> </li>
      <li> <a>Banner</a> </li>
      <li class="active"> <strong>Banner List</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<br/>
<div>
  <?php if($this->session->flashdata('success')){ ?>
  <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->session->flashdata('success'); ?></strong> </div>
  <?php } ?>
  <?php if($this->session->flashdata('failed')){ ?>
  <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->session->flashdata('failed'); ?></strong> </div>
  <?php } ?>
</div>
<div class="bannerlist">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Banner List</h5>
        </div>
        <div class="ibox-content">
         <div class=""> <a href="<?= base_url('admin/banner/add'); ?>" class="btn btn-success ">Add New</a> </div>
          <table class="table table-striped table-bordered table-hover" id="editable">
            <thead>
              <tr>
              	<th>Banner ID</th>
                <th>Banner Name</th>
                <th style="width: 46%;">Banner Description</th>
                <th>Banner Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 0; foreach($bannerlist as $bannerlists){ ?>
              <tr class="" id="<?php echo $bannerlists->banner_id;?>">
              	<td><?php echo ++$i; ?></td>
                <td><?php echo $bannerlists->banner_name; ?></td>
                <td><?php echo $bannerlists->banner_desc; ?></td>
                <td>
                	<?php
						if(is_file($this->config->item('image_path').'/banner/1910x800/'.$bannerlists->banner_image))
						{$imageURL = base_url().'photo/banner/1910x800/'.$bannerlists->banner_image;}
						else
						{$imageURL = base_url().'photo/banner/no-cover.jpg';}
					?>
                	<img src="<?php echo $imageURL; ?>" class="img-responsive" width="150" />
                </td>
                <td class="center"><button class="btn btn-primary" id="editGroup" dir="<?php echo $bannerlists->banner_id;?>" data-toggle="modal" data-target="#myModal">Edit</button>
                  <button class="btn btn-primary" id="deleteGroup" dir="<?php echo $bannerlists->banner_id;?>">Delete</button></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="loaderGroup text-center" style="position: absolute; z-index: 50; display: none; left: 0%; top: 0%; height: 532px; width: 100%; padding-top: 36%; background: rgba(90, 164, 180, 0.64) none repeat scroll 0px 0px;"><img src="https://www.bupa.com/Assets/Global/Components/Img/ajax-loader.gif" width="100px" /></div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Banner</h4>
      </div>
      <div class="modal-body">
        <div class="form-group" id="errors2" style="color:#F00"></div>
        <?php echo form_open_multipart('admin/banner/bannerupdate',array('class' => 'form-horizontal', 'id' => 'myform'));  ?>
        <input type="hidden" class="banner_id_hidden" name="banner_id_hidden" value="" />
        <div class="form-group">
          <label class="col-lg-4 control-label">Banner Name<span style="color:#F00">*</span></label>
          <div class="col-lg-6">
            <?php
                                    $banner_name = array(
												'name'        => 'banner_name',
												'id'          => 'banner_name_id',
												'class'       => 'banner_name_class form-control',
												'maxlength'   => '100',
												'size'        => '50',
												'placeholder' => 'Banner name',
												'value'       =>  $this->session->userdata('banner_name')
												);
                                    echo form_input($banner_name);
								    echo form_error('banner_name'); 
                                    ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-4 control-label">Banner Description<span style="color:#F00">*</span></label>
          <div class="col-lg-6">
            <?php
                                    $banner_desc = array(
												'name'        => 'banner_desc',
												'id'          => 'banner_desc_id',
												'class'       => 'banner_desc_class form-control',
												'maxlength'   => '100',
												'size'        => '25',
												'rows'        => '3',
												'placeholder'       => 'Banner Description',
												'value'       =>  $this->session->userdata('banner_desc')
												);
                                    echo form_textarea($banner_desc);
									echo form_error('banner_desc'); 
                                    ?>
          </div>
        </div>
        <?php /*?> <div class="form-group">
             <label class="col-lg-4 control-label">Banner Status</label>
             <div class="col-lg-6">
              <select name="action" id="actionmodel" dir="<?php echo $results->skill_id;?>">
              </select>
                </div>
            </div><?php */?>
        <div class="form-group">
          <label class="col-lg-4 control-label">Banner Image</label>
          <div class="col-lg-4">
            <?php
                                    $banner = array(
												'name'        => 'user_file',
												'id'          => 'banner_id',
												'class'       => 'banner_class',
												'onchange'    => 'loadFile(event)',
												'value'       =>  set_value('user_file')
												);
                                    echo form_upload($banner);
									echo form_error('user_file');
									
                                    ?>
          </div>
          <div class="col-lg-4"> <img class="preview img-responsive" src="<?php $this->session->userdata('group_image'); ?>" id="preview" alt=""> </div>
          <div class="col-lg-12 text-center"> <span style="color:#F00">(Please upload 1,910px × 800px diamension Image)</span> </div>
        </div>
        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <input type="submit" class="btn btn-primary editdatagroup" value="Update!" />
          </div>
          <?php echo form_close(); ?> </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal --> 
  
</div>
<script src="<?php echo base_url()?>assets/js/jquery_validate.js"></script> 
<script>
$(document).ready(function(){
	var loader = "<?php echo base_url('assets/frontend/img/ripple.gif'); ?>";
	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div>';
            var oTable = $('#editable').DataTable({order:[[0,"desc"]]});
		
			$("#action").change(function(){
				alert($("#action").val());
			     var actionval= $(this).val();
				 var idunique= $('#action').attr('dir');
				 $.ajax({
					   url: "<?php echo base_url().'admin/skill/skillupdateaction' ?>",
					   type: "POST",
					   data: {skill_id:idunique,actionval:actionval},
					   dataType:"json",
					   cache:false,
					   async:true,
					   beforeSend : function(){
							$("body").append(lhtml);
					   },
					   success: function(data){
							   window.location.href = '<?php echo base_url(); ?>admin/skill/listskill'; 
							   }
						 
						 
					   })
			});
		
			
		$('.close').click(function(){
		   $('.error').empty();
		   $('.form-control.error').css('border','1px dotted #E5E6E7');
		});
		
		$(document).on('click','#deleteGroup',function(){
				var catid=$(this).attr('dir');
				 if(catid!=""){
					bootbox.confirm("Are you sure you want delete?", function(result) {
						if(result){
							window.location.href = "<?php echo base_url().'admin/banner/bannerdelete/' ?>"+catid;
						}
					}); 
				 }
		});
		
		$(document).on('click','#editGroup', function(){
				
				   
				   $('#skill_name_id').val("");
                   $('#skill_desc_id').val("");

                   var idunique = $(this).attr('dir');
				   var banner_id =  $(this).attr('dir');
				   //var banner_name= $("#"+idunique).find("td:first").text();
				   var banner_name= $('#'+idunique+' td:nth-child(2)').text();
				   var banner_desc= $('#'+idunique+' td:nth-child(3)').text();
				   var banner_image= $('#'+idunique+' td:nth-child(4) img').attr('src');
				   var skill_action= $('#'+idunique+' option:selected').val();
				   
				   var one= 1;
				   var zero= 0;
				   var selected= "selected"
				   
				   $('.loaderGroup').show();
				   setTimeout(function(){ 
				        if(skill_action==1){
							
					$("#actionmodel").append("<option value='"+one+"' selected='"+selected+"'>Enable</option><option value='"+zero+"'>Desable</option>");
							
							}
							else{
								$("#actionmodel").append("<option value='"+zero+"' selected='"+selected+"'>Desable</option><option value='"+one+"'>Enable</option>");
								
							}
				        
						$('#banner_name_id').val(banner_name);
						$('#banner_desc_id').val(banner_desc);
						$('.preview').attr('src',''+banner_image+''); 
						
						$('.banner_id_hidden').val(banner_id);
						//$('#banner_id').val(banner_image);
						$('.loaderGroup').hide();
						$('.error').empty();
				   
				   },3000);
                    })
        
		
		 //validation rules
		$("#myform").validate({
			onkeyup: false,
			onfocusout: false,
			errorElement: "div",
			errorPlacement: function(error, element) {
				$("div#errors2").empty();
				error.appendTo("div#errors2");
			}, 
			rules: {
				"banner_name" : {
					required : true,
											},
				"banner_desc" : {
					required : true,
										  }
				
					
										
			   
			   
			},
			messages: {
				"banner_name": {
					required: "Please Fill Up The Skill Dame ",
				   
				} ,
				 "banner_desc": {
					required: "Please Fill Up The Skill Description ",
					
				}
				 
				
			}
		});
		$.validator.addMethod("customFunction", function(value) {
			return value == "input";
		}, 'Please enter "input"!'); 
		
		
		});
		var loadFile = function(event) {
								oldimg = $('.preview').attr('src');
								var preview = document.getElementById('preview');
								preview.src = URL.createObjectURL(event.target.files[0]);
								newimg = preview.src;
								if(newimg.indexOf('/null') > -1) {
									preview.src = oldimg;
								}
};
</script>