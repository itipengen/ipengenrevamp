<?php
/*function to covert href link tag to '' */
function remove_links($s){
      while(TRUE){
        @list($pre,$mid) = explode('<a',$s,2);
        @list($mid,$post) = explode('</a>',$mid,2);
        $s = $pre.$post;
        if (is_null($post))return $s;
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<title>Editable Order</title>
<link href="<?php echo base_url();?>assets/admin/invoice/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/invoice/css/print.css" rel="stylesheet">
</head>

<body>
<div id="page-wrap">
  <div align="center" class="invoice_header">Order</div>
  <div id="logo"> <img  src="<?php echo base_url();?>assets/admin/invoice/images/logo.png" alt="logo" /> <br />
  </div>
  <div id="identity"> <?php echo $orderdetails[0]->shipping_firstname.' '.$orderdetails[0]->shipping_lastname;?> ( #<?php echo $orderdetails[0]->order_id;?> ) <br />
    <?php echo $orderdetails[0]->email;?> <br />
    Phone: <?php echo $orderdetails[0]->mobile;?> <br />
    <?php echo $orderdetails[0]->shipping_address;?> <?php echo ','.$orderdetails[0]->shipping_city;?> <br />
    <?php echo $orderdetails[0]-> shipping_postcode.' '.$orderdetails[0]->shipping_kecamatan;?> <br />
  </div>
  <div style="clear:both"></div>
  <div id="customer">
    <table id="meta" >
      <tr>
        <td class="meta-head">Order Id</td>
        <td><?php  echo $orderdetails[0]->order_id; ?></td>
      </tr>
      <tr>
        <td class="meta-head">Date</td>
        <td><?php  echo date("jS F, Y", strtotime($orderdetails[0]->date_added)); ?></td>
      </tr>
      <tr>
        <td class="meta-head">Total Amount</td>
        <td><div class="due">
            <?php  echo $currency.' '.$orderPaymentsDetails[0]->gross_amount; ?>
          </div></td>
      </tr>
      <tr>
        <td class="meta-head">Billing Address</td>
        <td><?php echo $orderdetails[0]->shipping_firstname.' '.$orderdetails[0]->shipping_lastname;?> <br />
          <?php echo $orderdetails[0]->shipping_address;?> <?php echo ','.$orderdetails[0]->shipping_city;?> <?php echo ','.$orderdetails[0]-> shipping_postcode.' '.$orderdetails[0]->shipping_kecamatan;?> <br /></td>
      </tr>
    </table>
  </div>
  <table id="items" style="height: 10%" >
    <tr>
      <th>Item</th>
      <th>Description</th>
      <th>Unit Cost</th>
      <th>Quantity</th>
      <th>Price</th>
    </tr>
    <?php
		   $subtotal=''; 
		  foreach($orderdetails as $orderdetailsdata){ 
		    $subtotal =   $orderdetailsdata->subtotal + $subtotal ; ?>
    <tr class="item-row">
      <td  class="item-name"><div class="delete-wpr">
          <?php  echo $orderdetailsdata->name; ?>
        </div></td>
      <td class="description"><?php  echo $result =remove_links($orderdetailsdata->item_description)?></td>
      <td><?php  echo $currency.' '.$orderdetailsdata->price; ?></td>
      <td><?php  echo $orderdetailsdata->quantity; ?></td>
      <td><span class="price">
        <?php  echo $currency.' '.$orderdetailsdata->subtotal; ?>
        </span></td>
    </tr>
    <?php } ?>
    <tr id="hiderow">
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr height ="10%">
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Subtotal</td>
      <td class="total-value"><div id="subtotal"><?php echo $currency.' '.$subtotal ;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Discount Amount</td>
      <td class="total-value"><div id="subtotal">
          <?php  echo $currency.' '.$orderdetails[0]->discount_amount;?>
        </div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Shipping Charge</td>
      <td class="total-value"><div id="subtotal"><?php echo $currency.' '.$orderdetails[0]->shipping_cost ;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Total</td>
      <td class="total-value"><div id="total">
          <?php  echo $currency.' '.$orderdetails[0]->gross_amount; ?>
        </div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Amount Paid</td>
      <td class="total-value"> 0.00</td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line balance">Total Amount</td>
      <td class="total-value balance"><div class="due">
          <?php  echo $currency.' '.$orderPaymentsDetails[0]->gross_amount; ?>
        </div></td>
    </tr>
  </table>
</div>
</body>
</html>