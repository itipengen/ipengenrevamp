

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>User List</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index-2.html">Home</a>
                        </li>
                        <li>
                            <a>User</a>
                        </li>
                        <li class="active">
                            <strong>View</strong>
                        </li>
                    </ol>
                </div>

            </div>
            <div class="wrapper wrapper-content animated fadeInRight">

                        <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                           
                            <!--<div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>-->
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="get">
                                <div class="form-group"><label class="col-sm-2 control-label">First Name</label>

                                    <div class="col-sm-10"><?php echo $result[0]->fname;?></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Last Name</label>

                                    <div class="col-sm-10"><?php echo $result[0]->lname?></div>
                                </div>
                                 <div class="hr-line-dashed"></div>
                               
                               
                                <div class="form-group"><label class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10"><?php echo $result[0]->email?></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                        
								<div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                    

                                    <div class="col-sm-10">
                                       
                                        <div><label> <?php if($result[0]->is_block=='yes'){ echo 'Active';}else{echo 'Inactive';} ?></label></div>
                                        
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="<?php echo base_url()?>admin/user/userlist"><button type="button" class="btn btn-white">Cancel</button></a>
                                        <a href="<?php echo base_url()?>admin/user/edit/<?php echo $result[0]->id;?>"><button type="button" class="btn btn-primary">Edit</button></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
















