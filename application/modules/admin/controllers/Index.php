<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Index extends MX_Controller
	{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model(array('login_model','dashboard_model')); 
	}
	public function index()
	{
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}else{redirect('admin/dashboard');}
	}

	function login()
	{ 
	$data["page_title"] = "Admin login";
		if (!$this->session->userdata('logged_in'))
		{
			if(isset($_POST['submit']))
			{ 
				  $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	
				  $this->form_validation->set_rules('password', 'Password', 'required');
	
					if($this->form_validation->run() == TRUE)
					{
						$username = $this->input->post('email');
						$password = $this->input->post('password');
						$result   = $this->login_model->login($username, $password);
						//print_r($result);die('jjj');
						if(!empty($result))
						{
							if($result[0]->status != 0)
							{
								$this->session->set_userdata('logged_in',$result[0]->email);
								redirect('admin/dashboard');
							}
							else
							{
								
								$this->session->set_flashdata('msg','Access Denied by Administrator!');
								redirect("admin/login");
							}
						}
						else
						{
							$this->session->set_flashdata('msg','Email or Password is invalid!');
							$data["page_title"] = "Admin Log-In || iPengen";
							$this->load->view('admin/login',$data);	
						}
	
					}
					else
					{
						$this->session->set_flashdata('msg','Invalid Email or Password is required!');
						$data["page_title"] = "Admin Log-In || iPengen";
						$this->load->view('admin/login',$data);
					}
				}
			else
			{ 
				$data["page_title"] = "Admin Log-In || iPengen";
				$this->load->view('admin/login',$data);
			}
		}
		else
		{
			redirect('admin/dashboard');
		}
	 }

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		redirect('admin/login');
	}

	function forget_password()
	{
		$data["page_title"] = "Forget password";
		if(isset($_POST['email_submit']))
		{
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() == TRUE)
			{
				$cusd=$this->login_model->forgotemail($this->input->post('email'));
					if(!empty($cusd))
					{
						$this->email->initialize($this->config->item('smtp_info'));
						$email_to = $this->input->post('email');
						$subject="[Ipengen] Change Password";	//subject
						
						$data['username'] = ucfirst($cusd->first_name).' '.ucfirst($cusd->last_name);
						$data['imgURL'] = base_url('');
						$data['link'] = base_url('admin/resetpassword/'.base64_encode($email_to));
						$message = $this->load->view('e_template/password-change',$data, true);
						echo $message;
						$this->email->to($email_to);
						$this->email->from($this->config->item('email_form'), $this->config->item('wbsite_name'));
						$this->email->subject($subject);
						$this->email->message($message);
						/*if($this->email->send())
						{
						echo 'success';
							//$this->session->set_flashdata('msg','Thank You! Please check your mail to reset password.');
							//redirect('admin/login');
						}
						else
						{
							echo 'Faild';
							//$this->session->set_flashdata('forgetpwd_ErrMsg','Oops..!!! Something Wrong..Please try again.');
//							redirect('admin/forget_password');
						}
						echo $this->email->print_debugger();*/
	
						
					}
					else
					{
						$this->session->set_flashdata('msg','Email is not registered!');
						redirect('admin/login'); 
					}
			}
			else
			{
				$data["page_title"] = "Admin Forget Password | iPengen";
				$this->load->view('admin/fotgotpassword',$data);
			}
		}
		else
		{
			$data["page_title"] = "Admin Forget Password | iPengen";
			$this->load->view('admin/fotgotpassword',$data);
		}
	}

    function resetpassword($log_email)
	{
		$data["page_title"] = "reset password";
		if(!empty($_POST))
		{
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('re_password', 'Re-enter password', 'required|matches[password]');
			if($this->form_validation->run() == TRUE)
			{
				$password=$this->input->post('password');
				$total_result=$this->login_model->admin_exist();
				if($total_result)
				{
					foreach($total_result as $result_singal)
					{
						if($log_email !== NULL || $log_email != '')
						 $this->db->update('admin_login',array('password'=>md5($this->config->item('email_verification_salt').$password)),array('email'=>base64_decode($log_email)));
						 $this->session->set_flashdata('msg','Password Change successfully');
							redirect('admin/login');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('re-msg','Password is not matched.');
				redirect('admin/resetpassword/'.$log_id);}
			}
		else
		{
			$this->load->view('admin/resetpassword',array('id'=>$log_email,"page_title"=>"Admin Password Reset || iPengen"));
		}

	}	

	public function dashboard()
	{
		$data["page_title"] = "Dashboard";
		$data["user_count"] = $this->dashboard_model->registered_user_count();
		$data["total_order_count"] = $this->dashboard_model->total_order_count();
		$data["best_selling_product"] = $this->dashboard_model->best_selling_product();
		$data["wishlist_count"] = $this->dashboard_model->wishlist_create_count();
		$data["latest_order"] = $this->dashboard_model->latest_order();
		$data["latest_gift"] = $this->dashboard_model->latest_gift();
		$data["revenue_count"] = $this->dashboard_model->revenue_count();
		$this->load->template('site/index',$data);
	}


   public function notifiction(){
	   
	   $data["page_title"] = "Notification";
	   $data['notificationresult'] = "";
	   $this->load->template('site/notification',$data);
	   
	   
	}
	
	public function ajaxnotification(){
		// storing  request (ie, get/post) global array to a variable  
		$requestData= $_REQUEST;
		
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'type',
			2=> 'message',
			3=> 'date',
			4=> 'status',
		);
		
		// getting total number records without any search
		$this->db->select('*');
		$this->db->from('ig_admin_notification');
		$query = $this->db->get();
		$totalData = $query->num_rows();
		$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		
		
		
		
		
		
		
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('*');
			$this->db->from('ig_admin_notification');
			$this->db->like('message', $requestData['search']['value']);
			$this->db->or_like('type', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
		else{
			$this->db->select('*');
			$this->db->from('ig_admin_notification');
			$this->db->like('message', $requestData['search']['value']);
			$this->db->or_like('type', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			}

		$result11 = $query12->result_array();
		
        $data = array();
		foreach($result11 as $row){  // preparing an array
		
		if($row['status'] == 1){
				$status = '<span class="label label-sm label-warning"> Read </span>';
			}
			else{
				$status = '<span class="label label-sm label-success"> Unread </span>';
				}
		
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["type"]);
			$nestedData[] = ucfirst($row["message"]);
			$nestedData[] = date('d/m/Y', strtotime($row["date"]));
			$nestedData[] = $status;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		  "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
			echo json_encode($json_data);  // send data as json format
		
		}


		



	}



?>