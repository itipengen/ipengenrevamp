<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
	class Media extends MX_Controller { 
	
	public function __construct(){
		parent::__construct('pagination'); 
		$this->load->model('media_model'); 
		$this->load->library('session');    
		if (!$this->session->userdata('logged_in')){
			redirect('admin/login');
		}
	} 
				
				
				
	public function index(){   
	        
			  $totalMediaImages["page_title"] = "Server Image List | iPengen";
			 
			  if(isset($_POST['delsubmit'])){
			
			 $mediafiles =  $this->input->post('mediafiles');
			  
			 
			 foreach($mediafiles as $mediafile)
			 {
				 if(is_file('./files/uploads/'.$mediafile))
					unlink('./files/uploads/'.$mediafile);
				if(is_file('./files/uploads/thumbnail/'.$mediafile))
					unlink('./files/uploads/thumbnail/'.$mediafile);
					
				 }
				 $countmedia= count($mediafiles).'media files deleted successfully.';
				 $this->session->set_flashdata('mediafile_msg',$countmedia);
			  }
			    
			   
			   $thumbImages = glob('files/uploads/thumbnail/*', GLOB_NOSORT);
			   array_multisort(array_map('filemtime', $thumbImages), SORT_NUMERIC, SORT_DESC, $thumbImages);
			    $totalMediaImages['mediaThumbnailImages']= $thumbImages;
				   $this->load->template('media/media_images',$totalMediaImages); 
		       } 
			   
   public function add()
          {
			  $totalMediaImages["page_title"] = "Add Server Images | iPengen";
			  $this->load->template('media/add',$totalMediaImages); 
			  }
     }/*end class Media */ 
	 