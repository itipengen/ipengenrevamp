<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller { 

	public function __construct() 

				{parent::__construct('pagination'); 

				   $this->load->model(array('product_model','dashboard_model','order_model')); 
				    $this->load->library('session'); 
					$this->load->helper(array('ipengen_email_helper'));
					$this->load->library('imageresize');    
					$this->load->library(array('form_validation','csvimport','imageresize','upload','unzip','excel'));
				    if (!$this->session->userdata('logged_in')){redirect('admin/login');}
					$this->extension_array = array('jpg','png','jpeg','JPG','PNG','JPEG');
                 
				} 

	
	

			public function index() 
			{   
				$get_merchant=array();
				$data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$merchant = $this->product_model->get_merchant();
				if(!empty($merchant)){
					foreach($merchant as $val){
						$get_merchant[$val['marchent_id']]	= $val['marchent_name'];
					}	
				}
				$data['get_merchant'] = $get_merchant;
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->template('products/addproduct',$data); 
				
			} 

			public function addproduct()	 
			{ 
			 $this->load->helper('directory'); 
			 if($this->input->post('price') >= $this->input->post('saleprice'))
			 {
					
					$data=array( 
								 'merchant_id'=>$this->input->post('merchant'),
								 'sku'=>$this->input->post('SKU'), 
								 'stock_quantity'=>$this->input->post('stock_quantity'), 
								 'stock' => $this->input->post('stock'),
								 'price'=>$this->input->post('price'),
								 'category_id'=>$this->input->post('category'),
								 'brand_id'=>$this->input->post('brand'),
								 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
								 'sale_start_date'=>$this->input->post('startdate'),
								 'sale_end_date'=>$this->input->post('enddate'),
								 'create_date'=>date('Y-m-d H:i:s'), 
								 'shipping_weight'=>$this->input->post('shipping_weight'),
								 'shipping_dimension'=>$this->input->post('shipping_dimension'),
								  ); 
								  //print_r($data);die();
							$getProductId=$this->product_model->insert_products($data); 
							$insert_id = $this->db->insert_id();//$this->db->insert_id();2284
							$productDetails = $this->product_model->getProductById($insert_id);
							//print_r($productDetails->category_id);die();
							$result_id=$productDetails->category_id;
							if($result_id!=""){
								$category = $this->product_model->category_id($result_id);
								$cat_name = $category->cat_id;
								$parent_id = $this->product_model->getcatParentId($result_id);
								if($parent_id != 0){
									$catgry = $this->product_model->category_id($parent_id);
									$category_name = $catgry->cat_id;
									$parent_details = $this->product_model->getcategoryById($parent_id);
									foreach($parent_details as $parent){
										$category_id = $parent->parent_id;	
										if($category_id == 0){
											$string = ucfirst($category_name).','.ucfirst($cat_name);
										}
									}
								}else{
									$string = ucfirst($cat_name);
									
								}
									$updateProduct = $this->product_model->updateProduct($insert_id,$string);
								//echo $string;die();
						
							}
				
				
							
							$row_ids = $this->input->post('row');
							$product_name = $this->input->post('product_name');
							$language_id = $this->input->post('language_id');
							$short_description = $this->input->post('s_description'); 
							$message = $this->input->post('editor'); 
							$datatext = array();
							//print_r($language_id); die();
							$post = $this->input->post(); 
							for ($i = 0; $i < ($this->input->post('row')); $i++)
							{
								$datatext[] = array(
								'product_id' => $getProductId,
								'language_id' => $language_id[$i],
								'product_name' => $product_name[$i],
								'short_description' => $short_description[$i],
								'message' => $message[$i],
								);
								
						
							}

							$this->product_model->insert_producttext($datatext);	

						 
						if(!empty($_POST['product_image'] )) 
						{ 
						$product_image = $_POST['product_image'];
						/*if (!is_dir('media/'.$getProductId)) { 
							mkdir('./media/'.$getProductId, 0777, true); 
						}*/ 
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'), 0777, true); 
						}
						
								$productImageExtension= explode('.',$product_image); 
								$img_file_name = $getProductId."_".time().'.'.$productImageExtension[1]; 

								$config['image_library'] = 'gd2'; 

								$this->load->library('image_lib', $config); 

						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
						if ($width>$height) {
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$img_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
								$this->image_lib->initialize($config); 
								$this->image_lib->resize();

							/*--------------------Image Resize for 250*250------------------------------------------*/
							$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
							$this->image_lib->resize();
							/*------------------------Image Resize for 250*250------------------------------------------*/
							/*-------------------------Image Resize for 400*310------------------------------------------*/
							
							$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.
$this->config->item('medium_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
							/*----------------------------Image Resize for 400*310------------------------------------------*/
							/*--------------------------Image Resize for 650*500------------------------------------------*/
							
							$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
							$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
							$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$img_file_name;
							$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
							
							/*-----------------------Image Resize for 650*500------------------------------------------*/
							$file_name = $img_file_name;  
							$this->product_model->updateProductByImages($getProductId,$file_name); 
							
						} 
						/*additional images computer*/
						if(isset($_POST['serverhiddenid']))
						{
	
							$productImageIds = $_POST['serverhiddenid'];
	
							$index=0;
	
							foreach($productImageIds as $productImageId)
	
							{
	
								$productImageExtension= explode('.',$productImageId);
	
								$additional_file_name = $getProductId.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
	
								$config['image_library'] = 'gd2'; 
	
								$this->load->library('image_lib', $config); 
	
						list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height){
						$orientation = "width";
						} else {
						$orientation = "height";
						}
						
						/*------------------------original Image Upload----------------------------*/
						$config = array( 
										'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
										'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$additional_file_name, 
										'maintain_ratio'    => FALSE, 
										'width'             => $width, 
										'height'            => $height, 
										'master_dim' =>'width' 
										); 
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						
						/*--------------------Image Resize for 250*250------------------------------------------*/
						/*-----------------------Image Resize for 250*250------------------------------------------*/
						$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
						
						/*---------------Image Resize for 250*250------------------------------------------*/
						/*-----------------Image Resize for 450*450------------------------------------------*/
						
						$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
						/*-----------------Image Resize for 450*450------------------------------------------*/
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
						$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
						$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$additional_file_name;
						$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
						
						/*-------------------Image Resize for 800*800------------------------------------------*/
						
						$this->product_model->additionProductByImages($getProductId,$additional_file_name);
						$index = $index+1;
					  }
	
					}	
				redirect('admin/product/productlist');	
			 }
			 else
			 {
				 $this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
				 $data["page_title"] = "Add Product | iPengen";
				$data['productimages'] = glob('mediafiles/thumbnail/*', GLOB_NOSORT);
				$data['language'] = $this->product_model->get_language();
				$data['cat_list'] = $this->product_model->get_category_list();
				$data['brands'] = $this->product_model->get_brand_list();
				$this->load->template('products/addproduct',$data); 
			}

										

					} 

			public function view($id = null)
			{ 
				$data["page_title"] = "View Product | iPengen";
				$data['product'] = $this->product_model->getProductById($id); 
				$data['customers'] = $this->product_model->getCustomers(); 
				$data['assign_customers'] = $this->product_model->getCustomerByProductId($id);	 
				$this->load->template('products/view',$data);	 

			} 

	public function assignCustomer(){ 

		 

		$cusID = $_GET['customerID']; 

		$productID = $_GET['product_id']; 

            

         $rowno =$this->customer_model->isAssignproduct($productID,$cusID); 

		  

		 if($rowno) 

		{ 

		$products['success'] = $this->product_model->delete_customer($cusID,$productID); 

		$products['type'] = "delete"; 

		$products['status'] = $rowno; 

		} 

		else 

		{ 

		$products['success']  = $this->product_model->assignCustomersToProduct($cusID,$productID); 

		$products['type'] = "assign"; 

		$products['status'] = $rowno; 

		} 

		 

		echo json_encode($products); 

	}	 

	public function deleteCustomer(){ 

		$customer_id = $_GET['customer_id']; 

		$product_id = $_GET['product_id']; 

		$response = $this->product_model->delete_customer($customer_id,$product_id);	 

		echo json_encode($response); 

	} 

	public function checkassignCustomer(){ 

		$pro_id = $_GET['product_id']; 

		echo json_encode($this->product_model->getCustomerByProductId($pro_id)); 	 

		 

	} 

	public function productlist() {
		$data["page_title"] = "Product List | iPengen";
		$data['p_list']=$this->product_model->list_products();
		//print_r($data);die();
		$this->load->template('products/productlist',$data); 
	
		} 
		
	public function ajaxproductlist() {
		// storing  request (ie, get/post) global array to a variable  
		$requestData= $_REQUEST;
		
		
		 
		  
		$columns = array(  
		// datatable column index  => database column name
			0 =>'product_id', 
			1 =>'product_name',
			2=> 'marchent_name',
			3=> 'stock_quantity',
			4=> 'price',
			5=> 'sale_price',
			6=> 'product_image',
			7=> 'feature_status',
			8=> 'admin_status'
		);
		
		// getting total number records without any search
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
		$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
		$this->db->where('ig_products_text.language_id', 1);
		$query = $this->db->get();
		$totalData = $query->num_rows();
		$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		
		
		
		
		
		
		
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('ig_products.product_id as product_id,ig_products_text.product_name as product_name,ig_marchent_login.marchent_name as marchent_name,ig_products.stock_quantity as stock_quantity,ig_products.price as price,ig_products.sale_price as sale_price,ig_products.product_image as product_image,ig_products.feature_status as feature_status,ig_products.admin_status as admin_status');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
			$this->db->where('ig_products_text.language_id', 1);
			//$this->db->like('ig_products_text.product_name', $requestData['search']['value']);
			//$this->db->or_like('ig_products.product_id', $requestData['search']['value']);
			$where = '(ig_products_text.product_name='.$requestData['search']['value'].' or ig_products.product_id = '.$requestData['search']['value'].')';
            $this->db->where($where);
			
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			//echo $this->db->last_query();
			
			
		}
		else{
			$this->db->select('ig_products.product_id as product_id,ig_products_text.product_name as product_name,ig_marchent_login.marchent_name as marchent_name,ig_products.stock_quantity as stock_quantity,ig_products.price as price,ig_products.sale_price as sale_price,ig_products.product_image as product_image,ig_products.feature_status as feature_status,ig_products.admin_status as admin_status');
			$this->db->from('ig_products');
			$this->db->join('ig_products_text', 'ig_products.product_id =  ig_products_text.product_id');
			$this->db->join('ig_marchent_login', 'ig_products.merchant_id =  ig_marchent_login.marchent_id');
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->where('ig_products_text.language_id', 1);
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}

		
		
		
		$result11 = $query12->result_array();
		
		//print_r($query1->unbuffered_row());
		
        $data = array();
		foreach($result11 as $row){  // preparing an array
		
		    $price = strtoupper($this->config->item('currency')).' '.number_format($row["price"]);
			$sale_price = strtoupper($this->config->item('currency')).' '.number_format($row["sale_price"]);
			
			//image data store in this variable
			if(is_file($this->config->item('image_path').'/product/'.$row['product_id'].$this->config->item('thumb_size').$row['product_image'])){
			$product_image = "<img src='".base_url('photo/product/'.$row['product_id'].$this->config->item('thumb_size').$row['product_image'])."'   width='36' height='36' class='img-circle imageZoom' id='".base_url('photo/product/'.$row['product_id'].$this->config->item('medium_thumb_size').$row['product_image'])."'><img src='' style='display:none;'/>";
			
			}else{
				$product_image = "<img src=".base_url('photo/product/no_product.jpg')." width='36' height='36' class='img-circle imageZoom' 
				src=".base_url('photo/product/no_product.jpg').">";
			}
			// Feature Status check
			if ($row['feature_status'] == 0) {
                    $check = '';
                  }else{
                    $check = 'checked';
                  }
			
			$feature_status = "<input type='checkbox' value='".$row['product_id']."' class='js_switch_feature' ".$check." />";
			// Admin Status check
			 if ($row['admin_status'] == 0) {
                    $checkad = '';
                  }else{
                    $checkad = 'checked';
                  }
			$admin_status = "<input type='checkbox' value='".$row['product_id']."' class='js-switch' ".$checkad." />";
			
			$action = "<div style='margin-top: 10px;'> 
              <a href='".site_url('admin/product/productview/'.$row['product_id'])."' title='Product View'> 
              <i class='fa fa-eye fa-fw' style='color:#508DF2'></i> 
              </a> <a href='".site_url('admin/product/edit/'.$row['product_id'])."' title='Product Edit'>
               <i class='fa fa-pencil-square-o fa-fw action-btn'></i> </a> <a href='javascript:void(0)' class='product_delete' id='".$row['product_id']."' title='Product Delete'> <i class='fa fa-trash-o fa-fw action-btn text-danger'></i> </a> </div>";
			$view = '<a class="overlay" target="_blank" href="'.$this->dashboard_model->getProductUrl_name($row['product_id'],$row['product_name']).'" title="Front View"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>';
			
			$nestedData=array(); 
			$nestedData[] = $row["product_id"];
			$nestedData[] = $row["product_name"];
			$nestedData[] = $row["marchent_name"];
			$nestedData[] = $row["stock_quantity"];
			$nestedData[] = $price;
			$nestedData[] = $sale_price;
			$nestedData[] = $product_image;
			$nestedData[] = $feature_status;
			$nestedData[] = $admin_status;
			$nestedData[] = $view;
			$nestedData[] = $action;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
echo json_encode($json_data);  // send data as json format
	
		}

	public function edit($id = NULL) { 
		$data["page_title"] = "Edit Product | iPengen";
		$merchant = $this->product_model->get_merchant();
		if(!empty($merchant)){
			foreach($merchant as $val){
				$get_merchant[$val['marchent_id']]	= $val['marchent_name'];
			}	
		}
		$data['get_merchant'] = $get_merchant;
		$data['row'] = $this->product_model->edit_product($id); 
		$data['language'] = $this->product_model->get_editlanguage($id); 
		$data['galleries']=$this->product_model->exit_id($id);
		$data['cat_list'] = $this->product_model->get_category_list();
		$data['brands'] = $this->product_model->get_brand_list();
		$this->load->template('products/edit',$data); 
	
	}	 

	public function delete($pro_id = NULL) { 
	
	  $this->product_model->deleteProductById($pro_id); 
	
	  redirect('admin/product/list'); 
	
	  }	    

	public function editproduct($p_id = NULL)
	{ 
		if($this->input->post('price') >= $this->input->post('saleprice'))	
		{				
				$data=array( 
						'merchant_id'=>$this->input->post('merchant'),
						 'sku'=>$this->input->post('SKU'), 
						 'stock_quantity'=>$this->input->post('stock_quantity'), 
						 'stock' => $this->input->post('stock'),
						 'price'=>$this->input->post('price'),
						 'sale_price'=>($this->input->post('saleprice') != '')?$this->input->post('saleprice'):$this->input->post('price'),
						 'category_id'=>$this->input->post('category'),
						 'brand_id'=>$this->input->post('brand'),
						 'sale_start_date'=>$this->input->post('startdate'),
						 'sale_end_date'=>$this->input->post('enddate'),
						 'create_date'=>date('Y-m-d H:i:s'), 
						 'shipping_weight'=>$this->input->post('shipping_weight'),
						 'shipping_dimension'=>$this->input->post('shipping_dimension'),
						  ); 
					//print_r($data);die('fff');
					$getProductId=$this->product_model->update_product($data,$p_id);
					
					$row_ids = $this->input->post('row');
					$product_name = $this->input->post('product_name');
					$language_id = $this->input->post('language_id');
					$short_description = $this->input->post('s_description'); 
					$message = $this->input->post('editor'); 
					$datatext = array();
					//print_r($language_id); die();
					$post = $this->input->post(); 
					for ($i = 0; $i < ($this->input->post('row')); $i++)
						{
							$datatext[] = array(
							'product_id' => $getProductId,
							'language_id' => $language_id[$i],
							'product_name' => $product_name[$i],
							'short_description' => $short_description[$i],
							'message' => $message[$i],
							);
						 }	
					$this->product_model->updet_producttext($datatext,$p_id);	
	
								
					if (!empty($_POST['product_image'] )) 
					{ 

	$product_image = $_POST['product_image'];

	$product_previous_image= $this->product_model->getProductImageByID($p_id);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$product_previous_image->product_image);
	
	if(is_file($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image))
	   unlink($this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$product_previous_image->product_image);
	
	
	
	if (!is_dir($this->config->item('image_path').'product/')) { 
		 mkdir($this->config->item('image_path').'product/', 0777, true); 
		} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
	} 
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
	}
	
	if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
		 mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
	}
	
	$productImageExtension= explode('.',$product_image); 
    $img_file_name = $p_id."_".time().'.'.$productImageExtension[1]; 
	
	$config['image_library'] = 'gd2'; 
	$this->load->library('image_lib', $config); 
					

					/*$config = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100-100/'.$img_file_name, 

					'maintain_ratio'    => FALSE, 

					'width'             => 250, 

					'height'            => 250, 

					'master_dim' 		=> 'width' 

					);  

				   $this->image_lib->initialize($config); 

					$this->image_lib->resize();*/ 

					

					/*$config1 = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400-400/'.$img_file_name, 
					'maintain_ratio'    => TRUE, 
							
					'width'             => 250, 
				
					'height'             => 250, 

					'master_dim' 		=> 'width' 

					); 

					$this->image_lib->initialize($config1); 
					$this->image_lib->resize();	*/

		
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$product_image);
					if ($width > $height) {
						 $orientation = "width";
					} else {
						 $orientation = "height";
					}
					
		/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$img_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();

		/*----------------------------Image Resize for 250*250------------------------------------------*/
			$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
			$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
		
		/*--------------------------Image Resize for 250*250------------------------------------------*/
		/*-----------------------------Image Resize for 400*310------------------------------------------*/

			$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
			$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
			$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$img_file_name;
			$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
		/*-------------------Image Resize for 400*310------------------------------------------*/
		/*---------------Image Resize for 650*500------------------------------------------*/
		
		$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>100);
		$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$product_image;
		$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$img_file_name;
		$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
	
		/*------------------------------Image Resize for 650*500------------------------------------------*/
					

					 

					/*$config = array( 

					'source_image'      => $this->config->item('upload_image_path').'uploads/'.$product_image, 

					'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800-800/'.$img_file_name, 

					'maintain_ratio'    => FALSE, 

					'width'             => 650, 

					'height'            => 500, 

					'master_dim' 		=> 'width' 

					); 

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();	*/ 

					 

					 $file_name = $img_file_name;  

					 $this->product_model->updateProductByImages($p_id,$file_name); 

		} 
	
					if(isset($_POST['deleteproductsimages']))
					{
				 $deleteproductsimages = $_POST['deleteproductsimages'];
		 foreach($deleteproductsimages as $deleteproductsimage){
			  if($deleteproductsimage!=''){  
				 $getProductId=$this->product_model->deleteExistProductByID($deleteproductsimage,$p_id);
				}
			}
		}
					/*additional images computer*/
					if(isset($_POST['serverhiddenid']))
					{
						//echo "Starting Error";
						$productImageIds = $_POST['serverhiddenid'];
						$index=0;
						
						if (!is_dir($this->config->item('image_path').'product/')) { 
							mkdir($this->config->item('image_path').'product/', 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size'), 0777, true); 
						} 
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size'), 0777, true); 
						}
						if (!is_dir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'))) { 
							mkdir($this->config->item('image_path').'product/'.$p_id.$this->config->item('original'), 0777, true); 
						}
						/*---------------------------------------- End Create directory----------------------------------*/

						foreach($productImageIds as $productImageId)
						{

							$productImageExtension= explode('.',$productImageId);

							$additional_file_name = $p_id.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  

							$config['image_library'] = 'gd2'; 

							$this->load->library('image_lib', $config); 

							
					list($width, $height) = getimagesize($this->config->item('upload_image_path').'uploads/'.$productImageId);
						if($width > $height)
						{
						$orientation = "width";
						}
						else
						{
							$orientation = "height";
						}
					
					/*------------------------original Image Upload----------------------------*/
					$config = array( 
									'source_image'      => $this->config->item('upload_image_path').'uploads/'.$productImageId, 
									'new_image'         => $this->config->item('image_path').'product/'.$p_id.$this->config->item('original').$additional_file_name, 
									'maintain_ratio'    => FALSE, 
									'width'             => $width, 
									'height'            => $height, 
									'master_dim' =>'width' 
									); 
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
					
					
				$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path250_250 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path250_250 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
				
				/*------------------Image Resize for 250*250------------------------------------------*/
				/*---------------Image Resize for 400*310------------------------------------------*/
				
				$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path400_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path400_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('medium_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
				/*----- ----------Image Resize for 400*310------------------------------------------*/
				/*----------------Image Resize for 650*500------------------------------------------*/
				
				$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80);
				$source_path650_310 = $this->config->item('upload_image_path').'uploads/'.$productImageId;
				$despath_path650_310 = $this->config->item('image_path').'product/'.$p_id.$this->config->item('large_thumb_size').$additional_file_name;
				$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
				
				/*-----------------------Image Resize for 650*500------------------------------------------*/
							$this->product_model->updationProductByImages($p_id,$additional_file_name);
							$index = $index+1;
						}
					}	

			redirect('admin/product/productlist');	
		}
		else
		{
			$this->session->set_flashdata("error_msg","Sale Price must be less than Main Price.");
			$data["page_title"] = "Edit Product | iPengen";
			$data['row'] = $this->product_model->edit_product($p_id); 
			$data['language'] = $this->product_model->get_editlanguage($p_id); 
			$data['galleries']=$this->product_model->exit_id($p_id);
			$data['cat_list'] = $this->product_model->get_category_list();
			$data['brands'] = $this->product_model->get_brand_list();
			$this->load->template('products/edit',$data);
		}
	
	}	    

	public function gallery($p_id = NULL ) { 
	
	$files = $_FILES;   
	$product_img_arr = $_FILES['pimages']['name']; 
	
	$p_name=$this->input->post('get_p_name'); 
	
	$p_img_id=$this->input->post('pimageId[]'); 
	
	for($i=0;$i<count($product_img_arr);$i++) 
	
	{ 
	
		$_FILES['pimages']['name']= $files['pimages']['name'][$i]; 
	
		$_FILES['pimages']['type']= $files['pimages']['type'][$i]; 
	
		$_FILES['pimages']['tmp_name']= $files['pimages']['tmp_name'][$i]; 
	
		$_FILES['pimages']['error']= $files['pimages']['error'][$i]; 
	
		$_FILES['pimages']['size']= $files['pimages']['size'][$i];     
	
					$config = array(); 
	
					$config['upload_path'] = $this->config->item('image_path').'product/'.$p_id.'/originalImages/'; 
	
					$config['allowed_types'] = 'jpg|jpeg|gif|png'; 
	
					$config['file_name'] = $p_id.'_gallery_'.date("Ymdhis"); 
	
					$config['overwrite']     = FALSE; 
	
					$this->upload->initialize($config); 
	
					 
	
					 if ($this->upload->do_upload('pimages')) 
	
						 { 
	
							$img = $this->upload->data(); 
	
							 
	
							 
	
							$this->load->library('image_lib', $config);  
	
							 $config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/400*400/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 400, 
	
							'height'            => 300, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							 
	
							 
	
							 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/800*800/', 
	
							'maintain_ratio'    => TRUE, 
	
							'width'             => 650, 
	
							'height'            => 500, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$config = array( 
	
							'source_image'      => $this->config->item('image_path').'product/'.$p_id.'/originalImages/'.$img['file_name'], 
	
							'new_image'         => $this->config->item('image_path').'product/'.$p_id.'/100*100/', 
	
							'maintain_ratio'    => FALSE, 
	
							'width'             => 250, 
	
							'height'            => 250, 
	
							'master_dim' =>'width' 
	
							); 
	
							$this->image_lib->initialize($config); 
	
							$this->image_lib->resize();	 
	
							$file_name = $img['file_name']; 
	
							$get_product_gallery_id=$this->product_model->exit_id($p_id); 
	
							if(is_array($p_img_id) && isset($p_img_id[$i]) && $p_img_id[$i]!="" ) 
	
							{ 
	
								 
	
							$this->product_model->edit_gallery($file_name,$p_img_id[$i]); 
	
							} 
	
							else 
	
							{ 
	
							$this->product_model->insert_gallery($file_name,$p_id); 
	
							} 
	
						 } 
	
				} 
	
			$this->session->set_flashdata('message', 'Galleries updated successfully. '); 
	
				$data['row'] = $this->product_model->edit_product($p_id); 
	
				$data['gallery']=$this->product_model->exit_id($p_id); 
	
			   // $this->load->template('products/edit/'.$id.'#tab_1_2',$data); 
	
				redirect('admin/product/edit/'.$p_id.'#productIMG',$data);	 
	
					exit(); 
	
	}
	/*end function gallery*/ 
	public function search() { 
	
	$search=$this->input->get('searchData'); 
	
	 
	
	$elements = $this->product_model->searchProductByElement($search); 
	
	  
	
	 foreach($elements as $element) 
	
	 { 
	
		 $editUrl = site_url('admin/product/edit/'.$element->product_id); 
	
		$deleteUrl = site_url('admin/product/delete/'.$element->product_id); 
	
		 $imgPath= base_url().'uploads/productImages/'.$element->product_id.'/100*100/'.$element->product_image; 
	
		 $url = site_url('admin/product/view/'.$element->product_id); 
	
		 echo '<div class="col-md-3 col-sm-3 gapbottom_10" > 
	
											<div style="border: 1px solid #dcdbd9;"> 
	
												<div > 
	
												 
	
												 <a href="'.$url.'"><img  src="'.$imgPath.'"  width="100%"  id="product-img"/></a></div> 
	
												  
	
												  
	
												 <div style="height: 90px; border-top: 1px solid #dcdbd9; background-color: rgb(255, 255, 255);"> 
	
												<div class="text-center prodTxt" style="color:#000;font-size: 14px;font-weight: bold; margin-top:20px;">'.$element->product_name.'</div> 
	
												 
	
												 
	
												 <div class="text-center priceTxt" style="font-size:14px;font-weight:bold;color:#fb0b23;">'.$this->config->item('currency').$element->price.'</div> 
	
												  
	
												  
	
												  <div class="btn-group"> 
	
																	<span class="btn green"  data-toggle="dropdown"> 
	
																						  Actions 
	
																		<i class="fa fa-angle-down"></i> 
	
																	</span> 
	
																	<ul class="dropdown-menu"> 
	
																		<li> 
	
																			<a href="'.$editUrl.'"> 
	
																				<i class="fa fa-pencil"></i> Edit </a> 
	
																		</li> 
	
																		<li> 
	
																		   <a href="'.$deleteUrl.'"> 
	
																				<i class="fa fa-trash-o"></i> Delete </a> 
	
																		</li> 
	
																	</ul> 
	
																</div>';     
	
									   
	
												   if($element->stock_quantity=='0' || $element->stock=='0'){ 
	
		echo '<div class="outOfstock" style="color: red; width: 50%; position: absolute; right: 21px; z-index: 100; bottom: 13px; ; background: rgb(222, 231, 222) none repeat scroll 0% 0%; text-align:center; ">out of stock</div>'; 
	
	}                               
	
												  
	
												echo'<input type="hidden" name="chkbox_'.$element->product_id.'" id="chkbox_'.$element->product_id.'" value=""  /> 
	
												  
	
												 </div> 
	
												 
	
											</div> 
	
										</div>'; 
	
	 } 
	
	} 

	public function uploadserverimages()
	{ //print_r($_REQUEST['allimageid']);

	$allimageids = $_REQUEST['allimageid'];

	$i=0;

	foreach($allimageids as $allimageid )

	{

		//$result = $this->product_model->getimagebyproductid($allimageid);

		$imagedivision = explode('/',$allimageid);

		$diffentimage= base_url().$allimageid;

		echo "<div style='position: relative; width: 100px; float: left; margin-right: 6px;' id='".$i."'><img style=height:100px;width:100px; src='".$diffentimage."'  ><input type=hidden value='".$allimageid."' name=serverhiddenid[]/>

		<div style='position: absolute; z-index: 1000; top: 0px; right: 7px; color: red;'><a style='color: red' onclick= productdelete('".$i."','".$imagedivision[2]."');><i class='fa fa-times' aria-hidden='true'></i></a></div></div>";

		$i++;

		}

	}

	public function status($id=false,$value=NULL)
	{
		if($id != false && $value != NULL)
		{
			echo $result = $this->product_model->product_admin_status_change($id,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}
	
	public function feature_status($id=false,$value=NULL)
	{
		if($id != false && $value != NULL)
		{
			echo $result = $this->product_model->product_admin_feature_status_change($id,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}

	public function productview($id=false)
	{
		if ($id!=false) {
			$data["page_title"] = "View Product | iPengen";
			$data['data'] = $this->product_model->getProductDataById($id);
			$data['galary'] = $this->product_model->getProductImgGalaryById($id);
			$data['product_id'] = $id;
			$this->load->template('products/productview',$data);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
		
	}

	/*Method Name:additionalImagedelete
	  model: Dont Use
	  parameter: additionalimageid
	*/
	public function additionalImagedelete(){
					$additionimageid = $this->input->post('additionalimageid');
					$additionimageurl = $this->input->post('imageurl');
					$productid = $this->input->post('productid');
					$data = array('img_id' => $additionimageid);
					$delete = $this->product_model->additionalimagedelete($data);
					if($delete){
						$result['msg'] = 'success';
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.
$this->config->item('medium_thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('large_thumb_size').$additionimageurl);
								unlink($this->config->item('image_path').'product/'.$productid.$this->config->item('original').$additionimageurl);
								
						}
					else{
						$result['msg'] = 'error';

						}
					
					echo json_encode($result);

					}
	//Bulk Product add............
	public function bulkupload(){
		
		$data["page_title"] = "Add Products || iPengen";
		$categories = $this->product_model->getCategories();
		foreach ($categories as $key => $value) {
			$category['cat_id'] = $value['cat_id'];
			$category['name'] = $value['name'];
			$newArr[] = $category;
		}
		
		
		require_once '/home/ipengenc/public_html/staging/application/third_party/PHPExcel/IOFactory.php';
				
		$data['categories'] = $newArr;
		$insert_data_text = array();
		$image = array();
		$inputFileType = 'Excel2007';
		if(isset($_FILES) && !empty($_FILES)){
				$category = $this->input->post('category');
				$subcategory = $this->input->post('subcategory');
				$merchant_id = '1';
		        $data['error'] = '';    //initialize image upload error array to empty
		        if(isset($_FILES['product'])){
		        	$path = $this->config->item('upload_image_path').'csv/';
					//echo $path;
					
		        	$csv_files = $this->multiple_upload('product',$path,'xlsx','1000');
		        	if($csv_files == false){
		        		$data['error'] = "The filetype you are attempting to upload is not allowed!";
		        	}
		        	else{
						$file_path =  $path.$csv_files['file_name'];
						$inputFileName = $file_path;
		                $sheetnames = array("Products");
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objReader->setLoadSheetsOnly($sheetnames);
                        $objPHPExcel = $objReader->load($inputFileName);
						/**  Read the document's creator property  **/
                        $creator = $objPHPExcel->getProperties()->getCreator();
						$loadedSheetNames = $objPHPExcel->getSheetNames();
						
						$sheetData = $objPHPExcel->setActiveSheetIndexByName("Products")->toArray(null,true,true,true);
						$i=0;
						
			            if (!empty($sheetData)) {
			                foreach ($sheetData as $key=>$row) {
								if($key!=1){
								if($row['B']!='' || $row['C']){
			                	$brand = $row['F'];
			                	$brand_id = $this->product_model->searchBrand($brand);
			                    $insert_data = array(
			                        'sku'=>$row['B'],
									'stock_quantity'=>$row['C'],
			                        'price'=>$row['D'],
			                        'category_id'=>$subcategory,
			                        'brand_id'=>$brand_id,
			                        'sale_price'=>$row['E'],
			                        'sale_start_date'=>$row['G'],
			                        'sale_end_date'=>$row['H'],
			                        'create_date'=>date('Y-m-d H:i:s'),
			                        'merchant_id' => $merchant_id,
			                        'admin_status' => 0
			                        
			                    );
			                    $product_id = $this->product_model->insert_bulk_products($insert_data);
								$i++;
								$insert_id = $this->db->insert_id();//$this->db->insert_id();2284
								$productDetails = $this->product_model->getProductById($insert_id);
								//print_r($productDetails->category_id);die();
								$result_id=$productDetails->category_id;
								if($result_id!=""){
									$category = $this->product_model->category_id($result_id);
									//print_r($category);die();
									$cat_name = $category->cat_id;
									$parent_id = $this->product_model->getcatParentId($result_id);
									if($parent_id != 0){
										$catgry = $this->product_model->category_id($parent_id);
										$category_name = $catgry->cat_id;
										$parent_details = $this->product_model->getcategoryById($parent_id);
										foreach($parent_details as $parent){
											$category_id = $parent->parent_id;	
											if($category_id == 0){
												$string = ucfirst($category_name).','.ucfirst($cat_name);
											}
										}
									}else{
										$string = ucfirst($cat_name);
										
									}
										$updateProduct = $this->product_model->updateProduct($insert_id,$string);
									//echo $string;die();
							
								}
			                    $folder['product_id'] =  $product_id;
			                    $folder['name'] = $row['N'];
			                    array_push($image, $folder);
			                    $this->session->set_userdata('product_image_folder',$image);
			                    $insert_data_en_text = array(
			                        'product_id' => $product_id,
									'language_id' => 1,
									'product_name' => $row['I'],
									'short_description' => $row['J'],
									'message' => $row['K']
			                    );
			                    $insert_data_id_text = array(
			                        'product_id' => $product_id,
									'language_id' => 2,
									'product_name' => $row['L'],
									'short_description' => '',
									'message' => $row['M']
			                    );
			                    array_push($insert_data_text, $insert_data_en_text, $insert_data_id_text);
			                }
						  }
						}
			                 $this->product_model->insert_bulk_producttext($insert_data_text);
			                 $data['success'] = $i. ' Bulk Products Added Succesfully';
			            } else {
			                $data['error'] = 'Upload Failed!';
			            }
						
				    }
					if(isset($insert_id) && $insert_id !='')
					{
				    $merchant_name = "Admin Merchant";		
					$bulk_uplode_status = array(
			                        'merchant_id' => $merchant_id,
									'total_product' => $i,
									'date' => date('Y-m-d'),
			                    );
					
					$bulk_uplode_notification = array(
			                        'm_id' => $merchant_id,
									'type' => "bulk uplode",
									'message'=> $merchant_name . " uploded ". $i . " bulk propduct on ". date('d-m-Y'),
									'raw' => '',
									'date' => date('Y-m-d'),
			                    );
								
					$this->product_model->insert_bulk_status($bulk_uplode_status);
					$this->product_model->insert_admin_notification($bulk_uplode_notification);
					
					$data["status"] = "Bulk Product Upload";
					$data["merchantNAme"] = $merchant_name;
					$data["productcount"] = $i;
					$emailBody = $this->load->view("email_template/bulk_uplode.php",$data,true);
					$supportData = $this->order_model->support_data();
					if(!empty($supportData))
			         {
						$emailTo = $supportData[0]->info_email; 
						$subject = $merchant_name . " uploded ". $i . " bulk product on ". date('d-m-Y');
						
						$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>  $this->session->userdata('merchant_logged_in'),
								"to"		=>	$emailTo,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					    send_email($emailData);
						 
					 }
					
					
					}
		        }
		        if(isset($_FILES['product_image'])){ //print_r($_FILES);
		        	$zpath = $this->config->item('upload_image_path').'zip/';
		        	$zip_files = $this->multiple_upload('product_image',$zpath,'zip','');
		        	$zip_name = $zip_files['file_name']; 
		        	$encryptName = str_replace('.zip','',$zip_name);
		        	if(!is_dir($zpath.$encryptName)){ mkdir($zpath.$encryptName); }
		        	$source = $zpath.$zip_name;
		        	$destination = $zpath.$encryptName.'/';
		        	$this->unzip->extract($source,$destination);
		        	$dir = $destination.'images/';
						// Open a directory, and read its contents
					if (is_dir($dir)){ 
						if ($dh = opendir($dir)){
						  	$variable = $this->session->userdata('product_image_folder');
							//print_r($variable);
							//die('sssssssssss');
						  	foreach ($variable as $key => $value) {
						  		$subDir = $dir.$value['name'];
						  		if(is_dir($subDir)){
						  			if ($sdh = opendir($subDir)){
						  				$main_dir = $subDir.'/main/';
						  				$gallery_dir = $subDir.'/gallery/';
						  				if(is_dir($main_dir)){
					  						if($main = opendir($main_dir)){
								    			while (($file = readdir($main)) !== false){
											      	if($file!='')
											      	{
												      	$img_path = $main_dir.$file;
												      	if(is_file($img_path))
												      	{
												      		$this->originalImgUpload($file,$value['product_id'],$main_dir);
												      	}
											    	}
										    	}						  				
											}
										}else{
											$data['error'] = "No main directory found!";
										}
										if(is_dir($gallery_dir)){
					  						if($gallery = opendir($gallery_dir)){
					  							$index = 0;
								    			while (($gfile = readdir($gallery)) !== false){
											      	if($gfile!='')
											      	{
												      	$gallery_img_path = $gallery_dir.$gfile;
												      	if(is_file($gallery_img_path))
												      	{
												      		$this->galleryImgUpload($gfile,$value['product_id'],$index,$gallery_dir);
												      	}
											    	}
											    	$index = $index + 1;
										    	}						  				
											}
										}
										else{
											$data['error'] = "No gallery directory found!";
										}
									}
						  		}}
				    	}
				    	$data['success_image'] = "Image uploaded successfully!";
					}else{
						$data['error'] = "No directory found!";
					}
		    	}
			}
			$this->load->template('products/bulkproduct',$data);
	
		
		
		
		}
		
		public function multiple_upload($name, $upload_dir, $allowed_types, $size)
    {
        $config['upload_path']   = realpath($upload_dir);
        $config['allowed_types'] = $allowed_types;
        $config['max_size']      = $size;
        $config['overwrite']     = FALSE;
        $config['encrypt_name']  = TRUE;
        $ffiles = $this->upload->data();
       
        $this->upload->initialize($config);
        $errors = FALSE;
        if(!$this->upload->do_upload($name))://I believe this is causing the problem but I'm new to codeigniter so no idea where to look for errors
           echo $errors = $this->upload->display_errors();
        else:
            // Build a file array from all uploaded files
            $files = $this->upload->data();
        endif;
        // There was errors, we have to delete the uploaded files
         if($errors):                   
            @unlink($files['full_path']);
            return false;
        else:
            return $files;
        endif;
    }//end of multiple_upload()
    public function originalImgUpload($product_image,$getProductId,$dir){ 
		if(!empty($product_image)) 
		{ 
			if (!is_dir($this->config->item('image_path').'product/')) { 
				mkdir($this->config->item('image_path').'product/', 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size'), 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size'), 0777, true); 
			} 
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size'), 0777, true); 
			}
			if (!is_dir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'))) { 
				mkdir($this->config->item('image_path').'product/'.$getProductId.$this->config->item('original'), 0777, true); 
			}
		
			$productImageExtension= explode('.',$product_image); 
			
			if(in_array($productImageExtension[1], $this->extension_array)){
					$img_file_name = $getProductId."_".time().'.'.$productImageExtension[1]; 
					$config['image_library'] = 'gd2'; 
					$this->load->library('image_lib', $config); 
					list($width, $height) = getimagesize($dir.$product_image);
					if ($width>$height) {
					$orientation = "width";
					} else {
					$orientation = "height";
					}
					/*------------------------original Image Upload----------------------------*/
					$config = array( 
								'source_image'      => $dir.$product_image, 
								'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$img_file_name, 
								'maintain_ratio'    => FALSE, 
								'width'             => $width, 
								'height'            => $height, 
								'master_dim' =>'width' 
								); 
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
					/*--------------------Image Resize for 250*250------------------------------------------*/
					$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path250_250 = $dir.$product_image;
					$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
					$this->image_lib->resize();
					/*------------------------Image Resize for 250*250------------------------------------------*/
					/*-------------------------Image Resize for 400*310------------------------------------------*/
					
					$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path400_310 = $dir.$product_image;
					$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.
					$this->config->item('medium_thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
					/*----------------------------Image Resize for 400*310------------------------------------------*/
					/*--------------------------Image Resize for 650*500------------------------------------------*/
					
					$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#FFF','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
					$source_path650_310 = $dir.$product_image;
					$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$img_file_name;
					$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
					
					/*-----------------------Image Resize for 650*500------------------------------------------*/
					$file_name = $img_file_name;  
					$this->product_model->updateProductByImages($getProductId,$file_name); 
			}
			else{
				$data['error'] = "Invalid file extensions!";
			}
			
		} 
    }
    public function galleryImgUpload($productImageId,$getProductId,$index,$gallery_dir){
		$productImageExtension= explode('.',$productImageId);
		if(in_array($productImageExtension[1], $this->extension_array)){
			$additional_file_name = $getProductId.'_gallery'.$index.'_'.date("Ymdhis").'.'.$productImageExtension[1];  
			$config['image_library'] = 'gd2'; 
			$this->load->library('image_lib', $config); 
			list($width, $height) = getimagesize($gallery_dir.$productImageId);
			if($width > $height){
			$orientation = "width";
			} else {
			$orientation = "height";
			}
			
			/*------------------------original Image Upload----------------------------*/
			$config = array( 
							'source_image'      => $gallery_dir.$productImageId, 
							'new_image'         => $this->config->item('image_path').'product/'.$getProductId.$this->config->item('original').$additional_file_name, 
							'maintain_ratio'    => FALSE, 
							'width'             => $width, 
							'height'            => $height, 
							'master_dim' =>'width' 
							); 
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			
			/*--------------------Image Resize for 250*250------------------------------------------*/
			/*-----------------------Image Resize for 250*250------------------------------------------*/
			$settings250_250 = array('w'=>250,'h'=>250,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path250_250 = $gallery_dir.$productImageId;
			$despath_path250_250 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path250_250,$settings250_250,$despath_path250_250);
			
			/*---------------Image Resize for 250*250------------------------------------------*/
			/*-----------------Image Resize for 450*450------------------------------------------*/
			
			$settings400_310 = array('w'=>450,'h'=>450,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path400_310 = $gallery_dir.$productImageId;
			$despath_path400_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('medium_thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path400_310,$settings400_310,$despath_path400_310);
			/*-----------------Image Resize for 450*450------------------------------------------*/
			/*-------------------Image Resize for 800*800------------------------------------------*/
			
			$settings650_310 = array('w'=>800,'h'=>800,'canvas-color'=>'#F7F7F7','Imrtype'=>$orientation,'iscanvas'=>true,'quality'=>80,'shrink'=>true);
			$source_path650_310 = $gallery_dir.$productImageId;
			$despath_path650_310 = $this->config->item('image_path').'product/'.$getProductId.$this->config->item('large_thumb_size').$additional_file_name;
			$this->imageresize->customresize($source_path650_310,$settings650_310,$despath_path650_310);
			
			/*-------------------Image Resize for 800*800------------------------------------------*/
			
			$this->product_model->additionProductByImages($getProductId,$additional_file_name);
		}
		else{
			$data['error'] = "Invalid file extensions!";
		}
    }
	

     }/*end class Product */ 

	 