<?php
/**
* 
*/
class Contribute extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("contribute_model"));
		
	}
	
	function contributelist()
	{
		$data["page_title"] = "Contribute List | iPengen";
		$data["lists"] = $this->contribute_model->getContributelist();
		$this->load->template("contribute/list",$data);
	}
	
	//ajax contribution list.....................
	
	public function ajax_contribution_list(){
		
	
		$requestData= $_REQUEST;
		$columns = array(  
		// datatable column index  => database column name
			0 =>'contributor_id', 
			1 =>'wishlist_id',
			2=> 'user_id',
			3=> 'wishlist_name',
			4=> 'stock_quantity',
			5=> 'fname',
			6=> 'product_price',
			7=> 'total_amt_received',
			
		
		);
		
		$getajaxProduct = $this->contribute_model->ajax_contribution_list($columns,$requestData);
		$totalData = $getajaxProduct["FILTERCOUNT"];
		$totalFiltered = $getajaxProduct["FILTERCOUNT"];
		$result = $getajaxProduct["queryresult"];
	    $data = array();
		foreach($result as $row){  // preparing an array
		
		    if(is_file($this->config->item('image_path').'/product/'.$row["product_id"].$this->config->item('thumb_size').$row["product_image"]))
			{	
			$imageURL = '<img src="'.base_url('photo/product/'.$row["product_id"].$this->config->item('thumb_size').$row["product_image"]).'" alt="'.$row["product_image"].'" class="img-circle img-responsive imageZoom" width="40">'; 
			}
			else
			{	$imageURL = '<img src="'.base_url('photo/product/no_product.jpg').'" alt="'.$row["product_image"].'" class="img-circle img-responsive imageZoom" width="40">'; }
			
			
			
			 
			
			$nestedData=array(); 
			$nestedData[] = $row["contributor_id"];
			$nestedData[] = ucfirst($row["order_id"]);
			$nestedData[] = $imageURL;
			$nestedData[] = ucfirst($row["product_name"]);
			$nestedData[] = ucfirst($row["wishlist_name"]);
			$nestedData[] = ucfirst($row["w_owner_fname"])." ".ucfirst($row["w_owner_lname"]);
			$nestedData[] = $this->config->item("currency")." ".$row["product_price"];
			$nestedData[] = $this->config->item("currency")." ".$row["contribute_amount"];
			
			
			
						
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		// for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "draw"            => intval( $requestData['draw'] ),  
		// total number of records 
		  "recordsTotal"    => intval( $totalData ),  
		// total number of records after searching, if there is no searching then totalFiltered = totalData
		  "recordsFiltered" => intval( $totalFiltered ),
		   // total data array 
		  "data"            => $data  
			);

			echo json_encode($json_data);  // send data as json format
		
		
		
		
		
		}
}
?>