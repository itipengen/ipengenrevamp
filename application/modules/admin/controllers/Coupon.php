<?php
/**
* 
*/
class Coupon extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("coupon_model"));
		
	}

	function index()
	{

	}

	function addcoupon()
	{
		if($this->input->post("couponSubmit"))
		{
			if($this->input->post("couponType") != 1)
			{
				$data = array(
						"coupon_code"		=>	$this->input->post("couponcode"),
						"coupon_dropval"	=>	$this->input->post("couponType"),
						"coupon_percentage"	=>	$this->input->post("coupontypevalue"),
						"c_start_date"		=>	$this->input->post("couponStartDate"),
						"c_end_date"		=>	$this->input->post("couponEndDate"),
							 );
				$result = $this->coupon_model->coupon_add($data);
			}
			else
			{
				$data = array(
						"coupon_code"		=>	$this->input->post("couponcode"),
						"coupon_dropval"	=>	$this->input->post("couponType"),
						"coupon_fixed"	=>	$this->input->post("coupontypevalue"),
						"c_start_date"		=>	$this->input->post("couponStartDate"),
						"c_end_date"		=>	$this->input->post("couponEndDate"),
							 );
				$result = $this->coupon_model->coupon_add($data);
			}
			if($result)
			{
				$this->session->set_flashdata("successMsg","Coupon added successfully.");
				redirect("admin/coupon/list");
			}
			else
			{
				$this->session->set_flashdata("errorMsg","Opps..!!! Something went wrong. Try again.......");
				$this->load->template("coupon/add");
			}
		}
		else
		{
			$data["page_title"] = "Add Coupon | iPengen";
			$this->load->template("coupon/add",$data);
		}
	}

	function listcoupon()
	{
		$data["page_title"] = "Coupon List | iPengen";
		$data["couponlist"] = $this->coupon_model->getAllList();
		$this->load->template("coupon/list",$data);
	}

	function editcoupon($couponId = NULL)
	{
		if($couponId != NULL)
		{
			if($this->input->post("couponSubmit"))
			{
				$code = $this->input->post("couponcode");
				if($this->input->post("couponType") != 1)
				{
					$data = array(
							"coupon_code"		=>	$this->input->post("couponcode"),
							"coupon_dropval"	=>	$this->input->post("couponType"),
							"coupon_percentage"	=>	$this->input->post("coupontypevalue"),
							"c_start_date"		=>	$this->input->post("couponStartDate"),
							"c_end_date"		=>	$this->input->post("couponEndDate"),
								 );
					$result = $this->coupon_model->coupon_update($data,$couponId);
				}
				else
				{
					$data = array(
							"coupon_code"		=>	$this->input->post("couponcode"),
							"coupon_dropval"	=>	$this->input->post("couponType"),
							"coupon_fixed"		=>	$this->input->post("coupontypevalue"),
							"c_start_date"		=>	$this->input->post("couponStartDate"),
							"c_end_date"		=>	$this->input->post("couponEndDate"),
								 );
					$result = $this->coupon_model->coupon_update($data,$couponId);
				}
				if($result)
				{
					$this->session->set_flashdata("successMsg", "Coupon updated successfully.");
					redirect("admin/coupon/list");
				}
				else
				{
					$this->session->set_flashdata("successMsg","Coupon updated successfully with previous data.");
					redirect("admin/coupon/list");
				}
			}
			else
			{
				$data["couponData"] = $this->coupon_model->getCouponQueryById($couponId);
				$data["couponId"] = $couponId;
				if(!empty($data["couponData"]))
				{
					foreach($data["couponData"] as $list)
					{
						$data["couponCode"] = $list->coupon_code;
						$data["couponDropVal"] = $list->coupon_dropval;
						if($list->coupon_dropval == 1)
						{$coupenVal = $list->coupon_fixed;}else{$coupenVal = $list->coupon_percentage;}
						$data["couponVal"] = $coupenVal;
						$data["couponStartDate"] = $list->c_start_date;
						$data["couponEndDate"] = $list->c_end_date;
					}
				}
				else
				{
					$data["couponCode"] = "";
					$data["couponId"] = "";
					$data["couponDropVal"] = "";
					$data["couponVal"] = "";
					$data["couponStartDate"] = "";
					$data["couponEndDate"] = "";
				}
				$data["page_title"] = "Edit Coupon | iPengen";
				$this->load->template("coupon/edit",$data);
				
			}
			
		}
	}

	function deletecoupon($couponId = NULL)
	{
		if($couponId != NULL)
		{
			$query = $this->coupon_model->delete_coupon($couponId);
			if($query)
			{
				$this->session->set_flashdata("successMsg","Coupon deleted successfully.");
				redirect("admin/coupon/list");
			}
			else
			{
				$this->session->set_flashdata("errorMsg","Opps..!!! Something went wrong. Try again.......");
				$this->load->template("admin/coupon/list");
			}
		}
	}
}
?>