<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Static_block extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('block_model');
		
		$this->load->helper('url');
			if (!$this->session->userdata('logged_in')){redirect('admin/login');} 
	}
		 
	public function add()
	{	 
		$data["page_title"] = "Add Static Block | iPengen";
		$data['language']=$this->block_model->language_list();
		$this->load->template('static_block/block_add',$data);
	}
	   
	public function list_block()
	{
		$result["page_title"] = "Static Block List | iPengen";
		$result['data'] =$this->block_model->index2();
		$this->load->template('static_block/block_list',$result); 
	}
	
	public function submit()
    {
		$this->form_validation->set_rules('block_name[]', 'Block name', 'required');
		$this->form_validation->set_rules('block_description[]', 'Block description', 'required');
							  
		if($this->form_validation->run() == TRUE)
		{  	
		   $result['data']= $_POST;
		   $query=$this->block_model->insert($result);
		   if($query !='')
		   {   
				$this->session->set_flashdata('static_block1','Static Block added successfully');
				redirect('admin/static_block/list_block');
		   }
		   else
		   {    
				$this->session->set_flashdata('static_block3','Block key already used');
				redirect('admin/static_block/add');
		   }
		}
	}
    
	public function enablestatus($id=NULL)
	{
		$enableAdmin = $this->block_model->enable_block($id);
		if($enableAdmin)
		{echo 1;}else{echo 0;}
	}	
	
	public function disablestatus($id=NULL)
	{
		$disableAdmin = $this->block_model->disable_block($id);
		if($disableAdmin )
		{echo 1;}else{echo 0;}
	}
	
    public function edit($id)
    {  
		$this->form_validation->set_rules('block_name[]', 'Block name', 'required|min_length[2]|max_length[50]|trim');
		$this->form_validation->set_rules('block_description[]', 'Block description', 'required');
	     
		if($this->form_validation->run() == TRUE)
		{
			$this->block_model->update_details($id);
			$this->session->set_flashdata('pagesEditMsg','Updated successfully');
			redirect('admin/static_block/list_block');
		}
		else
		{
			$data['result'] = $this->block_model->getblockById($id);
			$data['page_title']="Edit Static Block | iPengen";
			$this->load->template('admin/static_block/block_edit',$data);
		}
	}
  
    public function delete($id)
    {    
	  $this->block_model->delete_block($id);
	  $this->session->set_flashdata('pagesDeleteMsg','Deleted successfully');
	  redirect('admin/static_block/list_block');
    }

    public function state()
    {
	   $block_key=$_POST['block_key'];
	   $state=$_POST['state'];
	   $this->load->model('block_model');
	    $query= $this->block_model->update_state($block_key,$state);
		
		if($query==1)
		    { 
	           $this->session->set_flashdata('blockupdatestate','Status successfully updated ');
	            redirect('admin/static_block/list_block'); 
			}
		else
		   {
			   $this->session->set_flashdata('blockupdatestatefail','Status not successfully updated ');
			     redirect('admin/static_block/list_block');
		   }
	   
   }
	 
	 
}