<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("Invoice_model","Merchant_model","user_model")); 
		$this->load->library('email');
		$this->load->library('M_pdf');
		$this->load->helper(array('form', 'url','ipengen_email_helper'));
	}

	function index()
	{
		$data["page_title"] = "Invoice | iPengen";
		$data["invoicedata"] = $this->Invoice_model->get_all_invoice();
		$merchant = $this->Invoice_model->get_all_merchant();
		//print_r($data);die('jj');
		if(!empty($merchant)){
			foreach($merchant as $val){
				$get_merchant[$val->marchent_id] = $val->marchent_name;
			}	
		}
		//print_r($get_merchant);die();
		$data["merchant"] = $get_merchant;
		//print_r($data);die();
		$this->load->template("invoice/invoice", $data);
	}
	
	
	function getFilteredInvoice()
	{
		if($this->input->is_ajax_request()){
			$post=$this->input->post();
			$invoicedata = $this->Invoice_model->get_all_invoice_bydate($post);
			echo json_encode($invoicedata);
			//print_r($data);die();
		}
	}
	
	//ajaxfilter invoice data..............................
	public function ajax_invoice_list(){
		
		
		if(isset($_POST['date1']) && isset($_POST['date2'])){
			$requestData= $_REQUEST;
			$date1 = $_POST['date1'];
			$date2 = $_POST['date2'];
			
			$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'invoice',
			2=> 'marchent_name',
			3=> 'date',
			4=> 'status',
			5=> 'total_amount',
			6=> 'id',
			7=> 'remarks',
		
		);
						
						
						
			//count number product get.....................
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			$this->db->where('date >=',$date1);
			$this->db->where('date <=',$date2);
			$query =$this->db->get();
			$totalData = $query->num_rows();
			$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
				
			
			//Now fetch the data between two event date..
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			$this->db->where('ig_invoice.date >=',$date1);
			$this->db->where('ig_invoice.date <=',$date2);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
			else{
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			$this->db->where('ig_invoice.date >=',$date1);
			$this->db->where('ig_invoice.date <=',$date2);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
					
			
			
						
			
			$result11 = $query12->result_array();
			$data = array();
			foreach($result11 as $row){  // preparing an array
			
			if($row['status']==0){$checkVal = "Pending";}else{$checkVal = "Paid";}
			 
			
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["invoice"]);
			$nestedData[] = $row["marchent_name"];
			$nestedData[] = date('d/m/Y', strtotime($row["date"]));
			$nestedData[] = $checkVal;
			$nestedData[] = $row['total_amount'];
			$nestedData[] = '<a href="invoice/details/'.$row['id'].'/'.$row['refix'].$row['invoice'].'" title="Order Details">
			<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>';
		    $nestedData[] = $row['remarks'];
			$data[] = $nestedData;
		}
//		
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
echo json_encode($json_data);  // send data as json format
exit(0);
			
			
			}
		
		
		$requestData= $_REQUEST;
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'invoice',
			2=> 'marchent_name',
			3=> 'date',
			4=> 'status',
			5=> 'total_amount',
			6=> 'id',
			7=> 'remarks',
		
		);
		
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			
			$query = $this->db->get();
			$totalData = $query->num_rows();
			$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
			
			
			
			
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			
			$this->db->like('ig_invoice.invoice', $requestData['search']['value']);
			$this->db->or_like('ig_marchent_login.marchent_name', $requestData['search']['value']);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
			else{
			$this->db->select('ig_invoice.*,ig_marchent_login.marchent_name');
			$this->db->from("ig_invoice");
			$this->db->join('ig_marchent_login', 'ig_invoice.merchant_id = ig_marchent_login.marchent_id');
			
			$this->db->like('ig_invoice.invoice', $requestData['search']['value']);
			$this->db->or_like('ig_marchent_login.marchent_name', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
			
			$result11 = $query12->result_array();
			$data = array();
			foreach($result11 as $row){  // preparing an array
			
			if($row['status']==0){$checkVal = "Pending";}else{$checkVal = "Paid";}
			 
			
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["invoice"]);
			$nestedData[] = $row["marchent_name"];
			$nestedData[] = date('d/m/Y', strtotime($row["date"]));
			$nestedData[] = $checkVal;
			$nestedData[] = $row['total_amount'];
			$nestedData[] = '<a href="invoice/details/'.$row['id'].'/'.$row['refix'].$row['invoice'].'" title="Order Details">
			<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>'.'<a href="invoice/downloadpdf/'.$row['invoice'].'" title="Download pdf">
			<i class="fa fa-file-pdf-o" style="color:#f00"></i>';
			
			
			$nestedData[] = $row['remarks'];
						
			$data[] = $nestedData;
		}
//		
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
echo json_encode($json_data);  // send data as json format
		
		
		
		
		
		
		
		}
	
	
	function generateInvoice()
	{
		if($this->input->is_ajax_request()){
			$post=$this->input->post();
			$generateInvoice = $this->Invoice_model->generateInvoice($post);
			echo json_encode($generateInvoice);
			//print_r($data);die();
		}
	}
	
	
	function details($mid,$invid)
	{
		$data["page_title"] = "Invoice Details | iPengen";
		
		//echo $mid.'---'.$invid;die();
		$data['getInvoiceProduct'] = $this->Invoice_model->invoiceProductDetails($mid,$invid);
		$inv_id=str_replace('IPGNINV', "", $invid);
		$data['getInvoiceDetails']=$this->Invoice_model->getInvoiceDetails($inv_id);
		//print_r($data);die();
		//die();
		$this->load->template("invoice/details", $data);
	}
	
	
	function invoicePay(){
		
		$data["result"] = $this->Invoice_model->invoice_pay();
		echo json_encode($data);
		//print_r($data);	
	}
	
	
	function ig_cron(){
		
		$data["result"] = $this->Invoice_model->insert_invoice();
		//print_r($data);	
	}
	
	
    /* method to download pdf*/
	
	 public function downloadpdf($inv_id){
		$data=array();
		if(!empty($inv_id)){
		   $data['getInvoiceDetails']=$this->Invoice_model->getInvoiceDetails($inv_id);
	        if(!empty($data['getInvoiceDetails'])){
				$mid=$data['getInvoiceDetails'][0]->merchant_id;
				$data['getInvoiceProduct'] = $this->Invoice_model->invoiceProductDetails($mid,$inv_id);
				$data['businessnamearray'] = $this->Invoice_model->getbusinessname($mid);
				//$data['currency']="RP ";
				$data['currency']=$this->config->item('currency');
				$html=$this->load->view("invoicepdfhtml/invoice_download", $data,true);
				$mpdf = new mPDF();
				$this->m_pdf->pdf->Bookmark('invoice ipengen');
				//$data['datetime']=date("d.m.Y h:i:sa");
				$this->m_pdf->pdf->WriteHTML($html);
				$this->m_pdf->pdf->Output($inv_id."-Ipegen", 'I');
			}
		}
	 }
  
	
	


}
?>