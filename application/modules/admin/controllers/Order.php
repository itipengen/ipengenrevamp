<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model("order_model");
		$this->load->library('M_pdf');
		$this->load->helper(array('ipengen_email_helper'));
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		
		
	}
	

	
	public function order_list()
	{
		$data["page_title"] = "Order List | iPengen";
		$data["orderList"] = $this->order_model->get_all_orders();
		$this->load->template('order/list',$data);
	}
	
	public function ajax_order_list(){
		$requestData= $_REQUEST;
		//print_r($requestData);die();
		$columns = array(  
		// datatable column index  => database column name
			0 =>'orderID', 
			1 =>'client_fname',
			2=> 'total_amount',
			3=> 'order_date',
			4=> 'order_status'
			
		);
		$query = $this->db->select("
							ig_order.order_id as orderID,
							ig_user.fname as client_fname,
							ig_user.lname as client_lname,
							ig_order.total as total_amount,
							ig_order.date_added as order_date,
							
							ig_order.status as order_status,
									")
						  ->from("ig_order")
						 
						  ->join("ig_user","ig_user.id = ig_order.user_id",'right')
						   ->where(array("ig_order.language_id"=>"1"))
						  ->get();
		$totalData = $query->num_rows();
		$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select("
							ig_order.order_id as orderID,
							ig_user.fname as client_fname,
							ig_user.lname as client_lname,
							ig_order.total as total_amount,
							ig_order.date_added as order_date,
							
							ig_order.status as order_status,
									");
			$this->db->from('ig_order');
			
			$this->db->join("ig_user","ig_user.id = ig_order.user_id",'right');
			$this->db->where(array("ig_order.language_id"=>"1"));
			$this->db->like('ig_user.fname', $requestData['search']['value']);
			$this->db->or_like('ig_user.lname', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
		else{
			$this->db->select("
							ig_order.order_id as orderID,
							ig_user.fname as client_fname,
							ig_user.lname as client_lname,
							ig_order.total as total_amount,
							ig_order.date_added as order_date,
							
							ig_order.status as order_status,
									");
			$this->db->from('ig_order');
			//$this->db->join("ig_transaction","ig_transaction.order_id = ig_order.order_id",'left');
			$this->db->join("ig_user","ig_user.id = ig_order.user_id",'left');
			$this->db->where(array("ig_order.language_id"=>"1"));
			$this->db->like('ig_user.fname', $requestData['search']['value']);
			$this->db->or_like('ig_user.lname', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
			
			$result11 = $query12->result_array();
			 $data = array();
		foreach($result11 as $row){  // preparing an array
			/*if($row['transaction_status'] == "capture")
			  {$t_status = "Success";}else
			  {$t_status = $row['transaction_status'];}*/
		
		    $action = '<a href="'.base_url("admin/order/details/".$row['orderID']).'" title="Order Details" >
                        	<i class="fa fa-eye fa-fw " style="color:#508DF2"></i>
                        </a>'.'<a href="'.base_url("admin/order/downloadpdf/".$row['orderID']).'" title="Download pdf" target="_blank">
                        	<i class="fa fa-file-pdf-o" style="color:#f00"></i>
                        </a>';
			
			
			$nestedData=array(); 
			$nestedData[] = $row["orderID"];
			$nestedData[] = ucfirst($row["client_fname"])." ".ucfirst($row['client_lname']);
			$nestedData[] = strtoupper($this->config->item('currency')).' '.$row["total_amount"];
			$nestedData[] = date("d M Y  h:i:s",strtotime($row["order_date"]));
			
			$nestedData[] = ucfirst($row['order_status']);
			$nestedData[] = $action;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		  "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
         echo json_encode($json_data);  // send data as json format
		
		
		
		
		}
	
	
	
	
	
	public function details($orderId=NULL)
	{
		//$orderId=117;
		$data['page_title']="Order Details |iPengen";
		$data['orderdetails']= $this->order_model->getOrderDetails($orderId);
		$data['orderPaymentsDetails']= $this->order_model->orderPaymentsDetails($orderId);
//	print_r($data['orderPaymentsDetails']);
//		die();
		$data['orderdetailshistory']= $this->order_model->getOrderDetailshistory($orderId);
		$this->load->template('order/details',$data);
	}
	
	public function getMerchantname($mId=NULL)
	{

		$data['orderdetails']= $this->order_model->getOrderDetails($orderId);
		$data['orderdetailshistory']= $this->order_model->getOrderDetailshistory($orderId);
		$this->load->template('order/details',$data);
	}
	
	public function admin_status()
	{
		$status = $this->input->post('status');
		$orderID = $this->input->post('oid');
		$productID = $this->input->post('pid');
		
		$adminstatuschange = array(
					'aos_order_id' => $orderID,
					'aos_order_status' => $status,
					'aos_date' => date('Y-m-d'),					
				);
	    $datastatus = array(
					'order_status' => $status,
				);			
				
		$order_status = $this->order_model->changeorderstatus($status,$orderID,$productID,$datastatus);
		$order_status_update = $this->order_model->changeorderstatusupdate($adminstatuschange,$orderID);
		

		if(!empty($order_status)){
				$result['msg'] = "success";
			}else{
			    $result['msg'] = "error";	
			}
			
			echo json_encode($result); 

	}
	public function send_notification()
	{
		    $orderId =  $this->input->post('oid');
			$merchnatId =  $this->input->post('mname');
			$productname =  $this->input->post('pname');
			$merchnatname =  $this->input->post('name');
			
		        $data["order_id"] = $orderId;
				$data["productname"] = $productname;
				$data["merchnatname"] = $merchnatname;
				
				$emailBody = $this->load->view("email_template/send_notification",$data,true);
				$supportData = $this->order_model->support_data();
				$merchainemail = $this->order_model->get_merchant($merchnatId);
				//$emailTo = $data["userData"][0]->userEmail;
				 if(!empty($supportData))
			      {
					 $productName = 'of "'.$productname.'"'; }
					
					$emailFrom = $supportData[0]->info_email;
					$subject = '[Ipengen] - Take action remainder ';
					$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$emailFrom,
								"to"		=>	$merchainemail,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					send_email($emailData);
					
			$login_notification = array(
			                        'm_id' => $merchnatId,
									'type' => "Action Reminder",
									'message'=> "Admin send reminder to take action for ". $productname . " in " .$productname. " on " . date('d-m-Y'),
									'raw' => '',
									'date' => date('Y-m-d'),
									'notification_for' => 2,
			                    );
								
					          $insert_notification = $this->order_model->insert_merchant_notification($login_notification);
							  
							  
							  
							  if(!empty($insert_notification)){
				$result['msg'] = "success";
			}else{
			    $result['msg'] = "error";	
			}
			
			echo json_encode($result); 
		
			    
	}
	
	
	public function change_status()
	{
		$emailFrom = "";
		
		$orderid=$this->input->post('orderid');
		$userid=$this->input->post('userid');
		$status=$this->input->post('status');
		$remark=$this->input->post('remark');
		
		
		
		$result=$this->order_model->change_status($orderid,$status,$remark);
		if($status == "shipping")
		{
			$productName = "";
			$data["order_id"] = $orderid;
			$data["userData"] = $this->order_model->getUserData($orderid);
			$data['orderdetails'] = $this->order_model->getOrderDetails($orderid);
			$emailBody = $this->load->view("email_template/order-status-delivery",$data,true);
			$supportData = $this->order_model->support_data();
			$emailTo = $data["userData"][0]->userEmail;
			if(!empty($supportData))
			{
				if(count($data['orderdetails']) == 1)
				{ $productName = 'of "'.$data['orderdetails'][0]->name.'"'; }
				
				$emailFrom = $supportData[0]->info_email;
				$subject = 'Your Ipengen order (#'.$orderid.') '.$productName.' has been dispatched!';
				$emailData = array(
							"title" 	=> "Ipengen",
							"from"		=>	$emailFrom,
							"to"		=>	$emailTo,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				send_email($emailData);
			}
		}
		
		echo 'success';
	}
	
	
	public function change_productstatus()
	{
		$emailFrom = "";
		
		$orderid=$this->input->post('orderid');
		$userid=$this->input->post('userid');
		$status=$this->input->post('status');
		$remark=$this->input->post('remark');
		$productid=$this->input->post('productid');
		
		
		$result=$this->order_model->change_productstatus($orderid,$status,$remark,$productid);
		if($status == "shipping")
		{
			$productName = "";
			$data["order_id"] = $orderid;
			$data["userData"] = $this->order_model->getUserData($orderid);
			$data['orderdetails'] = $this->order_model->getOrderDetails($orderid);
			$emailBody = $this->load->view("email_template/order-status-delivery",$data,true);
			$supportData = $this->order_model->support_data();
			$emailTo = $data["userData"][0]->userEmail;
			if(!empty($supportData))
			{
				if(count($data['orderdetails']) == 1)
				{ $productName = 'of "'.$data['orderdetails'][0]->name.'"'; }
				
				$emailFrom = $supportData[0]->info_email;
				$subject = 'Your Ipengen order (#'.$orderid.') '.$productName.' has been dispatched!';
				$emailData = array(
							"title" 	=> "Ipengen",
							"from"		=>	$emailFrom,
							"to"		=>	$emailTo,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				send_email($emailData);
			}
		}
		
		echo 'success';
	}
	
	
	
	   /* method to download pdf*/
	
	 public function downloadpdf($orderId){
		$data=array();
	   
		if(!empty($orderId)){
		    $data['orderdetails']= $this->order_model->getOrderDetails($orderId);
			if(!empty($data['orderdetails'])){
				$data['orderPaymentsDetails']= $this->order_model->orderPaymentsDetails($orderId);
				$data['orderdetailshistory']= $this->order_model->getOrderDetailshistory($orderId);
				//print_r($data); die();
				$data['currency']=$this->config->item('currency');
			    $html=$this->load->view("orderpdfhtml/oreder_download", $data,true);
				$mpdf = new mPDF();
				$this->m_pdf->pdf->Bookmark('order ipengen');
				//$data['datetime']=date("d.m.Y h:i:sa");
				$this->m_pdf->pdf->WriteHTML($html);
				$this->m_pdf->pdf->Output($orderId."-Ipegen", 'I');
			}
		}
		
	 }
	 
	 public function itemdetails($oid,$pid)
	 {
		 
		$data["page_title"] = "view product | iPengen";
		$data["orderproduct"] = $this->order_model->getProductInOrder($oid,$pid);
		$data['orderdetails']= $this->order_model->getOrderDetailsNew($oid,$pid);
		$data['productdetailshistory']= $this->order_model->getProductDetailshistory($oid,$pid);
		$this->load->template('order/viewproduct',$data);
		
		 
	 }
	
	
}
?>