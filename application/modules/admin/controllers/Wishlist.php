<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends MX_Controller{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){redirect('admin/login');}
	    $this->load->model(array('wishlist_model','event_model')); 
	}

	public function index()
	{
		$data['page_title'] = "Wishlist List | iPengen";
		//$data['wishlist'] = $this->wishlist_model->wishlist();
		$this->load->template('admin/wishlist/wishlist');
	}
	
	public function ajaxwishlist(){
		
		if(isset($_POST['date1']) && isset($_POST['date2'])){
			$requestData= $_REQUEST;
			$date1 = $_POST['date1'];
			$date2 = $_POST['date2'];
			$columns = array(  
							// datatable column index  => database column name
							0 =>'id', 
							1 =>'fname',
							2=> 'title',
							3=> 'category',
							4=> 'e_startdate',
							5=> 'e_enddate',
							6=> 'event_date',
						);
			//count number product get.....................
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$this->db->where('e_startdate >=',$date1);
			$this->db->where('e_enddate <=',$date2);
			$query =$this->db->get();
			$totalData = $query->num_rows();
			$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.	
			
			//Now fetch the data between two event date..
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$this->db->where('e_startdate >=',$date1);
			$this->db->where('e_enddate <=',$date2);
			
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
			else{
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$this->db->where('e_startdate >=',$date1);
			$this->db->where('e_enddate <=',$date2);
			
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
					
			
			
						
			
			$result11 = $query12->result_array();
			$data = array();
			foreach($result11 as $row){  // preparing an array
			
			if($row['admin_status'] == 1){$checkVal = "checked";}else{$checkVal = "";}
			 $checkeddata= '<input type="checkbox" value="'.$row['id'].'" class="js-switch" '.$checkVal.'>';
			 
			$viewdata = '<a href="'.base_url('admin/wishlist/view/'.$row['id']).'" title="Wishlist View">
			<i class="fa fa-eye fa-fw fa-2x" style="color:#508DF2"></i></a>';
			if(is_file($this->config->item('image_path').'/wishlist/'.$row['id'].'/100-100/'.$row['wishlist_image']))
				  		{
							$imagepathwish = base_url('photo/wishlist/'.$row['id'].'/100-100/'.$row['wishlist_image']);
						}
						else
						{
							$imagepathwish = base_url('photo/wishlist/event.jpg');
						}
			$dataimage = '<img src="'.$imagepathwish.'" width="36" height="36" class="img-circle imageZoom">';
			
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["fname"])." ".ucfirst($row['lname']);
			$nestedData[] = $row["title"];
			$nestedData[] = $row["category"];
			$nestedData[] = date('d/m/Y', strtotime($row["e_startdate"]));
			$nestedData[] = date('d/m/Y', strtotime($row['e_enddate']));
			$nestedData[] = date('d/m/Y', strtotime($row['event_date']));
			$nestedData[] = $dataimage;
			$nestedData[] = strtoupper($row['is_public']);
			$nestedData[] = $viewdata;
			$nestedData[] = $checkeddata;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		  "draw"            => intval($requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
echo json_encode($json_data);  // send data as json format
exit(0);
			
			
			}
		
		
		$requestData= $_REQUEST;
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'fname',
			2=> 'title',
			3=> 'category',
			4=> 'e_startdate',
			5=> 'e_enddate',
			6=> 'event_date',
			
			
			
			
		);
		
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$query =$this->db->get();
			$totalData = $query->num_rows();
			$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			
			$this->db->like('ig_user.fname', $requestData['search']['value']);
			$this->db->or_like('ig_user.lname', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
			else{
			$this->db->select('ig_wishlist.*, ig_user.fname as fname, ig_user.lname as lname');
			$this->db->from('ig_wishlist');
			$this->db->join('ig_user','ig_user.id = ig_wishlist.uid');
			$this->db->like('ig_user.fname', $requestData['search']['value']);
			$this->db->or_like('ig_user.lname', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
			
			$result11 = $query12->result_array();
			$data = array();
			foreach($result11 as $row){  // preparing an array
			
			if($row['admin_status'] == 1){$checkVal = "checked";}else{$checkVal = "";}
			 $checkeddata= '<input type="checkbox" value="'.$row['id'].'" class="js-switch" '.$checkVal.'>';
			 
			$viewdata = '<a href="'.base_url('admin/wishlist/view/'.$row['id']).'" title="Wishlist View">
			<i class="fa fa-eye fa-fw fa-2x" style="color:#508DF2"></i></a>';
			if(is_file($this->config->item('image_path').'/wishlist/'.$row['id'].'/100-100/'.$row['wishlist_image']))
				  		{
							$imagepathwish = base_url('photo/wishlist/'.$row['id'].'/100-100/'.$row['wishlist_image']);
						}
						else
						{
							$imagepathwish = base_url('photo/wishlist/event.jpg');
						}
			$dataimage = '<img src="'.$imagepathwish.'" width="36" height="36" class="img-circle imageZoom">';
			
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["fname"])." ".ucfirst($row['lname']);
			$nestedData[] = $row["title"];
			$nestedData[] = $row["category"];
			$nestedData[] = date('d/m/Y', strtotime($row["e_startdate"]));
			$nestedData[] = date('d/m/Y', strtotime($row['e_enddate']));
			$nestedData[] = date('d/m/Y', strtotime($row['event_date']));
			$nestedData[] = $dataimage;
			$nestedData[] = strtoupper($row['is_public']);
			$nestedData[] = $viewdata;
			$nestedData[] = $checkeddata;
			
			$data[] = $nestedData;
		}
//		
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "recordsTotal"    => intval( $totalData ),  // total number of records
		  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
		  "data"            => $data   // total data array
			);
//
echo json_encode($json_data);  // send data as json format
		
		
		}
	 
	function view($wishlistID = NULL)
	{
		if($wishlistID != NULL)
		{
			$data["wishlistData"] = $this->wishlist_model->wishlistGetId($wishlistID);
			$data["wishlistProduct"] = $this->wishlist_model->wishlistGetWithProductId($wishlistID);
			//$data["eventlist"] = $this->event_model->event_list();
			if(!empty($data["wishlistData"]))
			{
				$data["page_title"] = "Wishlist View | iPengen";
				$data["wishlistId"] = $wishlistID;
				$this->load->template('admin/wishlist/wishlistview',$data);
			}
			else
			{
				$this->load->view("errors/ipengen_error/505");
			}
			
		}
	}
	
	function status($wishlist = NULL, $value = NULL)
	{
		if($wishlist != NULL && $value != NULL)
		{
			echo $this->wishlist_model->update_status($wishlist, $value);
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
}