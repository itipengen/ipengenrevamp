<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->helper('url');
		$this->load->helper('form'); 
		$this->load->helper('cookie');
		$this->load->model('brand_model'); 
		$this->load->library('session'); 
		$this->load->library('form_validation');
	}

	function add()
	{
		if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('name','brand name','required');
			if($this->form_validation->run())
			{
				$brandName = $this->input->post('name');
				$data = array('brand_name'=>$brandName);
				$result = $this->brand_model->add_brand($data);
				if($result)
				{
					$this->session->set_flashdata('success_message','Brand added successfully.');
					redirect('admin/brand/listing');
				}
				else
				{
					$this->session->set_flashdata('error_message','Opps..!!! Something went wrong.Try again.');
					redirect('admin/brand/listing');
				}
			}
			else
			{
				$data["page_title"] = "Add Brand | iPengen";
				$this->load->template('brand/add',$data);
			}
		}
		else
		{
			$data["page_title"] = "Add Brand | iPengen";
			$this->load->template('brand/add',$data);
		}
	}

	function listing()
	{
		$data["page_title"] = "Brand List | iPengen";
		$data['brandlists'] = $this->brand_model->get_all_brand();
		$this->load->template('brand/list',$data);
	}

	function edit($brandId = false)
	{
		if($brandId != false)
		{
			if(isset($_POST['submit']))
			{
				$this->form_validation->set_rules('name','brand name','required');
				if($this->form_validation->run())
				{
					$data=array('brand_name'=>$this->input->post('name'));
					$result = $this->brand_model->update_event($brandId,$data);
					if($result)
					{
						$this->session->set_flashdata('success_message','Brand updated successfully.');
						redirect('admin/brand/listing');
					}
					else
					{
						$this->session->set_flashdata('error_message','Brand updated successfully with same data.');
						redirect('admin/brand/listing');
					}
				}
				else
				{
					$brandData = $this->brand_model->getBrandDataById($brandId);
					$data["page_title"] = "Edit Brand | iPengen";
					$data['brandID'] = '';
					$data['brandName'] = '';
					if (!empty($brandData)) {
						foreach ($brandData as $value) {
							$data['brandID'] = $value->brand_id;
							$data['brandName'] = $value->brand_name;
						}
					}
					$this->load->template('brand/edit',$data);
				}
			}
			else
			{
				$brandData = $this->brand_model->getBrandDataById($brandId);
				$data["page_title"] = "Edit Brand | iPengen";
				$data['brandID'] = '';
				$data['brandName'] = '';
				if (!empty($brandData)) {
					foreach ($brandData as $value) {
						$data['brandID'] = $value->brand_id;
						$data['brandName'] = $value->brand_name;
					}
				}
				$this->load->template('brand/edit',$data);
			}
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}

	function delete($brandId = false)
	{
		if($brandId != false)
		{
			$query = $this->brand_model->delete_brand($brandId);
			if($query)
			{
				$this->session->set_flashdata('success_message','Deleted successfully.');
				redirect('admin/brand/listing');
			}
			else
			{
				$this->session->set_flashdata('error_message','Opps...!!! Something went wrong..Try again.');
				redirect('admin/brand/listing');
			}
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}

	function changestatus($brandId = false, $value = null)
	{
		if($brandId != false && $value != null)
		{
			echo $query = $this->brand_model->update_status($brandId,$value);
		}
		else
		{
			echo '<div style="margin: 152px auto; font-size: 51px; text-align: center; font-family: helvetica; color: red; font-weight: bold;">
					Oops..!!! <br> 404 Page Not Found<div>';
		}
	}
}
?>