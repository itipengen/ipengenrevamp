<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("Merchant_model","user_model")); 
		$this->load->library('email');
		$this->load->helper(array('form', 'url','ipengen_email_helper'));
	}

	function merchantlist()
	{
		$data["page_title"] = "Merchant List | iPengen";
		$data["result"] = $this->Merchant_model->get_all_merchantdetails();
		//print_r($data);die();
		$this->load->template("merchant/list", $data);
	}
	//ajax merchant list............................
	public function ajax_merchant_list(){
		$requestData= $_REQUEST;
		$columns = array(  
		// datatable column index  => database column name
			0 =>'marchent_id', 
			1 =>'marchent_name',
			2=> 'send_approved_request',
			3=> 'marchent_email',
			4=> 'marchent_status',
			5=> 'merchant_admin_status',
			6=> 'send_approved_request',
			
			
			
			
		);
		
			$this->db->select('*');
			$this->db->from("ig_marchent_login");
			$this->db->order_by("marchent_id", "desc");
			$query = $this->db->get();
			$totalData = $query->num_rows();
			$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
			
			
			if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$this->db->select('*');
			$this->db->from("ig_marchent_login");
			
			
			$this->db->like('marchent_name', $requestData['search']['value']);
			$this->db->or_like('marchent_email', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['start'],$requestData['length']);
			$query12 = $this->db->get();
			
		}
			else{
			$this->db->select('*');
			$this->db->from("ig_marchent_login");
			
			
			$this->db->like('marchent_name', $requestData['search']['value']);
			$this->db->or_like('marchent_email', $requestData['search']['value']);
			$this->db->order_by("".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']."");
			$this->db->limit($requestData['length'],$requestData['start']);
			$query12 = $this->db->get();
			
			
			
			}
			
			$result11 = $query12->result_array();
			
			$data = array();
			foreach($result11 as $row){  // preparing an array
			
			if($row['send_approved_request'] == 1){ 
			
			$sendapp = "Complete";
			$sendadminstatus=  '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch adminStatus" checked  />';
			
			}else{
				$sendapp = "Incomplete";
				$sendadminstatus=  '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch adminStatus" />';
				
				}
			
			if($row['merchant_admin_status'] == 1){
				
				$checkValadmin = '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch verifyStatus" checked/>';
				}else{
					$checkValadmin = '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch verifyStatus" />';
					
					}
			
			if($row['marchent_status'] == 1){
				$checkVal = '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch verifyStatus" checked/>';
				}else{
					$checkVal = '<input type="checkbox" value="'.$row['marchent_id'].'" class="js-switch verifyStatus" />';
					
					}
			
			
				
		   $editfee= '<a type="button" class="" data-toggle="modal" data-target="#editFees"
                             data-id="'.$row['marchent_id'].'" id="merchantfeeedit" style="width: 85px; padding: 0px; font-size:15px;">
							 <i class="fa fa-pencil-square-o action-btn text-success"></i></a>';
			if($row['transaction_fee_type'] == 'RP'){
				 $editfeeamount = $row['transaction_fee_type'].' '.$row['transaction_fee'];
			}
			else if($row['transaction_fee_type'] == '%'){
				$editfeeamount = $row['transaction_fee'].' '.$row['transaction_fee_type'];
				}	
				else{
					$editfeeamount = '';
					}			 
		  
							 
		  $action ='<a title="View" href="'.base_url().'admin/merchant/view/'.$row['marchent_id'].'" class="viewNews" data-id=""> 
		  <i class="fa fa-eye action-btn text-success"></i></a>';
			
			 
			
			$nestedData=array(); 
			
			$nestedData[] = $row["marchent_id"];
			//....................................
			
			$nestedData[] = ucfirst($row["marchent_name"]);
			//....................................
			
			$nestedData[] = $sendapp;
			//....................................
			
			$nestedData[] = $row['marchent_email'];
			//....................................
			
			$nestedData[] = $checkVal;
			//....................................
			
			$nestedData[] = $checkValadmin;
			//....................................
			
			
			
			$nestedData[] = $editfee.'&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;         '.$editfeeamount;
			
			$nestedData[] = $action;
			
			
			$data[] = $nestedData;
		}
//		
			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			  "recordsTotal"    => intval( $totalData ),  // total number of records
			  "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			  "data"            => $data   // total data array
				);
//
			echo json_encode($json_data);  // send data as json format
		
		
		
		
		}
	

	function isApproved($id)
	{
		$result   = $this->Merchant_model->isApproved($id);
		if($result){
			redirect('admin/merchant/merchantlist');
	    }
	}
	
	function isnotApproved($id)
	{
		$result   = $this->Merchant_model->isnotApproved($id);
		$merchant = $this->Merchant_model->getMerchantById($id);
		$Transectionfee = $this->Merchant_model->getTransection();
		$name = $merchant->marchent_name;
		$email = $merchant->marchent_email;
		$transaction = $merchant->transaction_fee;
		
		$fees = $Transectionfee->transaction_fees;
		
		$feesexplode = explode(' ',$fees);
		
		if (stripos($fees, 'RP') !== FALSE){
			$tfees=$feesexplode[1];
			$ttype=$feesexplode[0];
		}
		if (stripos($fees, '%') !== FALSE){
			$tfees=$feesexplode[0];
			$ttype=$feesexplode[1];
		}	
		if($transaction == '')
		{
			$transection    = $this->Merchant_model->insertTransection($id,$tfees,$ttype);
		}
		
		if($result){
			$this->session->set_userdata('approval_failed', 1);
			/** Approved Mail Starts **/
					$userdata["userName"] = ucwords($name);
					$userdata["link"] = site_url("merchant/login");
					$supportData = $this->user_model->get_support_data();
					$emailFrom = $supportData[0]->info_email;
					$userEmail = $email;
					$emailBody = $this->load->view("email_template/ipengen-merchant-approve",$userdata, true);
					$subject = "Your account has successfully approved (".$email.")";
					$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$emailFrom,
								"to"		=>	$userEmail,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					send_email($emailData);

			/** Approved Mail Ends **/
			redirect('admin/merchant/merchantlist');
        }else{
        	$this->session->set_userdata('approval_failed', 0);
        	redirect('admin/merchant/merchantlist');
        }

	}

	function isVerified($id)
	{
		$result   = $this->Merchant_model->isVerified($id);
		if($result){
			redirect('admin/merchant/merchantlist');
        }
	}
	
	function isnotVerified($id)
	{
		$result   = $this->Merchant_model->isnotVerified($id);
		if($result){
			redirect('admin/merchant/merchantlist');
        }
	}
	
	function product()
	{
		$data["page_title"] = "Product | iPengen";
		$merchant = $this->Merchant_model->get_all_merchant();
		//print_r($merchant);
		if(!empty($merchant)){
			foreach($merchant as $val){
				$get_merchant[$val->marchent_id] = $val->marchent_name;
			}	
		}
		//print_r($get_merchant);die();
		$data["merchant"] = $get_merchant;
		$data["getAllProduct"] = $this->Merchant_model->get_all_product();
		//print_r($data["getAllProduct"]);die();
		$this->load->template("merchant/product", $data);
	}
	
	//ajax product list for merchant in admin....
	public function ajax_merchant_product_list(){
		
		
		
		
		$requestData= $_REQUEST;
		$columns = array(  
		// datatable column index  => database column name
			0 =>'id', 
			1 =>'product_id',
			2=> 'product_name',
			3=> 'marchent_name',
			4=> 'stock_quantity',
			5=> 'price',
			6=> 'sale_price',
			7=> 'id',
			8=> 'id',
			9=> 'id',
		
		);
		
		$getajaxProduct = $this->Merchant_model->get_ajax_merchant_product($columns,$requestData);
		$totalData = $getajaxProduct["totalData"];
		$totalFiltered = $getajaxProduct["totalData"];
		$result = $getajaxProduct["queryresult"];
	    $data = array();
		foreach($result as $row){  // preparing an array
		
		    $price 	 = strtoupper($this->config->item('currency')).' '.number_format($list->price);
			$saleprice = strtoupper($this->config->item('currency')).' '.number_format($list->sale_price);
			
			
			if(is_file($this->config->item('image_path').'/product/'.$list->product_id.$this->config->item('thumb_size').$list->product_image)){
				$imagepath = '<img src="'.base_url("photo/product/".$list->product_id.$this->config->item('thumb_size').$list->product_image).'" width="36" height="36" class="img-circle imageZoom" id="'.base_url('photo/product/'.$list->product_id.$this->config->item('medium_thumb_size').$list->product_image).'"> <img src="" style="display:none;"/>';
				
			}
			else{
				$imagepath= '<img src="'.base_url('photo/product/no_product.jpg').'" width="36" height="36" class="img-circle imageZoom" src="'.base_url('photo/product/no_product.jpg').'">';
				}
				
				
			if ($list->feature_status == 0) {
				$checkfeature = '<input type="checkbox" value="'.$row['product_id'].'" class="js_switch_feature" />';
			}else{
				$checkfeature = '<input type="checkbox" value="'.$row['product_id'].'" class="js_switch_feature" checked/>';
			}
			
			
			 if ($list->admin_status == 0) {
                    $checkadstatus = '<input type="checkbox" value="'.$row['product_id'].'" class="js-switch"  />';
                  }else{
                    $checkadstatus = '<input type="checkbox" value="'.$row['product_id'].'" class="js-switch" checked />';
                  }
			
			 
			
			$nestedData=array(); 
			$nestedData[] = $row["id"];
			$nestedData[] = ucfirst($row["product_id"]);
			$nestedData[] = ucfirst($row["product_name"]);
			$nestedData[] = ucfirst($row["marchent_name"]);
			$nestedData[] = $row['stock_quantity'];
			
			$nestedData[] = $price;
			$nestedData[] = $saleprice;
			$nestedData[] = $imagepath;
			$nestedData[] = $checkfeature;
			$nestedData[] = $checkadstatus;
						
			$data[] = $nestedData;
		}
//		
		$json_data = array(
		// for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
		  "draw"            => intval( $requestData['draw'] ),  
		// total number of records 
		  "recordsTotal"    => intval( $totalData ),  
		// total number of records after searching, if there is no searching then totalFiltered = totalData
		  "recordsFiltered" => intval( $totalFiltered ),
		   // total data array 
		  "data"            => $data  
			);

			echo json_encode($json_data);  // send data as json format
		
		
		}
	
	
	
	function add()
	{
		$data["page_title"] = "Add Merchant | iPengen";
		$this->load->template("merchant/add",$data);
	}
	
	function view($id)
	{
		$data["page_title"] = "view Merchant | iPengen";
		$data["merchant"] = $this->Merchant_model->get_merchant($id);
		$this->load->template("merchant/view",$data);
	}
	public function merchantAdd(){
		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		//set rule for validation in merchant user add from admin
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('phonenumber', 'Phone number', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[ig_marchent_login.marchent_email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[10]');
		$this->form_validation->set_rules('companyname', 'Company name', 'required');
		$this->form_validation->set_rules('companydesc', 'Company Description', 'required');
		$this->form_validation->set_rules('businessemail', 'Business Email', 'required');
		$this->form_validation->set_rules('businessphone', 'Business Phone Number ', 'required');
		
		$this->form_validation->set_rules('accountname', 'Account name', 'required');
		$this->form_validation->set_rules('accountnumber', 'Account number', 'required');
		$this->form_validation->set_rules('bankname', 'Bank name', 'required');
		$this->form_validation->set_rules('branchname', 'Branch name', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		
		$this->form_validation->set_rules('storename', 'Store name', 'required');
		$this->form_validation->set_rules('storedesc', 'Store Description', 'required');
		$this->form_validation->set_rules('brands', 'Brands', 'required');
		
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->template("merchant/add");
			}
			else
			{
				$data["getAllProduct"] = $this->Merchant_model->merchantadd();
			
				
				if($data["getAllProduct"]){
					
					    $this->session->set_flashdata('merchant_add_success_msg', 'success');
					    redirect("admin/merchant/list");
					
						}
				
				}
				
			
		
		
		
		}
	
	function getProduct()
	{
		$mid=$this->input->post('merchantId');
		$getProduct = $this->Merchant_model->get_product($mid);
		echo json_encode($getProduct);
		//print_r($getProduct);
	}
	public function subcategory(){
		if($this->input->is_ajax_request()){
			
	            $data['category_id'] = $this->input->post('catid');
	            $subcategories = $this->Merchant_model->getSubcategoriesBycatId($data['category_id'] );
//print_r($subcategories);die();
	            if(!empty($subcategories)){
		            foreach ($subcategories as $key => $value) {
		            	$subcat['sub_id'] = $value['cat_id'];
		            	$subcat['name'] = $value['name'];
		            	$subArr [] = $subcat;
		            }
		        }else{
		        	$subArr = array();
		        }
	            echo json_encode($subArr);  
	            
		}
	}
	
	public function getfees($mid){
		if($this->input->is_ajax_request()){
			 $merchantdfees = $this->Merchant_model->getfees($mid);
			 $data['result']=  array();
			 if(!empty($merchantdfees)){ 
				 $data['result'] = $merchantdfees;
				 
				 }
			
			echo json_encode($merchantdfees);
		}
	}
		
	public function feesupdate(){
		
		$mid     = $this->input->post('hideenmid');
		$feeamt  = $this->input->post('feeamt');
		$feetype = $this->input->post('feetype');
		
		$updatearray= array(
							'transaction_fee'=> $feeamt,
							'transaction_fee_type'=>$feetype
						   );
		
		$data["merchantfeeupdate"] = $this->Merchant_model->merchantfeesupdate($updatearray,$mid);
			if($data["merchantfeeupdate"]){
			$this->session->set_flashdata('merchant_update_success_msg', 'success');
			redirect("admin/merchant/list");
			
			}
	
	}
		
		
		
		
		
		
		

}
?>