<?php
/**
* 
*/
class City extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')){redirect('admin/login');}
		$this->load->model(array("city_model"));
		
	}

	function addcity()
	{
		if($this->input->post("citysubmit"))
		{
			$data = array("city_name"=>$this->input->post("name"));
			$result = $this->city_model->add_city($data);
			if($result)
			{
				$this->session->set_flashdata("successMsg","Successfully Added.");
				redirect("admin/city/list");
			}
			else
			{
				$data["page_title"] = "Add City | iPengen";
				$this->load->template("city/add",$data);
			}
		}
		else
		{
			$data["page_title"] = "Add City | iPengen";
			$this->load->template("city/add",$data);
		}
	}

	function editcity($cityCode = NULL)
	{
		if($cityCode != NULL)
		{
			if($this->input->post("citysubmit"))
			{
				$data = array("city_name"=>$this->input->post("name"));
				$result = $this->city_model->update_city($data,$cityCode);
				if($result)
				{
					$this->session->set_flashdata("successMsg","Successfully Update.");
					redirect("admin/city/list");
				}
				else
				{
					$this->session->set_flashdata("successMsg","Successfully Update with same data.");
					redirect("admin/city/list");
				}			
			}
			else
			{
				$data["cityId"] = $cityCode;
				$result = $this->city_model->city_data_for_edit($cityCode);
				if(!empty($result))
				{
					foreach($result as $row)
					{
						$data["page_title"] = "Edit City | iPengen";
						$data["cityName"] = $row->city_name;
						$this->load->template("city/edit",$data);
					}
				}
				else
				{
					$this->load->view("errors/ipengen_error/505");
				}
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}

	function citylist()
	{
		$data["page_title"] = "City List | iPengen";
		$data["cityList"] = $this->city_model->get_all_city();
		$this->load->template("city/list", $data);
	}

	function deletecity($cityCode = NULL)
	{
		if($cityCode != NULL)
		{
			$result = $this->city_model->deleteCityById($cityCode);
			if($result)
			{
				$this->session->set_flashdata("successMsg","Deleted Successfully.");
				redirect("admin/city/list");
			}
			else
			{
				$this->session->set_flashdata("errorMsg","Opps..Some thing wrong went.");
				redirect("admin/city/list");
			}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}

	function adminstatus($cityCode = NULL, $value = NULL)
	{
		if($cityCode != NULL && $value != NULL)
		{
			echo $result = $this->city_model->adminStatusChange($cityCode, $value);	
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
	}
}
?>