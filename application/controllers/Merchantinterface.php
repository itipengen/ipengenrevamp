<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Merchantinterface extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{
			$lang = "english"; 
			$this->langId = 1; 
			$this->lang_code = "en";
		}
		else
		{
			$lang = $sess_data["laguage_name"]; 
			$this->langId = $sess_data["laguage_id"]; 
			$this->lang_code = $sess_data["code"];
		}
	    $this->lang->load('home',$lang);
	    $this->load->library('email');
		$this->load->helper(array('form', 'url','ipengen_email_helper'));
		$this->load->library('session'); 
		$this->load->library('form_validation');
		$this->load->model('merchant_model');
		$this->load->model('user_model');
    }
	
	public function index(){ 

		$data['page_title'] = $this->lang->line("merchant_title");
		$this->form_validation->set_rules('mname', 'Name', 'trim|required');
		$this->form_validation->set_rules('mpassword', 'Password', 'trim|required|min_length[5]|max_length[10]');
		$this->form_validation->set_rules('memail', 'Email', 'trim|required|is_unique[ig_marchent_login.marchent_email]|valid_email');
		$this->form_validation->set_rules('mphone', 'Phone/Mobile No.', 'trim|required');
		$this->form_validation->set_error_delimiters( '<div class="error">','</div>' );
		if($this->form_validation->run() == TRUE){ 
			$data['mname'] = $this->input->post('mname');
			$data['mpassword'] = $this->input->post('mpassword');
			$data['memail'] = $this->input->post('memail');
			$data['mphone'] = $this->input->post('mphone');
			$merchant = $this->merchant_model->addMerchant($data);	
			$merchant_id = $merchant->marchent_id;	
			$activation_key = $merchant->activation_key;		
			if($merchant_id != 0){
				$sessiondata = array(
					 'merchant_log_in' => TRUE,
					 'merchant_id' => $merchant_id,
					);

					/*Email Content*/
					$userdata["userName"] = ucwords($this->input->post('mname'));
					$userdata["verifyAccount"] = site_url('merchantinterface/verify_email/'.$merchant_id.'/activation_key='.$activation_key);
					$supportData = $this->user_model->get_support_data();
					$emailFrom = $supportData[0]->info_email;
					$userEmail = $this->input->post('memail');
					$emailBody = $this->load->view("e_template/ipengen-merchant-signup",$userdata, true);
					$subject = "IPENGEN - Account Verification";
					$emailData = array(
								"title" 	=> "Ipengen",
								"from"		=>	$emailFrom,
								"to"		=>	$userEmail,
								"subject"	=>	$subject,
								"message"	=>	$emailBody
										);
					send_email($emailData);
					/*Email Content*/

					$this->session->set_userdata('merchant_log_in', $sessiondata);
					$this->session->set_flashdata('success_message', 'Registered successfully!! Please check your mail for verification link.');
					redirect("create-a-merchant");
			 }
		}
		else{
			$this->load->ftemplate('merchant_interface/signup',$data);
		}
		
	}

	public function verify_email($id,$activation_key){
		
		$key = str_replace('activation_key=','',$activation_key);
		if($this->merchant_model->updateStatus($id,$key)){
			$this->session->set_flashdata('smsg', 'Congratulations! You have successfully verified your email address.');
			redirect("merchant/login");
		}else{
			
			$this->session->set_flashdata('msg', 'You are already a verified Merchant.');
			redirect("merchant/login");
		}
	}
	
}
?>