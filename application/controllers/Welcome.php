<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct(){
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];;}
		$this->lang->load('home',$lang);
		$this->load->model(array('Welcome_model','Product_model'));
	}
	
	public function index()
	{
		$lanuagearray=$this->session->userdata('site_lang');
			if(!empty($lanuagearray)){
				$language_id=$lanuagearray['laguage_id'];
					$language_name=$lanuagearray['laguage_name'];
				}else{
			$language_id=1;
		}

		$data['page_title']=$this->lang->line("home_tag");
        $this->load->library('facebook'); // Automatically picks appId and secret from config
		$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/login'), 
                'scope' => array("email") // permissions here
            ));
		$data['eventcategory'] = $this->Welcome_model->getAlleventcategory($language_id);
		$data['blogdata'] = $this->Welcome_model->getAllblog($language_id);
		$data['featuredproduct'] = $this->Welcome_model->getAllfeaturedproduct($language_id);
		$this->load->ftemplate('site/index',$data);
	}
	public function faq()
	{ 
		$this->load->ftemplate('site/faq');
	}
	public function shop()
	{
		$lanuagearray=$this->session->userdata('site_lang');
			if(!empty($lanuagearray)){
				$language_id=$lanuagearray['laguage_id'];
					$language_name=$lanuagearray['laguage_name'];
				}else{
			$language_id=1;
		}

		$data['page_title']=$this->lang->line("shop");
        $this->load->library('facebook'); // Automatically picks appId and secret from config
		$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/login'), 
                'scope' => array("email") // permissions here
            ));
		$data['eventcategory'] = $this->Welcome_model->getAlleventcategory($language_id);
		$data['blogdata'] = $this->Welcome_model->getAllblog($language_id);
		$data['featuredproduct'] = $this->Welcome_model->getAllfeaturedproduct($language_id);
		$this->load->ftemplate('site/shop',$data);
	}
	public function blog_view($blogId = NULL,$blogTitle = NULL)
	{
		if($blogTitle != NULL)
		{
			$result = $this->Welcome_model->getBlogById($blogId,$this->langId);
			if(!empty($result))
			{
				$data["page_title"] = $result[0]->blog_title;
				$data["blogData"] = $result;
				$this->load->ftemplate("site/blog_view",$data);
			}
			else
			{
				$this->load->view("errors/ipengen_error/505");
			}
		}
		else
		{$this->load->view("errors/ipengen_error/404");}
	}
	
	public function seturl()
	{
		if($this->input->is_ajax_request())
		{
			$category = $this->input->post("catName");
			$url = base_url()."create-a-wishlist?cat=".$category;
			$this->session->set_userdata("createCatUrl",$url);
			if($this->session->userdata("createCatUrl"))
			{ echo 1; }
			else
			{ echo 0; }
			//print_r($category);
		}
	}
}
?>