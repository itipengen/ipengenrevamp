<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Addressbook extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
					$this->load->helper(array('form', 'url'));
					$this->load->helper('cookie');
					$this->load->library('session'); 
					 $this->load->library('email');
					$this->load->library('form_validation');
					$this->load->model('Transaction_model');
					$this->load->model('addressbook_model'); 
					$this->load->model('product_model');
					$this->load->model('checkout_model');
					$this->load->library('pagination');	
					$this->load->library('M_pdf');
					$params = array('server_key' => 'VT-server-FM2pqsRBwudKr-rMcWLxG0vK', 'production' => false);
					//$params = array('server_key' => 'VT-server-B1rRooqaNPSaKgsmSzbBXot1', 'production' => false);
					$this->load->library('veritrans');
					$this->veritrans->config($params);
					 if(empty($this->session->userdata['log_in']['user_id'])){
					   redirect('/');
					 }

       }


	 /* function to display addressbook data  after fetching data from ig_user_shippingaddress */	
	 			
	  public function index(){
		 $data =array();
		 $user_id = $this->session->userdata['log_in']['user_id']; 
	     $data['addressbookdetails'] = $this->addressbook_model->get_addressbook($user_id);
		 $this->load->ftemplate("addressbook/indexaddressbook",$data);
	  } 
	  
	  
 /* function to add and update address data  ig_user_shippingaddress  */
	  
	  public function addupdate_address($addressbook_id=''){
		  $addresspost=$this->input->post();
		  if(!empty($addresspost)){
			$data=array();  
			$addressdata = array();
			$user_id = $this->session->userdata['log_in']['user_id'];
			$addressdata['user_id'] = $user_id;
			$addressdata['fname'] = trim($this->input->post('firstName'));
			$addressdata['lname'] = trim( $this->input->post('lastName'));
			$addressdata['address'] = trim($this->input->post('address'));
			$addressdata['address2'] = trim( $this->input->post('address2'));
			$addressdata['postcode'] = trim($this->input->post('postcode'));
			$addressdata['kecamatan'] = trim($this->input->post('kecamatan'));
			$addressdata['city'] = trim( $this->input->post('city'));
			$addressdata['mobile'] = trim( $this->input->post('mobile')); 
			if(!empty($addressbook_id)){
				$data['addressbook_id'] = $addressbook_id;
			}
			$data['addressdata']= $addressdata;
		    $response = $this->addressbook_model->addupdate_addressbook($data);
			if($response == TRUE){
				$this->session->set_flashdata('smsg','Successfully Saved');
				redirect('addressbook/index');
			}else{
				$this->session->set_flashdata('msg','Not Successfully Saved');
			    redirect('addressbook/index');
			}
		  }
		  
		  
	  }
	  
	/* public function to delete address by address id  */
	
	  public function delete_address($addressbook_id){
		  if(!empty($addressbook_id)){
			  $response= $this->addressbook_model->delete_addressbook($addressbook_id);
			  if($response== TRUE){
				  $this->session->set_flashdata('msgdel_success','Successfully Deleted');
			      redirect('addressbook/index');
			  }else{
				  $this->session->set_flashdata('msgdel_fail','Not Successfully Deleted');
			      redirect('addressbook/index');
			  }
			  
		  }
	  }
	  
}