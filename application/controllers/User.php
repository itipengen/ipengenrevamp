<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
			$this->load->helper(array('form', 'url','cookie','common_helper','ipengen_email_helper'));
			$this->load->library('session'); 
			$this->load->library('email');
			$this->load->library('form_validation');
			$this->load->model(array('user_model', 'payment_model')); 
			
       }
		
		/*Method Name: signup
		  Model Name : sign_up
		  Parameter: $_POST
		*/
		public function signup(){ 
		if($this->input->is_ajax_request()){ 
			$data['email'] = $this->input->post('regemail');
			$data['password'] = $this->input->post('regpass');
			$data['fname'] = $this->input->post('regfname');
			$data['password'] = $this->input->post('reglname');

			$this->form_validation->set_rules('regemail', 'Email', 'trim|required|is_unique[ig_user.email]');
			$this->form_validation->set_rules('regpass', 'Password', 'required');
			$this->form_validation->set_rules('regfname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('reglname', 'Password', 'required');

			if($this->form_validation->run() == FALSE){
			   $result['msg'] = "unique";
			   $result['email'] = array("message"=>form_error('regemail'));
			   $result['password'] = array("message"=>form_error('regpass'));
			  }
			else{

				$data = $this->user_model->userregister();
				$result1=$this->user_model->getUsername($data)->row();
				$user_name=$result1->user_id;

				if($data){
					$sessiondata = array(
						 'log_in' => TRUE,
						 'user_id' => $data,
						 'user_name' => $user_name,
						 'email' => $this->input->post('regemail'),
						 'fname' => $this->input->post('regfname'),
						 'lname' => $this->input->post('reglname'),
						 'profile_pic' => "",
						);

						$this->session->set_userdata('log_in', $sessiondata);
						$result['msg'] = "success";
						$this->session->set_flashdata('sussess_message', 'Registered successfully! Please check your mail for verification link');
						/*Email Content*/
						$userdata["userName"] = ucfirst($this->input->post('regfname'))." ".ucfirst($this->input->post('reglname'));
						$supportData = $this->user_model->get_support_data();
						$emailFrom = $supportData[0]->info_email;
						$userEmail = $this->input->post('regemail');
						$emailBody = $this->load->view("e_template/ipengen-signup",$userdata, true);
						$subject = "Congrulations..!!! You have successfully registered.";
						$emailData = array(
									"title" 	=> "Ipengen",
									"from"		=>	$emailFrom,
									"to"		=>	$userEmail,
									"subject"	=>	$subject,
									"message"	=>	$emailBody
											);
						send_email($emailData);
						/*Email Content*/
				 }
				 else
				{
				 $this->session->set_flashdata('error_message', 'You are an unauthorized user');
				 $result['msg'] = 'error';
				}
		}

               echo json_encode($result);
	}
                        
}
		
		
		/*Method Name: login
		  Model Name : login
		  Parameter: $_POST
		*/
		public function login(){
			
			if($this->input->is_ajax_request()){
				   

								$data['username'] = $this->input->post('logemail');
								$data['password'] = $this->input->post('logpass');
								$currenturl = $this->input->post('currenturl');

								$this->form_validation->set_rules('logemail', 'Email', 'trim|required');
								$this->form_validation->set_rules('logpass', 'Password', 'required');
								
								
								if($this->form_validation->run() == FALSE){
								   $result['email'] = array("message"=>form_error('logemail'));
                                   $result['password'] = array("message"=>form_error('logpass'));
                                  }
								else{

									$data= $this->user_model->userlogin();
									if($data['msg']['msg'] == 'success'){
				
									$this->session->set_flashdata('sussess_message', 'Login successfully');
									foreach($data['result'] as $val){
										$blockCheck = $val->is_block;
											$sessiondata = array(
																 'log_in' => TRUE,
																 'user_id' => $val->id,
																 'user_name' => $val->user_id,
																 'email' => $val->email,
																 'fname' => $val->fname,
																 'lname' => $val->lname,
																 'profile_pic' => $val->profile_pic
																);
										}
									$cookie = array(
													'name'   => 'login',
													'value'  => $this->input->post('logemail'),
													'expire' => 86500,
													//'domain' => ".localhost",
													//'path'   => '/',
													//'prefix' => 'myprefix_',
													//'secure' => TRUE
													);
									 $this->session->set_userdata('log_in', $sessiondata);
									 $this->input->set_cookie($cookie);
									 if($blockCheck == "no")
									 { $result['msg'] = 'success'; }
									 else
									 { $result['msg'] = 'blocked'; }
									}
									else if($data['msg']['msg'] == 'blocked'){
                                        
                                         $result['msg'] = 'blocked';
										
										}
									 else
									{
										$this->session->set_flashdata('error_message', 'You are an unauthorized user');
									    $result['msg'] = 'error';
									}
							}

               echo json_encode($result);
			}

       /*-------------------------------------------------FACEBOOK LOGIN START-------------------------------------------------------------*/
			$this->load->library('facebook'); // Automatically picks appId and secret from config
			$user = $this->facebook->getUser();
			if ($user){
				$data['user_profile'] = $this->facebook->api('/me?fields=id,name,link,email,gender');
				//print_r($data['user_profile']);
				$fbemail= $data['user_profile']['email'];
				$result= $this->user_model->fbemailinfoDetails($fbemail);
                //$resultfbloginacivity= $this->auth_model->login_activity($result[0]->uid);
			 if($result){
				
				$sessiondata = array(
									'log_in' => TRUE,
									'user_id' => $result[0]->id,
									'user_name' =>$result[0]->user_id,
									'email' => $result[0]->email,
									'fname' => $result[0]->fname,
									'lname' => $result[0]->lname,
									'profile_pic' => $result[0]->profile_pic,
									);
	
				$this->session->set_userdata('log_in', $sessiondata);
				//$resultfbloginacivity= $this->auth_model->login_activity($result[0]->uid);
				if($this->session->userdata("createCatUrl"))
				{
					redirect($this->session->userdata("createCatUrl"));
				}
				else
				{ redirect('/'); }
				} 
			else{
				
				$imagefb= $data['user_profile']['id'].'.jpg';
				$fbname = explode(" ", $data['user_profile']['name']);
                $fbfname = $fbname[0];
				$fblname = $fbname[1];
				    
				$datafb = array(
								'fname'				=> $fbfname,
								'lname'				=> $fblname,
								'email' 			=> $fbemail,
								'user_id' 			=> $fbfname.rand(10000,99999),
								'profile_pic' 		=> $imagefb,
								'created_date'		=>date('Y-m-d H:i:s'),
								'modification_date'	=>date('Y-m-d H:i:s'),
								'status' 			=> 'yes',
								'is_block'			=> 'no',
								//'ip_address'		=>  $_SERVER['HTTP_X_FORWARDED_FOR'] 
								);
	
				$resultInsert= $this->user_model->fbinsert($datafb,$imagefb);

				$result12= $this->user_model->userinfoidd($resultInsert);
								
				if (!is_dir($this->config->item('image_path').'userimage/'.$result12[0]->id.'/')) {
					mkdir($this->config->item('image_path').'userimage/'.$result12[0]->id.'/');
				}
				if (!is_dir($this->config->item('image_path').'userimage/'.$result12[0]->id.'/thumb/')) {
					mkdir($this->config->item('image_path').'userimage/'.$result12[0]->id.'/thumb/');
				}

				//$mainDir= mkdir("photo/userimage/".$result12[0]->id."/");

				//$thumbDir= mkdir("photo/userimage/".$result12[0]->id."/thumb/");

				$LargeImageURL = "https://graph.facebook.com/".$data['user_profile']['id']."/picture?type=large"; //  Like "http:// ...."
				$SmallImageURL = "https://graph.facebook.com/".$data['user_profile']['id']."/picture?type=small"; //  Like "http:// ...."

				$LargeFileToSave = "photo/userimage/".$result12[0]->id."/".$data['user_profile']['id'].'.jpg';     //  Like "/home/.." or "C:/..."
				$LargeContent = file_get_contents($LargeImageURL);
				file_put_contents($LargeFileToSave, $LargeContent);

				$SmallFileToSave = "photo/userimage/".$result12[0]->id."/thumb/".$data['user_profile']['id'].'.jpg';     //  Like "/home/.." or "C:/..."
				$SmallContent = file_get_contents($SmallImageURL);
				file_put_contents($SmallFileToSave, $SmallContent);
	
				$sessiondataOne = array(
										'log_in' => TRUE,
										'user_id' =>$result12[0]->id,
										'user_name' =>$result12[0]->user_id,
										'email' => $result12[0]->email,
										'fname' => $result12[0]->fname,
										'lname' => $result12[0]->lname,
										'profile_pic' => $data['user_profile']['id'].'.jpg',
										);

				//$resultfbloginacivity= $this->auth_model->login_activity($result12[0]->uid);
				$this->session->set_userdata('log_in', $sessiondataOne);
				redirect($currenturl);
			  }
			}
			
     /*-------------------------------------------------FACEBOOK LOGIN END-------------------------------------------------------------*/


    }



        /*Method Name: logout
		  Model Name : logout
		  Parameter: $_POST
		*/
		public function logout(){
								$this->session->unset_userdata('log_in');
								$this->session->unset_userdata('discount');
			                    $this->session->unset_userdata('gross_amount');
								$this->session->unset_userdata('createCatUrl');
        						redirect('/'); 
					}

		 /*Method Name: profile
		  Model Name : logout
		  Parameter: $_POST
		*/
		public function profile(){
						if(empty($this->session->userdata['log_in']['user_id'])){
							redirect('/');
						}
						$userid= $this->session->userdata['log_in']['user_id'];
						$data['lang_code'] = $this->lang_code;
						$data['page_title'] = $this->lang->line("profile");
						$data['userdata'] = $this->user_model->userdeatails($userid);
						$data['useraddressdata'] = $this->user_model->userdeatails_address($userid);
						$this->load->ftemplate('user/profile', $data);
					} 

		/*Method Name: updateprofile
		  Model Name : updateprofile
		  Parameter: $_POST
		*/
		public function updateprofile(){
				
						        $userid= $this->session->userdata['log_in']['user_id'];

								if($this->input->is_ajax_request()){

							  	$img = $this->input->post('image');
								if(empty($img)){
										 $filename = "";
								}
								else{
					if(is_file($this->config->item('image_path').'userimage/'.$userid.'/'.$this->session->userdata['log_in']['profile_pic'])){
					unlink($this->config->item('image_path').'userimage/'.$userid.'/'.$this->session->userdata['log_in']['profile_pic']);
					}
					if(is_file($this->config->item('image_path').'userimage/'.$userid.'/thumb/'.$this->session->userdata['log_in']['profile_pic'])){
					unlink($this->config->item('image_path').'userimage/'.$userid.'/thumb/'.$this->session->userdata['log_in']['profile_pic']);
					}
									$imagepath = $this->config->item('image_path').'userimage/';
									//$imagethumpath = $this->config->item('image_path').'userimage/';
								if (!is_dir($this->config->item('image_path').'userimage/'.$userid.'/')) {
									 mkdir($this->config->item('image_path').'userimage/'.$userid.'/');
									 
								}
								if (!is_dir($this->config->item('image_path').'userimage/'.$userid.'/thumb/')) {
									mkdir($this->config->item('image_path').'userimage/'.$userid.'/thumb/');
									}
								$imagepathOne= $imagepath.$userid.'/';
								$imagepaththumb= $imagepath.$userid.'/thumb/';
								$img = str_replace('data:image/jpeg;base64,', '', $img);
								$img = str_replace(' ', '+', $img);
								$data = base64_decode($img);
								$filename = uniqid() . '.jpg';
								$file = $imagepathOne . $filename;
								$file2 = $imagepaththumb . $filename;
								$success = file_put_contents($file, $data);
								$success = file_put_contents($file2, $data);
								
								$sessiondataOne = array(
													'log_in' => TRUE,
													'user_id' =>$this->session->userdata['log_in']['user_id'],
													'email' => $this->session->userdata['log_in']['email'],
													'fname' => $this->session->userdata['log_in']['fname'],
													'lname' => $this->session->userdata['log_in']['lname'],
													'profile_pic' => $filename,
										);
								$this->session->set_userdata('log_in', $sessiondataOne);
								}
								$result['imagepath'] =$this->config->item('image_display_path').'userimage/'.$this->session->userdata['log_in']['user_id'].'/'.$this->session->userdata['log_in']['profile_pic'];
								
							  $data = $this->user_model->updateprofile($userid,$filename);
								if($data){$result['msg'] = "success";}else{$result['msg'] = "unsuccess";}
							echo json_encode($result);
						//$this->load->ftemplate('user/profile', $data);
					} 
		}
		
		public function updateuserid(){
								$userid= $this->session->userdata['log_in']['user_id'];

								if($this->input->is_ajax_request()){
									
									$data['user_id'] = $this->input->post('user_id');
									$this->form_validation->set_rules('user_id', 'User Id', 'trim|required|is_unique[ig_user.user_id]');
								
								
								if($this->form_validation->run() == FALSE){
                                   $result['msg'] = "unique";
								   $result['user_id'] = array("message"=>form_error('user_id'));
                                  
                                  }
								else{
								
							    $data = $this->user_model->updateuserid($userid);
								if($data){$result['msg'] = "success";}else{$result['msg'] = "unsuccess";}
							
								}
								
						//$this->load->ftemplate('user/profile', $data);
					} 
					echo json_encode($result);
				}
		
        /*Method Name: passwordsetting
		  Model Name : password
		  Parameter: $_POST
		*/
		public function passwordsetting(){
			if(empty($this->session->userdata['log_in']['user_id'])){
				redirect('/');
			}
			$userid= $this->session->userdata['log_in']['user_id'];
			$data['lang_code'] = $this->lang_code;
			$data['page_title'] = $this->lang->line("password_change");
			$data['userdata'] = $this->user_model->userdeatails($userid);
			$this->load->ftemplate('user/changepassword',$data);
						
		} 
		/*Method Name: ajaxchangepassword
		Model Name : password
		Parameter: $_POST
		*/
		public function ajaxchangepassword(){
				$userid			= $this->session->userdata['log_in']['user_id'];
				$password 		= md5($this->input->post('password'));
				$curentpassword = md5($this->input->post('curentpassword'));
				if($this->input->is_ajax_request()){
				
						$data['password'] = $this->input->post('password');
						$data['currenrpassword'] = $this->input->post('curentpassword');
						
						$this->form_validation->set_rules('password', 'Password', 'required');
						$this->form_validation->set_rules('curentpassword', 'Current Password', 'required');
						
						
						if($this->form_validation->run() == FALSE){
								$result['msg'] = "unique";
								$result['password'] = array("message"=>form_error('password'));
								$result['currentpassword'] = array("message"=>form_error('currenrpassword'));
						}
						else{
						$existpasswordcheck = $this->user_model->existpasswordcheck($curentpassword,$userid);
						if($existpasswordcheck){
							$data = $this->user_model->passwordupdate($password,$userid);
							if($data){

		
    							  
							   $result['msg'] = "success";
							   $this->session->set_flashdata('sussess_message', 'Registered successfully! Please check your mail for verification link');
							   }
						   else{
							   $this->session->set_flashdata('error_message', 'You are an unauthorized user');
							   $result['msg'] = 'error';
							  }
							}
						else{
							 $result['msg'] = 'notmatched';
							
							}
						}
				
				echo json_encode($result);
				}
		}


		public function forgetpassword(){
					if($this->input->is_ajax_request()){
						$configEmail = $this->payment_model->getAllConfigEmail();
						$emailFrom = $configEmail[0]->info_email;
						$from_email = "matainja020@gmail.com";
						$to_email = $this->input->post('email');
						$result = $this->user_model->checkemail();
						//print_r($result);
						//echo base_url()."auth/resetpassword/".md5($this->config->item('email_verification_salt').$result[0]->id);
						if(!empty($result)){
										//Load email library 
										$this->load->library('email'); 
										$subject = "Forget Password Request";
										$message = base_url()."user/resetpassword/".md5($this->config->item('email_verification_salt').$result[0]->id);
										$this->email->from($from_email, 'Ipengen'); 
										$this->email->to($to_email);
										$this->email->subject($subject); 
										$this->email->message($message); 
										 //Send mail 
										if($this->email->send()) {
											$data['msg']= "success";
										}
										else {
											$data['msg']= "error";
											} 

										}
									else{
										$data['msg'] = "passwordnotmatched";
										}
						}
					echo json_encode($data);
				}

		public function resetpassword($mdid){
		
	    
			$this->load->library('form_validation');

			$this->form_validation->set_rules('password', 'Password', 'required');

			$this->form_validation->set_rules('cpassword', 'Re-enter password','required|matches[password]');
			
			if($this->form_validation->run() == TRUE){
			$password=$this->input->post('password');
			$total_result=$this->user_model->allusers();
			if($total_result){
			foreach($total_result as $result){
				 if($mdid == md5($this->config->item('email_verification_salt').$result->id)){
					 $this->db->where('id',$result->id);
					 $this->db->update('ig_user',array('password'=>md5($password)));
					 $this->session->set_flashdata('sussess_message','Password Change successfully');
						redirect('/');
					}
				  }
				}
			}
			else{
				$this->session->set_flashdata('error_message','Password is not matched.');
				$data['lang_code'] = $this->lang_code;
				$data["Page_title"] = $this->lang->line("password_reset");
				$this->load->ftemplate('user/resetpassword');
				}
				
				
			}


	   public function user_profile($userID = NULL) {
		if($userID != NULL)
		{
			$data['lang_code'] = $this->lang_code;
			$data['userData'] = $this->user_model->userinfoid($userID);
			$data['wishlist'] = $this->user_model->lastFiveRecordWishlistInfo($userID);
			$data['wishlist_gift'] = $this->user_model->lastSIxRecordWishlistGift($userID);
			if(!empty($data['userData']))
			{
				foreach($data['userData'] as $list)
				{
					$data['page_title'] = ucfirst($list->fname)." ".ucfirst($list->lname); //$this->lang->line("profile");
					$data["userID"] = $list->id;
					$data["userName"] = ucfirst($list->fname)." ".ucfirst($list->lname);
					if($list->profile_pic != "")
					{
						if(is_file($this->config->item("image_path")."userimage/".$list->id."/".$list->profile_pic))
						{ $data["profileImage"] = base_url('photo/userimage/'.$list->id."/".$list->profile_pic); }
						else
						{ $data["profileImage"] = base_url('assets/img/userimage.png'); }
						
					}
					else
					{
						$data["profileImage"] = base_url('assets/img/userimage.png');
					}
					$data["profileCreationDate"] = date("d M Y",strtotime($list->created_date));
					$data["userEmail"] = $list->email;
				}
				$this->load->ftemplate('user/publicprofile',$data);
			}
			else
			{$this->load->view("errors/ipengen_error/505");}
		}
		else
		{
			$this->load->view("errors/ipengen_error/404");
		}
      }
	  public function dashboard() { 
	  $data['lang_code'] = $this->lang_code;
	  $data['page_title']=$this->lang->line("dashboard");
	  if($this->session->userdata('log_in')== false){
              redirect();
           }else{
			$this->load->model('Wishlist_model');
			$this->load->helper('common_helper');
			$data['uid'] = $uid = $this->session->userdata['log_in']['user_id'];
			$data['my_wishlist'] =  $this->Wishlist_model->get_my_wishlist5($uid);
			$data['wishlist_notification'] =  $this->Wishlist_model->wishlist_notification($uid, $this->langId);
			//print_r($data);
			//die();
			$this->load->ftemplate('user/dashboard',$data);
		   }
      }
		public function notification() {
			$this->load->model('Wishlist_model');
			$uid = $this->session->userdata['log_in']['user_id'];
			$receiverID = $this->input->post("receiverID");
			$senderuserData = $this->Wishlist_model->getUserdataUsingId($uid);
			$receiveruserData = $this->Wishlist_model->getUserdataUsingId($receiverID);
			$result=$this->Wishlist_model->get_user_noification($this->input->post('notification_id'));
			$this->Wishlist_model->update_user_noification($this->input->post('notification_id'));
			$date=date_create(date('Y-m-d H:i:s', time()));
        	$ndate= date_format($date,"Y-m-d H:i:s");
		
			$data=array(
						'notification_description'=>'Thanks for the gift',
						'notification_type'=>'reply',
						'notification_date'=>$ndate,
						'sender_uid'=>$result->uuid,
						'uuid'=>$result->sender_uid,
			);
			$this->Wishlist_model->add_user_noification($data);
			if(!empty($senderuserData) && !empty($receiveruserData))
			{
				$data["receiveruserData"] = $receiveruserData;
				$data["senderuserData"] = $senderuserData;
				$subject = "Thanks for the gift | Notification";
				$fromEmail = $senderuserData[0]->email;
				$userName = ucwords($senderuserData[0]->fname." ".$senderuserData[0]->lname);
				$email = $receiveruserData[0]->email;
				$emailBody = $this->load->view("e_template/wishlist-say-thanks-you",$data, true);
				$emailData = array(
							"title" 	=>  $userName,
							"from"		=>	$fromEmail,
							"to"		=>	$email,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				$result = send_email($emailData);
				if($result)
				{echo 1;}else{echo 0;}
			}
		}
		public function send_message()
		{
			
			$uid = $this->session->userdata['log_in']['user_id'];
			$user_name = $this->session->userdata['log_in']['user_name'];
			if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

			$this->form_validation->set_rules('message', 'Message','required');
			
			if($this->form_validation->run() == TRUE){
				$data['fname']=$this->session->userdata['log_in']['fname'];
				$data['lname']=$this->session->userdata['log_in']['lname'];
				$data['email'] = $this->session->userdata['log_in']['email'];
				//$contactEMail = $this->input->post('email');
				$data['contactData'] = $this->user_model->checkemail();
				$data['message']=$this->input->post('message');
				$emailBody = $this->load->view("e_template/user_contact_email",$data, true);
				$subject = 'You have One  messages from "'.ucfirst($data['fname']).' '.ucfirst($data['lname']).'"';
				$emailData = array(
							"title" 	=>  "Ipengen",
							"from"		=>	$this->session->userdata['log_in']['email'],
							"to"		=>	$this->input->post('email'),
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				$result = send_email($emailData);
				
				echo "success";
			}else{
				echo 'error';
			}
			}
		}
		
		public function storeSessionUrl(){
			$last_url = $this->input->post('lastUrl');	
			$this->session->set_userdata('last_url',$last_url);
			echo json_encode($last_url);
		}
}
?>