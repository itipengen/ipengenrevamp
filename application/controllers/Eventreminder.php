<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Eventreminder extends CI_Controller {
	private $langId;
	private $lang_code;
	 function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		
					$this->load->helper(array('form', 'url'));
					$this->load->helper('cookie');
					$this->load->library('session'); 
					 $this->load->library('email');
					$this->load->library('form_validation');
					$this->load->model('eventreminder_model'); 
					 if(empty($this->session->userdata['log_in']['user_id'])){
					    redirect('/');
					 }

       }


					
	public function index(){
		 $uid = $this->session->userdata['log_in']['user_id'];
		 $data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("event_remind");
	if(isset($_POST))
	   { 
	 
	   $this->form_validation->set_rules('event_name','Enent Name','required');
		$this->form_validation->set_rules('eventdate','Event date','required');
		$this->form_validation->set_rules('repeatevent','Reteat Event','required');
		$this->form_validation->set_rules('remindme','Remind me Event','required');
		if($this->form_validation->run() == TRUE)
		{
			
			$insertdata=array(
			'event_name'=>$this->input->post('event_name'),
			'event_date'=>$this->input->post('eventdate'),
			'event_remainder'=>$this->input->post('remindme'),
			'event_repeat'=>$this->input->post('repeatevent'),
			'uid'=>$uid
			);
			$result = $this->eventreminder_model->addevent($insertdata);
			$this->session->set_flashdata('successmessage','Event added succesfully.');
			$data['event_list']=$this->eventreminder_model->get_event_list($uid);
			//$data['my_wishlist'] = $this->eventreminder_model->get_dropdown_list($uid);
			//$this->load->ftemplate('eventreminder/eventreminder',$data);
			redirect('reminder');
		}else{
			$data['event_list']=$this->eventreminder_model->get_event_list($uid);
			//$data['my_wishlist'] = $this->eventreminder_model->get_dropdown_list($uid);
			$this->load->ftemplate('eventreminder/eventreminder',$data);
		}
	   
	   
	   }else{
	   
	
	
	 $data['event_list']=$this->eventreminder_model->get_event_list($uid);
			//$data['my_wishlist'] = $this->eventreminder_model->get_dropdown_list($uid);
			$this->load->ftemplate('eventreminder/eventreminder',$data);
	   }

	}
	public function delete($id=NULL)
	{
		$eventdate = $this->eventreminder_model->event_delete($id);
		if($eventdate)
		$this->session->set_flashdata('successmessage','Event deleted succesfully.');
		redirect('reminder');
	}
	public function edit()
	{
		
		
		 $this->form_validation->set_rules('event_name_edit','Enent Name','required');
		$this->form_validation->set_rules('eventdateedit','Event date','required');
		$this->form_validation->set_rules('editrepeatevent','Reteat Event','required');
		$this->form_validation->set_rules('eremindme','Remind me Event','required');
		if($this->form_validation->run() == TRUE)
		{
		$id=$this->input->post('eid');
		$data=array(
			'event_name'=>$this->input->post('event_name_edit'),
			'event_date'=>$this->input->post('eventdateedit'),
			'event_remainder'=>$this->input->post('eremindme'),
			'event_repeat'=>$this->input->post('editrepeatevent')
			);
		$eventdate = $this->eventreminder_model->event_edit($id,$data);
		if($eventdate)
		echo 'success';
		}else{
			echo "error";
		}
	}
	public function eventdate(){
					 $uid = $this->session->userdata['log_in']['user_id'];
					 $eventdate= $this->input->post('eventname');
					 $data= array('id'=>$eventdate);
					 
					 $eventdate = $this->eventreminder_model->get_eventdate($data);
					 foreach($eventdate as $eventdate){
								$result['eventdate'] = date('Y-m-d', strtotime($eventdate->event_date));
							  }
					 if(!empty($eventdate)){
						 $result['msg']= "success";
						 }
					 else{
						 $result['msg']= "blank";
						 }
					  echo json_encode($result);
					}
}