<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {
	private $langId;
	private $lang_code;
	public function __construct()
	{
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
        $this->load->library('cart');
		$this->load->model('product_model');
		$this->load->model('wishlist_model');
		$this->load->model('cart_model');
		$this->load->model('payment_model');
	}

	public function index()
	{
		$response = array();
		$data = array();	
		if($_POST['type'] == 'cash_gift'){
				$result = $this->wishlist_model->getCashGiftTransactionFee()->row();
				$fees = $result->transaction_fees;
				if (stripos($fees, 'RP') !== FALSE){
					$feeType='RP';
				}
				if (stripos($fees, '%') !== FALSE){
					$feeType='%';
				}
				$fee = intval(preg_replace('/[^0-9]+/', '', $fees), 10);
		}



        $giftCount=0;
		$contributionCount=0;
		$product_id = isset($_POST['product_id']) ? $_POST['product_id'] : 0;
		$productData = $this->product_model->getProductDetails($product_id,$this->langId);
		if(!empty($productData))
		{ $product_stock_qty = $productData[0]->stock_quantity; }

		if($product_id != 0){
			$product_sale_price = $product_price = $this->product_model->getProductPriceById($product_id);
		}else{
			$product_price = isset($_POST['amount']) ? $_POST['amount'] : 0;
			if($product_price){
				if (strpos($fee, '%') !== false) {
					$product_price = $product_price + (($product_price * $fee)/100);
				}else{
					$product_price = $product_price + $fee;
				}
			}
			$product_sale_price = isset($_POST['amount']) ? $_POST['amount'] : 0;
		}
		
		if( $_POST['type']=='buy_gift')
		{
			$giftCount=1;
			if(is_numeric($_POST['wid']) && ($_POST['wid']) !=0)
			{
				$option_value = $_POST['type'].'_'.$_POST['wid'];
			}else{
				
				$option_value = $_POST['type'].'_0';
				
			}
		}
		elseif( $_POST['type']=='cash_gift')
		{
			$i=rand(0,100000);
			if(is_numeric($_POST['wid']) && ($_POST['wid']) !=0)
			{
				$option_value = $_POST['type'].'_'.$_POST['wid'].'_'.$i;
			}else{
				$option_value = $_POST['type'].'_'.$i;				
			}
		}else
		{	
			$j=rand(0,100000);
			$contributionCount=1;
			if(is_numeric($_POST['wid']) && ($_POST['wid']) !=0)
			{
				$option_value = $_POST['type'].'_'.$_POST['wid'].'_'.$j;
			}else{
				
				$option_value = $_POST['type'].'_'.$j;
				
			}
			
			
		}
		$this->cart->product_name_rules = '\d\D';

		//$this->cart->product_name_rules = '[:print:]';
		// Set array for send data.
		$insert_data = array(
			'id' => $product_id,
			'qty' => isset($_POST['quantity']) ? $_POST['quantity'] : 1,
			'price' => isset($_POST['contribute_amount']) ? $_POST['contribute_amount']: (isset($product_price) ? $product_price : 0),
			'name' => isset($_POST['product_name']) ? htmlspecialchars($_POST['product_name']) : '',
			'type' => $_POST['type'],
			'wishlist_id' => (is_numeric($_POST['wid'])) ? $_POST['wid'] : 0,
			'product_sale_price' => isset($product_sale_price) ? $product_sale_price : 0,
			'options'=> isset($option_value) ? $option_value : 0,
			'giftCount'=>$giftCount,
			'contributionCount'=>$contributionCount,
			'stock_quantity' => (isset($product_stock_qty) ) ? $product_stock_qty : 0
		);


		$fprice = $insert_data['price'];
		$result = $this->cart_model->getMerchantByProductId($product_id); 
		if (!empty($result)) {
			$merchant_id  = $result->merchant_id;
		}
		if($merchant_id > 0){
				if($_POST['type']=='buy_gift'){
					$transaction=$this->payment_model->getTransactionfeebyMerchantId($merchant_id);
					$transaction_fee=$transaction->transaction_fee;
					$transaction_fee_type = $transaction->transaction_fee_type;
					if ($transaction_fee_type == 'RP'){
						$transFeeNum=$transaction_fee;
						$newTransFee='RP '.$transFeeNum;
						$fee = ($transaction_fee*$insert_data['qty']);
					}
					if ($transaction_fee_type == '%'){
						$transFeeNum=$transaction_fee;
						$newTransFee=$transFeeNum." %";
						$fee = $insert_data['qty'] * (($transaction_fee*$fprice) / 100);
					}

					$fees =	$newTransFee;
					$insert_data['transaction_fees']=$fees;
					$insert_data['transaction_amt']= $fee;
				}
		}else{
			if($_POST['type'] == 'cash_gift'){
					$result = $this->wishlist_model->getCashGiftTransactionFee()->row();
					$fees = $result->transaction_fees;
					if (stripos($fees, 'RP') !== FALSE){
						$feeType='RP';
					}
					if (stripos($fees, '%') !== FALSE){
						$feeType='%';
					}
					$fee = intval(preg_replace('/[^0-9]+/', '', $fees), 10);
			}
			elseif($_POST['type'] == 'buy_gift'){
				$result = $this->wishlist_model->getBuyGiftTransactionFee()->row();
				$fees = $result->transaction_fees;
				if (stripos($fees, 'RP') !== FALSE){
					$feeType='RP';
				}
				if (stripos($fees, '%') !== FALSE){
					$feeType='%';
				}
				$fee = intval(preg_replace('/[^0-9]+/', '', $fees), 10);
			}
			else{
				$fees = '';
				$fee = '';	
			}
			$insert_data['transaction_fees']=$fees;
			$insert_data['transaction_amt']= $fee;
		}
		
         // This function add items into cart.
		$cart = $this->cart->insert($insert_data);
		
        // This will show insert data in cart.
		$photopath = $this->config->item('image_display_path');
		$thumb_size= $this->config->item('thumb_size');
		$no_image = $photopath."product/cash_gift.png";
			
		if($cart){
			$login_details = $this->session->userdata('log_in');
		
			$data['uid'] = $login_details['user_id'];
			$data['session_id'] = $this->session->session_id;
			$data['product_id'] = $product_id;
			$data['product_name'] = isset($_POST['product_name']) ? $_POST['product_name'] : '';
			$data['wishlist_id'] = (is_numeric($_POST['wid'])) ? $_POST['wid'] : 0;
			$data['row_id'] = $cart;
			$data['quantity'] =  isset($_POST['quantity']) ? $_POST['quantity'] : 1;
			$data['price'] = isset($_POST['contribute_amount']) ? $_POST['contribute_amount']: (isset($product_price) ? $product_price : 0); 
			$data['product_sale_price'] = isset($product_sale_price) ? $product_sale_price : 0;
			$data['type'] = isset($_POST['type']) ? $_POST['type'] : '';
			$data['add_date'] = date('Y-m-d H:i:s');
			
			if($merchant_id > 0){
				if($_POST['type']=='buy_gift'){
					$transaction=$this->payment_model->getTransactionfeebyMerchantId($merchant_id);
					$transaction_fee=$transaction->transaction_fee;
					$transaction_fee_type = $transaction->transaction_fee_type;
					if ($transaction_fee_type == 'RP'){
						$transFeeNum=$transaction_fee;
						$newTransFee='RP '.$transFeeNum;
						$fee = ($transaction_fee*$data['quantity']);
					}
					if ($transaction_fee_type == '%'){
						$transFeeNum=$transaction_fee;
						$newTransFee=$transFeeNum." %";
						$fee = $data['quantity'] * (($transaction_fee*$data['price']) / 100);
					}

					$fees =	$newTransFee;
					
					$data['transaction_fees'] = $fees;
					$data['transaction_amt'] = $fee;
					
				}

			}else{
				if($_POST['type'] == 'cash_gift'){
					$result = $this->wishlist_model->getCashGiftTransactionFee()->row();
					$fees = $result->transaction_fees;
					if (stripos($fees, 'RP') !== FALSE){
						$feeType='RP';
					}
					if (stripos($fees, '%') !== FALSE){
						$feeType='%';
					}
					$fee = intval(preg_replace('/[^0-9]+/', '', $fees), 10);
				}elseif($_POST['type'] == 'buy_gift'){
					$result = $this->wishlist_model->getBuyGiftTransactionFee()->row();
					$fees = $result->transaction_fees;
					if (stripos($fees, 'RP') !== FALSE){
						$feeType='RP';
					}
					if (stripos($fees, '%') !== FALSE){
						$feeType='%';
					}
					$fee = intval(preg_replace('/[^0-9]+/', '', $fees), 10);
				}else{
					$fees = '';
					$fee = '';	
				}
				$data['transaction_fees'] = $fees;
				$data['transaction_amt'] = $fee;
			}

			//print_r($data);
			$cart_id = $this->cart_model->add($data);
			$photopath = $this->config->item('image_display_path');
			$thumb_size= $this->config->item('thumb_size');
			$no_image = $photopath."product/cash_gift.png";
			$no_product_image = $photopath."product/no_product.jpg";
			$response['cart_id'] = isset($cart_id) ? $cart_id : 0;
			$response['row_id'] = $cart;
			$response['item'] = $this->cart->get_item($cart);
			$response['count'] = count($this->cart->contents());
			if(isset($response['item']) && !empty($response['item'])){
				$item = $response['item'];
				$product_id = $item['id'];
				$product_name = $item['name'];
				$pslug = $this->product_model->getProductUrl_name($product_id,$product_name);
				$fetch_image = $this->product_model->getProductImageById($product_id);
				if($fetch_image!=''){
					$image = $photopath."product/".$product_id.$thumb_size.$fetch_image;
				}else{
					$image = $no_product_image;
				}
				$item['image'] = $image;

				$item['no_image'] = $no_image;
					if($item['wishlist_id']!='' && is_numeric($item['wishlist_id'])){ 
						$wishlist_det = $this->wishlist_model->getwishlistByid($item['wishlist_id']);
						if(isset($wishlist_det)){
							$item['wishlist_name'] = $wishlist_det->title;
							$item['wishlist_url'] = '~'.$wishlist_det->url;	
						}
					}
				$item['product_url'] = $pslug;
				$item['price'] = number_format($item['price']);
				$item['transaction_fees'] = (isset($item['type']) && $item['type']=='cash_gift') ? $fees : 0;
				$item['transaction_amt'] = (isset($item['type']) && $item['type']=='cash_gift') ? $fee : 0;
				$response['item'] = $item;
			}
		}
		echo json_encode($response);
	}
	
	public function view()
	{	
		$contents = $this->cart->contents();
		//print_r($contents);
		$photopath = $this->config->item('image_display_path');
		$thumb_size= $this->config->item('thumb_size');
		$no_image = $photopath."product/cash_gift.png";
		$no_product_image = $photopath."product/no_product.jpg";
		foreach($contents as $content){
			$product_id = $content['id'];
			$product_name = $content['name'];
			$product_slug = $this->product_model->getProductUrl_name($product_id,$product_name);
			$fetch_image = $this->product_model->getProductImageById($product_id);
			if($fetch_image!=''){
				$image = $photopath."product/".$product_id.$thumb_size.$fetch_image;
			}else{
				$image = $no_product_image;
			}
			$content['product_image'] = $fetch_image;
			$content['image'] = $image;
			$content['no_image'] = $no_image;
			$content['no_product_image'] = $no_product_image;
			if($content['wishlist_id']!='' && is_numeric($content['wishlist_id'])){ 
				$wishlist_det = $this->wishlist_model->getwishlistByid($content['wishlist_id']);
				if(isset($wishlist_det)){
					$content['wishlist_name'] = $wishlist_det->title;
					$content['wishlist_url'] = '~'.$wishlist_det->url;	
				}
			}
			$content['product_url'] = $product_slug;
			$cart_products[] = $content;
		}
		$discount = $this->session->userdata('discount');
		$coupon_code = $this->session->userdata('coupon_code');
		$cart_total = $this->cart->total();
		$discnt = str_replace(',', '', $discount);
		
		if(isset($discount) && $discount!=0){
			$gross = $cart_total - $discnt;
			$gross = isset($gross) ? $gross : '';
			$this->session->set_userdata('gross_amount',$gross);
			$gross_format = $this->config->item('currency').' '.number_format($gross);
		}else{
			$gross_format = $this->config->item('currency').' '.number_format($cart_total);
		}
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("cart");
		$data['total'] = $gross_format;
		$data['total_without_discount'] = $this->cart->total();
		$data['products'] = isset($cart_products) ? $cart_products : '';
		$data['discount'] = isset($discount) ? $discount : '';
		$data['coupon_code'] = isset($coupon_code) ? $coupon_code : '';
		//print_r($data);
        $this->load->ftemplate('cart/basket' , $data);
	}
	
	public function removeItem($rowid)
	{
        $data = array(
			'rowid'   => $rowid,
			'qty'     => 0
		);
		// Update cart data, after cancel.
		$this->cart->update($data);
		
		if (!$this->input->is_ajax_request()) {
		   $count = count($this->cart->contents());	
		   if($count == 0){
			   $this->session->unset_userdata('discount');
			   $this->session->unset_userdata('gross_amount');
			   $this->session->unset_userdata('coupon_code'); 
		   }
		   redirect($_SERVER['HTTP_REFERER']);
		}else{
			$json = count($this->cart->contents());
			echo json_encode($json);	
		}
		
	}
	public function destroy()
	{
	   $this->session->unset_userdata('discount');
	   $this->session->unset_userdata('gross_amount');
	   $this->session->unset_userdata('coupon_code');		
	   $json = 'No items available!';
	   echo json_encode($json);	
	}
	public function update(){
        // Recieve post values,calcute them and update
		$flashmsg = array();
        $cart_info =  $_POST['cart'] ;
 		foreach( $cart_info as $id => $cart)
		{
			$pid = $cart['id'];	
			$pname = $cart['name'];	
			$rowid = $cart['rowid'];
			$price = $cart['price'];
			$amount = $price * $cart['qty'];
			$qty = $cart['qty'];
			$type = $cart['type'];
			
			if($type == 'buy_gift'){
				/*Quantity Check of Product*/
				$pdata = $this->product_model->getProductDetails($pid,$this->langId);
				if(!empty($pdata)) { 
					$stock = $pdata[0]->stock_quantity; 
				}
				if($stock < $qty)
				{
					$flashmsg[$pid] = "<strong>".$pname."</strong> is not available in the desired quantity or not in stock!";
				}
				else
				{
					$data = array(
					'rowid'   => $rowid,
					'price'   => $price,
					'amount' =>  $amount,
					'qty'     => $qty
					);
					 
					$this->cart->update($data);
				}
			}
			
		}
		$this->session->set_flashdata("quantity",$flashmsg);
		redirect('cart/view');        
	}
	
	public function info(){
		$total = 0;
		$items = $this->cart->contents();
		$photopath = $this->config->item('image_display_path');
		$thumb_size= $this->config->item('thumb_size');
		$no_image = $photopath."product/cash_gift.png";
		$no_product_image = $photopath."product/no_product.jpg";
		foreach($items as $content){
			$product_id = $content['id'];
			$product_name = $content['name'];
			$product_slug = $this->product_model->getProductUrl_name($product_id,$product_name);
			$fetch_image = $this->product_model->getProductImageById($product_id);
			if($fetch_image!=''){
				$image = $photopath."product/".$product_id.$thumb_size.$fetch_image;
			}else{
				$image = $no_product_image;
			}
			$content['product_image'] = $fetch_image;
			$content['image'] = $image;
			$content['no_image'] = $no_image;
			$content['no_product_image'] = $no_product_image;
			if($content['wishlist_id']!='' && is_numeric($content['wishlist_id'])){ 
				$wishlist_det = $this->wishlist_model->getwishlistByid($content['wishlist_id']);
				if(isset($wishlist_det)){
					$content['wishlist_name'] = $wishlist_det->title;
					$content['wishlist_url'] = '~'.$wishlist_det->url;	
				}
			}
			$content['product_url'] = $product_slug;
			$content['price'] = number_format($content['price']);
			$subtotal = $content['subtotal'];
			$total = $total + $subtotal;
			$content['total'] = number_format($total);
			$cart_products[] = $content;
		}
		echo json_encode($cart_products);	
	}
	
	public function coupon(){
		$coupon_code = $_POST['coupon'];
		$total_product_price = $_POST['total_product_price'];
		$discount_amount = $this->cart_model->getDiscountByCoupon($coupon_code,$total_product_price);
		if($discount_amount != 'Coupon Expired'){
			$disc_amount = str_replace(',','',$discount_amount);
			$this->session->set_userdata('coupon_code',$coupon_code);
			$this->session->set_userdata('discount',$discount_amount);
			$amount = $this->config->item('currency').' '.$discount_amount;
			$gross_amount = $total_product_price - $disc_amount;
		}else{
			$amount = '';	
			$gross_amount = $total_product_price;
		}
			
		$gross_amount = $this->config->item('currency').' '.number_format($gross_amount);
		$json = array('gross_amount' => $gross_amount, 'discount' => $amount);
		echo json_encode($json);
	}
}