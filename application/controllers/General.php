<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class General extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct(){
		parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];;}
		$this->lang->load('home',$lang);
		//$this->load->model(array('Welcome_model','Product_model'));
	}

	public function index(){
		$this->page('index');
	}

	public function page($slug){

		$lanuagearray=$this->session->userdata('site_lang');
		if(!empty($lanuagearray)){
			$language_id=$lanuagearray['laguage_id'];
			$language_name=$lanuagearray['laguage_name'];
		}else{
			$language_id=1;
			$language_name = "";
		}

		$content = $this->get_content($slug, $language_id);
		$this->load->revamptemplate($content, $language_id, $language_name);
	}

	private function get_content($slug, $lang){
		switch ($slug){
			case 'index' : $content = $this->home($lang);break;
		}

		return $content;
	}


	private function home($language_id){
		$data['page_title']=$this->lang->line("home_tag");
		$this->load->model(array('Welcome_model', 'Search_model', 'Product_model'));
		$data['eventcategory'] = $this->Welcome_model->getFeaturedeventcategory($language_id);
		$data['categories'] = $this->Search_model->category_list_status();
		$data['blogdata'] = $this->Welcome_model->getAllblog($language_id);
		$products = $this->Welcome_model->getAllfeaturedproduct($language_id);
		$data['fproducts'] = $this->Welcome_model->getResultProducts($products);

		$data['add_to_wishlist_action'] = 'login';
		if($this->session->userdata('log_in')){
			$login =$this->session->userdata('log_in');
			$user_id = $login['user_id'];
			$data['mywishlist'] = $this->Product_model->getUserWishlist($user_id,$language_id);
			$data['add_to_wishlist_action'] = 'addtowishlist';
		}

		$content = $this->load->view('revamp/page/home', $data, true);
		return $content;
	}

	public function get_user_id(){
		$user_id = '';
		if($this->session->userdata('log_in')){
			$login =$this->session->userdata('log_in');
			$user_id = $login['user_id'];
		}

		echo $user_id;die();
	}

	public function faq()
	{ 
		$this->load->ftemplate('site/faq');
	}
	public function shop()
	{
		$lanuagearray=$this->session->userdata('site_lang');
			if(!empty($lanuagearray)){
				$language_id=$lanuagearray['laguage_id'];
					$language_name=$lanuagearray['laguage_name'];
				}else{
			$language_id=1;
		}

		$data['page_title']=$this->lang->line("shop");
        $this->load->library('facebook'); // Automatically picks appId and secret from config
		$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/login'), 
                'scope' => array("email") // permissions here
            ));
		$data['eventcategory'] = $this->Welcome_model->getAlleventcategory($language_id);
		$data['blogdata'] = $this->Welcome_model->getAllblog($language_id);
		$data['featuredproduct'] = $this->Welcome_model->getAllfeaturedproduct($language_id);
		$this->load->ftemplate('site/shop',$data);
	}
	public function blog_view($blogId = NULL,$blogTitle = NULL)
	{
		if($blogTitle != NULL)
		{
			$result = $this->Welcome_model->getBlogById($blogId,$this->langId);
			if(!empty($result))
			{
				$data["page_title"] = $result[0]->blog_title;
				$data["blogData"] = $result;
				$this->load->ftemplate("site/blog_view",$data);
			}
			else
			{
				$this->load->view("errors/ipengen_error/505");
			}
		}
		else
		{$this->load->view("errors/ipengen_error/404");}
	}
	
	public function seturl()
	{
		if($this->input->is_ajax_request())
		{
			$category = $this->input->post("catName");
			$url = base_url()."create-a-wishlist?cat=".$category;
			$this->session->set_userdata("createCatUrl",$url);
			if($this->session->userdata("createCatUrl"))
			{ echo 1; }
			else
			{ echo 0; }
			//print_r($category);
		}
	}
}
?>