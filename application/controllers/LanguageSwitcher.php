<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct(); 
		$this->load->model('language_model');    
    }
 
    function switchLang($language = "") {
        
        $language = ($language != "") ? $language : "english";
		$result = $this->language_model->languageswitch($language);
		$sessiondataOne = array(
								'laguage_id' =>$result->language_id,
								'laguage_name' =>$result->name,
								'code' =>$result->code,
								);
        $this->session->set_userdata('site_lang', $sessiondataOne);
        
        redirect($_SERVER['HTTP_REFERER']);
        
    }
}

?>