<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Surprise extends CI_Controller {
	private $langId;
	private $lang_code;
	function __construct()
    {
        parent::__construct();
		$sess_data = $this->session->userdata("site_lang");
		if(empty($sess_data))
		{$lang = "english"; $this->langId = 1; $this->lang_code = "en";}
		else
		{$lang = $sess_data["laguage_name"]; $this->langId = $sess_data["laguage_id"]; $this->lang_code = $sess_data["code"];}
		$this->lang->load('home',$lang);
		$this->load->helper(array('form', 'url'));
		$this->load->helper('cookie');
		$this->load->library('session'); 
		$this->load->library('email');
		$this->load->library('form_validation');
		$this->load->model(array('surprise_model','wishlist_model'));
		$this->load->helper(array('common_helper','ipengen_email_helper'));
		 if(empty($this->session->userdata['log_in']['user_id'])){
			redirect('/');
		 }
     }

	public function index(){
		$uid = $this->session->userdata['log_in']['user_id'];
		$surpriseList = $this->surprise_model->getSurprise($uid); 
		if(!empty($surpriseList)){
			foreach($surpriseList as $list){
				$list->user_url = site_url('u/'.$list->user_id);
				$lists[] = $list;
			}
		}
		$data['surpriselist'] = isset($lists) ? $lists : '';
		$data['lang_code'] = $this->lang_code;
		$data['page_title'] = $this->lang->line("my_surprise_box");
		$this->load->ftemplate('surprise/index',$data);
	}
	public function deatils($surpriseID = NULL){
		 $uid = $this->session->userdata['log_in']['user_id'];
		 $data['lang_code'] = $this->lang_code;
		 $data['page_title'] = $this->lang->line("my_surprise_details");
		 $data['surprisedetails']= $surprisedetails = $this->surprise_model->getSurpriseDetails($surpriseID,$uid); 
		 if(!empty($surprisedetails)){
		if ($surprisedetails[0]->u_id == $uid )
		{
		$this->load->ftemplate('surprise/deatils',$data);
		}else{ redirect ('surprise'); } 
		}else{ redirect ('surprise'); } 
		
	}
	public function email_notification(){
		$uid = $this->session->userdata['log_in']['user_id'];
		$receiverID = $this->input->post("receiverID");
		$senderuserData = $this->wishlist_model->getUserdataUsingId($uid);
		$receiveruserData = $this->wishlist_model->getUserdataUsingId($receiverID);
		if(!empty($senderuserData) && !empty($receiveruserData))
			{
				$data["receiveruserData"] = $receiveruserData;
				$data["senderuserData"] = $senderuserData;
				$subject = "Thanks for the gift | Notification";
				$fromEmail = $senderuserData[0]->email;
				$userName = ucwords($senderuserData[0]->fname." ".$senderuserData[0]->lname);
				$email = $receiveruserData[0]->email;
				$emailBody = $this->load->view("e_template/wishlist-say-thanks-you",$data, true);
				$emailData = array(
							"title" 	=>  $userName,
							"from"		=>	$fromEmail,
							"to"		=>	$email,
							"subject"	=>	$subject,
							"message"	=>	$emailBody
									);
				$result = send_email($emailData);
				if($result)
				{echo 1;}else{echo 0;}
			}	
	}
}