<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";?>
<?php class Extend_Loader extends MX_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('templates/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);
        return $content;
    else:
        $this->view('templates/header', $vars);
        $this->view($template_name, $vars);
        $this->view('templates/footer', $vars);
    endif;
    }
 public function ftemplate($template_name, $vars = array(), $return = FALSE)
    {
		$facebook = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('user/login'),'scope' => array("email") ));// permissions here
		$vars["login_url"] = $facebook;
        if($return):
			$content  = $this->view('common/header', $vars, $return);
			$content .= $this->view($template_name, $vars, $return);
			$content .= $this->view('common/footer', $vars, $return);
			return $content;
		else:
			$this->view('common/header', $vars);
			$this->view($template_name, $vars);
			$this->view('common/footer', $vars);
		endif;
    }

    public function revamptemplate($content, $language_id, $language_name,$return = FALSE){
        $data['content'] = $content;
        $this->load->model(array('Welcome_model', 'Search_model', 'Routes_model', 'Product_model'));

        //GET SHOPPING CART
        $this->load->library('cart');
        $cart = $this->cart->contents();
        $counter_cart = 0;
        $products = array();
        foreach($cart as $item){
            $product =  $this->Product_model->getProductDetails($item['id'],$language_id);
            $products[] = (object) array_merge($item, (array)$product[0]);
            $counter_cart++;
        }
        //print_r($products);
        $data['cart'] = $this->Welcome_model->getResultProducts($products);
        $data['cart_item'] = $counter_cart;

        // GET EVENTS AND CATEGORY
        $data['events'] =  $this->Welcome_model->getAlleventcategory($language_id);
        $data['categories'] = $this->Search_model->category_list_status();

        // GET LINKS AND ACTION
        $data['sublinks'] = $this->Routes_model->get_header_sub_links();
        $data['footerlinks'] = $this->Routes_model->get_footer_links();

        // GET TERMS AND PRIVACY CONTENT
        $data['terms'] = $this->Welcome_model->getTermsCondition($language_id);
        $data['privacy'] = $this->Welcome_model->getPrivacyPolicy($language_id);

        // SET LANGUANGE
        $data['language_id'] = $language_id;
        $data['language_name'] = $language_name;

        $this->load->library('facebook'); // Automatically picks appId and secret from config
        $data['login_url'] = $this->facebook->getLoginUrl(array(
            'redirect_uri' => site_url('user/login'),
            'scope' => array("email") // permissions here
        ));

        if($return){
            $html = $this->view('revamp/base_layout', $data, $return);
            return $html;
        }else{
            $this->view('revamp/base_layout', $data);
        }
    }

    public function mtemplate($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('mtemplates/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('mtemplates/footer', $vars, $return);
        return $content;
    else:
        $this->view('mtemplates/header', $vars);
        $this->view($template_name, $vars);
        $this->view('mtemplates/footer', $vars);
    endif;
    }
}