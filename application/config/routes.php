<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/

$route['default_controller'] = 'general';

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;



/**********Fronend Panel Route***************/ 
$rout['index'] = 'general/index';

$route['shop'] = 'welcome/shop';

$route['blog/~(:num)-(:any)'] = 'welcome/blog_view/$1/$2';

$route['affiliate'] = 'site/affiliate';

$route['about-us'] = 'site/about-us';

$route['careers'] = 'site/careers';

$route['contact-us'] = 'site/contact-us';

$route['privacy-security'] = 'site/privacy-security';

$route['term-condition'] = 'site/term-condition';

$route['help'] = 'site/help';

$route['track-order'] = 'site/track-order';

$route['news-subscription'] = 'site/news-subscription';

$route['search'] = 'search';

$route['dashboard'] = 'user/dashboard';

$route['reminder'] = 'eventreminder';

$route['my-wishlist'] = 'wishlist/my_wishlist';

$route['my-cash-gift-list'] = 'wallet/my_cash_gift_list';

$route['recipient'] = 'wishlist/recipient';

$route['my-wishlist'] = 'wishlist/my_wishlist';

$route['create-a-wishlist'] = 'wishlist/create';

$route['my-wishlist/page/(:num)'] = 'wishlist/my_wishlist/page/$l';

$route['my-wishlist/page'] = 'wishlist/my_wishlist';

$route['search/(:any)'] = 'search/index';

$route['p/(:num)-(:any)'] = 'product/details/$1';

$route['quantitycheck'] = 'product/quantitycheck';

$route['~(:any)'] = 'wishlist/wishlist/$1';

$route['search-list'] = 'wishlist/searchlist';

$route['bank-account'] = 'gift/bank_account';

$route['vtweb'] = 'Vtweb';

$route['u'] = 'user/user_profile';

$route['u/(:any)'] = 'user/user_profile/$1';

$route['transaction/page/(:num)'] = 'transaction/index/page/$i';

$route['transaction/page'] = 'transaction/index';

$route['event-notification'] = 'site/event_notification';

$route['seturl'] = 'welcome/seturl';

$route['create-a-merchant'] = 'merchantinterface/index';

/*********Admin Panel Route************/

$route['admin'] = 'admin/index';

$route['admin/login'] = 'admin/index/login';

$route['admin/dashboard'] = 'admin/index/dashboard';

$route['admin/logout'] = 'admin/index/logout';

$route['admin/forget_password'] = 'admin/index/forget_password';

$route['admin/resetpassword/(:any)'] = 'admin/index/resetpassword/$1';

$route['admin/add'] = 'admin/multiadmin/add';

$route['admin/list'] = 'admin/multiadmin/adminlist';

$route['admin/list'] = 'admin/multiadmin/adminlist';

$route['admin/edit/(:num)'] = 'admin/multiadmin/edit/$1';

$route['admin/delete/(:num)'] = 'admin/multiadmin/delete/$1';

$route['admin/enablestatus/(:num)'] = 'admin/multiadmin/enablestatus/$1';

$route['admin/disablestatus/(:num)'] = 'admin/multiadmin/disablestatus/$1';

$route['admin/category/list'] = 'admin/category/categorylist';

$route['admin/category/edit/(:num)'] = 'admin/category/edit/$1';

$route['admin/category/delete/(:num)'] = 'admin/category/delete/$1';

$route['admin/category/enablestatus/(:num)'] = 'admin/category/enablestatus/$1';

$route['admin/category/disablestatus/(:num)'] = 'admin/category/disablestatus/$1';

$route['admin/product/list'] = 'admin/product/productlist';

$route['admin/banner/list'] = 'admin/banner/bannerlist';

$route['admin/block/list'] = 'admin/block/list';

$route['admin/user/list'] = 'admin/user/userlist';

$route['admin/event/list'] = 'admin/event/eventlist';

$route['admin/coupon/add'] = 'admin/coupon/addcoupon';

$route['admin/coupon/list'] = 'admin/coupon/listcoupon';

$route['admin/coupon/edit/(:num)'] = 'admin/coupon/editcoupon/$1';

$route['admin/coupon/delete/(:num)'] = 'admin/coupon/deletecoupon/$1';

$route["admin/contribute/list"] = "admin/contribute/contributelist";

$route["admin/city/add"] = "admin/city/addcity";

$route["admin/city/edit/(:num)"] = "admin/city/editcity/$1";

$route["admin/city/list"] = "admin/city/citylist";

$route["admin/city/delete/(:num)"] = "admin/city/deletecity/$1";

$route["admin/city/status/(:num)/(:num)"] = "admin/city/adminstatus/$1/$2";


$route["admin/setting/transaction-fee"] = "admin/setting/transaction_fee";

$route["admin/setting/add"] = "admin/setting/transaction_fee";

$route["admin/setting/delete/(:num)"] = "admin/setting/transaction_delete/$1";

$route["admin/setting/configuration"] = "admin/setting/admin_configuration";

$route["admin/setting/config-add"] = "admin/setting/admin_configuration";

$route["admin/setting/config-email-list"] = "admin/setting/config_list";

$route["admin/setting/config-email-edit/(:num)"] = "admin/setting/config_edit/$1";

$route["admin/setting/config-email-delete/(:num)"] = "admin/setting/config_delete/$1";

$route["admin/setting/shipping-charge"] = "admin/setting/shipping_charge";

$route["admin/setting/shipping-charge-add"] = "admin/setting/shipping_charge";

$route["admin/setting/shipping-delete/(:num)"] = "admin/setting/sheeping_delete/$1";

$route["admin/order/list"] = "admin/order/order_list";

$route["admin/adminrevenue/list"] = "admin/adminrevenue/revenue_list";

$route["admin/blog/list"] = "admin/blog/bloglist";

$route['admin/merchant/list'] = 'admin/merchant/merchantlist';

$route["admin/order/itemdetails/(:any)/(:any)"] = "admin/order/itemdetails/$1/$2";



/*********Merchant Panel Route************/
$route['(:any)/(:any)/pr'] = 'psearch/index?rn=$1&pid=$2&cat=$3&brand=$4&limit=$5';
$route['(:any)/pr'] = 'psearch/index?rn=$1&pid=$2&cat=$3&brand=$4&limit=$5';
$route['merchant'] = 'merchant/index';

$route['merchant/login'] = 'merchant/index/login';

$route['merchant/dashboard'] = 'merchant/index/dashboard';

$route['merchant/profile'] = 'merchant/index/profile';

$route['merchant/logout'] = 'merchant/index/logout';

$route['merchant/forget_password'] = 'merchant/index/forget_password';

$route['merchant/product-list'] = 'merchant/product/productlist';

$route['admin/notificationlist'] = 'admin/index/notifiction'; 

$route['merchant/notifications'] = 'merchant/index/notifiction'; 

$route['myorder'] = 'transaction';

$route['myorder/downloadpdf/(:num)'] = 'transaction/downloadpdf/$1'; 

$route['myorder/(:num)'] = 'transaction/deatils/$1'; 