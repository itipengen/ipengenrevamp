<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkout_model extends CI_Model
{
    function __construct()
	{   
		parent::__construct();
		$this->load->database();
		$this->load->model('product_model');
		$this->load->model('wishlist_model');
	}
    public function get_cart_list($uid){
		$query=$this->db->get_where('ig_cart',array('uid'=>$uid));
		if($query->num_rows() > 0){ 
			foreach($query->result() as $row){
				$product_id = $row->product_id;
				$image = $this->product_model->getProductImageById($product_id);
				$row->product_url = $this->product_model->getProductUrl_name($product_id,$row->product_name);
				$row->image = $image;
				if($row->wishlist_id!='' && is_numeric($row->wishlist_id)){ 
						$wishlist_det = $this->wishlist_model->getwishlistByid($row->wishlist_id);
						if(isset($wishlist_det)){
							$row->wishlist_name = $wishlist_det->title;
							$row->wishlist_url =   '~'.$wishlist_det->url;	
						}
					}
				$data[] = $row;
			}
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function getAddressByWid($wid){
		$query = $this->db->get_where('ig_wishlist_shipping',array('wid' => $wid));
		if($query->num_rows() > 0){
			foreach($query->result() as $row){
				$address = 	strtoupper($row->name.','. $row->street_address.','.$row->country.','.$row->state.','.$row->city.','.
				$row->land_mark.','.$row->zip);	
				
			}
			
			return $address;
		}
		
	}
	
	
	public function getShippingRate(){
		$query = $this->db->get_where('ig_shipping',array('admin_status' => 1))->row();
		return $query;
	}

	public function getSavedAddressByUid($uid){
		
	}
	
	
	/*public function to to add or update billing details in table ig_billing*/
	
	public function insertorupdatebilling($billingdata){
		if(!empty($billingdata)){
			$this->db->select('*');
			$this->db->from('ig_billing');
			$this->db->where('user_id',$billingdata['user_id']);
			$query = $this->db->get();
			if ( $query->num_rows()> 0 ){
				$this->db->where('user_id',$billingdata['user_id']);
				$this->db->update('ig_billing',$billingdata['billing']);
            }else{
				$billingdata['billing']['user_id']=$billingdata['user_id'];
				$this->db->insert('ig_billing',$billingdata['billing']);
			}
		}
	}
	
	/*public function to get address details by id*/
	public function get_addressby_id($address_id){
		if(!empty($address_id)){
			$this->db->select('*'); 
			$this->db->from('ig_user_shippingaddress');
			$this->db->where('addressbook_id',$address_id);
			$query = $this->db->get();
			if($query->num_rows()> 0 ){
				$result=$query->result_array();
				return $result;
			}else{
				return FALSE;
			} 
			
	    }
	}

}

?>