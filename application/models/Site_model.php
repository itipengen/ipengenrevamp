<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Site_model extends CI_Model
{
	function check_news_subscription($email)
	{
		$query = $this->db->get_where("ig_subscription",array("subs_email" => $email));
		return $query->num_rows() > 0 ? true : false;
	}
	
	function allWishlistData()
	{
		$query = $this->db->select("* , sum(ig_contribution.total_amt_received) as contribute_amount")
							->from("ig_wishlist")
							->join("ig_contribution" , "ig_contribution.wishlist_id = ig_wishlist.id")
							->group_by(array("ig_contribution.wishlist_id", "ig_contribution.product_id" ))
							->order_by("ig_wishlist.id","desc")
							->where(array("ig_contribution.cron_status" => "0"))
							->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	function addWallet($walletData, $wishlist_owner_id)
	{
		$query = $this->db->get_where("ig_wallet",array("receipent_uid" => $wishlist_owner_id));
		$result = $query->result();
		if($query->num_rows() > 0)
		{
			$balance = $result[0]->wallet_ammount;
			$newBalance = $balance + $walletData["wallet_ammount"];
			$this->db->update("ig_wallet",array("wallet_ammount" => $newBalance),array("receipent_uid" => $wishlist_owner_id));
			return $this->db->affected_rows();
		}
		else
		{
			$this->db->insert("ig_wallet",$walletData);
			return $this->db->affected_rows();
		}
	}
	function addWalletTransaction($walletDataTransaction)
	{
		$this->db->insert("ig_wallet_transaction",$walletDataTransaction);
		return $this->db->affected_rows();
	}
	function updateContribute($product_id, $wishlistId)
	{
		$this->db->update("ig_contribution",array("cron_status" => "1"),array("wishlist_id" => $wishlistId,"product_id" => $product_id));
			return $this->db->affected_rows();
	}
}
?>