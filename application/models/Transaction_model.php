<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

    function __construct(){
		parent::__construct();
		$this->load->database();
    }
	public function getTransaction($id,$limit,$offset){
       
		$this->db->select('*');
		$this->db->from('ig_order');
		$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');
		$this->db->where('ig_order.user_id',$id);
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	public function getTransactiondetails($id){
       
		$this->db->select('*');
		$this->db->from('ig_order');
		$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');
		$this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id','left');
		$this->db->where('ig_order.order_id',$id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	
	public function getOrderID($id,$limit,$offset){
       
		$this->db->select('*');
		$this->db->from('ig_order');
		$this->db->where('user_id',$id);
		$this->db->limit($limit,$offset);
		$this->db->order_by("order_id","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	
	public function getOrderDetailshistory($orderid,$pId){
			$this->db->where(array('order_id'=>$orderid));
			$this->db->where(array('order_product_id'=>$pId));
			$this->db->order_by("id","desc");
		    $query=	$this->db->get('ig_order_status_history');
			return $query->result();
		}
	
	public function row_count($uid)
	{
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->join('ig_order', 'ig_order.order_id = ig_transaction.order_id');
		$this->db->where('ig_order.user_id',$uid);
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function getOrderDetailsPdf($oid,$uid){
	
			$lanuagearray=$this->session->userdata('site_lang');
				if(!empty($lanuagearray)){
					$language_id=$lanuagearray['laguage_id'];
						$language_name=$lanuagearray['laguage_name'];
					}else{
						$language_id=1;
			}
	
			$this->db->select('ig_order.*,ig_order_items.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_order_items.user_id',$uid);
			$query = $this->db->get();
			$result = $query->result();
			return $result;
			
			
		}
		
	public function getOrderDetails($oid,$uid,$pid){
	
			$lanuagearray=$this->session->userdata('site_lang');
				if(!empty($lanuagearray)){
					$language_id=$lanuagearray['laguage_id'];
						$language_name=$lanuagearray['laguage_name'];
					}else{
						$language_id=1;
			}
	
			$this->db->select('ig_order.*,ig_order_items.*,ig_user.*');
			$this->db->from('ig_order');
			$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');
			$this->db->join('ig_user', 'ig_user.id = ig_order.user_id');
			$this->db->where('ig_order.order_id',$oid);
			$this->db->where('ig_order_items.user_id',$uid);
			$this->db->where('ig_order_items.product_id',$pid);
			$query = $this->db->get();
			$result = $query->result();
			return $result;
			
			
		}
	
	public function getOrder($oid,$uid){
		
			$query = $this->db->get_where('ig_order',array('order_id'=>$oid,'user_id'=>$uid))->row();
			return $query;
			
		}
		
		function orderPaymentsDetails($orderId)
	{  
		$this->db->select('*');
		$this->db->from('ig_transaction');
		$this->db->where('ig_transaction.order_id',$orderId);
		$query=$this->db->get();
		//echo $this->db->last_query();die();
		if($query->num_rows() > 0){
			$res=$query->result();	
		}
		else{
			$res=array();	
		}
		return $res;
	}
	
	/*public function to get company address details from table 'ipengen_address'*/
	public function getcompany_address(){
		$this->db->select('*');
		$this->db->from('ipengen_address');
		$this->db->where('address_id',1);
		$query= $this->db->get();
		$result= $query->result();
		if($query->num_rows() > 0){
		  return $result;		
		}		
	}
		
		

}