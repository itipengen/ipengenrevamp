<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

  class Addressbook_model extends CI_Model{
    function __construct()
	{   
		parent::__construct();
		$this->load->database();
		
	}
	
	
	/* public function vto add and update address ig_user_shippingaddress  */
	 
    public function addupdate_addressbook($data){
		$count='';
		if($data['addressbook_id']){
			$this->db->where('addressbook_id',$data['addressbook_id']);
			$this->db->update('ig_user_shippingaddress',$data['addressdata']);
		}else{
		    $this->db->insert('ig_user_shippingaddress',$data['addressdata']);
		}
		$count=$this->db->affected_rows();
        if($count>0){
		  return TRUE;
	    }else{
		  return FALSE;
		}
        			
   }
   
   /* public function to get address details from by user id*/
   
   public function get_addressbook($user_id=''){
	   if(!empty($user_id)){
		$result=array();   
		$this->db->select('*');	   
		$this->db->from('ig_user_shippingaddress');
		$this->db->where('user_id', $user_id );
		$query = $this->db->get();
		$result=$query ->result();
		if ($query->num_rows() > 0 ){
			return $result;
		}

	  }
  }
  
   /* public function to delete address details from by addressbook id */
   
  public function delete_addressbook($addressbook_id){
	  if(!empty($addressbook_id)){
		  $this->db->where('addressbook_id', $addressbook_id);
          $this->db->delete('ig_user_shippingaddress');
		  $count=$this->db->affected_rows();
		  if($count>0){
			return TRUE;
		  }else{
			return FALSE;
		  } 
	  }
  }
}