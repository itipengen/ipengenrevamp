<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gift_model extends CI_Model {

    function __construct(){
							parent::__construct();
							$this->load->database();
    }
	/*Method Name: eventreminder
	  Tableaname : ig_user
	  Parameter: fname, lname, email, password
	*/
  public function get_bank_details($uid){
	 $query =$this->db->get_where('ig_bank_details',array('uid'=>$uid));
	  if($query->num_rows() > 0){
			return  $query->row();
	    }else{
			return false;
		}
  }
  public function add_bank_details($data){
	   $query= $this->db->insert('ig_bank_details',$data);
	 if($query){return $this->db->insert_id();}
	 else {return false;}
  }
  public function edit_bank_details($data,$uid){
	  $this->db->where(array('uid'=>$uid));
	   $query= $this->db->update('ig_bank_details',$data);
	 if($query){return true;}
	 else {return false;}
  }
}