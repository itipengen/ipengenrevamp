<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Language_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function languageswitch($lang){
		$query=$this->db->get_where('ig_language',array('name'=>$lang));
		return $query->row();
	}
}