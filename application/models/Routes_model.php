<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 01/02/2017
 * Time: 21:09
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Routes_model extends CI_Model{

    public function get_header_sub_links(){
        $data['user']['createwishlist'] = array(
            'label' => $this->lang->line("create_wishlist"),
            'type' => 'link',
            'url' => base_url().'create-a-wishlist'
        );

        $data['user']['dashboard'] = array(
            'label' => $this->lang->line("dashboard"),
            'type' => 'link',
            'url' => base_url().'dashboard'
        );

        $data['user']['my_wishlist'] = array(
            'label' => $this->lang->line("my_wishlist"),
            'type' => 'link',
            'url' => base_url().'my_wishlist'
        );

        $data['user']['transaction'] = array(
            'label' => $this->lang->line("transaction"),
            'type' => 'link',
            'url' => base_url().'transaction'
        );

        $data['user']['profile'] = array(
            'label' => ucwords($this->lang->line("profile")),
            'type' => 'link',
            'url' => base_url().'u/'.$this->get_user_name()
        );

        $data['logout']['logout'] = array(
            'label' => ucwords($this->lang->line("logout")),
            'type' => 'link',
            'url' => base_url().'user/logout'
        );

        $data['login']['login'] = array(
            'label' => $this->lang->line("login"),
            'type' => 'script',
            'action' => 'login',
            'url' => 'javascript:void(0)'
        );

        $data['login']['register'] = array(
            'label' => $this->lang->line("register"),
            'type' => 'script',
            'action' => 'register',
            'url' => 'javascript:void(0)'
        );

        return $data;
    }

    public function get_footer_links(){
        $data['wishlist']['login'] = array(
            'label' => $this->lang->line("login"),
            'type' => 'script',
            'action' => 'login',
            'url' => 'javascript:void(0)'
        );

        $data['wishlist']['register'] = array(
            'label' => $this->lang->line("register"),
            'type' => 'script',
            'action' => 'register',
            'url' => 'javascript:void(0)'
        );


        $data['questions']['contactus'] = array(
            'label' => $this->lang->line("contact_us"),
            'type' => 'link',
            'url' => base_url().'contact-us'
        );

        $data['questions']['terms'] = array(
            'label' => $this->lang->line("terms_conditions"),
            'type' => 'script',
            'action' => 'terms',
            'url' => 'javascript:void(0)'
        );

        $data['ourcompany']['affiliate'] = array(
            'label' => $this->lang->line("affiliate"),
            'type' => 'link',
            'url' => base_url().'create-a-merchant'
        );

        $data['ourcompany']['contactus'] = array(
            'label' => $this->lang->line("contact_us"),
            'type' => 'link',
            'url' => base_url().'contact-us'
        );


        $data['footer']['privacy'] = array(
            'label' => $this->lang->line("privacy_security"),
            'type' => 'script',
            'action' => 'privacy',
            'url' => 'javascript:void(0)'
        );

        $data['footer']['terms'] = array(
            'label' => $this->lang->line("terms_conditions"),
            'type' => 'script',
            'action' => 'terms',
            'url' => 'javascript:void(0)'
        );

        return $data;

    }

    private function get_user_name(){
        $userName = '';
        if($this->session->userdata('log_in')) {
            $sessArr = $this->session->userdata('log_in');
            $sessUserName = isset($sessArr['user_name']) ? $sessArr['user_name'] : '';
            if (strlen($sessUserName) == 0) {
                $query = $this->db->get_where('ig_user', array('id' => $sessArr['user_id']))->row();
                $userName = $query->user_id;
            } else {
                $userName = $sessArr['user_name'];
            }
        }

        return $userName;
    }

}


?>