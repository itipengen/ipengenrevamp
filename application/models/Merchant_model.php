<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Merchant_model extends CI_Model
{
	function __construct()
	{ 
		parent::__construct();
		$this->load->database();
	}
		 
	public function addMerchant($data)
	{
		$merchantArr = array(
			'marchent_name' => $data['mname'],
			'marchent_email' => $data['memail'],
			'marchent_password' =>  md5($this->config->item('email_verification_salt').$data['mpassword']),
			'merchant_phone' => $data['mphone'],
			'activation_key' => md5(rand(0,10000))
		);
		
	    if($this->db->insert('ig_marchent_login',$merchantArr)){
	    	$mid = $this->db->insert_id();
	    	$merchant = $this->getMerchantById($mid);
	    	return $merchant;
	    }

	    return false;
	}

	public function getMerchantById($id){
		$query = $this->db->get_where("ig_marchent_login",array('marchent_id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}
		return false;
	}

	public function updateStatus($id,$key){
		$merchant = $this->getMerchantById($id);
		$activation_key = $merchant->activation_key;
		if($activation_key == $key){
			$updateArr = array('marchent_status' => 1, 'activation_key' => '' );
			if($this->db->update('ig_marchent_login',$updateArr)){
				return true;
			}
		}
		return false;
	}
}