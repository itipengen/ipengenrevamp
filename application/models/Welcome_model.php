<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome_model extends CI_Model
{
	function __construct()
		 { parent::__construct();
		   $this->load->database();
		 }

	// EDITED BY IRFAN //
	// public function getAlleventcategory($langId)
	public function getFeaturedeventcategory($langId)
	{ 		
		$this->db->select('ig_event_category_text.event_name,ig_event_category.image_name,ig_event_category.id');
		$this->db->from('ig_event_category');
		$this->db->join('ig_event_category_text', 'ig_event_category.id = ig_event_category_text.event_category_id');
		$this->db->where('ig_event_category_text.lang_id',$langId);
		$this->db->where('ig_event_category.status',1);
		$this->db->where('ig_event_category.feature_status',1);
		$this->db->order_by("ig_event_category.id","desc");
		$this->db->limit(8);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function getAlleventcategory($langId){
		$this->db->select('ig_event_category_text.event_name,ig_event_category.image_name,ig_event_category.id');
		$this->db->from('ig_event_category');
		$this->db->join('ig_event_category_text', 'ig_event_category.id = ig_event_category_text.event_category_id');
		$this->db->where('ig_event_category_text.lang_id',$langId);
		$this->db->where('ig_event_category.status',1);
		$this->db->order_by("ig_event_category.id","desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	public function getAllfeaturedproduct($langId)
	{ 
		$this->db->select('*');
		$this->db->from('ig_products');
		$this->db->join('ig_products_text', 'ig_products.product_id = ig_products_text.product_id');
		$this->db->where('ig_products.feature_status',1);
		$this->db->where('ig_products.admin_status',1);
		$this->db->where('ig_products_text.language_id',$langId);
		$this->db->order_by("ig_products.product_id","desc");
		$this->db->limit(8);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function getAllblog($langId){
		$this->db->select('*');
		$this->db->from('ig_blog');
		$this->db->join('ig_blog_text', 'ig_blog.blog_id = ig_blog_text.blog_id');
		$this->db->where('ig_blog.blog_status',1);
		$this->db->where('ig_blog_text.blog_language',$langId);
		$this->db->limit(4);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
		
	}
	public function getBlogById($blogId, $langId)
	{
		$this->db->select('*');
		$this->db->from('ig_blog');
		$this->db->join('ig_blog_text', 'ig_blog.blog_id = ig_blog_text.blog_id');
		$this->db->where('ig_blog.blog_status',1);
		$this->db->where('ig_blog_text.blog_language',$langId);
		$this->db->where('ig_blog.blog_id',$blogId);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	public function getTermsCondition($langId){
		$this->db->select('*');
		$this->db->from('ig_static_block');
		$this->db->join('ig_static_block_text','ig_static_block.block_id = ig_static_block_text.block_id','inner');
		$this->db->where(array('ig_static_block.block_id' => 11,'ig_static_block_text.lang_id' => $langId ));
		$query = $this->db->get();
		$result = $query->row();

		return $result;
	}

	public function getPrivacyPolicy($langId){
		$this->db->select('*');
		$this->db->from('ig_static_block');
		$this->db->join('ig_static_block_text','ig_static_block.block_id = ig_static_block_text.block_id','inner');
		$this->db->where(array('ig_static_block.block_id' => 12,'ig_static_block_text.lang_id' => $langId ));
		$query = $this->db->get();
		$result = $query->row();

		return $result;
	}

	public function getResultProducts($products){
		$data = array();
		foreach($products as $item){
			//$imgpurl=$this->config->item('image_display_path').'product/'.$item->product_id.$this->config->item('medium_thumb_size').$item->product_image;

			if(is_file($this->config->item('image_display_path').'product/'.$item->product_id.$this->config->item('medium_thumb_size').$item->product_image)){
				$imgpurl=$this->config->item('image_display_path').'product/'.$item->product_id.$this->config->item('medium_thumb_size').$item->product_image ;
			}  else{
				$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
			}

			$item->product_url = $this->Product_model->getProductUrl_name($item->product_id,$item->product_name);
			$item->product_image_url = $imgpurl;

			//SHORT NAME
			$max_length = 32;
			$product_name = $item->product_name;
			if(strlen($product_name) > $max_length){
				$max_length -= 3;
				$product_name = substr($product_name,0, $max_length).'...';
			}
			$item->product_name_short = $product_name;

			//VERY SHORT NAME
			$max_length = 25;
			$product_name = $item->product_name;
			if(strlen($product_name) > $max_length){
				$max_length -= 3;
				$product_name = substr($product_name,0, $max_length).'...';
			}
			$item->product_name_very_short = $product_name;


			$data[] = $item;
		}

		return $data;
	}
}