<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Payment_model extends CI_Model {

   public function __construct(){



		parent::__construct();



		$this->load->database();



		$this->load->library('cart');



		$this->load->model('wishlist_model');



   }



    



   public function addOrder($data){ 
	   $order = array();
	   $order_item = array();
	    if(!empty($data)){ 
			$user_id = $data['user_id'];
			$billing = isset($data['billing_address']) ? $data['billing_address'] : '';
			$shipping = isset($data['shipping_address']) ? $data['shipping_address'] : '';
			if(!empty($billing)){
				$order['user_id'] = $user_id;
				$order['payment_firstname'] = $billing['first_name'];	
				$order['payment_lastname'] = $billing['last_name'];
				$order['payment_address_1'] = $billing['address'];
				$order['payment_address_2'] = $billing['address2'];
				$order['payment_city'] = $billing['city'];
				$order['payment_postcode'] = $billing['postal_code'];
				$order['payment_country'] = $billing['country'];
				$order['payment_method'] = $data['payment_method'];
				$order['payment_type'] = 'vtweb';
				$order['payment_code'] = '';
				$order['shipping_firstname'] = $shipping['first_name'];	
				$order['shipping_lastname'] = $shipping['last_name'];
				$order['shipping_address'] = $shipping['address'];
				$order['shipping_city'] = $shipping['city'];
				$order['shipping_postcode'] = $shipping['postal_code'];
				$order['shipping_kecamatan'] = $shipping['kecamatan'];
				$order['shipping_contact'] = $shipping['phone'];
				$order['shipping_method'] = $data['shipping_method'];
				$order['shipping_cost'] = $data['shipping_charge'];
				$order['coupon_code'] = $data['coupon_code'];
				$order['discount_amount'] = $data['discount'];
				$order['status'] = 'pending';
				$order['total'] = $data['total_price'];
				$order['total_product_price'] = $data['total_product_price'];
				$order['language_id'] = $data['language_id'];
				$order['date_added'] = date('Y-m-d H:i:s');
				$order['date_modified'] = date('Y-m-d H:i:s');
				$order['shipping_redirect_type'] = isset($data['shipping_redirect_type']) ? $data['shipping_redirect_type'] : '';
				if(!empty($order)){
					$this->db->insert('ig_order',$order);
					$order_id = $this->db->insert_id();
					if($order_id > 0){ 
						$products = $data['products'];
						if(!empty($products)){
							foreach($products as $product){



								$pname = addslashes($product['name']);



								$wname = (isset($product['wishlist_name']) && $product['wishlist_name']!='') ? addslashes($product['wishlist_name']) : '';



								$wurl = (isset($product['wishlist_url']) && $product['wishlist_url']!='') ? site_url($product['wishlist_url']) : '';



								$plink = "<a href='{$product['product_url']}'>{$pname}</a>";



								$wlink = "<a href='{$wurl}'>{$wname}</a>";

								if($product['type'] == 'buy_gift'){



									if($product['wishlist_id']!=0){



										$description =  "Buy Gift - ".$plink." for Wishlist ".$wlink;



									}else{



										$description = $plink;



									}

								}else if($product['type'] == 'cash_gift'){



										$transaction = "(".$this->lang->line("if_transaction_fee").' '.$product['transaction_fees'].")";



										$description =  "Cash Gift - ".$wlink.$transaction;

								}else{



										$description =  "Contribution - ".$plink." from Wishlist ".$wlink;

								}

								if($product['type'] == 'cash_gift'){



									$merchant_id = 0;

								}else{



									$query = $this->db->get_where('ig_products',array('product_id'=>$product['id'])); 



									$result = $query->result();



									if (!empty($result)) {



										foreach ($result as $value) {



											$merchant_id = $value->merchant_id;



										}



									}
								}	

								$order_item['order_id'] = $order_id;
								$order_item['merchant_id'] = $merchant_id;
								$order_item['item_type'] = $product['type'];
								$order_item['wishlist_id'] = $product['wishlist_id'];
								$order_item['user_id'] = $user_id;
								$order_item['contributor_id'] = 0;
								$order_item['contribute_amt'] = ($product['type'] == 'contributed_cash_gift') ? $product['price'] : 0;
								$order_item['product_id'] = $product['id'];
								$order_item['suborder_id'] = $order_id.'-'.$product['id'];
								$order_item['name'] = $product['name'];
								$order_item['quantity'] = $product['qty'];
								$order_item['price'] = ($product['type'] != 'contributed_cash_gift') ? $product['price'] : $product['product_sale_price'];
								$order_item['item_description'] = $description;
								$order_item['cash_amt'] = ($product['type'] == 'cash_gift') ? $product['product_sale_price'] : 0;
								if($merchant_id!=0){
									if($product['type']=='buy_gift'){
										$transaction=$this->getTransactionfeebyMerchantId($merchant_id);
										$transaction_fee=$transaction->transaction_fee;
										$transaction_fee_type = $transaction->transaction_fee_type;
										if ($transaction_fee_type == 'RP'){
											$transFeeNum=$transaction_fee;
											//$newTransFee='RP '.$transFeeNum;
											$newTransFee=$transFeeNum;
											$order_item['transaction_amt'] = ($transaction_fee*$product['qty']);
										}
										if ($transaction_fee_type == '%'){
											$transFeeNum=$transaction_fee;
											//$newTransFee=$transFeeNum." %";
											$newTransFee=$transFeeNum;
											$order_item['transaction_amt'] = $product['qty'] * (($transaction_fee*$order_item['price']) / 100);
										}
										$order_item['fee_type']=$transaction_fee_type;
										$order_item['transaction_fees'] =	$newTransFee;
									}
								}
								else{
									if($product['type']=='cash_gift'){
										if (stripos($product['transaction_fees'], 'RP') !== FALSE){
											$tfeearr=explode(' ',$product['transaction_fees']);
											$transFeeNum=(int)$tfeearr[1];
											$newTransFee=$transFeeNum;
											$order_item['fee_type']='RP';
										}
										if (stripos($product['transaction_fees'], '%') !== FALSE){
											$tfeearr=explode(' ',$product['transaction_fees']);
											$transFeeNum=(int)$tfeearr[0];
											$newTransFee=$transFeeNum;
											$order_item['fee_type']='%';
										}
										$order_item['transaction_fees'] =	$newTransFee;
										$order_item['transaction_amt'] = $product['transaction_amt'];		
									}
									elseif($product['type']=='buy_gift'){
										$trans_fee=$product['transaction_fees'];
										if (stripos($trans_fee, 'RP') !== FALSE){
											$tfeearr=explode(' ',$trans_fee);
											$transFeeNum=$product['qty']*(int)$tfeearr[1];
											$newTransFee=$transFeeNum;
											$order_item['fee_type']='RP';
										}
										if (stripos($trans_fee, '%') !== FALSE){
											$tfeearr=explode(' ',$trans_fee);
											$transFeeNum=$product['qty']*(int)$tfeearr[0];
											$newTransFee=$transFeeNum;
											$order_item['fee_type']='%';
										}
										
										$order_item['transaction_fees'] =	$newTransFee;
										$order_item['transaction_amt'] = $product['transaction_amt']*$product['qty'];
									}
									else{
										$order_item['transaction_fees'] =	'';
										$order_item['transaction_amt'] = '';
									}	
								}
								$order_item['subtotal'] = ($product['type'] != 'contributed_cash_gift') ? ($product['qty'] * $order_item['price']) : $order_item['contribute_amt'];
								$order_item['total'] = $data['total_price'];
								$order_item['order_status'] = 'process';
								if(!empty($order_item)){
									$this->db->insert('ig_order_items',$order_item);
									$order_item_id = $this->db->insert_id();
								}
							}
						}
						return $order_id;
						


					}	



				}



			}



			



		}	



   }



   



	public function getTransactionfeebyMerchantId($merchant_id){



		return	$this->db->get_where('ig_marchent_login',array('marchent_id'=>$merchant_id))->row();



	}



   



   public function deleteCartForUser($user_id){



	   



	   $this->db->where('uid',$user_id);



	   if($this->db->delete('ig_cart')){



		    $this->cart->destroy();



			



			return true;   



	   }else{



			return false;   



	   }     



   }



   public function getmerchant($p_id){



	   



	    $this->db->select('*');



		$this->db->from('ig_products');



 		$this->db->where('product_id', $p_id); 



		$query = $this->db->get();



		return $query->result(); 



	 



   }



   public function transaction_cart($ndata){



	   



	   // duplicate entry avoiding



	    



	   if($this->getTransaction($ndata->order_id)== 0)



	   {



		 $data=array(



			   'transaction_id'=>$ndata->transaction_id,



			   'order_id'=>$ndata->order_id,



			   'payment_type'=>$ndata->payment_type,



			   'transaction_time'=>$ndata->transaction_time,



			   'transaction_status'=>$ndata->transaction_status,



			   //'fraud_status'=>$ndata->fraud_status,



			   'rawdata'=>json_encode($ndata),



			   'gross_amount'=>$ndata->gross_amount,



			   'status_message'=>$ndata->status_message,



			   'status_code'=>$ndata->status_code,



		);



		$query=$this->db->insert('ig_transaction',$data);



	   }



	} 



	



	 public function updateTransactionOrder($ndata){



	   $array = array();



	   



	   $data=array(



			   'transaction_id'=>$ndata->transaction_id,



			  



			   'payment_type'=>$ndata->payment_type,



			   'transaction_time'=>$ndata->transaction_time,



			   'transaction_status'=>$ndata->transaction_status,



			   //'fraud_status'=>$ndata->fraud_status,



			   'rawdata'=>json_encode($ndata),



			   'gross_amount'=>$ndata->gross_amount,



			   'status_message'=>$ndata->status_message,



			   'status_code'=>$ndata->status_code,



		);



	   $this->db->where('order_id', $ndata->order_id);



	   



	   if($this->db->update('ig_transaction', $data))



	   		return true;



	   else



			return false; 



   } 



	



   public function updateOrder($status, $order_id){
	   $array = array();
	   $statusArr = array();
	   $statusArr['p_status'] = $array['status'] = $status;
	   $this->db->where('order_id', $order_id);
	   if($this->db->update('ig_order', $array)){
	   		$this->db->where('order_id', $order_id);	
	   		if($this->db->update('ig_order_items', $statusArr)){
	   			return true;
	   		}
	   }else{
	   		return false;
	   }
   }

   public function getOrderById($order_id){



		$this->db->select('*');



		$this->db->from('ig_order');



		$this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id');



		$this->db->join('ig_user', 'ig_user.id = ig_order.user_id');



 		$this->db->where('ig_order.order_id', $order_id); 



		$query = $this->db->get();



		return $query->result();   



   }



   



   public function addContributor($data,$order_id){



		if(!empty($data)){ 



			$con = array();



			$srg = array();



			foreach($data as $d){



				$con['order_id'] = $d['order_id'];



				$con['wishlist_id'] = $d['wishlist_id'];



				$con['product_id'] = $d['product_id'];	



				$con['user_id'] = $d['user_id'];



				$con['user_email'] = $d['user_email'];



				$con['product_name'] = $d['product_name'];	



				$con['total_amount'] = $d['total_amount'];



				$con['total_amt_received'] = $d['total_amt_received'];



				$con['date_added'] = $d['date_added'];	



			



							$this->db->insert('ig_contribution',$con);



							$contributor_id = $this->db->insert_id();



						if($contributor_id!=0){




							$contributeArr = array('contributor_id' => $contributor_id);



							$srg['order_id'] = $order_id;



							$this->db->where($srg);



						    $this->db->update('ig_order_items', $contributeArr);



								



						}	


			}			
		}



   }



   



   public function addWallet($data){



	   $walletArr = array();



	   $wallet = array();

	  
mail("magic.solve@gmail.com","wallet data=",json_encode($data));


	    if(!empty($data)){

			

			foreach($data as $cdata)

			

			{

				$amt =0;
				$amount = 0;

				$walletArr['wishlist_id'] = $cdata['wishlist_id'];



				$walletArr['sender_id'] = $cdata['sender_id'];	



				$walletArr['order_id'] = $cdata['order_id'];



				$walletArr['transaction_id'] = $cdata['transaction_id'];



				$walletArr['amount'] = $cdata['cash_amt'];	

				$walletArr['fees'] = $cdata['fees'];

				$walletArr['wallet_status'] = $cdata['wallet_status'];
				$walletArr['fees'] = $cdata['cash_amt'];



				$walletArr['type'] = $cdata['type'];



				$walletArr['date_added'] = $cdata['date_added'];



				$walletArr['is_new'] = $cdata['is_new'];	



			$this->db->insert('ig_wallet_transaction',$walletArr);



			$wallet_trasaction_id = $this->db->insert_id();



			if($wallet_trasaction_id!=0){ 



				if($walletArr['wishlist_id']!=0){



					$receipientObj = $this->wishlist_model->getwishlistByid($cdata['wishlist_id']);



					$query = $this->db->get_where("ig_wallet",array('receipent_uid' => $receipientObj->uid));



					if($query->num_rows() > 0){



						foreach($query->result() as $row){



							$amt  = $row->wallet_ammount;



							$amount = $amt + $cdata['cash_amt'];



							$updArr = array('receipent_uid' => $receipientObj->uid, 'wallet_ammount' => $amount );



						}	



						$this->db->where('receipent_uid',$receipientObj->uid);



						$this->db->update('ig_wallet', $updArr);



					}else{



						$wallet['receipent_uid'] = $receipientObj->uid;



						$wallet['wallet_ammount'] = $cdata['cash_amt'];



						$this->db->insert('ig_wallet',$wallet);



						$wallet_id = $this->db->insert_id();



					}



				}



			}	

		}

		}	



   }



   



	public function getUserData($orderId = NULL)



	{



		$query =	$this->db->select("



								ig_order.order_id as order_id,



								ig_order.payment_firstname as fname,



								ig_order.payment_lastname as lname,



								ig_user.mobile as mobile,



								ig_user.email as userEmail,



								ig_order.payment_address_1 as address,



								ig_order.payment_address_2 as address_1,



								ig_order.payment_city as city,



								ig_order.payment_postcode as postCode,



										")



							 ->from("ig_order")



							 ->where(array("ig_order.order_id"=>$orderId))



							 ->join("ig_user","ig_user.id = ig_order.user_id")



							 ->get();



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	public function productData($productId, $langID)



	{



		$query =	$this->db->select("*")



							 ->from("ig_products")



							 ->where(array(



										"ig_products.product_id"=>$productId, 



										"ig_products_text.language_id"=>$langID



										  )



									)



							 ->join("ig_products_text","ig_products_text.product_id = ig_products.product_id")



							 ->get();



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	public function updateProductQuantityData($prodcut_id, $update)



	{



		$this->db->update("ig_products",$update,array("product_id" => $prodcut_id));



		return $this->db->affected_rows();



	}



	public function getProductData($wishlistId, $order_id, $productId, $langID)



	{



		$query =	$this->db->select("ig_products.*,ig_products_text.*")



							 ->from("ig_order_items")



							 ->where(array(



							 			"ig_order_items.wishlist_id" => $wishlistId,



										"ig_order_items.order_id"=>$order_id, 



										"ig_order_items.product_id"=>$productId, 



										"ig_products_text.language_id"=>$langID



										  )



									)



							 ->join("ig_products","ig_products.product_id = ig_order_items.product_id")



							 ->join("ig_products_text","ig_products_text.product_id = ig_products.product_id")



							 ->get();



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	



	public function getWishlistData($order_id,$langID)



	{



		$query =	$this->db->select("ig_wishlist.id as wishlistId ,ig_user.*")



							 ->from("ig_order_items")



							 ->where(array("ig_order_items.order_id"=>$order_id))



							 ->join("ig_wishlist","ig_wishlist.id = ig_order_items.wishlist_id")



							 ->join("ig_user","ig_user.id = ig_wishlist.uid")



							 ->get();



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	public function getAllConfigEmail()



	{



		$query = $this->db->get("ig_setting");



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	



	public function getWishlistProductData($wishlistId , $order_id)



	{



		$query =	$this->db->select("ig_order_items.*, ig_user.fname as fname  ")



							 ->from("ig_order_items")



							 ->where(array("ig_order_items.wishlist_id" => $wishlistId, "ig_order_items.order_id" => $order_id))



							 ->join("ig_user","ig_user.id = ig_order_items.user_id")



							 ->get();



		return $query->num_rows() > 0 ? $query->result() : array();



	}



	



	public function checkContributionOrderId($order_id)



	{



		$query = $this->db->get_where('ig_contribution',array('order_id' => $order_id));



		return $query->num_rows();



	}



	



	public function checkWalletOrderId($order_id)



	{



		$query = $this->db->get_where('ig_wallet_transaction',array('order_id' => $order_id));



		return $query->num_rows();



	}



	



	public function checkNotificationsOrderId($order_id)



	{



		$query = $this->db->get_where('ig_notification',array('order_id' => $order_id));



		return $query->num_rows();



	}



	public function orderDetails($oid)



	{



		$this->db->select('ig_order.*,ig_order.status as stuts,ig_order_items.*,ig_products.*,ig_user.*');



	   $this->db->from('ig_order');



	   $this->db->join('ig_order_items', 'ig_order.order_id = ig_order_items.order_id', 'left');



	   $this->db->join('ig_user', 'ig_user.id = ig_order.user_id', 'left');



	   $this->db->join('ig_products', 'ig_products.product_id = ig_order_items.product_id', 'left');



	   //$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_order_items.product_id', 'left');



	   $this->db->where('ig_order.order_id',$oid);



	   



	   



	   $query = $this->db->get();



	   $result = $query->result();



	   return $result;   



	}



   



   public function addNotifications($notification){



	   if(!empty($notification)){



		   foreach($notification as $data){ 



			    $arr = array(



					'notification_description' => '',



					'notification_type' => isset($data['notification_type']) ? $data['notification_type'] : '',



					'order_id' => $data['order_id'],



					'product_id' => $data['product_id'],



					'wishlistid' => $data['wishlistid'],



					'notification_date' => $data['notification_date'],



					'sender_uid' => $data['sender_uuid'],



					'uuid' => $data['uuid'],



					'notification_amount' => $data['notification_amount'],



					'notification_status' => 1



				);



			    $this->db->insert('ig_notification',$arr);



			    $notifyId[] = $this->db->insert_id();



			}



			if(!empty($notifyId)){



				return true;	



			}else{



				return false;	



			}



		}	



	   else{



		  	return false; 



		  }



	}



	public function order_is_yours($orderid,$uid)



	{



		$this->db->where(array('order_id'=>$orderid,'user_id'=>$uid));



		$query=$this->db->get('ig_order');



		if($query->num_rows()>0)



		{



			return true;



		}else{



			return false;



		}



	}



	



	



		



////////////////gettransaction row///////////////////////



		



		public function getTransaction($order_id)



	{



		$this->db->select('*');



		$this->db->from('ig_transaction');



		



		$this->db->where('order_id',$order_id);



		$query = $this->db->get();



		



		return $query->num_rows();



	}



		/////////////////////////////////////////





	////////////////gettransaction row///////////////////////



		



		public function getTransactionData($order_id)



	{



		$this->db->select('*');



		$this->db->from('ig_transaction');



		



		$this->db->where('order_id',$order_id);



		$query = $this->db->get();



		 $result = $query->result();



	   return $result; 



		//return $query->num_rows();



	}



		/////////////////////////////////////////

		

		////////////////Get single order row///////////////////////



		



		public function GetOrderData($order_id)



	{



		$this->db->select('*');



	   $this->db->from('ig_order');





	   //$this->db->join('ig_products_text', 'ig_products_text.product_id = ig_order_items.product_id', 'left');



	   $this->db->where('ig_order.order_id',$order_id);



	   



	   



	   $query = $this->db->get();



	   $result = $query->result();





		//return $query->num_rows();



	}



		/////////////////////////////////////////



	



}