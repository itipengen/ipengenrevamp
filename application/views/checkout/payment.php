 <div id="content">
            <div class="container white">
  <div class="row bluewhiteheader raleway"> <?= $this->lang->line("payment") ?></div>            
                   <!--Review-->

              <div class="linerows" id="basket">

                <h3><?= $this->lang->line("your_order") ?></h3>
                                 <?php if(!empty($cart_data)){
									 $total_price=0;
									 ?>                      
                             <table class="table table-condensed">
                                 <thead>
                                     <tr>
                                         <th colspan="2"><?= $this->lang->line("product") ?></th>
                                         <th><?= $this->lang->line("price") ?></th>
                                         <th><?= $this->lang->line("table_total") ?></th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                 <?php foreach($cart_data as $cart){?>
                                     <tr>
                                         <td>
                                              <?php if($cart['type'] != 'cash_gift'){ ?>
                                                        <a href="<?php echo $cart['product_url']; ?>"><img src="<?php echo $cart['image_path'] ?>" alt="<?php //echo $cart['name'] ?>"></a>
											<?php }else{ ?>
                                            <img src="<?php echo $cart['no_image_path'] ?>" alt="<?php echo $cart['name'] ?>">
                                            <?php } ?>
                                         </td>
                                         <?php if($cart['type'] == 'buy_gift'){
												if(isset($cart['wishlist_id']) && $cart['wishlist_id']!=0){
										 ?>			
                                             <td> <?= $this->lang->line("buygift") ?> - <a href="<?php echo $cart['product_url']; ?>"><?php echo $cart['name'] ?></a> <?= $this->lang->line("for_wishlist") ?> <a href="<?php echo site_url($cart['wishlist_url'])
                                             ; ?>"><?php echo $cart['wishlist_name']; ?> </a> x <?php echo $cart['qty'];?></td>
                                             <td><?php echo $this->config->item('currency').' '. number_format($cart['price']); ?></td>
                                             <td><?php echo $this->config->item('currency').' '. number_format($cart['subtotal']);?></td>
											<?php }else { ?>
                                             <td><a href="<?php echo $cart['product_url']; ?>"><?php echo $cart['name'] ?></a> x <?php echo $cart['qty'];?></td>
                                             <td><?php echo $this->config->item('currency').' '. number_format($cart['price']); ?></td>
                                             <td><?php echo $this->config->item('currency').' '.  number_format($cart['subtotal']);?></td>
                                             <?php } ?>

                                         <?php }else if($cart['type'] == 'cash_gift'){ ?>
                                         
                                         <td><?= $this->lang->line("cashgift") ?> - <a href="<?php echo site_url($cart['wishlist_url']); ?>">
												 <?php echo $cart['wishlist_name']; ?></a>(<?php echo $this->lang->line("if_transaction_fee")." ".$cart['transaction_fees'] ?>)  </td>
                                         <td><?php echo $this->config->item('currency').' '.  number_format($cart['product_sale_price']); ?></td>
                                         <td><?php echo $this->config->item('currency').' '.  number_format($cart['price']); ?></td>
                                         <?php }else{ ?>
                                         
                                         <td>	  <?= $this->lang->line("contribution_text") ?> - <a href="<?php echo $cart['product_url']; ?>"><?php echo $cart['name']; ?></a> from Wishlist <a href="<?php echo site_url($cart['wishlist_url'])
												 ; ?>"><?php echo $cart['wishlist_name'] ?></a>   </td>
                                    <td><?php echo $this->config->item('currency').' '.  number_format($cart['product_sale_price']); ?></td>
                                         <td><?php echo $this->config->item('currency').' '.  number_format($cart['price']); ?></td>
                                         <?php } ?>
                                         
                                         
                                     </tr>
                                   <?php
								   //$total=$cart->price*$cart->quantity;
								   //$total_price=$total_price+$total;
								    }?>  
                                    
                                     
                                    
                                 </tbody>
                                 <tfoot>
                                    <tr id="discountHeader" <?php echo (isset($discount) && $discount!='') ? '' : "style='display:none;'" ?>>
                                        <th colspan="3" bgcolor="#f5f5f5" class="text-right"><?= $this->lang->line("discount")?> <?php echo (isset($coupon_code) && $coupon_code!='') ? "(".$coupon_code.")" : '' ?></th>
                                        <th bgcolor="#f5f5f5" id="discountAmt"><?php echo (isset($discount) && $discount!='') ? $this->config->item('currency').' '.$discount : '' ?></th>
                                    </tr>
                                    <?php if($shipping_method == 'onedayservice_jakarta'){?>
                                    <tr id="shippingMethodHeader">
                                        <th colspan="3" bgcolor="#f5f5f5" class="text-right"><?= $this->lang->line("shipping_charge") ?> <?php echo (isset($shipping_charge) && $shipping_charge!='') ? "(".$shipping_charge.")" : '' ?></th>
                                        <th bgcolor="#f5f5f5" id="shippingAmt"><?php echo (isset($shipping_charge) && $shipping_charge!='') ? $shipping_charge : '' ?></th>
                                    </tr>
                                    
                                    <?php }?>
                                     <tr>
                                         <th colspan="3" bgcolor="#99CCCC" class="text-right"><?= $this->lang->line("table_total") ?></th>
                                         <th bgcolor="#99CCCC"><?php echo $this->config->item('currency').' '.  number_format($total);?></th>
                                     </tr>
                                 </tfoot>
                             </table>
                             </div>
                             <?php }?>
                            <!-- /.EOF Review-->
<!--Payment-->
<form action="<?php echo site_url("checkout/payment")?>" method="POST" id="payment-form">
  <div class="linerows">
  <h3><?= $this->lang->line("checkout") ?> - <?= $this->lang->line("payment_method") ?></h3>
  <div class="content">
   <div class="row">
   
   <div class="col-sm-6">
   <div class="box payment-method">
   <div class="checkbox">
   <label>
   <h4> <input type="radio" name="payment_method" class="checked" value="master/visa">
   <i class="fa fa-credit-card" aria-hidden="true"></i> <?= $this->lang->line("master") ?> / <?= $this->lang->line("visa")?></h4>
   </label>
  </div>

  <div class="css-error"><?php echo form_error('payment_method'); ?></div></br>
  </div>
  </div>
  
   <!--<div class="col-sm-6">
   <div class="box payment-method">
   <div class="checkbox">
   <label>
   <h4> <input type="radio" name="payment_method" class="checked" value="paypal">
   <i class="fa fa-paypal" aria-hidden="true"></i> Paypal</h4>
   </label>
  </div>
  </div>
  </div>
  
     <div class="col-sm-6">
   <div class="box payment-method">
   <div class="checkbox">
   <label>
   <h4> <input type="radio" name="payment_method" class="checked" value="banktransfer">
   <i class="fa fa-exchange" aria-hidden="true"></i> Bank Transfer</h4>
  
   </label>
  </div>
  </div>
  </div>-->
  
  
</div>                                    
 <!-- /eof Payment -->

  <div class="row">
  <div class="col-md-6 padtop"><input type="checkbox" name="termncon" id="termncon"  value="1" required="required"/>&nbsp;&nbsp;  <?= $this->lang->line("agree") ?><a data-toggle="modal" data-target="#termsandaconditionModal" href="javascript:void(0)">&nbsp;&nbsp;<?= $this->lang->line("terms_conditions") ?></a></div>  </div>                       
                           

                            <div class="row linerows text-center">
<div class="listform">
<button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-chevron-right"></i><?= $this->lang->line("confirm_my_order") ?></button>
</div>
</div>
                            <div class="box-footer">
                                <div class="col-xs-6">
                                    <a href="<?php echo site_url("checkout"); ?>" class="btn btn-warning"><i class="fa fa-chevron-left"></i><?= $this->lang->line("shipping_method") ?></a>
                                </div>
                                
                            </div>
                        
                    
                    <!-- /.box -->



               

            </div>
            <!-- /.container -->
        </div>
        </form>
        <!-- /#content -->


        <!-- *** FOOTER ***
 _________________________________________________________ -->
        
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
       
        <!-- *** COPYRIGHT END *** -->



    </div></div>
