<div id="all">
  <div id="content">
    <div class="container white"> <?php echo form_open("checkout",array("id"=>"checkout")) ?>
      <div class="row bluewhiteheader raleway">
        <?= $this->lang->line("checkout")?>
      </div>
      <?php if(!empty($addressbookdetails)){   ?>
      <div class="row">
        <div class="col-xs-6">
          <h4> Your Saved Addresses </h4>
          <select class="form-control">
            <option value="0" class="addressbookclass">Please Select</option>
            <?php foreach($addressbookdetails as $addressbook){?>
            <option id="<?php echo $addressbook->addressbook_id;?>" class="addressbookclass"><?php echo $addressbook->fname.' '.$addressbook->lname ;?>, <?php echo $addressbook->address ;?>, <?php echo $addressbook->city ;?>,<?php echo $addressbook->postcode ; ?> </option>
            <?php }?>
          </select>
        </div>
      </div>
      <?php } ?>
      <hr/>
      <div class="row" id="checkout">
        <div class="col-md-6"> 
          <!--<div id="hidden_fields" style="display:none">-->
          <div id="hidden_fields" >
            <h4>
              <?= $this->lang->line("shipping_address") ?>
            </h4>
            <div class="row">
              <div class="col-md-6">
                <div class="listform"> <?php echo form_input(array("name"=>"shipfirstName","id"=>"shipfirstName",
				"value"=> 
			  ($user->fname!='') ? $user->fname :
				 (isset($fname) ? $fname : set_value('shipfirstName')),
				 "placeholder"=>$this->lang->line("f_name"),"class"=>"form-control")); ?>
                  <div class="css-error"> <?php echo form_error('shipfirstName'); ?></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="listform"> <?php echo form_input(array("name"=>"shiplastName","id"=>"shiplastName",
				"value"=>($user->lname!='') ? $user->lname : 
				(isset($lname)?$lname:set_value('lastName')), "placeholder"=>$this->lang->line("l_name"),"class"=>"form-control")); ?>
                  <div class="css-error"> <?php echo form_error('shiplastName'); ?></div>
                </div>
              </div>
            </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipAddress","id"=>"shipAddress","value"=>(isset($shipAddress)?
			$shipAddress:set_value('shipAddress')), "placeholder"=>$this->lang->line("w_event_add"),"class"=>"form-control")); ?>
              <div class="css-error"> <?php echo form_error('shipAddress'); ?></div>
            </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipAddress2","id"=>"shipAddress2","value"=>(isset($shipAddress2)?
			$shipAddress2:set_value('shipAddress2')), "placeholder"=>$this->lang->line("w_event_add_2"),"class"=>"form-control")); ?> </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipPostcode","id"=>"shipPostcode","value"=>(isset($shipPostcode)?
			$shipPostcode:set_value('shipPostcode')), "placeholder"=>$this->lang->line("w_event_postcode"),"size"=>"10","class"=>"form-control")); ?>
              <div class="css-error"> <?php echo form_error('shipPostcode'); ?></div>
            </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipKecamatan","id"=>"shipKecamatan","value"=>(isset($shipKecamatan)?
			$shipKecamatan:set_value('shipKecamatan')), "placeholder"=>$this->lang->line("kecamatan"),"class"=>"form-control")); ?>
              <div class="css-error"> <?php echo form_error('shipKecamatan'); ?></div>
            </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipCity","id"=>"shipCity","value"=>"Jakarta Raya", "placeholder"=>$this->lang->line("w_event_city"),"class"=>"form-control","readonly" => "readonly")); ?> </div>
            <div class="listform"> <?php echo form_input(array("name"=>"shipMobile","id"=>"shipMobile","value"=>($user->mobile!='') ? $user->mobile : (isset($mobile)?
			$mobile:set_value('shipMobile')), "placeholder"=>$this->lang->line("w_event_contact")." +62 xxx...","maxlength"=>"50","class"=>"form-control")); ?>
              <div class="css-error"> <?php echo form_error('shipMobile'); ?></div>
            </div>
          </div>
          <p></p>
          <div id="hiddenShippingAdd">
            <?php if(isset($shipping_address) && $shipping_address!=''){
            	foreach($shipping_address as $shipping){
			?>
            <input type="hidden" value="<?php echo $shipping ?>" class="hidShippingAdd" />
            <?php
					}
             } ?>
          </div>
        </div>
        
        <!-- /.row --> 
        
        <!--Shipping Address-->
        <div class="col-md-6">
          <h4>
            <?= $this->lang->line("billing_address") ?>
          </h4>
          <div class="checkbox">
            <label>
            <h4>
              <input type="checkbox" name="billing_same_shipping" value="1" id="billing_same_shipping"
                <?php echo isset($billing_same_shipping) ? "checked='checked'" : '' ?>
                 />
              <p>Billing Address  is same as Shipping Address. </p>
            </h4>
            </label>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"firstName","id"=>"firstName", "placeholder"=>$this->lang->line("f_name"),"value"=>(isset($shipfirstName)?$shipfirstName:set_value('firstName')),
				"class"=>"form-control")); ?>
                <div class="css-error"><?php echo form_error('firstName'); ?></div>
                </br>
              </div>
            </div>
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"lastName","id"=>"lastName","value"=>(isset($shiplastName)?
			$shiplastName:set_value('lastName')), "placeholder"=>$this->lang->line("l_name"),"class"=>"form-control")); ?>
                <div class="css-error"><?php echo form_error('lastName'); ?></div>
                </br>
              </div>
            </div>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"address","id"=>"Address","value"=>($user->ship_address1!='') ? $user->ship_address1 : 
   (isset($address) ? $address : set_value('address')), "placeholder"=>$this->lang->line("w_event_add"),"class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('address'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"address2","id"=>"Address2","value"=>($user->ship_address2!='') ? $user->ship_address2 : 
			(isset($address2)?$address2:set_value('address2')), "placeholder"=>$this->lang->line("w_event_add_2"),"class"=>"form-control")); ?> </div>
          <div class="listform"> <?php echo form_input(array("name"=>"postcode","id"=>"Postcode","value"=>($user->ship_postcode!='') ? $user->ship_postcode :
			(isset($postcode)?$postcode:set_value('postcode')), "placeholder"=>$this->lang->line("w_event_postcode"),"size"=>"10","class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('postcode'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"kecamatan","id"=>"Kecamatan","value"=>($user->ship_kecamatan!='') ? $user->ship_kecamatan :(isset($kecamatan)?$kecamatan:set_value('kecamatan')), "placeholder"=>$this->lang->line("kecamatan"),"class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('kecamatan'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"city","id"=>"City","value"=>"Jakarta Raya", "placeholder"=>$this->lang->line("w_event_city"),"class"=>"form-control","readonly" => "readonly")); ?>
            <div class="css-error"><?php echo form_error('city'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"mobile","id"=>"Mobile","value"=>(isset($shipMobile)?
			$shipMobile:set_value('mobile')), "placeholder"=> $this->lang->line("w_event_contact")." +62 xxx...","maxlength"=>"50","class"=>"form-control")); ?>
            <div class="css-error"> <?php echo form_error('mobile'); ?></div>
            </br>
          </div>
          <div class="alert alert-info">
            <div class="checkbox">
              <label>
              <h4> <?php echo form_checkbox(array("id"=>"trigger","name"=>"question","value"=>"1","checked"=>"")) ?>
                <?= $this->lang->line("ask_ship_address") ?>
              </h4>
              <?= $this->lang->line("ask_ship_address_1") ?>
              </label>
            </div>
            <div class="css-error"> <?php echo form_error('question'); ?></div>
          </div>
        </div>
        
        <!--EOF shipping Address--> 
      </div>
      <!--EOF Row-->
      
      <hr>
      
      <!--Shipping Method-->
      <div class=" row allrows" id="shipping">
        <h3><i class="fa fa-truck" aria-hidden="true"></i>
          <?= $this->lang->line("delivery") ?>
        </h3>
        <div class="row">
          <div class="col-sm-6">
            <div class="box shipping-method">
              <div class="checkbox">
                <label>
                <h4>
                  <input type="radio" name="shipping_method" class="checked" value="freeshipping_jakarta" checked="<?php echo ((isset($shipping_method)?$shipping_method:'')== 'freeshipping_jakarta') ?  "checked" : "checked" ; ?>">
                  <i class="fa fa-gift" aria-hidden="true"></i>
                  <?= $this->lang->line("free_shipping") ?>
                  - Jakarta</h4>
                3-5
                <?= $this->lang->line("days") ?>
                </label>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="box shipping-method">
              <div class="checkbox">
                <label>
                <h4>
                  <input type="radio" name="shipping_method" class="checked" value="onedayservice_jakarta" <?php echo ((isset($shipping_method)?$shipping_method:'') == 'onedayservice_jakarta') ?  "checked" : "" ; ?>>
                  <i class="fa fa-bolt" aria-hidden="true"></i>
                  <?= $this->lang->line("one_day_service") ?>
                  - Jakarta</h4>
                <?= $this->lang->line("same_day_service") ?>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="css-error"><?php echo form_error('shipping_method'); ?></div>
        </br>
      </div>
      <!--EOF Shipping Method-->
      <div class="row linerows text-center">
        <div class="listform"> <?php echo form_submit(array("name"=>"checkoutBtn","class"=>"btn btn-primary","value"=>$this->lang->line("continue_to_payment_method"),"id"=>"checkoutBtn")); ?> 
          <!--<a href="<?php //echo site_url("checkout/payment"); ?>" class="btn btn-primary">Continue to Payment Method<i class="fa fa-chevron-right"></i> </a>--> 
        </div>
      </div>
      <div class="box-footer">
        <div class="col-xs-6"> <a href="<?php echo site_url("cart/view"); ?>" class="btn btn-warning"><i class="fa fa-chevron-left"></i>
          <?= $this->lang->line("view_basket") ?>
          </a> </div>
      </div>
      
      <!-- /.box --> 
      <?php echo form_close(); ?> </div>
    <!-- /.col-md-9 --> 
    
  </div>
  <!-- /.container --> 
</div>

<script type="text/javascript">

$(document).ready(function(){
	  $(document).on('click', '.addressbookclass', function(){
	  var address_id = $(this).attr('id');
	
	  $.ajax({
			type: 'POST',
		   dataType: "json",
			url: '../checkout/getunique_address',
			data: {'address_id': address_id},
			success: function(data){
			$("#shipfirstName").val(data.success.fname);
			$("#shiplastName").val(data.success.lname);
			$("#shipAddress").val(data.success.address);
			$("#shipAddress2").val(data.success.address2);
			$("#shipPostcode").val(data.success.postcode);
			$("#shipKecamatan").val(data.success.kecamatan);
			$("#shipCity").val(data.success.city);
			$("#shipMobile").val(data.success.mobile);
			}
		})
	 
	   
	 });
	
});
</script>


<script type="text/javascript">
    
	var wishlist_exists = <?php echo json_encode($wishlist_exists) ?>;
	var checkbox = <?php echo $checkbox ?>;
	
	if((wishlist_exists.length > 0) && checkbox == 0 ){
		$("#trigger").attr('checked','checked');
		$("#hidden_fields").show();  
	}
	$("#trigger").on('change',function() {
		if ($(this).is(':checked')) {
		  $("#hidden_fields").show();  
		} else {
		  $("#hidden_fields").hide();
		}
	});
	
	$("#billing_same_shipping").change(function(e) {
        if ($(this).is(':checked')) {
		  	var firstName = $("#shipfirstName").val();
			var lastName = $("#shiplastName").val();
			var address = $("#shipAddress").val();
			var address2 = $("#shipAddress2").val();
			var postcode = $("#shipPostcode").val();
			var kecamatan = $("#shipKecamatan").val();
			var city = $("#shipCity").val();
			var mobile = $("#shipMobile").val();
			
			$("#firstName").val(firstName);
			$("#lastName").val(lastName);
			$("#Address").val(address);
			$("#Address2").val(address2);
			$("#Postcode").val(postcode);
			$("#Kecamatan").val(kecamatan);
			$("#City").val(city);
			$("#Mobile").val(mobile);
		}else{
			$("#firstName").val('');
			$("#lastName").val('');
			$("#Address").val('');
			$("#Address2").val('');
			$("#Postcode").val('');
			$("#Kecamatan").val('');
			$("#City").val('Jakarta Raya');
			$("#Mobile").val('');
			
		}
		
		
	   
     
		
		
		
		
		
		
		
		
		
});
</script>