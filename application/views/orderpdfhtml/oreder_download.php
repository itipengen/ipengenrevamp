<?php
/*function to covert href link tag to '' */
function remove_links($s){
      while(TRUE){
        @list($pre,$mid) = explode('<a',$s,2);
        @list($mid,$post) = explode('</a>',$mid,2);
        $s = $pre.$post;
        if (is_null($post))return $s;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<title>Invoice id #
<?php  echo $orderdetails[0]->order_id; ?>
</title>
<link href="<?php echo base_url();?>assets/admin/invoice/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/admin/invoice/css/print.css" rel="stylesheet">
</head>

<body>
<div id="page-wrap">
  <div align="center" class="invoice_header">Invoice</div>
  <div id="logo"> <img  src="<?php echo base_url();?>assets/admin/invoice/images/logo.png" alt="logo" /> <br />
  </div>
  <?php if(!empty($company_address[0])){ ?> 
  <div id="identity"> <?php echo $company_address[0]->company_name;?> 
     <br />
   <?php echo $company_address[0]->place;?>
  </div>
  <?php } ?>
  <div style="clear:both"></div>
  <div style="border-bottom:1px dotted #CCC; height:3px; margin:20px 0;"></div>
  <div id="customer">
    <div style="width:100%;">
      <div style="width:48%; float:left">
        <div id="identity"> Billing Address<br />
          <?php echo $orderdetails[0]->shipping_firstname.' '.$orderdetails[0]->shipping_lastname;?> <br />
          <?php echo $orderdetails[0]->shipping_address;?> <br />
          <?php echo $orderdetails[0]->shipping_city;?> <br />
          <?php echo $orderdetails[0]-> shipping_postcode.' '.$orderdetails[0]->shipping_kecamatan;?> <br />
        </div>
      </div>
      <div style="width:48%; float:right">
        <table id="meta" style="width:100%" >
          <tr>
            <td class="meta-head">Order Id</td>
            <td><?php  echo $orderdetails[0]->order_id; ?></td>
          </tr>
          <tr>
            <td class="meta-head">Date</td>
            <td><?php  echo date("jS F, Y", strtotime($orderdetails[0]->date_added)); ?></td>
          </tr>
          <tr>
            <td class="meta-head">Total Amount</td>
            <td><div class="due">
                <?php  echo $currency.' '.$orderPaymentsDetails[0]->gross_amount; ?>
              </div></td>
          </tr>
        </table>
      </div>
      <div style="clear:both"></div>
    </div>
  </div>
  <table id="items" style="height: 10%" >
    <tr>
      <th>Item</th>
      <th>Description</th>
      <th>Unit Cost</th>
      <th>Quantity</th>
      <th>Price</th>
    </tr>
    <?php
		   $subtotal=''; 
		  foreach($orderdetails as $orderdetailsdata){ 
		    $subtotal =   $orderdetailsdata->subtotal + $subtotal ; ?>
    <tr class="item-row">
      <td  class="item-name"><div class="delete-wpr">
          <?php  echo str_replace('_', " ", $orderdetailsdata->item_type);?>
        </div></td>
      <td class="description"><?php  echo $result = strip_tags($orderdetailsdata->item_description)?></td>
      <td><?php  echo $currency.' '.$orderdetailsdata->price; ?></td>
      <td><?php  echo $orderdetailsdata->quantity; ?></td>
      <td><span class="price">
        <?php  echo $currency.' '.$orderdetailsdata->subtotal; ?>
        </span></td>
    </tr>
    <?php } ?>
    <tr id="hiderow">
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr height ="10%">
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Subtotal</td>
      <td width="100" class="total-value"><div id="subtotal"><?php echo $currency.' '.$subtotal ;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Discount Amount</td>
      <td class="total-value"><div id="subtotal">
          <?php  echo $currency.' '.$orderdetails[0]->discount_amount;?>
        </div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Shipping Charge</td>
      <td class="total-value"><div id="subtotal"><?php echo $currency.' '.$orderdetails[0]->shipping_cost ;?></div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line">Total</td>
      <td class="total-value"><div id="total">
          <?php  echo $currency.' '.$orderPaymentsDetails[0]->gross_amount; ?>
        </div></td>
    </tr>
    <tr>
      <td colspan="2" class="blank"></td>
      <td colspan="2" class="total-line balance">Total Amount</td>
      <td class="total-value balance"><div class="due">
          <?php  echo $currency.' '.$orderPaymentsDetails[0]->gross_amount; ?>
        </div></td>
    </tr>
  </table>
 </div>
</body>
</html>