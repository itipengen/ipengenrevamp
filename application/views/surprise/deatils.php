<div id="content">
            <div class="container white">
            <div class="row">
<!--Side Bar-->  <div class="col-md-3 nopadding">
<?php $this->load->view('common/leftsidebar')?>

<!--Eof original sidebar-->
</div><!-- Right Column-->
<div class="col-md-9">
<div class="dashboard-header"> <?= $this->lang->line("my_surprise_box") ?> </div>
<?php if ( !empty ($surprisedetails)){
foreach ($surprisedetails as $sdetails){
	$sDate = new DateTime($sdetails->transaction_time);
	$order['date']=$sDate->format('d/m/Y');
	$orderid = $sdetails->order_id;
	$title = $sdetails->title;
	$friends = $sdetails->fname .' '. $sdetails->lname;
	$email = $sdetails->email;
	$urlSlug=$sdetails->url;
	
 }} ?>
<div class="allrows">
            <h4><?= $this->lang->line("order") ?> #<?php echo $orderid;?> </h4>
                        <p> <?= $this->lang->line("you_received") ?> <a href="<?php echo getWishlistUrl($urlSlug);?>"><?php echo $title;?></a> - <?= $this->lang->line("cash_gift_from") ?> <a href="<?= base_url().'u/'.$this->session->userdata['log_in']['user_name'];?>"><?php echo $friends; ?></a> <?= $this->lang->line("on_for") ?> <strong><?php echo $order['date']; ?></strong> <?= $this->lang->line("and_is_currently") ?> <strong><?= $this->lang->line("being_prepared") ?></strong>.                        </p>
                    <hr>
                        <div class="row addresses">
                            <div class="col-md-6">
                                <h5><?= $this->lang->line("sender") ?></h5>
                                <p><?php echo $friends; ?>
                                    <br>
                                    <?php echo $email; ?>
                                    <br>
                              <?= $this->lang->line("hidden_address") ?></p>
                          </div>
                            <div class="col-md-6">
                                <h5><?= $this->lang->line("shipping_address") ?> </h5>
                                <p>Wishlist User Address
                                    <br>
                                    13/25 New Avenue
                                    <br>New Heaven
                                    <br>45Y 73J
                                    <br>England
                              <br>Great Britain</p>
                          </div>
            </div>
<hr>
                        <div class="table table-condensed">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2"><?= $this->lang->line("product") ?></th>
                                        <th><?= $this->lang->line("table_total") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $sum = 0;
											if (!empty ($surprisedetails)) {
                                            foreach($surprisedetails as $allsurprise){
												$sum+= ( $allsurprise->price * $allsurprise->quantity);
												
											$photopath = $this->config->item('image_display_path');
											$photopath_absolute = $this->config->item('image_path');
											$thumb_size= $this->config->item('thumb_size');
											$medium_thumb_size= $this->config->item('medium_thumb_size');
											
											$photopath_is_check =$photopath_absolute."product/".$allsurprise->product_id;
											$thumpath = $photopath."product/".$allsurprise->product_id.$medium_thumb_size;
											
											if(is_file($this->config->item('image_path').'/product/'.$allsurprise->product_id.$medium_thumb_size.$allsurprise->product_image)){
											$imgpurl=$this->config->item('image_display_path').'product/'.$allsurprise->product_id.$medium_thumb_size.$allsurprise->product_image ;
											}  else{
											$imgpurl= $this->config->item('image_display_path').'product/no_product.jpg';
											}
                                            ?>
                                
                                    <tr>
                                        <td>
                                            <a href="#">
                                                <img src="<?php echo $imgpurl;?>" alt="<?php echo $allsurprise->product_name?>" width="100" class="thumbnail">
                                            </a>
                                        </td>
                                        <td>
                                        <?php
										$this->load->model('Product_model');
										
										
										
										 ?><a href="<?php echo $this->Product_model->getProductUrl_name($allsurprise->product_id,$allsurprise->product_name)?>"><?php echo $allsurprise->product_name?></a> 
                                        
                                        
                                        <p>Qty : <?php echo $allsurprise->quantity?></p>
                                        <p>Price : <?php echo $this->config->item('currency'); ?> <?php echo number_format($allsurprise->price)?></p>
                                        </td>
                                        <td><?php echo $this->config->item('currency'); ?> <?php echo( number_format(($allsurprise->price * $allsurprise->quantity)))?></td>
                                    </tr>
                                    <?php }} ?>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2" class="text-right"><?= $this->lang->line("table_total") ?></th>
                                        <th><?php echo $this->config->item('currency').' '.number_format($sum);?></th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                        <!-- /.table-responsive -->

                        

          </div>
 

   


</div><!--eof infobox for Dashboard-->


</div><!--Eof Right Column-->
</div>
            
            </div>