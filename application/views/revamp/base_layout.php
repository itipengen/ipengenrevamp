<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 26/01/2017
 * Time: 21:34
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Share your wishlist to Friend</title>
    <link rel="shortcut icon" href="<?= base_url();?>/assets/frontend/img/favicon.ico">
    <link href="<?php echo base_url(); ?>assets/revamp/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/revamp/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/frontend/css/jqueryformvalidation.css" rel="stylesheet">

    <script>
        var base_url = "<?php echo base_url(); ?>";
        var currency = "<?php echo $this->config->item('currency'); ?>";
        var HOSTNAME = base_url;
    </script>

    <script src="<?php echo base_url(); ?>assets/revamp/js/jquery-3.1.1.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/revamp/js/numeral.js"></script>
    <script src="<?php echo base_url(); ?>assets/revamp/js/base.js"></script>


</head>
<body>
<div class="row-full navigation-bar bg-base">
    <div class="container">
        <div class="row-full">
            <div class="row-15 tablet-25">
                <img class="navigation-logo" src="<?php echo base_url(); ?>assets/revamp/img/logo_nav.png" />
            </div>
            <div class="row-25 mobile-off tablet-45">
                <div class="row-full">
                    <div id="menu-create" class="row-50 navigation-link open-navigation" data-id="create">
                        <a class="float-left font-white font12" href="#"><?php echo strtoupper($this->lang->line("create")); ?></a>
                        <i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>
                    </div>
                    <div id="menu-catalogue" class="row-50 navigation-link open-navigation" data-id="catalogue">
                        <a class="float-left font-white font12" href="#"><?php echo strtoupper($this->lang->line("catalogue")); ?></a>
                        <i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>
                    </div>
                </div>
            </div>
            <div class="row-85 mobile-on tablet-on tablet-30">
                <div class="row-15 navigation-link float-right tablet-off">
                    <a class="float-right font24 font-white navigation-icon cursor-pointer nav-action" data-action="nav_mobile"><i class="fa fa-bars"></i></a>
                </div>
                <div class="row-15 tablet-25 navigation-input-select float-right">
                    <select class="font12" onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($language_name == 'english') echo 'selected="selected"'; ?>>EN</option>
                        <option value="bahasa" <?php if($language_name == 'bahasa') echo 'selected="selected"'; ?>>ID</option>
                    </select>
                </div>
                <div id="menu-shoppingcarttablet" class="row-30 navigation-link float-right">
                    <a class="float-left font18 font-white navigation-icon link-action" href="javascript:void(0)" data-action="opensub" data-id="shoppingcarttablet">
                        <i class="fa fa-shopping-cart"></i>
                        <?php
                        $temp_cart_item = 0;
                        if($cart_item){
                            if($cart_item > 9){
                                $temp_cart_item = '+9';
                            }else{
                                $temp_cart_item = $cart_item;
                            }
                            echo '<r id="cart-item-tablet" class="font14">&nbsp;&nbsp;'.$temp_cart_item.'</r>';
                        }
                        ?>
                    </a>
                    <i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>
                    <div id="navigation-sub-shoppingcarttablet" class="navigation-link-sub bg-white" data-open="0">
                        <div class="row-full navigation-link-sub-wrapper">
                            <?php
                            $counter = $subtotal = 0;
                            foreach($cart as $item){
                                echo '<div id="sub-item-tablet'.$item->rowid.'" class="navigation-link-sub-item">';
                                echo '<div class="row-full">';
                                echo '<div class="row-30">';
                                echo '<img class="navigation-link-sub-image" src="'.$item->product_image_url.'" / >';
                                echo '</div>';
                                echo '<div class="row-70">';
                                echo '<div class="row-full font14 font-bold font-base">'.$item->product_name_very_short.'</div>';
                                echo '<div class="row-full">';
                                echo '<div class="row-50 font-grey font14 font-light">'.$this->lang->line('qty').': '.$item->qty.'</div>';
                                echo '<div class="row-50 font-grey font14 font-bold align-right">'.$this->config->item('currency').' '.number_format($item->price, 0).'</div>';
                                echo '</div>';
                                echo '<div class="row-full">';
                                echo '<a class="font12 font-bold-hover font-grey font-light link-action" href="javascript:void(0)" data-action="removeitemcart" data-id="'.$item->rowid.'">'.strtoupper($this->lang->line('remove')).'</a>';
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                $subtotal += ($item->qty * $item->price);
                                $counter++;
                                if($counter < $cart_item){
                                    echo '<div class="border-h"></div>';
                                }
                            }
                            ?>
                            <div class="navigation-link-sub-item bg-base">
                                <div class="row-full">
                                    <div class="row-40 font14 font-white font-bold"><?php echo $this->lang->line('subtotal'); ?>:</div>
                                    <div class="row-60 font14 font-white font-bold align-right"><?php echo $this->config->item('currency').' <r id="cart-subtotal-tablet">'.number_format($subtotal, 0).'</r>'; ?></div>
                                </div>
                            </div>
                            <div class="navigation-link-sub-item">
                                <div class="row-full">
                                    <div class="row-50 align-center">
                                        <a class="font14 font-grey font-light font-blue-hover font-bold-hover" href="<?php echo base_url().'cart/view'; ?>"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $this->lang->line('view_cart'); ?></a>
                                    </div>
                                    <div class="row-50 align-center">
                                        <a class="font14 font-grey font-light font-blue-hover font-bold-hover" href="<?php echo base_url().'checkout'; ?>"><i class="fa fa-check-circle"></i>&nbsp;<?php echo $this->lang->line('checkout'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="menu-usertablet" class="row-15 tablet-25 mobile-off navigation-link float-right">
                    <a class="float-left font18 font-white navigation-icon link-action" href="javascript:void(0)" data-action="opensub" data-id="usertablet"><i class="fa fa-user"></i></a>
                    <i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>
                    <div id="navigation-sub-usertablet" class="navigation-link-sub bg-white" data-open="0">
                        <div class="row-full navigation-link-sub-wrapper">
                        <!-- PROFILE SUB NAVIGATION TABLET -->
                        <?php
                            if($this->session->userdata('log_in')){
                                echo '<div class="navigation-link-sub-item bg-base">';
                                echo '<a class="font14 font-white">'.$this->lang->line("greeting_text").', '.strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']).'</a>';
                                echo '</div>';
                                foreach($sublinks['user'] as $item){
                                    echo '<div class="navigation-link-sub-item bg-grey-hover">';
                                    echo '<a class="font14 font-grey" href="'.$item['url'].'">'.$item['label'].'</a>';
                                    echo '</div>';
                                }
                                echo '<div class="border-h"></div>';
                                foreach($sublinks['logout'] as $item){
                                    echo '<div class="navigation-link-sub-item bg-grey-hover">';
                                    echo '<a class="font14 font-grey" href="'.$item['url'].'">'.$item['label'].'</a>';
                                    echo '</div>';
                                }
                            }else{
                                echo '<div class="navigation-link-sub-item bg-base">';
                                echo '<a class="font14 font-white link-action" href="javascript:void(0)" data-action="login">'.$this->lang->line("login").'</a>';
                                echo '</div>';
                                echo '<div class="navigation-link-sub-item bg-base">';
                                echo '<a class="font14 font-white link-action" href="javascript:void(0)" data-action="register">'.$this->lang->line("register").'</a>';
                                echo '</div>';
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-30 mobile-full tablet-full">
                <div class="row-full navigation-input">
                    <?= form_open("search-list",array("method"=>"get", 'style'=> 'height:100%;')); ?>
                    <div class="row-90">
                        <input type="text" class="input-full-no-margin" name="searchdata" placeholder="<?php echo $this->lang->line("search_placeholder"); ?>" value="<?php echo @$searchVal; ?>" autocomplete="off" />
                    </div>
                    <div class="row-10 height-full">
                        <button type="submit" name="searchbutton" class="row-full navigation-button bg-blue align-center font14 font-white height-full" value="search"><i class="fa fa-search"></i></button>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
            <div class="row-30 mobile-off tablet-off">
                <div class="row-full">
                    <div class="row-15 navigation-input float-right">
                        <select class="font12" onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;">
                            <option value="english" <?php if($language_name == 'english') echo 'selected="selected"'; ?>>EN</option>
                            <option value="bahasa" <?php if($language_name == 'bahasa') echo 'selected="selected"'; ?>>ID</option>
                        </select>
                    </div>
                    <div id="menu-shoppingcart" class="row-20 navigation-link float-right">
                        <a class="float-left font18 font-white navigation-icon link-action" href="javascript:void(0)" data-action="opensub" data-id="shoppingcart">
                            <i class="fa fa-shopping-cart"></i>
                            <?php
                                $temp_cart_item = 0;
                                if($cart_item){
                                    if($cart_item > 9){
                                        $temp_cart_item = '+9';
                                    }else{
                                        $temp_cart_item = $cart_item;
                                    }
                                    echo '<r id="cart-item" class="font14">&nbsp;&nbsp;'.$temp_cart_item.'</r>';
                                }
                            ?>
                        </a>
                        <i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>
                        <div id="navigation-sub-shoppingcart" class="navigation-link-sub bg-white" data-open="0">
                            <div class="row-full navigation-link-sub-wrapper">
                                <?php
                                    $counter = $subtotal = 0;
                                    foreach($cart as $item){
                                        echo '<div id="sub-item-'.$item->rowid.'" class="navigation-link-sub-item">';
                                        echo '<div class="row-full">';
                                        echo '<div class="row-30">';
                                        echo '<img class="navigation-link-sub-image" src="'.$item->product_image_url.'" / >';
                                        echo '</div>';
                                        echo '<div class="row-70">';
                                        echo '<div class="row-full font14 font-bold font-base">'.$item->product_name_very_short.'</div>';
                                        echo '<div class="row-full">';
                                        echo '<div class="row-50 font-grey font14 font-light">'.$this->lang->line('qty').': '.$item->qty.'</div>';
                                        echo '<div class="row-50 font-grey font14 font-bold align-right">'.$this->config->item('currency').' '.number_format($item->price, 0).'</div>';
                                        echo '</div>';
                                        echo '<div class="row-full">';
                                        echo '<a class="font12 font-bold-hover font-grey font-light link-action" href="javascript:void(0)" data-action="removeitemcart" data-id="'.$item->rowid.'">'.strtoupper($this->lang->line('remove')).'</a>';
                                        echo '</div>';
                                        echo '</div>';
                                        echo '</div>';
                                        echo '</div>';
                                        $subtotal += ($item->qty * $item->price);
                                        $counter++;
                                        if($counter < $cart_item){
                                            echo '<div class="border-h"></div>';
                                        }
                                    }
                                ?>
                                <div class="navigation-link-sub-item bg-base">
                                    <div class="row-full">
                                        <div class="row-40 font14 font-white font-bold"><?php echo $this->lang->line('subtotal'); ?>:</div>
                                        <div class="row-60 font14 font-white font-bold align-right"><?php echo $this->config->item('currency').' <r id="cart-subtotal">'.number_format($subtotal, 0).'</r>'; ?></div>
                                    </div>
                                </div>
                                <div class="navigation-link-sub-item">
                                    <div class="row-full">
                                        <div class="row-50 align-center">
                                            <a class="font14 font-grey font-light font-blue-hover font-bold-hover" href="<?php echo base_url().'cart/view'; ?>"><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $this->lang->line('view_cart'); ?></a>
                                        </div>
                                        <div class="row-50 align-center">
                                            <a class="font14 font-grey font-light font-blue-hover font-bold-hover" href="<?php echo base_url().'checkout'; ?>"><i class="fa fa-check-circle"></i>&nbsp;<?php echo $this->lang->line('checkout'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        if($this->session->userdata('log_in')){
                            echo '<div id="menu-user" class="row-15 tablet-25 navigation-link float-right">';
                            echo '<a class="float-left font18 font-white navigation-icon link-action" href="javascript:void(0)" data-action="opensub" data-id="user"><i class="fa fa-user"></i></a>';
                            echo '<i class="icon-xsmall icon-nav-arrow-hide icon-navigation"></i>';
                            echo '<div id="navigation-sub-user" class="navigation-link-sub bg-white" data-open="0">';

                            //PROFILE SUB NAVIGATION
                            ?>
                                <div class="row-full navigation-link-sub-wrapper">
                                    <div class="navigation-link-sub-item bg-base">
                                        <a class="font14 font-white"><?php echo $this->lang->line("greeting_text"); ?>, <?php echo strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']); ?></a>
                                    </div>
                                    <?php
                                        foreach($sublinks['user'] as $item){
                                            echo '<div class="navigation-link-sub-item bg-grey-hover">';
                                            echo '<a class="font14 font-grey" href="'.$item['url'].'">'.$item['label'].'</a>';
                                            echo '</div>';
                                        }
                                        echo '<div class="border-h"></div>';
                                        foreach($sublinks['logout'] as $item){
                                            echo '<div class="navigation-link-sub-item bg-grey-hover">';
                                            echo '<a class="font14 font-grey" href="'.$item['url'].'">'.$item['label'].'</a>';
                                            echo '</div>';
                                        }
                                    ?>
                                </div>
                            <?php

                            echo '</div>';
                            echo '</div>';
                        }else{
                            echo '<div class="row-50 navigation-link float-right">';
                            echo '<a class="float-left font-white font12 link-action" href="javascript:void(0)" data-action="login">'.strtoupper($this->lang->line("login")).'</a>';
                            echo '<r class="float-left font-white font12">&nbsp;&nbsp;|&nbsp;&nbsp;</r>';
                            echo '<a class="float-left font-white font12 link-action" href="javascript:void(0)" data-action="register">'.strtoupper($this->lang->line("register")).'</a>';
                            echo '</div>';
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearing50"></div>
<div class="clearing20"></div>
<div class="clearing20 mobile-on tablet-on"></div>
<div class="clearing20 mobile-on tablet-on"></div>
<!-- CONTENT -->
<?php
    echo $content;
?>
<!-- END CONTENT -->
<div class="clearing20"></div>
<!-- FOOTER -->
<div class="row-full footer">
    <div class="container">
        <div class="row-full">
            <div class="row-50 mobile-full tablet-full">
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <div class="padding-box-large mobile-full mobile-no-padding">
                            <div class="row-full font20 font-base font-bold"><?php echo $this->lang->line('wishlist'); ?></div>
                            <div class="clearing10"></div>
                            <?php
                                foreach($footerlinks['wishlist'] as $item){
                                    $class_action = '';
                                    $data_action = '';
                                    if($item['type'] == 'script'){
                                        $class_action = 'link-action';
                                        $data_action = 'data-action="'.$item['action'].'"';
                                    }
                                    echo '<a href="'.$item['url'].'" class="row-full font14 font-light font-grey font-bold-hover '.$class_action.'" '.$data_action.' >'.$item['label'].'</a>';
                                    echo '<div class="clearing5"></div>';
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row-50 mobile-full">
                        <div class="padding-box-large mobile-full mobile-no-padding">
                            <div class="row-full font20 font-base font-bold"><?php echo $this->lang->line('questions_mark'); ?></div>
                            <div class="clearing10"></div>
                            <?php
                            foreach($footerlinks['questions'] as $item){
                                $class_action = '';
                                $data_action = '';
                                if($item['type'] == 'script'){
                                    $class_action = 'link-action';
                                    $data_action = 'data-action="'.$item['action'].'"';
                                }
                                echo '<a href="'.$item['url'].'" class="row-full font14 font-light font-grey font-bold-hover '.$class_action.'" '.$data_action.' >'.$item['label'].'</a>';
                                echo '<div class="clearing5"></div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="clearing10"></div>
                <div class="clearing10 mobile-on"></div>
            </div>
            <div class="clearing20 mobile-on tablet-on"></div>
            <div class="border-h mobile-on tablet-on"></div>
            <div class="clearing20 mobile-on tablet-on"></div>
            <div class="row-25 mobile-full tablet-half">
                <div class="padding-box-large mobile-full mobile-no-padding">
                    <div class="row-full font20 font-base font-bold"><?php echo $this->lang->line('ourcompany'); ?></div>
                    <div class="clearing10"></div>
                    <?php
                    foreach($footerlinks['ourcompany'] as $item){
                        $class_action = '';
                        $data_action = '';
                        if($item['type'] == 'script'){
                            $class_action = 'link-action';
                            $data_action = 'data-action="'.$item['action'].'"';
                        }
                        echo '<a href="'.$item['url'].'" class="row-full font14 font-light font-grey font-bold-hover '.$class_action.'" '.$data_action.' >'.$item['label'].'</a>';
                        echo '<div class="clearing5"></div>';
                    }
                    ?>
                    <div class="clearing20"></div>
                    <a href="<?php echo base_url().'contact-us'; ?>" class="row-full font14 font-light font-base font-bold-hover"><?php echo $this->lang->line("goto_contact"); ?></a>
                </div>
            </div>
            <div class="clearing20 mobile-on"></div>
            <div class="row-25 mobile-full tablet-half">
                <div class="padding-box-large mobile-full mobile-no-padding">
                    <div class="row-full font20 font-base font-bold">Get the news</div>
                    <div class="clearing10"></div>
                    <div class="row-full font14 font-light font-grey">
                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpes egestas
                    </div>
                    <div class="clearing20"></div>
                    <div class="row-full mobile-half input-no-margin-wrapper">
                        <div class="row-60">
                            <input type="text" class="input-full-no-margin" />
                        </div>
                        <div class="row-40 height-full">
                            <button class="row-full button-full-no-margin mobile-no-padding border-box-grey bg-white bg-base-hover align-center font14 line-no-space font-grey font-white-hover">Subscribe</button>
                        </div>
                    </div>
                    <div class="clearing20"></div>
                    <div class="border-h mobile-on"></div>
                    <div class="clearing20 mobile-on"></div>
                    <div class="row-full font20 font-base font-bold">Stay in touch</div>
                    <div class="clearing10"></div>
                    <div class="row-full">
                        <div class="row-15 mobile-auto mobile-margin">
                            <a href="#" class="icon-medium icon-facebook"></a>
                        </div>
                        <div class="row-15 mobile-auto mobile-margin">
                            <a href="#" class="icon-medium icon-twitter"></a>
                        </div>
                        <div class="row-15 mobile-auto mobile-margin">
                            <a href="#" class="icon-medium icon-instagram"></a>
                        </div>
                        <div class="row-15 mobile-auto mobile-margin">
                            <a href="#" class="icon-medium icon-google"></a>
                        </div>
                        <div class="row-15 mobile-auto mobile-margin">
                            <a href="#" class="icon-medium icon-email"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearing20"></div>
<div class="row-full bg-base">
    <div class="container">
        <div class="row-full">
            <div class="clearing10"></div>
            <div class="row-50 mobile-full">
                <div class="row-full font-white font12 align-center-mobile">&copy; 2016 iPengen</div>
            </div>
            <div class="row-50 mobile-full">
                <div class="row-full align-center-mobile">
                    <!-- <a href="#" class="font12 font-white float-right font-bold-hover mobile-off">Feedback</a> -->
                    <!-- <a href="#" class="font12 font-white font-bold-hover mobile-on mobile-auto mobile-no-float">Privacy Policy</a> -->

                    <!-- <r class="float-right font14 font-white mobile-no-float">&nbsp;&nbsp;|&nbsp;&nbsp;</r> -->
                    <?php
                        $total_item = count($footerlinks['footer']);
                        $counter_item = 0;
                        foreach($footerlinks['footer'] as $item){
                            $class_action = '';
                            $data_action = '';
                            if($item['type'] == 'script'){
                                $class_action = 'link-action';
                                $data_action = 'data-action="'.$item['action'].'"';
                            }
                            echo '<a href="'.$item['url'].'" class="font12 font-white float-right font-bold-hover mobile-no-float '.$class_action.'" '.$data_action.'>'.$item['label'].'</a>';
                            $counter_item++;
                            if($counter_item < $total_item){
                                echo '<r class="float-right font14 font-white mobile-no-float">&nbsp;&nbsp;|&nbsp;&nbsp;</r>';
                            }
                        }
                    ?>

                    <!-- <a href="#" class="font12 font-white float-right font-bold-hover mobile-no-float">Term of Use</a>
                    <r class="float-right font14 font-white mobile-no-float">&nbsp;&nbsp;|&nbsp;&nbsp;</r>

                    <a href="#" class="font12 font-white float-right font-bold-hover mobile-auto mobile-no-float">Privacy Policy</a>
                    <a href="#" class="font12 font-white font-bold-hover mobile-on mobile-auto mobile-no-float">Feedback</a> -->
                </div>
            </div>
            <div class="clearing10"></div>
        </div>
    </div>
</div>
<!-- END FOOTER -->
<div id="popup-navigation-create" class="row-full popup-navigation" data-id="create">
    <div class="container height-full tablet-full">
        <div class="popup-navigation-content bg-white" data-active="">
            <div class="row-full height-full">
                <div class="padding-box min-height-full">
                    <?php
                        //print_r($events);
                        foreach($events as $item){

                            echo '<div class="row-10 mobile-25 tablet-20">';
                            echo '<div id="adjusted-height-sample-4" class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-4">';
                            echo '<div class="row-full height-full overflow-hidden">';
                            /*<?php echo $this->config->item('image_display_path')?>eventcategory/<?php echo $ecat->id;?>/400-400/<?php echo $ecat->image_name;?> */
                            echo '<img class="image-full" src="'.$this->config->item('image_display_path').'eventcategory/'.$item->id.'/400-400/'.$item->image_name.'"/>';
                            echo '</div>';
                            echo '<div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">';
                            echo '<a href="'.base_url().'create-a-wishlist?cat='.$item->event_name.'" class="centering font-white font14 align-center"><span>'.$item->event_name.'</span></a>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="clearing10"></div>
            </div>
        </div>
    </div>
</div>
<div id="popup-navigation-catalogue" class="row-full popup-navigation" data-id="catalogue">
    <div class="container height-full tablet-full">
        <div class="popup-navigation-content bg-white" data-active="">
            <div class="row-full height-full">
                <div class="row-20 height-full tablet-30">
                    <div class="padding-box min-height-full border-box-right-grey">
                        <div class="row-full font-bold font-base font16"><?php echo strtoupper($this->lang->line('categories')); ?></div>
                        <div class="clearing10"></div>
                        <?php
                            foreach($categories as $item){
                                echo '<div id="category-menu-'.$item->id.'" class="category-item row-full font-light font-grey font14 cursor-pointer" data-id="'.$item->id.'">'.strtoupper($item->name).'</div>';
                                echo '<div class="clearing5"></div>';
                            }
                        ?>
                    </div>
                </div>
                <div class="row-80 float-right tablet-70">
                    <div class="padding-box">
                        <div class="row-full font-bold font-base font16"><?php echo strtoupper($this->lang->line('subcategories')); ?></div>
                        <div class="clearing10"></div>
                        <div class="row-full">
                            <div class="row-80 tablet-full">
                                <div class="row-full subcategory-empty">&nbsp;</div>
                                <?php
                                    foreach($categories as $item){
                                        echo '<div id="category-'.$item->id.'" class="row-full subcategory-item" data-id="'.$item->id.'">';
                                        if(isset($item->subcategory)){
                                            foreach($item->subcategory as $sub){
                                                echo '<a class="row-33 margin-top font-light font-base-hover font-bold-hover font14 font-grey tablet-half" href="'.base_url().'search#&categoryid='.$sub['id'].'">'.$sub['name'].'</a>';
                                            }
                                        }
                                        echo '<div class="clearing10"></div>';
                                        echo '<a class="row-full font-light font-base font-bold-hover font14" href="'.base_url().'search#&categoryid='.$item->id.'">View All '.$item->name.' Category</a>';
                                        echo '</div>';
                                    }
                                ?>
                            </div>
                            <div class="row-20 tablet-off">
                                <div class="row-full"><a href="#"><img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/product/product1_square.png" /></a></div>
                                <div class="clearing10"></div>
                                <div class="row-full"><a href="#"><img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/product/product2_square.png" /></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-full mobile-navigation overflow-hidden mobile-on" data-open="0">
    <div class="height-full mobile-navigation-content overflow-auto bg-white">
        <div class="padding-box-large">
            <div id="mobile-navigation-menu-main" class="row-full mobile-navigation-menu ">
                <div class="clearing20"></div>
                <?php
                    if($this->session->userdata('log_in')){
                        echo '<div class="row-full">';
                        echo '<a class="font16 font-bold font-base">'.strtoupper($this->lang->line("greeting_text")).', '.strtoupper($this->session->userdata['log_in']['fname']).' '.strtoupper($this->session->userdata['log_in']['lname']).'</a>';
                        echo '</div>';
                        echo '<div class="clearing10"></div>';
                    }
                ?>


                <div class="row-full">
                    <a href="/" class="font16 font-bold-hover font-base-hover font-light font-grey"><?php echo strtoupper($this->lang->line('home')); ?></a>
                </div>
                <div class="clearing10"></div>
                <div class="row-full">
                    <a class="font16 font-bold-hover font-base-hover font-light font-grey cursor-pointer nav-action" data-action="nav-page" data-id="create"><?php echo strtoupper($this->lang->line('create')); ?></a>
                </div>
                <div class="clearing10"></div>
                <div class="row-full">
                    <a class="font16 font-bold-hover font-base-hover font-light font-grey cursor-pointer nav-action" data-action="nav-page" data-id="catalogue"><?php echo strtoupper($this->lang->line('catalogue')); ?></a>
                </div>
                <div class="clearing10"></div>
                <div class="border-h"></div>
                <div class="clearing10"></div>
                <?php
                    if($this->session->userdata('log_in')){
                        foreach($sublinks['user'] as $item){
                            echo '<div class="row-full">';
                            $class_action = $data_action = '';
                            if($item['type'] == 'script'){
                                $class_action = 'link-action';
                                $data_action = 'data-action="'.$item['action'].'"';
                            }
                            echo '<a href="'.$item['url'].'" class="font16 font-bold-hover font-base-hover font-light font-grey '.$class_action.'" '.$data_action.'>'.strtoupper($item['label']).'</a>';
                            echo '</div>';
                            echo '<div class="clearing10"></div>';
                        }
                        echo '<div class="border-h"></div>';
                        echo '<div class="clearing10"></div>';
                        foreach($sublinks['logout'] as $item){
                            echo '<div class="row-full">';
                            $class_action = $data_action = '';
                            if($item['type'] == 'script'){
                                $class_action = 'link-action';
                                $data_action = 'data-action="'.$item['action'].'"';
                            }
                            echo '<a href="'.$item['url'].'" class="font16 font-bold-hover font-base-hover font-light font-grey '.$class_action.'" '.$data_action.'>'.strtoupper($item['label']).'</a>';
                            echo '</div>';
                            echo '<div class="clearing10"></div>';
                        }
                    }else{
                        foreach($sublinks['login'] as $item){
                            echo '<div class="row-full">';
                            $class_action = $data_action = '';
                            if($item['type'] == 'script'){
                                $class_action = 'link-action';
                                $data_action = 'data-action="'.$item['action'].'"';
                            }
                            echo '<a href="'.$item['url'].'" class="font16 font-bold-hover font-base-hover font-light font-grey '.$class_action.'" '.$data_action.'>'.strtoupper($item['label']).'</a>';
                            echo '</div>';
                            echo '<div class="clearing10"></div>';
                        }
                    }
                
                ?>
                

            </div>
            <div id="mobile-navigation-menu-catalogue" class="row-full mobile-navigation-menu" style="display: none;" >
                <div class="clearing10"></div>
                <div class="row-full">
                    <a class="font16 font-bold-hover font-base-hover font-light font-grey cursor-pointer nav-action" data-action="nav-page" data-id="main"><i class="font18 fa fa-angle-left"></i>&nbsp;&nbsp;<?php echo strtoupper($this->lang->line('back')); ?></a>
                </div>
                <div class="clearing5"></div>
                <div class="border-h"></div>
                <div class="row-full">
                    <div class="clearing10"></div>
                    <div class="row-full font-bold font-base font16"><?php echo strtoupper($this->lang->line('categories')); ?></div>
                    <div class="clearing10"></div>
                    <?php
                        foreach($categories as $item){
                            echo '<div class="row-full font-light font-grey font-bold-hover font-base-hover font14 cursor-pointer nav-action" data-action="nav-submenu" data-id="'.$item->id.'">'.strtoupper($item->name).'</div>';
                            echo '<div class="clearing5"></div>';
                            echo '<div id="mobile-navigation-submenu-'.$item->id.'" class="row-full mobile-navigation-submenu">';
                            if(isset($item->subcategory)){
                                foreach($item->subcategory as $sub){
                                    echo '<a class="row-full margin-top font-light font-base-hover font-bold-hover font14 font-grey" href="'.base_url().'search#&categoryid='.$sub['id'].'">'.$sub['name'].'</a>';
                                }
                            }
                            echo '<div class="clearing10"></div>';
                            echo '<a class="row-full font-light font-base font-bold-hover font14" href="'.base_url().'search#&categoryid='.$item->id.'">'.sprintf($this->lang->line('view_all_category'),ucwords(strtolower($item->name))).'</a>';
                            echo '<div class="clearing10"></div>';
                            echo '</div>';
                            echo '<div class="border-h"></div>';
                            echo '<div class="clearing5"></div>';        
                        }
                        
                    ?>
                </div>
            </div>
            <div id="mobile-navigation-menu-create" class="row-full mobile-navigation-menu" style="display: none;" >
                <div class="clearing10"></div>
                <div class="row-full">
                    <a class="font16 font-bold-hover font-base-hover font-light font-grey cursor-pointer nav-action" data-action="nav-page" data-id="main"><i class="font18 fa fa-angle-left"></i>&nbsp;&nbsp;<?php echo strtoupper($this->lang->line('back')); ?></a>
                </div>
                <div class="clearing5"></div>
                <div class="border-h"></div>
                <div class="row-full">
                    <div class="clearing10"></div>
                    <div class="row-full font-bold font-base font16"><?php echo strtoupper($this->lang->line('events')); ?></div>
                    <div class="clearing10"></div>
                    <?php
                        foreach($events as $item){
                            echo '<a href="'.base_url().'create-a-wishlist?cat='.$item->event_name.'" class="row-full font-light font-grey font-bold-hover font-base-hover font14">'.strtoupper($item->event_name).'</a>';
                            echo '<div class="clearing5"></div>';
                            echo '<div class="border-h"></div>';
                            echo '<div class="clearing5"></div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="back-popup-mobile-navigation" class="back-popup mobile-on" style="display : none;"></div>
<div id="back-popup-general" class="back-popup"></div>
<div id="popup-register" class="popup-small">
    <div class="row-full bg-base">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <img class="login-logo" src="<?php echo base_url(); ?>assets/revamp/img/logo_nav.png" />
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full align-center font14 font-white font-light"><?php echo $this->lang->line("signup_text_popup"); ?></div>
            <div class="row-full align-center"><a class="font-white font-bold font14 font-line-hover link-action" href="javascript:void(0)" data-action="alereadyregister"><?php echo $this->lang->line("already_register"); ?></a></div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <a href="<?= $login_url ?>" > <img class="login-facebook" src="<?= base_url();?>/assets/frontend/img/ZW4QC.png"></a>
            </div>
            <div class="clearing10 mobile-off"></div>
            <div class="clearing5 mobile-on"></div>
            <div class="row-full font14 font-light font-grey align-center">OR</div>
            <div class="clearing10 mobile-off"></div>
            <div class="clearing5 mobile-on"></div>
            <div class="row-full font16 font-base font-light align-center">
                <?php echo strtoupper($this->lang->line("register_with_email")); ?>
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
            <?php
                $data = array('name'=>'registrationform','id'=>'registrationform');
                echo form_open('user/signup',$data);

            ?>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <?php $fdata = array(
                            'name'          => 'fname',
                            'id'            => 'fname',
                            'value'         => '',
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                            'placeholder'   => $this->lang->line("f_name")
                        );
                        echo form_input($fdata); ?>
                    </div>
                    <div class="clearing5 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <?php
                        $ldata = array(
                            'name'          => 'lname',
                            'id'            => 'lname',
                            'value'         => '',
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                            'placeholder'   => $this->lang->line("l_name")
                        );
                        echo form_input($ldata); ?>
                    </div>
                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <?php
                        $data1 = array(
                            'name'          => 'regemail',
                            'id'            => 'regemail',
                            'value'         => '',
                            'type'          => 'email',
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                            'placeholder'   => $this->lang->line("email")
                        );
                        echo form_input($data1);
                        ?>
                    </div>
                    <div class="clearing5 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <?php
                        $passdata = array(
                            'name'          => 'password',
                            'id'            => 'regpassword',
                            'value'         => '',
                            'type'          => 'password',
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                            'placeholder'   => $this->lang->line("password")
                        );
                        echo form_input($passdata);?>
                    </div>
                </div>
                <div id="regemail-error12" class="row-full" style="display:none;">
                    <div class="clearing10"></div>
                    <div class="row-full align-center error"><?= $this->lang->line("unique_email") ?></div>
                </div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="row-full font16 font-base font-light align-center">
                    <?php echo strtoupper($this->lang->line("agrement")); ?>
                </div>
                <div class="clearing5"></div>
                <div class="row-full font14 font-grey font-light align-center">
                    <?php echo $this->lang->line("agrement_part1") ?> <a href="<?php echo base_url("term-condition"); ?>" class="font-base font-bold-hover font-line-hover" target="_blank"><?php echo $this->lang->line("terms_conditions") ?></a> <?php echo $this->lang->line("agrement_part2") ?>.
                </div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-base font-white font14 link-action" data-action="register-submit"><i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("create_wishlist") ?></button>
                    </div>
                    <div class="clearing10 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-blue font-white font14 float-right link-action" data-action="register-submit"><i class="fa fa-gift" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("buy_gift") ?></button>
                    </div>
                </div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div id="popup-login" class="popup-xsmall">
    <div class="row-full bg-base">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <img class="login-logo" src="<?php echo base_url(); ?>assets/revamp/img/logo_nav.png" />
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <a href="<?= $login_url ?>" > <img class="login-facebook" src="<?= base_url();?>/assets/frontend/img/ZW4QC.png"></a>
            </div>
            <div class="clearing10 mobile-off"></div>
            <div class="clearing5 mobile-on"></div>
            <div class="row-full font14 font-light font-grey align-center">OR</div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <?php
                $data = array('name'=>'loginform','id'=>'loginform');
                echo form_open('user/login',$data);

                ?>
                <div class="row-full">
                    <div class="error autherror" style="display:none"><?= $this->lang->line("u_p_invalid") ?></div>
                </div>
                <div class="row-full">
                    <?php
                        $data1 = array(
                            'name'          => 'logemail',
                            'id'            => 'logemail',
                            'value'         => '',
                            'type'          => 'email',
                            'placeholder'   => $this->lang->line("email"),
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                        );
                        echo form_input($data1);
                    ?>
                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="row-full">
                    <?php
                        $passdata = array(
                            'name'          => 'logpass',
                            'id'            => 'logpass',
                            'value'         => '',
                            'type'          => 'password',
                            'placeholder'   => $this->lang->line("password"),
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                        );
                        echo form_input($passdata);
                    ?>
                </div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-base font-white font14 link-action" data-action="notyetregister"><i class="fa fa-user" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("register") ?></button>
                    </div>
                    <div class="clearing10 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-blue font-white font14 float-right link-action" data-action="login-submit"><i class="fa fa-sign-in" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("login"); ?></button>
                    </div>
                </div>
                <div class="clearing20"></div>
                <div class="row-full align-center">
                    <a href="javascript:void(0)" class="font-base font-bold-hover font-line-hover font12 link-action" data-action="forgotpassword"><?php echo $this->lang->line("f_password"); ?></a></div>
                </div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
<div id="popup-forgotpassword" class="popup-xsmall">
    <div class="row-full bg-base">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <img class="login-logo" src="<?php echo base_url(); ?>assets/revamp/img/logo_nav.png" />
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full font16 font-base font-light align-center">
                <?php echo strtoupper($this->lang->line("enter_your_mail")); ?>
            </div>
            <div class="clearing10 mobile-off"></div>
            <div class="clearing5 mobile-on"></div>
            <div class="row-full">
                <?php
                $forgetpwd = array('name'=>'forgetpwdform','id'=>'forgetpwdform');
                echo form_open('user/porgetpwd',$forgetpwd);
                ?>
                <div class="row-full">
                    <div class="font-base align-center font14 forgetpwdmsg" style="display:none"><?= $this->lang->line("email_sent") ?></div>

                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="row-full">
                    <?php
                        $fdata = array(
                            'name'          => 'forgetemail',
                            'id'            => 'forgetemail',
                            'value'         => '',
                            'type'			=> 'email',
                            'placeholder'   => $this->lang->line("email"),
                            'class'         => 'font14 font-light input-mobile-full-no-margin',
                        );
                        echo form_input($fdata);
                    ?>
                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-base font-white font14 link-action" data-action="cancelforgot"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("cancel") ?></button>
                    </div>
                    <div class="clearing10 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <button type="submit" class="button-link button-full button-full-mobile bg-blue font-white font14 float-right link-action" data-action="submitforgot"><i class="fa fa-send" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("submit") ?></button>
                    </div>
                </div>
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<div id="popup-terms" class="popup-large">
    <div class="row-full bg-base">
        <div class="popup-content">
            <div class="clearing10"></div>
            <div class="row-full font20 font-bold font-white"><?php echo $this->lang->line("terms_conditions"); ?></div>
            <div class="clearing10"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="popup-content">
            <div class="clearing10"></div>
            <?php
            $description = $this->lang->line('no_description').$this->lang->line("terms_conditions");
            if(isset($terms->block_description)){
                $description = $terms->block_description;
            }
            ?>
            <div class="row-full font-grey font14 font-light"><?php echo $description; ?></div>
            <div class="clearing10"></div>
        </div>
    </div>
</div>
</div>
<div id="popup-privacy" class="popup-large">
    <div class="row-full bg-base">
        <div class="popup-content">
            <div class="clearing10"></div>
            <div class="row-full font20 font-bold font-white"><?php echo $this->lang->line("privacy_security"); ?></div>
            <div class="clearing10"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="popup-content">
            <div class="clearing10"></div>
            <?php 
                $description = $this->lang->line('no_description').$this->lang->line("privacy_security");
                if(isset($privacy->block_description)){
                    $description = $privacy->block_description;
                }
            ?>
            <div class="row-full font-grey font14 font-light"><?php echo $description; ?></div>
            <div class="clearing10"></div>
        </div>
    </div>
</div>
</div>
</body>
</html>

<?php //die(); ?>