<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 14/02/2017
 * Time: 22:39
 */
?>
<link href="<?php echo base_url(); ?>assets/revamp/css/pages/create_wishlist.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/revamp/js/create_wishlist.js"></script>

<div class="row-full">
    <div class="container">
        <div class="clearing50"></div>
        <div class="row-full">
            <div class="clearing50"></div>
            <div class="row-full">
                <div class="row-33">
                    <div class="row-50 float-right">
                        <div class="border-h-bold"></div>
                    </div>
                </div>
                <div class="row-33">
                    <div class="row-full">
                        <div class="border-h-bold"></div>
                    </div>
                </div>
                <div class="row-33">
                    <div class="row-50">
                        <div class="border-h-bold"></div>
                    </div>
                </div>
            </div>
            <div class="row-full create-step-wrapper">
                <div class="row-33">
                    <div class="row-full align-center font16 font-grey font-bold">STEP 1</div>
                    <div class="clearing5"></div>
                    <div class="row-full">
                        <div class="create-step-item middle bg-base font40 align-center font-white border-box-white-bold"><i class="fa fa-file-text-o" aria-hidden="true""></i></div>
                    </div>
                    <div class="clearing10"></div>
                    <div class="row-full font-base font12 font-italic align-center">
                        Congratulations! Let's get going and<br />begin to receive gifts soon!!
                    </div>
                </div>
                <div class="row-33">
                    <div class="row-full align-center font16 font-grey-light font-bold">STEP 2</div>
                    <div class="clearing5"></div>
                    <div class="row-full">
                        <div class="create-step-item middle bg-white font40 align-center font-grey-light border-box-grey-bold"><i class="fa fa-gift" aria-hidden="true""></i></div>
                    </div>
                </div>
                <div class="row-33">
                    <div class="row-full align-center font16 font-grey-light font-bold">STEP 3</div>
                    <div class="clearing5"></div>
                    <div class="row-full">
                        <div class="create-step-item middle bg-white font40 align-center font-grey-light border-box-grey-bold"><i class="fa fa-smile-o" aria-hidden="true""></i></div>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="clearing10"></div>
            <div class="row-full bg-base">
                <div class="section-title font-white font14 font-bold">Wishlist Information</div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full font-grey font-bold font14">Wishlist Name: <i class="font-light">(to identify your wishlist)</i></div>
            <div class="clearing5"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder=""/>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <div class="row-50">
                            <select class="input-full font-grey font14">
                                <option>Hhehe</option>
                            </select>
                        </div>
                        <div class="row-50">
                            <input type="text" name="" class="input-with-icon font-grey font14" placeholder="dd/mm/yyyy"/>
                            <div class="input-icon-button font-grey font14 bg-white bg-grey-hover border-box-grey align-center"><i class="fa fa-calendar"></i></div>
                            <!-- <input type="date" name="" class="font-grey font14 input-full" placeholder=""/> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full font-grey font-bold font14">Wishlist Start Date:</div>
                        <div class="clearing5"></div>
                        <div class="row-full">
                            <input type="text" name="" class="input-with-icon font-grey font14" placeholder="dd/mm/yyyy"/>
                            <div class="input-icon-button font-grey font14 bg-white bg-grey-hover border-box-grey align-center"><i class="fa fa-calendar"></i></div>
                            <!-- <input type="date" name="" class="font-grey font14 input-full" placeholder=""/> -->
                        </div>
                        <div class="clearing20"></div>
                        <div class="row-full font-grey font-bold font14">Wishlist End Date:</div>
                        <div class="clearing5"></div>
                        <div class="row-full">
                            <input type="text" name="" class="input-with-icon font-grey font14" placeholder="dd/mm/yyyy"/>
                            <div class="input-icon-button font-grey font14 bg-white bg-grey-hover border-box-grey align-center"><i class="fa fa-calendar"></i></div>
                            <!-- <input type="date" name="" class="font-grey font14 input-full" placeholder=""/> -->
                        </div>
                        <div class="clearing5"></div>
                        <div class="row-full font12 font-italic error">Maximum Range 60 days from start to close wishlist date!</div>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <div class="row-full font-grey font-bold font14">Event Address:</div>
                        <div class="clearing5"></div>
                        <input type="text" name="" class="font-grey font14 input-full" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full font-grey font-bold font14">Partner Information: <i class="font-light">(will appear on your wishlist banner)</i></div>
                        <div class="clearing5"></div>
                        <div class="row-full">
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="First Name"/>
                            </div>
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="Surname"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <div class="row-full font-grey font-bold font14">Partner Information: <i class="font-light">(will appear on your wishlist banner)</i></div>
                        <div class="clearing5"></div>
                        <div class="row-full">
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="First Name"/>
                            </div>
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="Surname"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full font-grey font-bold font14">Wishlist Banner/Image: <i class="font-light">(max size: 800kb, if not, default image will be used)</i></div>
                        <div class="clearing5"></div>
                        <div id="box-img-file" class="box-input border-box-grey">
                            <div class="clearing5"></div>
                            <div class="row-full">
                                <div class="row-60">
                                    <img id="img-file" class="row-full" src="<?php echo base_url(); ?>assets/revamp/img/img_template_file.jpg" />
                                </div>
                                <div class="row-40">
                                    <div id="button-browse-image" class="row-full">
                                        <div class="row-25">&nbsp;</div>
                                        <div class="row-50">
                                            <button type="button" class="button-full bg-base font-white font14 align-center">Browse</button>
                                        </div>
                                        <div class="row-25">&nbsp;</div>
                                        <div class="clearing10"></div>
                                        <div class="row-full align-center"><i class="font-light font12 font-grey">no file selected</i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearing5"></div>
                        </div>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <div class="row-full font-grey font-bold font14">Personalized Message: <i class="font-light">(will appear on your wishlist banner)</i></div>
                        <div class="clearing5"></div>
                        <textarea id="textarea-personalized" name="" class="font-grey font14 input-full" placeholder="RSVP, info, Dress Code, etc... (max 250 character)"></textarea>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full">
                            <div class="row-5">
                                <input type="checkbox" name="" />
                            </div>
                            <div class="row-95 font-base font16 font-bold">Would you like to receive iPengen Angpao</div>
                        </div>
                        <div class="clearing5"></div>
                        <div class="row-full font14">
                            iPengen Angpao Fund will be transfered to your bank account*
                        </div>
                        <div class="clearing5"></div>
                        <div class="row-full font-italic error font12">*Terms & Condition Apply.</div>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="clearing10"></div>
            <div class="row-full bg-base">
                <div class="section-title font-white font14 font-bold">Wishlist Privacy</div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full">
                            <div class="row-30">
                                <input type="radio" name="radio1" class="float-left"/>
                                <r class="float-left font-grey font14 font-bold">&nbsp;&nbsp;Public</r>
                            </div>
                            <div class="row-70">
                                <input type="radio" name="radio1" class="float-left" checked/>
                                <r class="float-left font-grey font14 font-bold">&nbsp;&nbsp;Private <i class="font-light">(with password)</i></r>
                            </div>
                        </div>
                        <div class="clearing10"></div>
                        <div class="row-full font-grey font-light font14"><i>(Only people whom you share this password with will be able to access your wishlist. Do not use the same password as your account password.)</i></div>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <div class="row-full">
                            <div class="row-50">
                                <div class="row-full font-grey font-bold font14">Wishlist Password:</div>
                                <div class="clearing5"></div>
                                <input type="password" name="" class="input-with-icon font-grey font14"/>
                                <div class="input-icon-button font-grey font14 bg-white bg-grey-hover border-box-grey align-center"><i class="fa fa-eye"></i></div>
                            </div>
                            <div class="row-50 float-right">
                                <div class="row-full font-grey font-bold font14">Wishlist Confirm Password:</div>
                                <div class="clearing5"></div>
                                <input type="password" name="" class="input-with-icon font-grey font14" />
                                <div class="input-icon-button font-grey font14 bg-white bg-grey-hover border-box-grey align-center"><i class="fa fa-eye"></i></div>
                            </div>
                        </div>
                        <div class="clearing5"></div>
                        <div class="row-full font-italic error font12">*Password must be less than 15 characters</div>
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-full font-grey font-bold font14">Wishlist Public URL: </div>
                <div class="clearing5"></div>
                <div class="row-full bg-grey">
                    <div class="row-45">
                        <div class="box-input font14 font-bold">https://staging.ipengen.com/~</div>
                    </div>
                    <div class="row-50">
                        <input type="text" name="" class="input-full font-grey font14" placeholder="Wishlist Publish URL" />
                    </div>
                </div>
            </div>
            <div class="clearing20"></div>
            <div class="clearing10"></div>
            <div class="row-full bg-base">
                <div class="section-title font-white font14 font-bold">Shipping Information</div>
            </div>
            <div class="clearing20"></div>
            <div class="row-full font-base font16 font-bold">Where should we send your gift?</div>
            <div class="clearing5"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <div class="row-full">
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="First Name"/>
                            </div>
                            <div class="row-50">
                                <input type="text" name="" class="font-grey font14" placeholder="Last Name" />
                            </div>
                        </div>
                        <div class="clearing20"></div>
                        <div class="row-full font-grey font-bold font14">Set Address: </div>
                        <div class="clearing5"></div>
                        <div class="row-full">
                            <select class="input-full-select font-grey font14">
                                <option>Select 1</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing10"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="Address"/>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="Province"/>
                    </div>
                </div>
            </div>
            <div class="clearing10"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="Address Line 2"/>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="City"/>
                    </div>
                </div>
            </div>
            <div class="clearing10"></div>
            <div class="row-full">
                <div class="row-50">
                    <div class="row-95">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="Post Code"/>
                    </div>
                </div>
                <div class="row-50">
                    <div class="row-95 float-right">
                        <input type="text" name="" class="font-grey font14 input-full" placeholder="Mobile Number"/>
                    </div>
                </div>
            </div>
            <div class="clearing50"></div>
            <div class="row-full align-center font-base font16">
                <input type="checkbox" name="" />
                <r> I have read, understood and aggree to the <a href="#" class="font-underline font-blue">terms and conditions</a> in creating a wishlist at iPengen</r>
            </div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="row-40">&nbsp;</div>
                <div class="row-20">
                    <button type="button" class="button-full bg-blue font-white font16 align-center">Save and Continue &nbsp;&nbsp;<r class="font12"><i class="fa fa-chevron-right"></i></r></button>
                </div>
                <div class="row-40">&nbsp;</div>
            </div>
        </div>
        <div class="clearing50"></div>
    </div>
</div>