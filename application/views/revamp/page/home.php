<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 27/01/2017
 * Time: 21:49
 */
?>
<link href="<?php echo base_url(); ?>assets/revamp/css/pages/home.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/revamp/js/home.js"></script>

<!-- SLIDER CONTENT -->
<div class="row-full slider">
    <div class="container height-full">
        <div class="row-full height-full">
            <div class="row-70 mobile-full tablet-full slider-box height-full overflow-hidden">
                <div class="row-full slider-container height-full" data-counter="1" data-maxslide="3">
                    <div class="slider-item slider-large bg-slider slide-1">
                        <div class="slider-content">
                            <div class="clearing20 mobile-on"></div>
                            <div class="row-full font-white font40 font-light align-center-mobile">SHARE THE THINGS YOU LOVE!</div>
                            <div class="clearing20 mobile-off"></div>
                            <div class="clearing10"></div>
                            <div class="row-full font-white font16 font-light align-center-mobile">iPengen offer gift that encourage people to sharing their surroundings. a simple solution option for birthday, holiday, and wedding presents, or any occasion that calls for the right gift.</div>
                            <div class="clearing20"></div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="row-full">
                                <div class="button-link mobile-30 bg-base font-white font14 margin-center-mobile">START NOW</div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item slider-large bg-slider slide-2">
                        <div class="slider-content">
                            <div class="clearing20 mobile-on"></div>
                            <div class="row-full font-white font40 font-light align-center-mobile">SHARE THE THINGS YOU LOVE!</div>
                            <div class="clearing20 mobile-off"></div>
                            <div class="clearing10"></div>
                            <div class="row-full font-white font16 font-light align-center-mobile">iPengen offer gift that encourage people to sharing their surroundings. a simple solution option for birthday, holiday, and wedding presents, or any occasion that calls for the right gift.</div>
                            <div class="clearing20"></div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="row-full">
                                <div class="button-link mobile-30 bg-base font-white font14 margin-center-mobile">START NOW</div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-item slider-large bg-slider slide-3">
                        <div class="slider-content">
                            <div class="clearing20 mobile-on"></div>
                            <div class="row-full font-white font40 font-light align-center-mobile">SHARE THE THINGS YOU LOVE!</div>
                            <div class="clearing20 mobile-off"></div>
                            <div class="clearing10"></div>
                            <div class="row-full font-white font16 font-light align-center-mobile">iPengen offer gift that encourage people to sharing their surroundings. a simple solution option for birthday, holiday, and wedding presents, or any occasion that calls for the right gift.</div>
                            <div class="clearing20"></div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="row-full">
                                <div class="button-link mobile-30 bg-base font-white font14 margin-center-mobile">START NOW</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-full slider-pagination">
                    <div id="slider-page-0" class="slider-pagination-box first bg-base"></div>
                    <div id="slider-page-1" class="slider-pagination-box bg-grey"></div>
                    <div id="slider-page-2" class="slider-pagination-box bg-grey"></div>
                </div>
            </div>
            <div class="row-30 mobile-full tablet-full height-full">
                <div class="highlight-medium mobile-full tablet-half bg-highlight highlight-1">
                    <div class="highlight-item">
                        <div class="row-full highlight-item-image">
                            <img src="<?php echo base_url(); ?>assets/revamp/img/ipengen_cash_icon.png" />
                        </div>
                        <div class="row-full highlight-item-text font-white font14 mobile-font14 font-light align-center">
                            Whatever you wish, iPengen also has the perfectly unique cash gift funds for anything you can think of.
                        </div>
                        <div class="row-full highlight-item-link align-center">
                            <a class="font-white font12 font-light font-underline-hover" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="highlight-small mobile-full tablet-half bg-highlight highlight-2">
                    <a class="highlight-item-link font14 font-white align-center" href="#"><span>HOME COLLECTIONS</span></a>
                </div>
                <div class="highlight-small mobile-full tablet-half bg-highlight highlight-3">
                    <a class="highlight-item-link font14 font-white align-center" href="#"><span>MOST WANTED FASHION</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SLIDER CONTENT -->
<div class="clearing20"></div>
<div class="row-full">
    <div class="container">
        <div class="row-full font32 mobile-font20 tablet-font20 font-light font-base align-center"><?php echo $this->lang->line('home_big_title'); ?></div>
        <div class="clearing20"></div>
        <div class="row-full bg-light-base bg-white-mobile">
            <div class="row-full">
                <?php
                    //print_r($eventcategory);
                    foreach($eventcategory as $item){
                        echo '<div class="row-12 mobile-25 tablet-25">';
                        echo '<div id="adjusted-height-sample-1" class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-1">';
                        echo '<div class="row-full height-full overflow-hidden">';
                        echo '<img class="image-full" src="'.$this->config->item('image_display_path').'eventcategory/'.$item->id.'/400-400/'.$item->image_name.'">';
                        echo '</div>';
                        echo '<div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">';
                        echo '<a href="'.base_url().'create-a-wishlist?cat='.$item->event_name.'" class="centering font-white font14 align-center"><span>'.$item->event_name.'</span></a>';
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                    }
                ?>
            </div>
            <div class="clearing20"></div>
            <div class="row-full font24 tablet-font20 font-light font-base align-center mobile-off"><?php echo $this->lang->line('home_sub_title_web'); ?></div>
            <div class="row-full mobile-font16 tablet-font16 font-light font-base align-center mobile-on"><?php echo $this->lang->line('home_sub_title_mobile'); ?></div>
            <div class="clearing20"></div>
            <div class="row-full">
                <div class="container mobile-full">
                    <div class="row-full product-highlight-container">
                        <div class="product-highlight-large mobile-full">
                            <div class="row-full product-highlight-large-image" style="background-image: url('<?php echo $fproducts[0]->product_image_url; ?>') "></div>
                            <div class="row-60 product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-1" class="row-full product-highlight-options-items" data-counter="1">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[0]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[0]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="1">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-large-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[0]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[0]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[0]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-medium mobile-full">
                            <div class="row-full product-highlight-medium-image" style="background-image: url('<?php echo $fproducts[1]->product_image_url; ?>') "></div>
                            <div class="row-25 product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-2" class="row-full product-highlight-options-items" data-counter="2">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[1]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[1]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="2">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-medium-content bg-white overflow-hidden">
                                <div class="padding-box mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[1]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-70 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[1]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-30 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[1]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-small mobile-full">
                            <div class="row-full product-highlight-small-image" style="background-image: url('<?php echo $fproducts[2]->product_image_url; ?>') "></div>
                            <div class="row-half product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-3" class="row-full product-highlight-options-items" data-counter="3">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[2]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[2]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="3">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-small-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[2]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-60 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[2]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-40 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[2]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-small mobile-full">
                            <div class="row-full product-highlight-small-image" style="background-image: url('<?php echo $fproducts[3]->product_image_url; ?>') "></div>
                            <div class="row-half product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-4" class="row-full product-highlight-options-items" data-counter="4">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[3]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[3]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="4">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-small-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[3]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-60 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[3]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-40 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[3]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-full product-highlight-container">
                        <div class="product-highlight-large float-right mobile-full">
                            <div class="row-full product-highlight-large-image" style="background-image: url('<?php echo $fproducts[4]->product_image_url; ?>') "></div>
                            <div class="row-60 product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-5" class="row-full product-highlight-options-items" data-counter="5">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[4]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[4]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="5">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-large-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[4]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-half mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[4]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-half mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[4]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-small mobile-full">
                            <div class="row-full product-highlight-small-image" style="background-image: url('<?php echo $fproducts[5]->product_image_url; ?>') "></div>
                            <div class="row-half product-highlight-options float-right  mobile-half tablet-half">
                                <div id="product-options-item-6" class="row-full product-highlight-options-items" data-counter="6">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[5]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[5]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="6">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-small-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[5]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-60 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[5]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-40 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[5]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-small mobile-full">
                            <div class="row-full product-highlight-small-image" style="background-image: url('<?php echo $fproducts[6]->product_image_url; ?>') "></div>
                            <div class="row-half product-highlight-options float-right  mobile-half tablet-half">
                                <div id="product-options-item-7" class="row-full product-highlight-options-items" data-counter="7">
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[6]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[6]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="7">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-small-content bg-white">
                                <div class="padding-box-large mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[6]->product_name_short; ?><</div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-60 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[6]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-40 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[6]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-highlight-medium mobile-full">
                            <div class="row-full product-highlight-medium-image" style="background-image: url('<?php echo $fproducts[7]->product_image_url; ?>') "></div>
                            <div class="row-25 product-highlight-options float-right mobile-half tablet-half">
                                <div id="product-options-item-8" class="row-full product-highlight-options-items" data-counter="8">
                                    <div class="row-full product-highlight-options-item bg-secondary">

                                        <a href="javascript:void(0)" class="align-center font14 font-white centering line-no-space font-underline-hover link-home-action" data-action="<?php echo $add_to_wishlist_action; ?>" data-id="<?php echo  $fproducts[7]->product_id; ?>"><span><?php echo strtoupper($this->lang->line('add_wishlist')); ?></span></a>
                                    </div>
                                    <div class="row-full product-highlight-options-item bg-secondary">
                                        <a href="<?php echo $fproducts[7]->product_url; ?>" class="align-center font14 font-white centering line-no-space font-underline-hover"><span><?php echo strtoupper($this->lang->line('buy_for_myself')); ?></span></a>
                                    </div>
                                </div>
                                <div class="row-full bg-base product-highlight-options-button product-options" data-counter="8">
                                    <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span><?php echo strtoupper($this->lang->line('i_want_to')); ?></span></a>
                                </div>
                            </div>
                            <div class="row-full product-highlight-medium-content bg-white">
                                <div class="padding-box mobile-full mobile-no-padding">
                                    <div class="clearing20 tablet-off"></div>
                                    <div class="clearing10 tablet-on"></div>
                                    <div class="clearing5"></div>
                                    <div class="row-full font-bold font-base font16 product-highlight-title-tablet"><?php echo $fproducts[7]->product_name_short; ?></div>
                                    <div class="clearing10"></div>
                                    <div class="row-full">
                                        <div class="row-70 mobile-half tablet-full font-base font-light font14 product-highlight-content-tablet"><?php echo $fproducts[7]->short_description; ?></div>
                                        <div class="clearing10 tablet-on"></div>
                                        <div class="row-30 mobile-half tablet-full font-base font-bold font14 align-right align-left-tablet">IDR <?php echo number_format($fproducts[7]->price, 2); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing50 mobile-off"></div>
        </div>
        <div class="clearing20"></div>
        <div class="row-full font24 mobile-font20 tablet-font20 font-base align-center font-bold"><?php echo $this->lang->line('home_categories_title_web'); ?></div>
        <div class="clearing20"></div>
        <div class="row-full">
            <?php
                //print_r($categories);
                foreach($categories as $item){
                    echo '<div class="row-12 mobile-25 tablet-25">';
                    echo '<div id="adjusted-height-sample-2" class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">';
                    echo '<div class="row-full height-full overflow-hidden">';
                    echo '<img class="image-full" src="'.$this->config->item('image_display_path').'category/'.$item->banner.'">';
                    echo '</div>';
                    echo '<div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">';
                    echo '<a href="'.base_url().'search#&categoryid='.$item->id.'" class="row-full height-full font-white font14 align-center">';
                    echo '<div class="icon-square-auto icon-category-mobile"></div>';
                    echo '<div class="row-full">'.$item->name.'</div>';
                    echo '</a>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                }

            ?>
            <!--
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_camera.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-camera"></div>
                            <div class="row-full">Cameras</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_notebook.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-notebook"></div>
                            <div class="row-full">Computers & Laptops</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_tv.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-tv"></div>
                            <div class="row-full">TV, AV & Games</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_homeappliances.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-homeappliances"></div>
                            <div class="row-full">Home Appliances</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_homeliving.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-homeliving"></div>
                            <div class="row-full">Home & Living</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_babytoddler.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-baby"></div>
                            <div class="row-full">Baby & Toddler</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_toysgames.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-toys"></div>
                            <div class="row-full">Toys & Games</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_menfashion.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-menfashion"></div>
                            <div class="row-full">Men's Fashion</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_womenfashion.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-womenfashion"></div>
                            <div class="row-full">Women's Fashion</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_sportoutdoor.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-sport"></div>
                            <div class="row-full">Sports and Outdoors</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_honeymoon.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-honeymoon"></div>
                            <div class="row-full">Honeymoon</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_travelluggage.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-luggage"></div>
                            <div class="row-full">Travel & Luggage</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_healthbeauty.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-health"></div>
                            <div class="row-full">Health & Beauty</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_weddingcatering.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-catering"></div>
                            <div class="row-full">Wedding Catering</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_automotives.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-automotives"></div>
                            <div class="row-full">Automotives</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_jewelry.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-jewelry"></div>
                            <div class="row-full">Jewelry</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_weddingvenue.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-wedding"></div>
                            <div class="row-full">Wedding Venue</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg__category_photostudio.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-photo"></div>
                            <div class="row-full">Photo Studio</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row-12 mobile-25 tablet-25">
                <div class="margin-side adjust-height overflow-hidden event-item image-zoom" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/category/bg_category_tourtravel.png">
                    </div>
                    <div class="row-full height-full bg-transparent-black event-item-show image-zoom-text">
                        <a href="#" class="row-full height-full font-white font14 align-center">
                            <div class="icon-square-auto icon-category-tour"></div>
                            <div class="row-full">Tour & Travel</div>
                        </a>
                    </div>
                </div>
            </div>
            -->
            <div class="row-50 mobile-full tablet-full">
                <div class="margin-side-large adjust-height overflow-hidden" data-source="adjusted-height-sample-2">
                    <div class="row-full height-full overflow-hidden">
                        <a href="#" class="row-full height-full">
                            <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/banner_iklan.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearing20"></div>
        <div class="row-full font24 mobile-font20 tablet-font20 font-base align-center font-bold"><?php echo $this->lang->line('home_tour_travel_title_web'); ?></div>
        <div class="clearing20"></div>
        <div class="row-full">
            <?php
                //print_r($blogdata);
                foreach($blogdata as $item){
                    echo '<div class="row-25 mobile-full tablet-half">';
                    echo '<div class="margin-side mobile-full">';
                    echo '<div id="adjust-height-package" class="row-full adjust-height overflow-hidden" data-source="adjust-height-package">';
                    echo '<img class="image-full" src="'.$this->config->item('image_display_path').'/blog/'.$item->blog_id.'/'.$item->blog_image.'" />';
                    echo '</div>';

                    /*
                    echo '<div class="row-half product-highlight-options float-right">';
                    echo '<div id="product-options-item-9" class="row-full product-highlight-options-items" data-counter="9">';
                    echo '<div class="row-full product-highlight-options-item bg-secondary">';
                    echo '<a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>ADD TO WISHLIST</span></a>';
                    echo '</div>';
                    echo '<div class="row-full product-highlight-options-item bg-secondary">';
                    echo '<a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>BUY FOR MYSELF</span></a>';
                    echo '</div>';
                    echo '</div>';
                    echo '<div class="row-full bg-base product-highlight-options-button product-options" data-counter="9">';
                    echo '<a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span>I WANT TO</span></a>';
                    echo '</div>';
                    echo '</div>';
                    */
                    echo '<div class="row-full">';
                    echo '<div class="padding-box mobile-full mobile-no-padding">';
                    echo '<div class="clearing20 mobile-off tablet-off"></div>';
                    echo '<div class="clearing10 mobile-on tablet-on"></div>';
                    echo '<div class="row-full product-title font-bold font-base font16">'.$item->blog_title.'</div>';
                    echo '<div class="clearing10 mobile-off"></div>';
                    echo '<div class="clearing5 mobile-on"></div>';
                    echo '<div class="row-full">';
                    //echo '<div class="row-half font-base font-light font14">'.$item->blog_description.'</div>';
                    //echo '<div class="row-half font-base font-bold font14 align-right">IDR 29.000.000</div>';
                    $max_length = 45;
                    $description = $item->blog_description;
                    if(strlen($description) > $max_length){
                        $max_length -= 3;
                        $description = substr($description, 0, $max_length).'...';
                    }
                    echo '<div class="row-60 font-base font-light font14">'.$description.'</div>';
                    echo '<div class="row-40">';
                    echo '<a href="'.base_url('blog/~'.$item->blog_id.'-'.$item->blog_auto_title).'" class="button-link-normal bg-base font-white font14 float-right">'.$this->lang->line('read').'</a>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '<div class="clearing10 mobile-on"></div>';
                }
            ?>

            <!--
            <div class="row-25 mobile-full tablet-half">
                <div class="margin-side mobile-full">
                    <div class="row-full adjust-height overflow-hidden" data-source="adjust-height-package">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/product/product5_square.png" />
                    </div>
                    <div class="row-half product-highlight-options float-right">
                        <div id="product-options-item-10" class="row-full product-highlight-options-items" data-counter="10">
                            <div class="row-full product-highlight-options-item bg-secondary">
                                <a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>ADD TO WISHLIST</span></a>
                            </div>
                            <div class="row-full product-highlight-options-item bg-secondary">
                                <a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>BUY FOR MYSELF</span></a>
                            </div>
                        </div>
                        <div class="row-full bg-base product-highlight-options-button product-options" data-counter="10">
                            <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span>I WANT TO</span></a>
                        </div>
                    </div>
                    <div class="row-full">
                        <div class="padding-box mobile-full mobile-no-padding">
                            <div class="clearing20 mobile-off tablet-off"></div>
                            <div class="clearing10 mobile-on tablet-on"></div>
                            <div class="row-full product-title font-bold font-base font16">7 Days Japan Honeymoon Package</div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="clearing5 mobile-on"></div>
                            <div class="row-full">
                                <div class="row-half font-base font-light font14">Plan your honeymoon in Japan with Anta Tour</div>
                                <div class="row-half font-base font-bold font14 align-right">IDR 59.500.000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing10 mobile-on tablet-on"></div>
            <div class="row-25 mobile-full tablet-half">
                <div class="margin-side mobile-full">
                    <div class="row-full adjust-height overflow-hidden" data-source="adjust-height-package">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/product/product6_square.png" />
                    </div>
                    <div class="row-half product-highlight-options float-right">
                        <div id="product-options-item-11" class="row-full product-highlight-options-items" data-counter="11">
                            <div class="row-full product-highlight-options-item bg-secondary">
                                <a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>ADD TO WISHLIST</span></a>
                            </div>
                            <div class="row-full product-highlight-options-item bg-secondary">
                                <a href="#" class="align-center font14 font-white centering line-no-space font-underline-hover"><span>BUY FOR MYSELF</span></a>
                            </div>
                        </div>
                        <div class="row-full bg-base product-highlight-options-button product-options" data-counter="11">
                            <a onclick="" class="cursor-pointer align-center font14 font-white centering line-no-space"><span>I WANT TO</span></a>
                        </div>
                    </div>
                    <div class="row-full">
                        <div class="padding-box mobile-full mobile-no-padding">
                            <div class="clearing20 mobile-off tablet-off"></div>
                            <div class="clearing10 mobile-on tablet-on"></div>
                            <div class="row-full product-title font-bold font-base font16">8 Day Italy Photo Tour by Smilling Tour</div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="clearing5 mobile-on"></div>
                            <div class="row-full">
                                <div class="row-half font-base font-light font14">A Cultural journey, capturing and enriching all of the...</div>
                                <div class="row-half font-base font-bold font14 align-right">IDR 79.000.000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-25 mobile-full tablet-half">
                <div class="margin-side mobile-full">
                    <div class="row-full adjust-height overflow-hidden" data-source="adjust-height-package">
                        <img class="image-full" src="<?php echo base_url(); ?>assets/revamp/img/product/product7_square.png" />
                    </div>
                    <div class="row-full">
                        <div class="padding-box mobile-full mobile-no-padding">
                            <div class="clearing20 mobile-off tablet-off"></div>
                            <div class="clearing10 mobile-on tablet-on"></div>
                            <div class="row-full product-title font-bold font-base font16">12 Steps To Creating A Minimalist Home</div>
                            <div class="clearing10 mobile-off"></div>
                            <div class="clearing5 mobile-on"></div>
                            <div class="row-full">
                                <div class="row-60 font-base font-light font14">My home may not be minimalist. My dream...</div>
                                <div class="row-40">
                                    <a href="#" class="button-link-normal bg-base font-white font14 float-right">READ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->
        </div>
        <div class="clearing50"></div>
    </div>
</div>
<div id="popup-addtowishlist" class="popup-xsmall">
    <div class="row-full bg-base">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full">
                <img class="login-logo" src="<?php echo base_url(); ?>assets/revamp/img/logo_nav.png" />
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
        </div>
    </div>
    <div class="row-full">
        <div class="container">
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
            <div class="row-full font16 font-base font-light align-center">
                <?php echo strtoupper($this->lang->line("select_Your_Wishlist")); ?>
            </div>
            <div class="clearing10 mobile-off"></div>
            <div class="clearing5 mobile-on"></div>
            <div class="row-full">
                <div class="row-full">
                    <div class="font-base align-center font14 wishlistmsg" style="display:none"><?php echo $this->lang->line('home_success_add_to_wishlist'); ?></div>
                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="row-full">
                    <select id="home-wishlist-id" class="font14 input-full">
                        <?php
                            foreach($mywishlist as $item){
                                echo '<option value="'.$item->id.'">'.$item->title.'</option>';
                            }
                        ?>
                    </select>
                    <input type="hidden" id="home-product-id" value="" />
                </div>
                <div class="clearing10 mobile-off"></div>
                <div class="clearing5 mobile-on"></div>
                <div class="clearing20 mobile-off"></div>
                <div class="clearing10 mobile-on"></div>
                <div class="row-full">
                    <div class="row-50 mobile-full">
                        <button type="button" class="button-link button-full button-full-mobile bg-base font-white font14 link-action" data-action="canceladdtowishlist"><i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("cancel") ?></button>
                    </div>
                    <div class="clearing10 mobile-on"></div>
                    <div class="row-50 mobile-full">
                        <button type="submit" class="button-link button-full button-full-mobile bg-blue font-white font14 float-right link-action" data-action="submitaddtowishlist"><i class="fa fa-send" aria-hidden="true"></i> &nbsp;<?php echo $this->lang->line("submit") ?></button>
                    </div>
                </div>
            </div>
            <div class="clearing20 mobile-off"></div>
            <div class="clearing10 mobile-on"></div>
        </div>
    </div>
</div>