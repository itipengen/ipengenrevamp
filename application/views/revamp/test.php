<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 27/01/2017
 * Time: 20:23
 */

print_r($featuredproduct);
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iPengen - Home</title>

    <link href="assets/revamp/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/revamp/css/pages/home.css" rel="stylesheet" type="text/css" />
    <link href="assets/revamp/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script src="assets/revamp/js/jquery-3.1.1.js"></script>
    <script src="assets/revamp/js/base.js"></script>
    <script src="assets/revamp/js/home.js"></script>
</head>
