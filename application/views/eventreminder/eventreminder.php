<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding"> 
        <!--Original sidebar-->
        <?php $this->load->view('common/leftsidebar')?>
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <?php if($this->session->flashdata('successmessage')){ ?>
		<div class="alert alert-success msg" style="margin-top:10px;"><?php echo $this->session->flashdata('successmessage'); ?></div>
	 	<?php } ?>
        <div class="dashboard-header"><?php echo $this->lang->line("event_reminder") ?>
          <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#AddEvent" aria-expanded="false" aria-controls="AddEvent"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo $this->lang->line("event_add") ?></button>
        </div>
        <div class="row"> 
          
          <!--Event Form-->
          <div class="collapse" id="AddEvent">
            <div class="alert alert-warning">
              <?php $attributes = array('class' => 'form-horizontal', 'id' => 'eventform','method' => 'post' ,'role' =>'form');

				echo form_open('reminder', $attributes);?>
              <div class="linerows">
                <div class="listform">
                  <?php  echo form_input ('event_name',set_value('event_name'),'placeholder="'.$this->lang->line("event_add_name").'" 
							class="form-control" id="event_name" required="required"'); ?>
                  <div class="css-error">
                    <?= form_error('event_name'); ?>
                  </div>
                </div>
                <div class="listform">
                  <?php  echo form_input ('eventdate',set_value('eventdate'),'placeholder="'.$this->lang->line("event_add_date").'" 
							class="form-control" id="eventdate" required="required"'); ?>
                  <div class="css-error">
                    <?= form_error('eventdate'); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="listform">
                      <?php
							$options=array(
							//''=>$this->lang->line("event_repeat_1"),
							'0'=>$this->lang->line("event_repeat_2"),
							'7'=>$this->lang->line("event_repeat_3"),
							'14'=>$this->lang->line("event_repeat_4"),
							'30'=>$this->lang->line("event_repeat_5"),
							'365'=>$this->lang->line("event_repeat_6")
							);

 echo form_dropdown('repeatevent', $options,set_value('repeatevent'), 'class="form-control" id="repeatevent" required="required"'); ?>
                      <div class="css-error">
                        <?= form_error('repeatevent'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="listForm">
                      <?php
							$remindmeoptions=array(
								''=>$this->lang->line("event_remind_1"),
								'3'=>$this->lang->line("event_remind_2"),
								'7'=>$this->lang->line("event_remind_3"),
								'14'=>$this->lang->line("event_remind_4"),
								'30'=>$this->lang->line("event_remind_5"),
							);

 echo form_dropdown('remindme', $remindmeoptions,set_value('remindme'), 'class="form-control" id="remindme" required="required"'); ?>
                      <div class="css-error">
                        <?= form_error('remindme'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-info"><i class="fa fa-check-square" aria-hidden="true"></i> <?php echo $this->lang->line("event_save_btn") ?> </button>
                  </div>
                </div>
                <!--EOF Row--> 
              </div>
              <!--EOF allRow--> 
              <?php echo form_close();?> </div>
            <!--EOF alert warning--> 
          </div>
          <!--EOF Collapse--> 
        </div>
        <!--EOF Event Form--> 
        
        <!--start listing event-->
        <?php if(!empty($event_list)){
		foreach($event_list as $val){ 
		?>
			<div class="linerows">
			  <div class="row row-eq-height">
				<div class="col-xs-2 col-md-1 dateblock"><span class="datedate">
				  <?php $date=date_create($val->event_date);echo ' '.date_format($date,"d");?>
				  </span></br>
				  <span class="datemonth">
				  <?php $date=date_create($val->event_date);echo date_format($date,"M");?>
				  </span></div>
				<div class="col-xs-8 col-md-10 eventblock"><?php echo $val->event_name;?></div>
				<div class="col-xs-2 col-md-1 buttonblock text-center">
				  <button type="button" class="btn btn-link editclass" data-toggle="modal" data-target="#editModal" data-eid="<?php echo $val->event_id ; ?>" data-ename="<?php echo $val->event_name ; ?>"  data-edate="<?php echo $val->event_date ; ?>" data-eremind="<?php echo $val->event_remainder ; ?>" data-repeat="<?php echo $val->event_repeat ; ?>"
	   id="e<?php echo $val->event_id ; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>
				  <button type="button" id="<?php echo $val->event_id ?>" class="btn btn-link deletecls"><i class="fa fa-times" aria-hidden="true"></i></button>
				</div>
			  </div>
			</div>
			<!--EOF listing event-->
			<?php } }else{
				 echo $this->lang->line("empty_eventremainder");
			}?>
        <!--start listing event--> 
        
      </div>
    </div>
    <!--Eof Right Column--> 
  </div>
  <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line("event_edit") ?></h4>
        </div>
        <div class="modal-body">
          <div class="" id="successmsg"></div>
          <div class="" id="errormsg"></div>
          <div class="alert alert-warning">
            <?php $attributes = array('class' => 'form-horizontal', 'id' => 'editeventform','method' => 'post' ,'role' =>'form');

				echo form_open('eventreminder/edit', $attributes);?>
            <div class="linerows">
              <div class="listform">
                <?php  echo form_input ('event_name_edit',set_value('event_name_edit'),'placeholder="'.$this->lang->line("event_add_name").'" 
							class="form-control" id="event_name_edit" required="required"'); ?>
                <div class="css-error">
                  <?= form_error('event_name_edit'); ?>
                </div>
              </div>
              <div class="listform">
                <?php  echo form_input ('eventdateedit',set_value('eventdateedit'),'placeholder="'.$this->lang->line("event_add_date").'" 
							class="form-control" id="eventdateedit" required="required"'); ?>
                <div class="css-error">
                  <?= form_error('eventdate'); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="listform">
                    <?php
						$options=array(
							''=>$this->lang->line("event_repeat_1"),
							'0'=>$this->lang->line("event_repeat_2"),
							'7'=>$this->lang->line("event_repeat_3"),
							'14'=>$this->lang->line("event_repeat_4"),
							'30'=>$this->lang->line("event_repeat_5"),
							'365'=>$this->lang->line("event_repeat_6")
						);

 echo form_dropdown('editrepeatevent', $options,set_value('editrepeatevent'), 'class="form-control" id="editrepeatevent" required="required"'); ?>
                    <div class="css-error">
                      <?= form_error('editrepeatevent'); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="listForm">
                    <?php
							$remindmeoptions=array(
								''=>$this->lang->line("event_remind_1"),
								'3'=>$this->lang->line("event_remind_2"),
								'7'=>$this->lang->line("event_remind_3"),
								'14'=>$this->lang->line("event_remind_4"),
								'30'=>$this->lang->line("event_remind_5"),
								);

 echo form_dropdown('eremindme', $remindmeoptions,set_value('eremindme'), 'class="form-control" id="eremindme" required="required"'); ?>
                    <div class="css-error">
                      <?= form_error('remindme'); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-4"></div>
              </div>
              <!--EOF Row--> 
            </div>
            <!--EOF allRow--> 
            <?php echo form_hidden('eid','','id="eid"'); ?> <?php echo form_close();?> </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-info" id="updateevent"><i class="fa fa-check-square" aria-hidden="true"></i> <?php echo $this->lang->line("event_update_btn") ?> </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--container White--> 

<script>

$(document).ready(function(){
	HOSTNAME = base_url;
	var loader = HOSTNAME +'assets/frontend/img/ripple.gif';
   	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="100px"></div></div></div>';
	$('.msg').delay(5000).fadeOut('slow');
	
	$("#eventdate").datepicker({
    format: "yyyy-mm-dd",
    weekStart: 1,
   
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
   
    endDate : new Date('2050-09-08'),
});
$("#eventdateedit").datepicker({
    format: "yyyy-mm-dd",
    weekStart: 1,
   
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
   
    endDate : new Date('2050-09-08'),
});

 	$(document).on('click','.deletecls',function(){
		var eventid=$(this).attr('id');
		 if(eventid!=""){
			bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					
					window.location.href = "<?php echo base_url()?>eventreminder/delete/"+eventid; 
				}
			}); 
		 }
	});
	
	$('.editclass').on('click',function(){
		$('input[name=eid]').val($(this).data('eid'));
		$('#eventdateedit').val($(this).data('edate'));
		$('#event_name_edit').val($(this).data('ename'));
		$('#eremindme').val($(this).data('eremind'));
		$('#editrepeatevent').val($(this).data('repeat'));
		});
		
		$('#updateevent').on('click',function(){
			var data=$('#editeventform').serialize();
			$.ajax({
			method: "POST",
			url: "<?php echo base_url();?>eventreminder/edit",
			data: data,
			beforeSend: function(){
	    			$("body").append(lhtml);
	   		},
			success: function (result) {
			$('#loaderBg2').remove();
			if(result=='success'){
				$("#successmsg").text("Event updated successfully");
				window.location.href = "<?php echo base_url()?>eventreminder";
			}
			if(result=='error'){
				
				$("#errormsg").text("All field are Required");
			}
			}
			});
			});
});

</script> 
