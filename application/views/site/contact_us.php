<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyxFVJEm9BK5W-0nCjp6yhMgNWkMl2Mtw &callback=initMap"
 type="text/javascript"></script>

<div id="content">
  <div class="container white" style="margin-bottom: 30px;"> <?php echo $map['js']; ?> <?php echo $map['html'];?>
    <div class="linerows">
      <h3>Contact US</h3>
      <?php  if($this->session->flashdata('msg')) {?>
      <div class="alert alert-danger display-hide"><?php echo $this->session->flashdata('msg');?>
        <button class="close" data-close="alert"></button>
      </div>
      <?php }?>
      <?php  if($this->session->flashdata('smsg')) {?>
      <div class="alert alert-success"><?php echo $this->session->flashdata('smsg');?>
        <button class="close" data-close="alert"></button>
      </div>
      <?php }?>
      <?php echo form_open('contact-us#'); ?>
      <div class="linerows">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"firstName","id"=>"firstName", "placeholder"=>$this->lang->line("f_name"),
				"class"=>"form-control", 'required' => 'required')); ?>
                <div class="css-error"><?php echo form_error('firstName'); ?></div>
                </br>
              </div>
            </div>
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"lastName","id"=>"lastName","placeholder"=>$this->lang->line("l_name"),"class"=>"form-control", 'required' => 'required')); ?>
                <div class="css-error"><?php echo form_error('lastName'); ?></div>
                </br>
              </div>
            </div>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"mobile","id"=>"Mobile", "placeholder"=> $this->lang->line("w_event_contact")." +62 xxx...","maxlength"=>"50","class"=>"form-control", 'required' => 'required')); ?>
            <div class="css-error"> <?php echo form_error('mobile'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"email","type"=>"email","id"=>"email","placeholder"=>"Email ID","class"=>"form-control", 'required' => 'required')); ?> </div>
           <div class="css-error"> <?php echo form_error('email'); ?></div>
            </br>
           
          <div class="listform"> <?php echo form_input(array("name"=>"address","id"=>"Address", "placeholder"=>$this->lang->line("w_event_add"),"class"=>"form-control", 'required' => 'required')); ?>
            <div class="css-error"><?php echo form_error('address'); ?></div>
            </br>
          </div>
          <div class="listform">
            <?php
		    $options = array(
                  'Order Delivery'  => 'Order Delivery',
                  'Offer Redemption'    => 'Offer Redemption',
                  'Order Payment/refund'   => 'Order Payment/refund',
                  'Problem With Item Received/Return' => 'Problem With Item Received/Return',
				  'Post Delivery Services' => 'Post Delivery Services',
				  'Change Order' => 'Change Order',
				  'Other' => 'Other',
				  
                );
		   
		   echo form_dropdown(array("name"=>"subject","class"=>"form-control",'required' => 'required'),$options); ?>
            <div class="css-error"><?php echo form_error('city'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_textarea(array("name"=>"comment_question","id"=>"City","value"=>"", "placeholder"=>"Write your Comment and Questions...","class"=>"form-control",'required' => 'required')); ?>
            <div class="css-error"><?php echo form_error('city'); ?></div>
            </br>
          </div>
          <button class="btn btn-primary addrsclass" type="submit" value="Submit" >Submit </button>
        </div>
        <?php echo form_close(); ?>
        <div id="basket" style="position:absolute; right:200px;">
          <table>
            <tr>
              <td><h3>Opening Hours </h3></td>
            </tr>
            <tr>
              <td><p> <strong>Monday to Friday:</strong> 10am – 6pm <br>
                  <strong>Saturday:</strong> 10 am – 4pm <br>
                  <strong>Sunday: </strong> Closed </p></td>
            </tr>
            <?php if(!empty($company_address)){ ?>
            <tr>
              <td><h3>Address</h3></td>
            </tr>
            <tr>
              <td><p> <i class="omsc-icon fa fa-map-marker omsc-icon-color-theme omsc-with-border omsc-border-color-theme" style=""></i> <?php echo $company_address[0]->place;?> </p>
                <p> <i class="omsc-icon fa fa-globe omsc-icon-color-theme omsc-with-border omsc-border-color-theme" style=""></i><?php echo $company_address[0]->web;?></p>
                <p> <i class="omsc-icon fa fa-envelope-o omsc-icon-color-theme omsc-with-border omsc-border-color-theme" style=""></i><?php echo $company_address[0]->mail ;?>  </p>
                <p> <i class="omsc-icon fa fa-phone omsc-icon-color-theme omsc-with-border omsc-border-color-theme" style=""></i><?php echo $company_address[0]->phone;  ?></p>
                <p> <i class="" style=""></i> <b>Fax</b> <?php echo $company_address[0]->fax;?></p></td>
            </tr>
            <?php } ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
