<div id="content">
            <div class="container white">
            <div class="row">
<!--Side Bar-->  <div class="col-md-3 nopadding">
<?php $this->load->view('common/leftsidebar')?>

<!--Eof original sidebar-->
</div><!-- Right Column-->
<div class="col-md-9">
<div class="dashboard-header"><?= $this->lang->line("cash_gift") ?> </div>
<?php 
if(!empty ($cashgiftlist)){
foreach ($cashgiftlist as $giftlist){
	$sDate = new DateTime($giftlist->date_added);
	$send['date']=$sDate->format('d-m-Y');
	
		$photopath = $this->config->item('image_display_path');
		$photopath_absolute = $this->config->item('image_path');
		$thumb_size= $this->config->item('thumb_size');
		
		$photopath_is_check =$photopath_absolute."wishlist/".$giftlist->id;
		$thumpath = $photopath."product/".$giftlist->id.$thumb_size;
		
		if(is_file($this->config->item('image_path').'/wishlist/'.$giftlist->id.$thumb_size.$giftlist->wishlist_image)){
		$imgpurl=$this->config->item('image_display_path').'wishlist/'.$giftlist->id.$thumb_size.$giftlist->wishlist_image ;
		}  else{
		$imgpurl= $this->config->item('image_display_path').'wishlist/no-event.png';
		}

                                       
	?>
<div class="allrows">
            <h4><?= $this->lang->line("order") ?> #<?php echo $giftlist->order_id?> </h4>
                        <p>You received <a href="<?php echo base_url();?>~<?php echo $giftlist->url; ?>"><?php echo $giftlist->title?></a> - <?= $this->lang->line("cash_gift_from") ?> <a href="<?php echo base_url();?>u/<?php echo $giftlist->sender_id;?>"><?php echo $giftlist->fname?> <?php echo $giftlist->lname?></a> <?= $this->lang->line("on_for") ?> <strong><?php echo $send['date']; ?></strong>.                        </p>
                    <hr>
                        <div class="row">
                           
  <div class="alert alert-warning">
  <div class="row">
  <div class="col-md-4">
  
  <img src="<?php echo $imgpurl;?>" width="150" class="thumbnail">
  
  </div>
  <div class="col-md-4"><h5><?= $this->lang->line("sender") ?></h5>
                                <p><a href="<?php echo base_url();?>u/<?php echo $giftlist->sender_id;?>"><?php echo $giftlist->fname?> <?php echo $giftlist->lname?></a><br>
                                    <?php echo $giftlist->email?>
                                    <br>
                    <?= $this->lang->line("hidden_address") ?></p></div>
  <div class="col-md-4"><h2> <span class="label label-warning">Rp <?php echo number_format($giftlist->amount)?></span></h2></div>
  </div

                        ></div>
                        <!-- /.alert -->
                          
            </div>
                     
          </div>
<?php } }?>
</div><!--eof infobox for Dashboard-->


</div><!--Eof Right Column-->
</div>
            
            </div>