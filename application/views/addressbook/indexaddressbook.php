<div id="all">
  <div id="content">
    <div class="container white">
      <div class="row bluewhiteheader raleway"> Address Book </div>
      <hr/>
      <div class="row">
        <?php  if($this->session->flashdata('msgdel_success')) {?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('msgdel_success');?>
          <button class="close" data-close="alert"></button>
        </div>
        <?php }?>
        <?php  if($this->session->flashdata('msgdel_fail')) {?>
        <div class="alert alert-danger display-hide"><?php echo $this->session->flashdata('msgdel_fail');?>
          <button class="close" data-close="alert"></button>
        </div>
        <?php }?>
        <?php if (!empty($addressbookdetails)){
			$i=1;
			foreach($addressbookdetails as $addressbook){?>
        <div class="col-xs-6 col-md-3">
          <h4>Address <?php echo ' '.$i; ?> </h4>
          <div class="form-group">
            <label class="col-xs-4" for="fname" style="padding-left:0">First Name:</label>
            <label id="<?php echo 'fname'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->fname; ?>" type="fname"><?php echo $addressbook->fname; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="lname" style="padding-left:0">Last Name:</label>
            <label id="<?php echo 'lname'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->lname; ?>" type="lname"><?php echo $addressbook->lname; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="address" style="padding-left:0">Address:</label>
            <label id="<?php echo 'address'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->address; ?>" type="address"><?php echo $addressbook->address; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="address2" style="padding-left:0">Address2:</label>
            <label id="<?php echo 'address2'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->address2; ?>" type="address2"><?php echo $addressbook->address2; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="postcode" style="padding-left:0">postcode:</label>
            <label id="<?php echo 'postcode'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->postcode; ?>" type="postcode"><?php echo $addressbook->postcode; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="kecamatan" style="padding-left:0">Kecamatan:</label>
            <label id="<?php echo 'kecamatan'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->kecamatan; ?>" type="kecamatan"><?php echo $addressbook->kecamatan; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="city" style="padding-left:0">City:</label>
            <label id="<?php echo 'city'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->city; ?>" type="city"><?php echo $addressbook->city; ?></label>
            <div class="clearfix"> </div>
            <label class="col-xs-4" for="city" style="padding-left:0">Mobile No:</label>
            <label id="<?php echo 'mobile'.$addressbook->addressbook_id;?>" class="col-xs-6" style="background:#eee;pading:2px 0px"value="<?php echo $addressbook->mobile; ?>" type="mobile"><?php echo $addressbook->mobile; ?></label>
            <div class="clearfix"> </div>
            <label for="Edit"> </label>
            <a href="#" class="btn btn-primary addrsclass" addressbook_id="<?php echo $addressbook->addressbook_id ?>">Edit</a> <a href="#" title="Delete" id="remove_address" addressbook_id="<?php echo $addressbook->addressbook_id ?>"> <i class="fa fa-trash-o action-btn text-danger"></i> </a> </div>
        </div>
        <?php $i=$i+1; } } ?>
      </div>
      <hr/>
      <div class="row" id="checkout">
      <!-- /.row --> 
      <!--Shipping Address-->
      <?php // echo form_open("addressbook/addupdate_address") ?>
      <form role="form" id="addressbookurl_id"  method="post" action="<?php echo base_url();?>addressbook/addupdate_address">
        <div class="col-md-6">
          <h4> Add  Address </h4>
          <div class="row">
            <?php  if($this->session->flashdata('msg')) {?>
            <div class="alert alert-danger display-hide"><?php echo $this->session->flashdata('msg');?>
              <button class="close" data-close="alert"></button>
            </div>
            <?php }?>
            <?php  if($this->session->flashdata('smsg')) {?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('smsg');?>
              <button class="close" data-close="alert"></button>
            </div>
            <?php }?>
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"firstName","id"=>"firstName", "placeholder"=>$this->lang->line("f_name"),"value"=>(isset($shipfirstName)?$shipfirstName:set_value('firstName')),
				"class"=>"form-control",'required' => 'required')); ?>
                <div class="css-error"><?php echo form_error('firstName'); ?></div>
                </br>
              </div>
            </div>
            <div class="col-md-6">
              <div class="listform"> <?php echo form_input(array("name"=>"lastName","id"=>"lastName","value"=>(isset($shiplastName)?
			$shiplastName:set_value('lastName')),'required' => 'required',"placeholder"=>$this->lang->line("l_name"),"class"=>"form-control")); ?>
                <div class="css-error"><?php echo form_error('lastName'); ?></div>
                </br>
              </div>
            </div>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"address","id"=>"Address",'required' => 'required',"value"=>($user->ship_address1!='') ? $user->ship_address1 : 
   (isset($address) ? $address : set_value('address')), "placeholder"=>$this->lang->line("w_event_add"),"class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('address'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"address2","id"=>"Address2",'required' => 'required',"value"=>($user->ship_address2!='') ? $user->ship_address2 : 
			(isset($address2)?$address2:set_value('address2')), "placeholder"=>$this->lang->line("w_event_add_2"),"class"=>"form-control")); ?> </div>
          <div class="css-error"><?php echo form_error('address2'); ?></div>
          </br>
          <div class="listform"> <?php echo form_input(array("name"=>"postcode","id"=>"Postcode",'required' => 'required',"value"=>($user->ship_postcode!='') ? $user->ship_postcode :
			(isset($postcode)?$postcode:set_value('postcode')), "placeholder"=>$this->lang->line("w_event_postcode"),"size"=>"10","class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('postcode'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"kecamatan",'required' => 'required',"id"=>"Kecamatan","value"=>($user->ship_kecamatan!='') ? $user->ship_kecamatan :(isset($kecamatan)?$kecamatan:set_value('kecamatan')), "placeholder"=>$this->lang->line("kecamatan"),"class"=>"form-control")); ?>
            <div class="css-error"><?php echo form_error('kecamatan'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"city","id"=>"City",'required' => 'required',"value"=>"Jakarta Raya", "placeholder"=>$this->lang->line("w_event_city"),"class"=>"form-control","readonly" => "readonly")); ?>
            <div class="css-error"><?php echo form_error('city'); ?></div>
            </br>
          </div>
          <div class="listform"> <?php echo form_input(array("name"=>"mobile",'required' => 'required',"id"=>"Mobile","value"=>(isset($shipMobile)?
			$shipMobile:set_value('mobile')), "placeholder"=> $this->lang->line("w_event_contact")." +62 xxx...","maxlength"=>"50","class"=>"form-control")); ?>
            <div class="css-error"> <?php echo form_error('mobile'); ?></div>
            </br>
          </div>
        </div>
        <!--EOF shipping Address-->
        </div>
        <!--EOF Row-->
        <hr>
        <!--Shipping Method--> 
        <!--EOF Shipping Method-->
        <div class="row linerows text-center">
          <div class="listform"> <?php echo form_submit(array("name"=>"checkoutBtn","class"=>"btn btn-primary","value"=>'Save Address',"id"=>"checkoutBtn")); ?> 
            <!--<a href="<?php //echo site_url("checkout/payment"); ?>" class="btn btn-primary">Continue to Payment Method<i class="fa fa-chevron-right"></i> </a>--> 
          </div>
        </div>
        <!-- /.box -->
      </form>
      <input hidden="true" id="base_url_id"  value="<?php echo base_url();?>" />
    </div>
    <!-- /.col-md-9 --> 
    
  </div>
  <!-- /.container --> 
</div>
<script type="text/javascript">
/* To delete address*/
$(document).ready(function(){ 

$(document).on("click", "#remove_address", function(){
var addressbook_id = $(this).attr("addressbook_id");	
bootbox.confirm("Are you sure you want delete ?", function(result) {
	if(result){
	 window.location.href = "<?php echo base_url()?>addressbook/delete_address/"+addressbook_id; 
	}
});
});
/*for edit and add address*/
 
$(document).on("click", ".addrsclass", function(){
	var addressbook_id = $(this).attr("addressbook_id");
	var  base_url = $("#base_url_id").val();
	var  firstName = $("#fname"+addressbook_id).text();
    var lastName = $("#lname"+addressbook_id).text();
	var address = $("#address"+addressbook_id).text();
	var address2 = $("#address2"+addressbook_id).text();
	var postcode = $("#postcode"+addressbook_id).text();
	var kecamatan = $("#kecamatan"+addressbook_id).text();
	var city = $("#city"+addressbook_id).text();
	var mobile = $("#mobile"+addressbook_id).text();
	
	$('#addressbookurl_id').attr('action',base_url+"addressbook/addupdate_address/"+addressbook_id);
	$("#firstName").val(firstName);
	$("#lastName").val(lastName);
	$("#Address").val(address);
	$("#Address2").val(address2);
	$("#Postcode").val(postcode);
	$("#Kecamatan").val(kecamatan);
	$("#City").val(city);
	$("#Mobile").val(mobile);
	
});

})
</script>