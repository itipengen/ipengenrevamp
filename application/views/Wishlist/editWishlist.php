<?php include './PhpMd5Decrypter.inc.php';?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key= AIzaSyAog-FDBbkmoWJrDnPhTdyLJcLGh8XcuB4"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="<?php echo base_url()?>assets/js/placepicker.js"></script>
<script>
var autocomplete = [];
var els = [];
function initialize() {
    add_auto();
    /* options = {
      language: 'en-GB',
      types: ['(cities)'],
      componentRestrictions: { country: "uk" }
    }*/
}
function add_auto(){
    autocomplete.length = null;
    els.length = null;
    var $controls = $('#controls input[type=text]');
 //alert($controls);
    $controls.each(function(i,el){
        els[i] = el;
        autocomplete[i] = new google.maps.places.Autocomplete(el);
        google.maps.event.addListener(autocomplete[i], 'place_changed', function() {
           // infobox.close();
            var place = autocomplete[i].getPlace();
            if (!place.geometry) {
                return;
            }
        });
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
.imgloaderwait {
    border-radius: 2px;
    left: 32%;
    padding: 10px;
    position: absolute;
    top: 30%;
}
.imgwaitprocess {
    background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 9999;
}
.coverdisble {
    background: rgba(3, 3, 3, 0.5) none repeat scroll 0 0;
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1000000;
}
</style>
<div id="all">
        <div id="content">
            <div class="container">
<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </span>
                        </a>
                     <div class="tabtittle"> <?= $this->lang->line("your_event") ?></div>   
                    </li> 
                    <li role="presentation" class="disabled" id="tab2">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                               <i class="fa fa-pencil" aria-hidden="true"></i>
                            </span>
                        </a> 
                       <div class="tabtittle"> <?= $this->lang->line("personalize") ?></div>  
                    </li>
                    <li role="presentation" class="disabled" id="tab3">
                        <a href="#step3" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </a> 
                        <div class="tabtittle"> <?= $this->lang->line("ready") ?> </div>
                    </li>
                </ul>
            </div>
 
<div class="tab-content">
<div class="tab-pane active" role="tabpanel" id="step1">

<!--Titleheader--> 
<div class="Titleheader">
<span class="txt-header"><?= $this->lang->line("your_event") ?></span>
<span class="line"></span>
</div>
<!--eof Titleheader--> 
<?php 
if (!empty($wishlist_details)){?>
<?php foreach ($wishlist_details as $mylist){?>
<?php
                        $data = array('name'=>'wishlistform','id'=>'wishlistform');
                        echo form_open('wishlist/edit_wishlist',$data);
                   	 ?>
        <div class="row">
            <div class="col-md-6 listField">
            <input type="hidden" class="wishid" id="wishid" value="<?php echo $mylist->wid ?>">
            <input type="hidden" class="userid" id="userid" value="<?php echo $this->session->userdata['log_in']['user_id']?>">
            <input type="hidden" class="usetotwish" id="usetotwish" value="<?php echo $totalwishlist;?>">
				<?php  echo form_input ('wishlistname',$mylist->title,'placeholder="'.$this->lang->line("w_name_placeholder").'" class="form-control" id="wishlistname"'); ?>
                <!--<label id="wishlistname-error" class="error" for="wishlistname" style="display:none"></label> -->
                <div class="css-error"> <?= form_error('wishlistname'); ?></div>
            </div>
            <div class="col-md-3 listField">
                <select class="form-control" name="Event Category" id="eventcategory">
                <option selected><?= $this->lang->line("w_event_cat") ?></option>
                 <?php $cat =  (isset($mylist->category)?$mylist->category:$row->name);  ?>
                <?php 
                foreach($groups as $row)
                { 
				 $check = $row->name == $mylist->category ? ' selected="selected"' : '';
                echo '<option value="'.$cat.'"'.$check.'>'.$row->name.'</option>';
                }
                ?>
                </select>
            </div>
        </div>  
     <div class="row">                                          
    <div class="col-md-6 listField url"><?= $this->lang->line("w_date") ?> :<br>
		<?php
        $sDate = new DateTime($mylist->e_startdate);
        $start['date']=$sDate->format('d-m-y');
        ?>
        <?php  echo form_input ('eventdate',$start['date'],'placeholder="'.$this->lang->line("w_date").'" 
        class="form-control" id="eventdate"'); ?> 
    </div>
    <div class="col-md-6 listField"><?= $this->lang->line("w_end_date") ?> :  <button type="button" class="btn btn-info btn-xs" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Max 60 days after Event Date">
        <i class="fa fa-question" aria-hidden="true"></i>
        </button><br>
        <?php
        $sDate = new DateTime($mylist->e_enddate);
        $end['date']=$sDate->format('d-m-y');
        ?>
        <?php  echo form_input ('eventenddate',$end['date'],'placeholder="'.$this->lang->line("w_end_date").'" 
        class="form-control" id="eventenddate"'); ?>
        <br />- <?= $this->lang->line("w_warning_info") ?>---</div>
    </div>  
    
    
        <div class="row">                                          
        <div class="col-md-12 listField url"><p><?= $this->lang->line("w_p_url") ?> :</p>
        <div class="">  
        <div class="total-form">
        <div class="col-md-5" style="padding:0px"><div class="urllink"><?php echo base_url();?></div></div>
        <div class="col-md-6 lodimag" style="padding:0px">
        <?php  echo form_input ('seturl',$mylist->url,'placeholder="'.$this->lang->line("url").'" 
                            class="form-control mod" id="seturl"'); ?> 
        </div>
        <div class="col-md-1 lodimg" >                           
        <img src="<?php echo base_url().'files/loader/loader.gif'?>" id="loding" style="display:none" />
        <img src="<?php echo base_url().'files/loader/check.png'?>" id="checkloding" style="display:none" />
        <img src="<?php echo base_url().'files/loader/cross.png'?>" id="crossloding" style="display:none" />                            
        </div>  
        <div class="clearfix"></div> 
        </div>
        
        </div>                      
        </div>
        
        </div>
    
	<p></p>                  
    <div class="checkbox">
    <?php $cashcheck = ($mylist->is_case_received =="yes" ) ? 'checked' : ''; ?>
        <label><h4> <?php echo form_checkbox('cashgift', 'yes',$cashcheck,'id="cashgift"'); ?>
        <?= $this->lang->line("w_receive_cash_gift") ?></h4></label>
        	<p><?= $this->lang->line("w_receive_info_1") ?><br>
        	<span class="text-danger"><?= $this->lang->line("w_receive_info_2") ?></span></p>
    </div>
                      
    <div class="row">  <!--form col 1-->
        <div class="col-md-6">
        	<h4><?= $this->lang->line("w_gift_sent") ?></h4>
            
			<?php
			
            $username = explode(" " ,$mylist->name);
            $name1 = $username[0];
            $name2 = $username[1];
            ?>
            <div class="row">
                <div class="col-xs-6 listform"> 
					<?php  echo form_input ('firstname',$name1,'placeholder="'.$this->lang->line("f_name").'" 
                    class="form-control" id="firstname"'); ?> 
                </div>
                <div class="col-xs-6 listform"> 
                <?php  echo form_input ('lastname',$name2,'placeholder="'.$this->lang->line("f_name").'" 
                class="form-control" id="lastname"'); ?> 
                </div>
            </div>
            
        <div class="listform">
		<?php if ($status == true){ ?>
        <?= $this->lang->line("w__prev_event_add") ?> : 
        <select class="form-control" name="Event" onchange="getvalue(this.value)">
        <option selected><?= $this->lang->line("w_event_choose") ?></option>
        <?php 
        foreach($all_wishlist as $row)
        { 
        echo '<option value="'.$row->title.'" onchange="getvalue(this.value)">'.$row->title.'</option>';
        }
        ?>
        </select>
        
        <?php } ?>
        </div>
        <?php
		$add = explode("," ,$mylist->street_address);
		$add1 = $add[0];
        $add2 = $add[1];
		?>
        <div class="listform">
			<?php  echo form_input ('addr',$add1,'placeholder="'.$this->lang->line("w_event_add").'" 
            class="form-control" id="addr"'); ?> 
        </div>                       
        <div class="listform">
        <?php  echo form_input ('addr2',$add2,'placeholder="'.$this->lang->line("w_event_add_2").'" 
        class="form-control" id="addr2"'); ?>
        </div>
        <div class="listform">
        <?php  echo form_input ('pcode',$mylist->zip,'placeholder="'.$this->lang->line("w_event_postcode").'" 
        class="form-control" id="pcode" size="10"'); ?>
        
        <div class="css-error"> <?= form_error('pcode'); ?></div>
        </div>
        <div class="listform">
        <?php  echo form_input ('dist',$mylist->district,'placeholder="'.$this->lang->line("w_event_district").'" 
        class="form-control" id="dist" size="20"'); ?>
        </div>
        <div class="listform">
        <?php 
        $options = array(
        '' => $this->lang->line("w_event_city"),
        '1507' => 'Aceh',
        '1508' => 'Bali',
        '1509' => 'Banten',
        '1510' => 'Bengkulu',
        '1511' => 'BoDeTaBek',
        '1512' => 'Gorontalo',
        '1513' => 'Jakarta Raya',
        '1514' => 'Jambi',
        '1515' => 'Jawa Barat',
        '1516' => 'Jawa Tengah',
        '1517' => 'Jawa Timur',
        '1518' => 'Kalimantan Barat',
        '1519' => 'Kalimantan Selatan',
        '1520' => 'Kalimantan Tengah',
        '1521' => 'Kalimantan Timur',
        '1522' => 'Kepulauan Bangka Belitung',
        '1523' => 'Lampung',
        '1524' => 'Maluku',
        '1525' => 'Maluku Utara',
        '1526' => 'Nusa Tenggara Barat',
        '1527' => 'Nusa Tenggara Timur',
        '1528' => 'Papua',
        '1529' => 'Riau',
        '1530' => 'Sulawesi Selatan',
        '1531' => 'Sulawesi Tengah',
        '1532' => 'Sulawesi Tenggara',
        '1533' => 'Sulawesi Utara',
        '1534' => 'Sumatera Barat',
        '1535' => 'Sumatera Selatan',
        '1536' => 'Sumatera Utara',
        '1537' => 'Yogyakarta',
        );
        //echo form_dropdown('city', $options,$mylist->city, 'class="form-control" id="city"'); 
		echo form_input ('city','JAKARTA RAYA','class="form-control" id="city" readonly');
        ?>
        <div class="css-error"> <?= form_error('city'); ?></div>
        </div>
        <div class="listform">
        <?php  echo form_input ('mobileno',$mylist->ph_no,'placeholder="'.$this->lang->line("w_event_contact").'" 
        class="form-control" id="mobileno"'); ?>
        <div class="css-error"> <?= form_error('mobile'); ?></div>
        </div>
        </div><!--form col 1-->
    <!--form col 2-->
    <div class="col-md-6">
    <h4><?= $this->lang->line("privacy")?></h4>
    <div class="radio">

    <label>
    <?php //$ispublic =  (isset($mylist->is_public)?$mylist->is_public:''); ?>
    <?php //echo form_radio('privacy', 'public',(isset($ispublic) && $ispublic == 'public')? 'checked':'', 'id=privacy');?>
    <input id="privacy" type="radio" name="privacy" value="public" <?php echo ($mylist->is_public=='public') ? 'checked' : '' ?> size="17"  />

    <?= $this->lang->line("w_event_privacy")?> <strong><?= $this->lang->line("public")?></strong>
    </label></div>
    <div class="checkbox">
    <label> 
    <?php $shipcheck = ($mylist->is_show_shipping_address =="yes" ) ? 'checked' : ''; ?>                      
    <?php echo form_checkbox('shiprivacy', 'yes',$shipcheck,'id = "shiprivacy" '); ?> <?= $this->lang->line("w_event_add_show")?>
    </label></div> 
    <div class="radio">
    <label>  
    <?php //echo form_radio('privacy', 'private',(isset($ispublic) && $ispublic == 'private')? 'checked':'', 'id=password');?> 
    
    <input id="password" type="radio" name="privacy" value="private" <?php echo ($mylist->is_public=='private') ? 'checked' : '' ?> size="17"  />
    
    <?= $this->lang->line("w_event_visitor_pwd")?> 
    <div class="formlist"> 
	<?php foreach ($getpassword as $getpasswords)
				{
					 $pass = $getpasswords->wishlist_password;
				}?>
                
	<?php 
	if ($pass !=''){
	echo form_password ('visitpassword','','class="form-control" id="visitpassword" placeholder="'.$this->lang->line("sent_password").'"'); 
	echo '<input type="hidden" value="'.$pass.'" id="hidpassdeta">';
	}else{
	echo form_password ('visitpassword','','class="form-control" id="visitpasswordblanck" ');
	}
	 ?> 
    
    <div id="visitpassword-error" class="error" for="visitpassword" style="display:none"></div> 
    <div class="css-error"> <?= form_error('visitpassword'); ?></div><span class="text-danger"> * <em><?= $this->lang->line("w_event_visitor_pwd_warning")?> </em> </span></div>
    
    
    
    </label></div> 
    <?php //echo form_submit('submit','Save and continue','class="btn btn-primary next-step" id="submit"'); ?>
    <button type="button" class="btn btn-primary btn-lg next-step editcontinue"><?= $this->lang->line("edit_continue") ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
    <p>&nbsp; </p>
    </div>
    <?php } ?> 
 <?php } ?>                         
    </div>
<?= form_fieldset_close(); ?>
<?= form_close(); ?> 

 
</div><!--eof form col 2--> 
<!--eof Form Section 1-->









<!--Form Section 2-->
<div class="tab-pane" role="tabpanel" id="step2">
 <div id="img" class="modal fade" role="dialog">
</div>
 <!--Titleheader--> 
<div class="Titleheader">
<span class="txt-header"><?= $this->lang->line("personalize") ?></span>
<span class="line"></span>
</div>
<!--eof Titleheader-->

	<?php
    $data = array('name'=>'personalizeform','id'=>'personalizeform');
    echo form_open_multipart('wishlist/edit_personalize',$data);
    ?>
    <?php if (!empty($wishlist_details)){?>
    <?php foreach ($wishlist_details as $mylist){?>
    
        <div class="row">
        <div class="col-md-4"><div class="listForm">
        <input type="hidden" name="wishidforimage" class="wishid" id="wishid" value="<?php echo $mylist->wid ?>">
        <div style="margin: 7% auto 0px; display: table; position: relative;" id="imageMain">
        <!-- begin_picedit_box -->
        <!-- end_picedit_box -->
        <input type="hidden" value="" id="wid" name="lastwid"/>
        <input type="hidden" value="" id="imagecropdata" name="imagecropname" class="imagecropclass"/>
        
       
			<?php 
			
				if($mylist->wishlist_image !=''){
					
					if(is_file($this->config->item('image_path')."wishlist/".$mylist->wid."/100-100/".$mylist->wishlist_image)){
					$imagepath= base_url()."photo/wishlist/".$mylist->wid."/100-100/".$mylist->wishlist_image;
					}
					else{
			        $imagepath= base_url().'photo/wishlist/no-event.png';
			      } 
					
				}else{
					$imagepath= base_url().'photo/wishlist/no-event.png';
				}
            ?>
         <!--<iframe src="<?php echo $imagepath; ?>" width="365px" height="235px" style="border:0px;" id="uplode_img" class="imgreviewcrop"></iframe>-->
        <img style="width:365px" class="imgreviewcrop" id="uplode_img" src="<?php echo $imagepath; ?>" />
        <div id="file-upload-cont">
        <input type="file" name="wisimage" id="wisimage" size="20" />
        <div id="my-button"><?= $this->lang->line("w_img_upload") ?></div>
        </div>
        <!--<div class="text-danger">** Max image size 2 MB under the image box</div>-->
      </div>
     </div></div>
        <div class="col-md-8">
        <?= $this->lang->line("w_note") ?> :<br>
            <div class="listForm">
				<?php
                $data = array(
                'name'        => 'guest_note',
                'class'       => 'form-control',
                'id'          => 'guest_note',
                'rows'        => '3',
                'cols'        => '',
				'maxlength'   => '250',
				'value'       => $mylist->rsvp_info,
                );
                echo form_textarea($data);
				 echo'<div class="panelNT pull-right cractercount">250</div>';
                ?>
                <div class="css-error"> <?= form_error('guest_note'); ?></div>
            <span class="text-danger"><em><?= $this->lang->line("w_rsvp_info") ?> </em> </span></div>
        <div class="listForm"><?= $this->lang->line("w_e_date") ?><br>
        <?php
        $sDate = new DateTime($mylist->event_date);
        $event['date']=$sDate->format('d-m-y H:i:s');
        ?>
        </div>
        
        <div class='input-group date' id='datetimepicker1'>
                    <?php  echo form_input ('eventpdate',$event['date'],'placeholder="'.$this->lang->line("w_e_date").'" 
							class="form-control" id="eventpdate" data-date-format="DD-MM-YYYY hh:mm A"'); ?> 
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
        <div class="css-error"> <?= form_error('eventpdate'); ?></div>
        
        <?php //echo (isset($_POST['event_address'])?$_POST['event_address']:'') ?>
        <div class="listForm"  id="controls"><?= $this->lang->line("w_e_add") ?>:<br>
        <!--<textarea name="Event Address" cols="" rows="3" class="form-control"></textarea>-->
        <input id="from" name="event_address" class="form-control locationg" type="text" value="<?php echo $mylist->wishlist_address ?>">
        </div>
        <div class="listform">
        <ul class="list-inline">
        <li><button type="button" class="btn btn-default prev-step previous"><?= $this->lang->line("previous") ?></button></li>
        <li><button type="button" class="btn btn-info next-step skipletter"><?= $this->lang->line("skip") ?></button></li>
        <li>
        <?php /*?><?php echo form_submit('submit','Save and Finish','class="btn btn-primary btn-info-full next-step" id="submit"'); ?><?php */?>
        <button type="button" class="btn btn-primary btn-info-full next-step editpersonalize"><?= $this->lang->line("w_e_save_finish") ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
        </li>
        </ul> 
        </div>    
        </div><!--eof col md 12-->
</div>

     <?php }}?>  


	<?= form_fieldset_close(); ?>
    <?= form_close(); ?>
    
    
</div>
<!--eof Form Section 2-->
<!--Form Section 3-->
<div class="tab-pane" role="tabpanel" id="complete">
<!--Titleheader--> 
<div class="Titleheader">
<span class="txt-header"><?= $this->lang->line("w_almost_ready") ?></span>
<span class="line"></span>
</div>
<!--eof Titleheader-->
 
<div class="row">
<div class="col-md-12">
<p class="text-center"><?= $this->lang->line("w_add_item") ?></p> 
<div class="col-md-12">
<div class="listField text-center">
<a href="<?= base_url("my-wishlist");?>"><button type="button" class="btn btn-primary btn-lg"><i class="fa fa-heart" aria-hidden="true"></i> <?= $this->lang->line("w_my_wishlist") ?></button></a>
<p></p></div></div>
<div class="col-md-12">
<!--Titleheader--> 
<!--<div class="Titleheader">
<span class="txt-header">BROWSE GIFT CATEGORIES</span>
<span class="line"></span>
</div>-->
<!--eof Titleheader-->
<span class="txt-header" style="font-size:15px"><?= $this->lang->line("w_browse_gift") ?></span>
<div class="row">
    <div class="col-sm-3">
        <?php $count=0; ?>
 <?php if(!empty($maincategory)){
        foreach ($maincategory as $mcat ){
			
			$urlN=base_url().'search';
			echo '<a href="'.$urlN.'">'.$mcat->name."</a>".'<br>';
			$count++;
			?>
    
            <?php if ($count % 12 == 0):?>
                </div>
                <div class="col-sm-3">
            <?php endif;?>
       <?php } }?> 
    </div>
</div>


</div>
</div>
</div>
</div>
</div>
</section>
<script>
var base_url = '<?php echo base_url(); ?>';


$(document).on("change","#wisimage",function(){
	
	
	 var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("wisimage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uplode_img").src = oFREvent.target.result;
        };
	
	var data = new FormData($('#personalizeform')[0]);
	
	var lhtmt='<div class="coverdisble"></div><div class="imgwaitprocess" id="loaderBg2"><div class="imgloaderwait"><div></div><div style=""><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"><p style="position: relative; width: 100%; text-align: center; font-size: 15px; color: rgb(255, 255, 255); left: -60px; top: -113px;">Due to Large Image, Its taking time. Please Wait.......</p></div></div></div>';
	var wishlistId = $('#wid').val();
	$("#imageMain").append(lhtmt);
 
 
     $.ajax({
               type:"POST",
               url:base_url+"wishlist/upload_image_personalize",
               data:data,
               mimeType: "multipart/form-data",
               contentType: false,
               cache: false,
			   dataType:"json",
               processData: false,
			   
               success:function(data)
              {
                        if(data.msg == 'success'){
							
							$('#loaderBg2').hide();
							$('.coverdisble').hide();
							
							
							
							}
 
               }
       });
	
	
});



 
</script>


</div>
</div> <!-- /.container -->
</div> <!-- /#content -->
</div>

<script>
$(document).ready(function() {
		 
	$("#imgInp").change(function(){
    readURL(this);
}); 
    
});  
function getvalue(selectedValue)
 {
    $.ajax({
        url: '<?php echo base_url();?>wishlist/get_wishlist_address',
        type: 'POST',
        data: {option : selectedValue},
		dataType : "json",
		//contentType : "application/json",
        success: function(result) {
		
			$.each(result, function (i,j) {
				
				var name =j['name'];
				var sname = name.split(" ");
				var fname = sname[0];
				var lname = sname[1];
				
				var address =j['street_address'];
				var adr = address.split(",");
				var adr1 = adr[0];
				var adr2 = adr[1];
				
				$('#firstname').val(fname);
				$('#lastname').val(lname);
				$('#addr').val(adr1);
				$('#addr2').val(adr2);
				$('#pcode').val(j['zip']);
				$('#dist').val(j['district']);
				$('#mobileno').val(j['ph_no']);
				//$('#city').val(j['city']);
				
			});
		
		}
		
    });
}

</script> 
<script src="<?php echo base_url();?>assets/js/cropbox.js"></script>
       
<script type="text/javascript">


$(function () {
		$('#datetimepicker1').datetimepicker({
			
			useCurrent: false,
			});
	});
	
	$(document).ready(function() {
		
	$('#guest_note').keyup( function() {
			var value = $(this).val();
			
			if (value.length == 0) {
			$(".cractercount").html(0);
			return;
			}
			else
			{
			var regex = /\s+/gi;
			var wordCount = value.trim().replace(regex, ' ').split(' ').length;
			var totalChars = value.length;
			var charCount = value.trim().length;
			var charCountNoSpace = value.replace(regex, '').length;
			$(".cractercount").html(250-charCount);
			}
   });
		
		
		$("#eventdate").datepicker({
    format: "dd-mm-yy",
    weekStart: 1,
   
    autoclose: 1,
    todayHighlight: 1,
    //startView: 2,
    minView: 2,
    startDate : 'now',
    endDate : new Date('2050-09-08'),
}).on('changeDate', function (selected) {
	$('#eventenddate').datepicker('remove');
	$('#eventenddate').val('');
    var minDate = new Date(selected.date.valueOf());
	//var date2 = $('#eventdate').datepicker('getDate', '+60d');
	var date2 =  new Date(selected.date.valueOf());
	
	 date2.setDate(date2.getDate()+60);
	
    $('#eventenddate').datepicker({clearBtn: true,format: "dd-mm-yy",autoclose: 1,	startDate: minDate,	endDate:date2,});
});
		
	
	});
	

</script> 
<style>
.listForm input#wisimage {
    height: 42px;
    opacity: 0;
    position: relative;
    width: 100%;
    z-index: 100;
	cursor: pointer;
}
#my-button {
    border: 0 solid black;
    border-radius: 5px;
    padding: 10px 15px;
    position: absolute;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 1;
}
#file-upload-cont {
    background: #50c9cf none repeat scroll 0 0;
    color: #fff;
    position: relative;
}
.form-control.error {
    border-color: red;
}
</style>      