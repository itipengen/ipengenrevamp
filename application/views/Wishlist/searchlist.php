<style>
#content #checkout .table tbody tr td img, #content #basket .table tbody tr td img, #content #customer-order .table tbody tr td img {
    width: 50% !important;
}
div#admintable_wrapper .row:first-child{ display:none;}
.dataTables_info {
    display: none;
}
table.dataTable thead .sorting_asc::after {
    content: "";
}
table.dataTable thead .sorting::after {
    content: "";
    opacity: 0;
}
.card{
   box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
   transition: all 0.5s ease-in-out 0s;
   margin-bottom:20px;
   padding: 2px;
   height: 323px;
}
.card:hover{
  transform: scale(1.1);
}
.img-responsive-custom{
	display: block;
	margin:auto;
    /*height: 170px;
    width: 100%;*/
}
</style>
<div id="content">
  <div class="container white">
    <div class="">
      <div class="col-md-12"><h3><?= $this->lang->line("search_list") ?> </h3></div>
      <?php if(count($result)){ ?>
      
        <?php 
      if(!empty($result)){ foreach($result as $row){ 
      if(is_file($this->config->item("image_path")."wishlist/".$row["id"]."/100-100/".$row["image"]))
      {$imagePath = base_url()."photo/wishlist/".$row["id"]."/100-100/".$row["image"];}
      elseif(is_file($this->config->item('image_path').'/product/'.$row["id"].$this->config->item('thumb_size').$row["image"]))
      {$imagePath = base_url("photo/product/".$row["id"].$this->config->item('thumb_size').$row["image"]);}
      else
      {$imagePath = base_url("photo/wishlist/no-event.png");}
      if($row["url"] != "")
      { $url = base_url("~".$row["url"]); }
      else
      { $url = base_url("p/".$row["id"]."-".slugify($row["title"])); }
      $owner_name = $row["fname"]." ".$row["lname"];
    ?>
       <div class="col-md-3 text-center">
        <div class="card">
          <a href="<?= $url ?>" target="_blank"> <img src="<?= $imagePath; ?>" alt="<?= $row["title"]; ?>" class="img-responsive-custom"> </a>
                   <p class="text-card" style="margin-bottom: 5px; margin-top: 12px;"> <a href="<?= $url ?>" target="_blank"><?= ucwords($this->general->truncate($row["title"],40)); ?></a> <br> 
                    <?php if($row["fname"] != ""){  echo "Owner : ".ucwords($owner_name); } ?>&nbsp; 
                   </p>   
          </a>
        </div>
      </div>
         <?php } }else{ ?>
          <div class="col-md-12">
            <h3 align="center"> <?= '"'.$searchkey.'" '.$this->lang->line("not_found_pls_try") ?> </h3></td>
         </div>
         <?php }  ?>
      <?php }?>
           
    </div>
  </div>
</div>
