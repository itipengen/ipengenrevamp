
<div id="content">
  <div class="container white">
    <div class="row"> 
      <!--Side Bar-->
      <div class="col-md-3 nopadding"> 
        <!--Original sidebar-->
        <?php $this->load->view('common/leftsidebar')?>
        <!--Eof original sidebar--> 
      </div>
      <!-- Right Column-->
      <div class="col-md-9">
        <div class="linerows">
          <div class="alert alert-warning">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("search_wishlist_placeholder") ?>">
              <span class="input-group-btn">
              <button class="btn btn-info" type="button"><?php echo $this->lang->line("go_btn") ?></button>
              </span> </div>
            <!-- /input-group --> 
          </div>
        </div>
        <div class="dashboard-header"><?php echo $this->lang->line("my_recipients") ?> </div>
        <div class="allrows">
          <?php if(!empty($recipient_result)){?>
          <table class="table table-condensed ">
            <thead>
              <tr>
                <td>&nbsp;</td>
                <td><?php echo $this->lang->line("wishlist_title") ?></td>
                <td><?php echo $this->lang->line("wishlist_action") ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach($recipient_result as $value){
				  
				  $urlSlug=$value->url; 
               if($value->wishlist_image !=''){
			        if(is_file($this->config->item('image_path')."wishlist/".$value->wishlist_id."/100-100/".$value->wishlist_image)){
					$imagepath= base_url()."photo/wishlist/".$value->wishlist_id."/100-100/".$value->wishlist_image;
					}else{
			        $imagepath= base_url().'photo/wishlist/no-event.png';
			      	}}else{ 
					$imagepath= base_url()."photo/wishlist/no-event.png"; 
					}
			
		  ?>
              <tr>

                <td><a href="<?php echo getWishlistUrl($urlSlug);?>"><img src="<?php echo $imagepath; ?>" class="img-rounded img-responsive userphoto"></a></td>
                <td><div class="listform"><a href="<?php echo getWishlistUrl($urlSlug);?>"><?php echo $value->title; ?></a>(<?php echo ucfirst($value->fname).'  '.ucfirst($value->lname); ?>)</div>

                  <div class="listform"><?php echo $this->lang->line("end") ?> : <span class="text-danger"><strong>
                    <?php  
$date=date_create($value->e_enddate);
echo date_format($date,"d/m/Y");?>
                    </strong></span></div></td>

                <td><div class="listform"><a  id="<?php echo $value->id ?>" href="javascript:void(0)" class="btn btn-danger btn-xs delrec" title="Delete"><?php echo $this->lang->line("delete") ?></a> </div></td>

              </tr>
              <?php } ?>
              <?php /*?><tr>
          <td><a href="#wishlist"><img src="img/13912729_10154458996988035_4150869453462429502_n.jpg" class="img-rounded img-responsive userphoto"></a></td>
          <td class="wishlisttable"><div class="listform"><a href="#wishlist">Mel & Elon Musk Wedding</a></div>
          <div class="listform"> End : <span class="text-danger"><strong>16/08/2017</strong></span></div>
          


 </td> 
          <td><div class="listform"><a href="#" data-toggle="tooltip" title="" class="btn btn-danger btn-xs" data-original-title="View">Delete</a></div>

</td>
        </tr><?php */?>
            </tbody>
          </table>
          <?php }else{
	   echo $this->lang->line("no_recipients");
   }?>
        </div>
        <!--eof allrows--> 
      </div>
      <!--eof infobox for Dashboard--> 
      
    </div>
    <!--Eof Right Column--> 
  </div>
</div>
<!--container White--> 

<script>
	$(document).ready(function(){
		$('.delrec').on('click',function(){
			rid=$(this).attr('id');
			
			 if(rid!=""){
			bootbox.confirm("Are you sure want to delete?", function(result) {
				if(result){
					
					window.location.href = "<?php echo base_url()?>wishlist/delete_recipient/"+rid; 
				}
			}); 
		 }
			});
	 });


	</script> 
