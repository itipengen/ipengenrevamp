<?php
	if($my_wishlist->e_startdate!=''){
		$sDate = new DateTime($my_wishlist->e_startdate);
		$start['date']=$sDate->format('dS F o');
		$start['time']=$sDate->format('H.i a');
	}else{
		$start['date']="Not Specified";
		$start['time']="Not Specified";
	}
	if($my_wishlist->event_date!=''){
		$esDate = new DateTime($my_wishlist->event_date);
		$estart['date']=$esDate->format('dS F o');
		$estart['time']=$esDate->format('H.i a');
	}else{
		$estart['date']="Not Specified";
		$estart['time']="Not Specified";
	}
	if($my_wishlist->e_enddate!=''){
		$eDate = new DateTime($my_wishlist->e_enddate);
		$end['date']=$eDate->format('dS F o');
		$end['time']=$eDate->format('H.i a');
	}else{
		$end['date']="Not Specified";
		$end['time']="Not Specified";
	}
	
	
	
	 if(is_file($this->config->item('image_path').'userimage/'.$my_wishlist->uid.'/'.$wishlist_owner->profile_pic)){
		$imagepath = 'photo/userimage/'.$my_wishlist->uid.'/'.$wishlist_owner->profile_pic;
	 }
     else{
		$imagepath = 'assets/img/userimage.png';
	 }
?>
<div id="all">

        <div id="content">
            <div class="container white">
<?php /*?><?php if(!empty($loginUserArray['user_id'])){if($loginUserArray['user_id']!=$my_wishlist->uid){ ?>
<div class="topbutton">
	<div class="allbutton allbuttonExtra">
    <?php if($checkHasRecipient=='0'){ ?>
		<button data-target="#signup-modal" data-toggle="modal" type="button" class="btn btn-info btn-default blink btnExtraAdd" id='addRecipient'><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $this->lang->line("add_recipient_list"); ?></button><strong></strong>
        <button type="button" class="btn btn-info btn-default blink btnExtra displayNone btnExtraRemove" id='removeRecipient'><i class="fa fa-minus" aria-hidden="true"></i> <?php echo $this->lang->line("remove_recipient_list"); ?></button><strong></strong>
	<?php } else{?>
    	<button type="button" class="btn btn-info btn-default blink btnExtra" id='removeRecipient'><i class="fa fa-minus" aria-hidden="true"></i> <?php echo $this->lang->line("remove_recipient_list"); ?></button><strong></strong>
        <button type="button" class="btn btn-info btn-default blink btnExtraAdd displayNone btnExtraRemove" id='addRecipient'><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $this->lang->line("add_recipient_list"); ?></button><strong></strong>
        <?php  } ?>
        <button type="button" class="btn btn-warning btn-xs btn-xs-extra" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Please add this wishlist to buy gift">
 			<i class="fa fa-question" aria-hidden="true"></i>
		</button>
		
	</div>
</div>
<?php } } ?>
<?php */?>
<div class="row topbutton">
<div class="col-md-8 rowgap">
<table width="100%" cellspacing="5" cellpadding="5" border="0">
  <tbody><tr>
    <td width="85" valign="middle" align="left"><a href="<?php echo $wishlist_owner_url ?>"><img src="<?php echo $imagepath ?>" class="img-circle img-responsive userphoto" width="80"></a></td>
    <td valign="middle" align="left"><strong><a href="<?php echo $wishlist_owner_url ?>"><?php echo strtoupper($wishlist_owner->fname).' '.strtoupper($wishlist_owner->lname) ?></a></strong>    
</td>
  </tr>
</tbody></table>

</div>

</div>
<div class="row usertitle"><div class="wishlisttitle"><?php echo $my_wishlist->title; ?></div></div>

            
<!--User Photo and Information-->
<div class="row lightbg">
 
  <div class="col-md-6">
  <div class="imgbox">
	<?php if($my_wishlist->wishlist_image!='' && file_exists('./photo/wishlist/'.$my_wishlist->id.'/700-700/'.$my_wishlist->wishlist_image)){ ?>
   		<a href="<?php echo base_url();?>photo/wishlist/<?php echo $my_wishlist->id;?>/<?php echo $my_wishlist->wishlist_image;?>" class="image-link"><img src="<?php echo base_url();?>photo/wishlist/<?php echo $my_wishlist->id;?>/700-700/<?php echo $my_wishlist->wishlist_image;?>" class="bg-image magnify"></a>
   	<?php }else{ ?>
		<img src="<?php echo base_url();?>photo/wishlist/no-event.png" class="bg-image">   
   	<?php } ?>
    <input type="hidden" id="mapLatlong" value=""/>
  </div>       
   </div>
  
<div class="col-md-6">
<div class="wishlistinfo pull-right"> <span class="label label-danger"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $this->lang->line("end"); ?> : <?php echo $end['date']; ?></span></div>
<div class="infobox">
<div class="wishlisteventinfo">
<div class="listform">
  <h5><?php echo $this->lang->line("event_info"); ?></h5>
    <?php echo $this->lang->line("w_e_date"); ?> : <?php echo $estart['date']; ?>. <?php echo $this->lang->line("time"); ?> : <?php echo $estart['time']; ?></div>
<div class="listform">    
    <h5><?php echo $this->lang->line("special_note"); ?></h5>
   <?php echo $my_wishlist->rsvp_info?$my_wishlist->rsvp_info : $this->lang->line("not_specified"); ?></div>
  <div class="listform"><h5><?php echo $this->lang->line("event_address"); ?></h5>
     <?php echo $my_wishlist->wishlist_address?$my_wishlist->wishlist_address : $this->lang->line("not_specified"); ?> </div>
     <?php if($my_wishlist->is_show_shipping_address=='yes'){ ?>
     <div class="listform"><h5><?php echo $this->lang->line("shipping_address"); ?></h5>
     <?php echo !empty($my_wishlist_shipping)?$my_wishlist_shipping->name."<br/>".$my_wishlist_shipping->street_address."<br/>".$my_wishlist_shipping->zip."<br/>".$my_wishlist_shipping->district."<br/>".$my_wishlist_shipping->city."<br/>".$my_wishlist_shipping->ph_no : $this->lang->line("not_specified"); ?> </div>
     <?php } ?>
     <div class="listform">
     <div id="map"></div>
     <!--<div id='latDiv' style='display:none;'></div>
     <div id='longDiv' style='display:none;'></div>-->
     <input type='hidden' id='latDiv' value=''>
     <input type='hidden' id='longDiv' value=''>

    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15866.129682380873!2d106.823825!3d-6.193256!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbe4125a0d8fb1ddc!2sPullman+Jakarta+Indonesia+Thamrin+CBD!5e0!3m2!1sen!2sid!4v1472230218756" width="400" height="120" frameborder="0" style="border:1" allowfullscreen></iframe>--></div>    
 </div>
    
    </div>
  

 
  </div>
</div>
<!--eof User Photo and Information--> 
<!--Gift Cash--> 
<div class="row">
<div class="allrows">
<div class="item-title text-center"><?php echo $this->lang->line("send_cash_gift") ?></div>
<div class="item-desc text-center"><?php echo $this->lang->line("send_cash_gift_instead") ?></div>
 <form class="form-inline text-center">
  <div class="form-group">
    <label class="sr-only" for="exampleInputAmount"><?php echo $this->lang->line("amount_info") ?></label>
    <div class="input-group">
      <div class="input-group-addon"><?php echo $this->config->item('currency'); ?></div>
      <input type="text" class="form-control input-lg" id="exampleInputAmount" placeholder="<?php echo $this->lang->line("amount") ?>">
    </div>
  </div>
  <button type="button" class="btn btn-info btn-lg" data-target="#signup-modal" data-toggle="modal" id="send_gift_cash"><?php echo $this->lang->line("send_cash_gift") ?></button>
</form>
 </div>
 
</div>
           
 <!--eof Gift Cash--> 
 
 <div id='productListDiv'>
<?php  
//print_r($my_wishlist_product);die();
if(!empty($my_wishlist_product)){
	foreach($my_wishlist_product as $val){ ?>
		<div class="row">
<div class="allrows">
<!--list detail item 1-->
<div class="product-layout product-list">
<div class="product-thumb">
<div class="image"><a href="#productdetailpage">
	<?php if($val->product_image!='' && file_exists('./photo/product/'.$val->product_id.'/100-100/'.$val->product_image)){ ?>
		<img src="<?php echo base_url();?>photo/product/<?php echo $val->product_id; ?>/100-100/<?php echo $val->product_image;?>" width="228" height="228" class="img-responsive">
	<?php } else{ ?>
		<img src="<?php echo base_url();?>photo/product/no_product.jpg" width="228" height="228" class="img-responsive">
	<?php }?>
    
    </a></div>
<div>
<div class="caption">
<div class="row">
  <div class="col-md-6"> 
 
  <ul class="list-inline">
  <li><?php echo $this->lang->line("desired") ?> <span class="badge badgeblue">2</span></li>
  <li><?php echo $this->lang->line("received") ?> <span class="badge badgeblue">0</span></li>
</ul> 
  <h4><a href="<?php echo $val->slug;?>"><?php echo $val->product_name;?></a></h4>
<p>
<?php echo $val->short_description;?></p>
<p class="price">

<?php 
  $price = intval($val->price);
  $sale_price  = intval($val->sale_price);

  if(($sale_price > 0) && ($price > $sale_price)){
?>
<span class="price-new"><?php echo $this->config->item('currency') .' '. number_format($sale_price); ?></span>
<span class="price-old"><?php echo $this->config->item('currency') .' '. number_format($price); ?></span>
<?php }else{ ?>
<span class="price-new"><?php echo $this->config->item('currency') .' '. number_format($price); ?></span>
<?php } ?>
</p>
</div>
<div class="col-md-5 pull-right">

<div class="listform"><button data-target="#signup-modal" data-toggle="modal" type="button" class="btn btn-primary"><i class="fa fa-gift" aria-hidden="true"></i> <?php echo $this->lang->line("buy_this_as_gift"); ?></button></div>

<div class="buttoncontribute hhh">
<div class="listform text-success"><?php echo $this->lang->line("contribute"); ?> <em><strong><?php echo $this->lang->line("any_amount"); ?></strong></em> <?php echo $this->lang->line("gift_text"); ?></div>
<form class="form-inline">
  <div class="form-group">
    <label class="sr-only" for="exampleInputAmount"><?php echo $this->lang->line("amount_info"); ?></label>
    <div class="input-group">
      <div class="input-group-addon"><?php echo $this->config->item('currency'); ?></div>
      <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $this->lang->line("amount"); ?>">     
    </div>
    <button data-target="#signup-modal" data-toggle="modal" type="button" class="btn btn-info btn-block"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $this->lang->line("contribute_with_friends"); ?></button>
  </div>
  
</form>
<a data-target="#signup-modal" data-toggle="modal" class="btn btn-warning" role="button" data-toggle="collapse" href=".inviteEmail_<?php echo $val->product_id; ?>" aria-expanded="false" aria-controls="inviteEmail">
<i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $this->lang->line("email_friends_contribute"); ?>
</a>
<div class="collapse inviteEmail_<?php echo $val->product_id; ?>">
<div class="alert alert-warning" role="alert">
<textarea name="" rows="4" class="form-control" placeholder="Enter email address per line">
</textarea>
<br>
<button type="submit" class="btn btn-info"><?php echo $this->lang->line("invite"); ?></button>
</div>
</div>
<!--eof collapse-->
</div>
</div>
</div>

</div>

</div>
</div>
</div>
<!--eoflist detail item 1-->
</div></div>
<?php	
	}	 
}
?>

</div>                     
</div></div><!-- /.container -->         
</div>
<div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog" style="width:440px;">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="border:none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("success"); ?></h4>
              </div>
              <div class="modal-body">
              	<table class="table modalTbl"></table>
              </div>
              <div class="modal-footer">
              <div class="row">
                	<div class="col-xs-4 pull-left"><a href="<?php echo site_url("cart/view"); ?>"><button class="btn btn-info"><?php echo $this->lang->line("view_cart"); ?></button></a></div>
                  <div class="col-xs-4 pull-left"><a href="<?php echo site_url("psearch"); ?>"><button class="btn btn-primary"><?php echo $this->lang->line("continue_shop"); ?></button></a></div>
                  <div class="col-xs-4 pull-right"><a href="<?php echo site_url("checkout/index"); ?>"><button class="btn btn-success"><?php echo $this->lang->line("checkout"); ?></button></a></div>
              </div>
            </div>
        	</div>
          </div>
        </div>
<script type="text/javascript">
function initMap(myLatLng,address) {
       // var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: address
        });
  
  
}

function addMarker(map){
 $.each(map,function(index,value){
  var lat = value['lat'];
  var long = value['long'];
  var add = value['address'];
  
  var myLatLng = {lat: lat, lng: long};
  var address = value['address'];
  var hidVal = lat +','+long;
  $("#mapLatlong").val(encodeURIComponent(address));
  initMap(myLatLng,address);
 }) 
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDavhJIxC8MxS_fW7b3Dd3CEAGEkmRSn4g&callback=initMap"></script>       
<script>

var mapArr = [];
//Function to covert address to Latitude and Longitude
var getLocation =  function(address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
      	var latitude = results[0].geometry.location.lat(); 
      	var longitude = results[0].geometry.location.lng();
	  	mapArr.push({
            lat: latitude, 
            long:  longitude,
			 address:address
        });
		
		addMarker(mapArr);
      } 
  }); 
}


<?php if($my_wishlist->wishlist_address!=''){ ?>
	getLocation('<?php echo $my_wishlist->wishlist_address; ?>');
<?php } else{ ?>
	getLocation('kolkata,India');
<?php }?>

$(".listform").find("#map").click(function(){
 var hidden = $("#mapLatlong").val();
 //window.location.href = "http://maps.google.com/maps?q="+hidden;
  window.open("http://maps.google.com/maps?q="+hidden,'_blank');
});

</script>