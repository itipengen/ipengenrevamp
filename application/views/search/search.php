<style>.ui-widget-content{z-index:2 !important}
.closecross.glyphicon.glyphicon-remove {
    float: right;
	 display:none;
}


@media only screen and (max-width: 991px) {
	.filterburron {
    float: right;
	display:block;
}
  .col-md-3.filtercategory {
    background: #ffffff none repeat scroll 0 0;
    display: none;
    height: 100%;
    left: 0;
    opacity: 1;
    overflow-y: scroll;
    padding: 29px;
    position: fixed;
    top: 0;
    z-index: 1000;
}
.closecross.glyphicon.glyphicon-remove {
   display:block;
}
}
@media only screen and (max-width: 767px) {

.filterburron {
    display: block;
    left: 85%;
    padding: 6px;
    position: absolute;
    top: 71px;
}
#search > form {
    width: 85%;
}
}

</style>

<script src="<?= base_url();?>assets/frontend/js/s000search.js"></script>
<div id="content">
            <div class="container white">
                <div class="col-md-12">
                    <span>
                    <h4></h4>
                
                    </span>
                    <hr>
                </div>
                
                <div style="display:none;" class="searchGlobalJs none">
  <div class="hostName"><?php echo base_url();?></div>
    <div class="categoryid"></div>
  <div class="brand"></div>
  <div class="price_range"></div>
  <div class="id"></div>
  <div class="rShort"></div>

  <div class="villId"></div>
   <div class="hashVal"><?php echo $_SERVER['REQUEST_URI'];?></div>

</div>
             <?php  $this->load->view('search/_left', $data); ?>

                <div class="col-md-9">
                    <div id="searchall" class="searchall"></div>

                    <div class="categorybox info-bar" id="ctagrybox">
                        <div class="row">
                        <input type="hidden" name="count-limit" id="count-limit" value="12" />
                            <div class="col-sm-12 col-md-4 products-showing">
                                <?php echo $this->lang->line("product_show_1"); ?> <strong id="disp-count"></strong> <?php echo $this->lang->line("product_show_2"); ?> <strong id="total-count"></strong> <?php echo $this->lang->line("product_show_3"); ?>
                           <button class="filterburron filterbuttonclick btn">Filter</button>
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-number" id="product-show">
                                                <strong><?php echo $this->lang->line("show"); ?></strong>  <a id="12" href="javascript:void(0)" class="btn btn-default btn-sm btn-primary">12</a>  <a href="javascript:void(0)" id="24" class="btn btn-default btn-sm">24</a>  <a href="javascript:void(0)" id="0" class="btn btn-default btn-sm"><?php echo $this->lang->line("all"); ?></a> <?php echo $this->lang->line("product_show_3"); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong><?php echo $this->lang->line("sort_by"); ?></strong>
                                                <select name="sort-by" id="sort-by" class="form-control">
                                                	 <option value=""><?php echo $this->lang->line("select");?></option>

                                                    <option value="pricelow"><?php echo $this->lang->line("Price_Low_to_High"); ?></option>
                                                    <option value="pricehigh"><?php echo $this->lang->line("Price_High_to_Low"); ?></option>
                                                    <option value="nameasc"><?php echo $this->lang->line("Name_A_Z"); ?></option>
                                                    <option value="namedesc"><?php echo $this->lang->line("Name_Z_A"); ?></option>
<!--                                                    <option value="salseFirst"><?php echo $this->lang->line("sales_first"); ?></option>
-->                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="alert alert-danger prod-msg" style="display:none;"> Sorry! No product available. </div>
                    <div class="row products" id="allproductid">
 
 
                    </div>
                    <!-- /.products -->

                    <div class="pages">

                       <!-- <p class="loadMore">
                            <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>-->

                        <ul class="pagination" id="paginationid">
                        
                        </ul>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
<script>
$('.filterbuttonclick').click(function(){

  $('.filtercategory').css('display','block');


});
</script>		