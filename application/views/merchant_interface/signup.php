<div id="all">
 <div id="content">
 <div class="container white">
 <div class="row">
 <div class="col-md-9">
 <h3><?php echo $this->lang->line("merchant_register_text"); ?></h3>
 <p>&nbsp;</p>

 <?php if ( $this->session->flashdata( 'success_message' ) ) : ?>
    <div class="alert alert-success"><strong><?php echo $this->session->flashdata( 'success_message' ); ?></strong></div>
<?php endif; ?>

<?php $attributes = array('class' => 'form-horizontal', 'id' => 'merchantFrm');
			echo form_open('create-a-merchant', $attributes);?>
<div class="form-group">
 <?php  $mname_label = array('class'=>'col-sm-2 control-label');
		echo  form_label($this->lang->line("merchant_name"),'mname',$mname_label);
 ?>
<div class="col-sm-10">
<?php
	echo form_input(array('name' => 'mname','class' => 'form-control', 'id'=>'mname', 'placeholder' => $this->lang->line("merchant_name"),'required'=>'required','value' => set_value("mname"))); 
	echo form_error('mname'); 
?>
</div>
</div><!--Eof Form Field-->

<div class="form-group">
<?php  $mphone_label = array('class'=>'col-sm-2 control-label');
		echo  form_label($this->lang->line("merchant_phone"),'mphone',$mphone_label);
 ?>
<div class="col-sm-10">
<?php 
	echo form_input(array('name' => 'mphone','class' => 'form-control', 'id'=>'mphone', 'placeholder' => $this->lang->line("merchant_phone_text"),'required'=>'required','value' => set_value("mphone"))); 
	echo form_error('mphone'); 
?>
</div>
</div><!--Eof Form Field-->

<div class="form-group">
<?php  $memail_label = array('class'=>'col-sm-2 control-label');
		echo  form_label($this->lang->line("merchant_email"),'memail',$memail_label);
 ?>
<div class="col-sm-10">
<?php 
	echo form_input(array('name' => 'memail','class' => 'form-control', 'id'=>'memail', 'placeholder' => $this->lang->line("merchant_email"),'required'=>'required','value' => set_value("memail")));
	echo form_error('memail'); 
?>
</div>
</div><!--Eof Form Field-->

<div class="form-group">
<?php  $mpassword_label = array('class'=>'col-sm-2 control-label');
	   echo  form_label($this->lang->line("merchant_password"),'mpassword',$mpassword_label);
 ?>
<div class="col-sm-10">
<?php 
	echo form_input(array('name' => 'mpassword','class' => 'form-control','type' => 'password', 'id'=>'mpassword', 'placeholder' => $this->lang->line("merchant_password"),'required'=>'required','value' => set_value("mpassword"),'maxlength'=>'10'));
	echo form_error('mpassword'); 
?>
</div>
</div><!--Eof Form Field-->

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
     <button type="submit" class="btn btn-primary"><?php echo $this->lang->line("merchant_submit"); ?></button>
    </div>
</div><!--Eof Button-->


</form><!--eof form-->
</div><!--eof col-md-9-->
</div><!--eof div Row-->
</div><!--eof div Container White-->
</div><!--eof div Content-->
</div><!--eof div All-->



<script type="text/javascript">
	window.setTimeout(function() {
	    $(".alert").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
   }, 4000);
</script>