<div id="content">
 <div class="container white">
   <div class="row"> 
     <!--Side Bar-->
     <div class="col-md-3 nopadding">
       <?php $this->load->view('common/leftsidebar')?>
       
       <!--Eof original sidebar--> 
     </div>
     <!-- Right Column-->
     <div class="col-md-9">
       <div class="dashboard-header"><?php echo $this->lang->line("transaction"); ?> / <?php echo $this->lang->line("order"); ?> </div>
       <div class="wrapper wrapper-content animated fadeInRight">
       <div class="row">
       
           <div class="col-lg-12">
           <?php if (!empty ($orderid)){foreach($orderid as $orderids){?>
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                <span style="font-size:20px;"><b>Order Id - <?php echo $orderids->order_id;?></b></span>
                
                <a href="<?php echo base_url(); ?>myorder/downloadpdf/<?php echo $orderids->order_id ?>" title="Download invoice" target="_blank">
			        <img src="<?php echo base_url(); ?>assets/frontend/img/pdf-icon.png" />
                                                         
                                                         </a>
                </div>
           
		   <?php 
		   $paymentdetailss= $this->Transaction_model->getTransactiondetails($orderids->order_id);
		  // print_r($paymentdetailss);
		  if (!empty($paymentdetailss)){
		  foreach ($paymentdetailss as $paymentdetailssnew){
			 if(is_file($this->config->item('image_path').'/product/'.$paymentdetailssnew->product_id."/450X450/".$paymentdetailssnew->product_image))
					{ $imageURL = base_url()."photo/product/".$paymentdetailssnew->product_id."/450X450/".$paymentdetailssnew->product_image; } 
					else
					{$imageURL = base_url('photo/product/no_product.jpg');} 
			  
			  
			  ?>
			  <div class="ibox-content">
                   <div class="row">
                       <div class="col-md-2">
                       <img width="100px" height="100px" class="img-responsive" src="<?php echo $imageURL;?>">
                       </div>
                       <div class="col-md-4 fontdetails" >
                       <div> <?php echo $paymentdetailssnew->item_description;?></div>
                       <div>
                       <?php if($paymentdetailssnew->item_type!='cash_gift'){?>

                        <div>Quantity : <?php echo $paymentdetailssnew->quantity;?></div>

                        <?php }?>
                        <div>
                        <?php if($paymentdetailssnew->item_type=='contributed_cash_gift'){?>

                        Contributed amount for this product (<?php echo strtoupper($this->config->item('currency')).' '.number_format($paymentdetailssnew->sale_price);?>) :

                        <?php }else{?>

                        Price : 

                        <?php }?>
                        
                         <?php
						 switch($paymentdetailssnew->item_type){

							 case 'contributed_cash_gift':

							            $prce= $paymentdetailssnew->contribute_amt; 

										break;                  

							 

							 case 'cash_gift':

							 $prce= $paymentdetailssnew->cash_amt+$paymentdetailssnew->transaction_amt;

							 break;

							 case 'buy_gift':$prce= $paymentdetailssnew->price;

							 break;

						 }
						  echo $prce;
						  ?>
                       </div>
                        <div>Awb Id : <?php echo $paymentdetailssnew->awb_no;?></div>
                       </div>
                       </div>
                       
                   <div class="col-md-3 text-right fontdetails">RP <?php echo $paymentdetailssnew->price;?></div>
                   
                   <?php if( $paymentdetailssnew->item_type =='buy_gift'){ ?>
                   <div class="col-md-3 viewstatus">
                   <div class="statuschange"> <?php echo $paymentdetailssnew->p_status;?></div>
                   <a title="Product Details" href="<?php echo base_url(); ?>transaction/deatils/<?php echo $paymentdetailssnew->order_id ?>/<?php echo $paymentdetailssnew->product_id ?>" target="_blank"> <button class="btn btn-primary" type="button" name="view"> view </button></a>
                   </div>
                   <?php } ?>
                   </div>

               </div>
            <?php } } else { echo "No Product Found";}?></div>
           <?php } } else { echo "NO Order Found";}?>
           
                         
           </div>
      
       </div>
       </div>
       <!--eof allrows--> 
       <?php echo $this->pagination->create_links(); ?>
     </div>
     <!--eof infobox for Dashboard--> 
     
   </div>
   <!--Eof Right Column--> 
 </div>
</div>
	<style>
    .col-md-3.viewstatus {
    border-left: 1px solid #bbb;
    min-height: 95px;
    }
    .billadd {
    font-weight: bold;
    }
    .row.billingad .col-md-3 {
    border: 1px solid #eee;
    margin-right: 15px;
    }
    .col-md-2.viewstatus &gt; a {
    bottom: 0;
    left: 15px;
    position: absolute;
    z-index: 100;
    }
    .ibox-content {
    clear: both;
    }
    .ibox-content {
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
    color: inherit;
    padding: 15px 20px 20px;
    }
    .ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 3px 0 0;
    color: inherit;
    margin-bottom: 0;
    min-height: 48px;
    padding: 14px 15px 7px;
    }
    .hr-line-dashed {
    background-color: #ffffff;
    border-top: 1px dashed #e7eaec;
    color: #ffffff;
    height: 1px;
    margin: 20px 0;
    }
    .col-md-2.viewstatus > a {
    bottom: 0;
    left: 15px;
    position: absolute;
    z-index: 100;
    }
	.ibox.float-e-margins {
    border: 1px solid #ddd;
    margin-top: 14px;
}
.col-md-3.viewstatus > a {
    bottom: 0;
    left: 15px;
    position: absolute;
    z-index: 100;
}
.statuschange {
    font-size: 21px;
    font-weight: bold;
    text-transform: uppercase;
}
.ibox-title {
    background-color: #ceede6 !important;
}
.ibox-title img {
    float: right;
    margin-top: -5px;
}
.fontdetails { font-size:14px;}
    </style>
