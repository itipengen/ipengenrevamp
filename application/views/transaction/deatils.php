<style>
.sender-reciver{ text-transform:capitalize; font-size: 14px;}
.row.addresses {
    border: 1px solid #eae8e8;
}
#basket {
    border: 1px solid #ede9e9;
    margin-top: 35px;
}
.allrows {
    border-bottom: medium none;
}
.table > tbody {
    border: medium none !important;
}
tfoot {
    font-size: 14px !important;
   
}
.pdfgenerate {
    margin-top: 13px;
    text-align: right;
}
#customers {
    /*position: absolute;
    visibility: hidden;*/
}
.allrows span {
    float: right;
}
</style>
<div id="content">



        <div class="container white">
        <div class="row">
        <!--Side Bar-->  <div class="col-md-3 nopadding">
        <?php $this->load->view('common/leftsidebar')?>
        
        <!--Eof original sidebar-->
        </div><!-- Right Column-->
        <div class="col-md-9" id="printarea">
        <?php //print_r($orderdetails); ?>
        <div class="dashboard-header">
        <?= $this->lang->line("transaction")."/".$this->lang->line("order") ?>
        </div>





        <div class="allrows">
		<?php if (!empty ($orderdetails)) 
			{ 
			//print_r($orderdetails);
				foreach($orderdetails as $allorder)
				{
					$orderid = $allorder->order_id;
					$fname = $allorder->fname;
					$lname = $allorder->lname;
					$email = $allorder->email;
				}
			}
		?>
                    <h4><?= $this->lang->line("table_order") ?> #<?php echo (isset ($orderid)) ? $orderid : '' ?> 
                    </h4>
                                <!--<p>You send gift for <a href="mywishlistname">My Birthday
                                                Wishlist</a> on for <strong>22/06/2013</strong> and is currently <strong>Being prepared</strong>.-->                        </p>
                           
                                <div class="row addresses">
                                    <div class="col-md-6">
                                        <h5><?= $this->lang->line("shipping_address") ?></h5>
                                        <p class="sender-reciver">
                                         <?php
									  $address = "";
									if($order->shipping_firstname != "" && $order->shipping_lastname != "")
									{$address .= ucfirst($order->shipping_firstname)." ".ucfirst($order->shipping_lastname);}
									if($order->shipping_address != "")
									{$address .= "<br>".$order->shipping_address;}
									if($order->shipping_city != "")
									{$address .= ",".$order->shipping_city;}
									if($order->shipping_kecamatan != "")
									{$address .= "<br>".$order->shipping_kecamatan;}
									if($order->shipping_postcode != "")
									{$address .= ",".$order->shipping_postcode;}
									if($order->shipping_contact != "")
									{$address .= "<br> T - ".$order->shipping_contact;}
									echo $address;
									
									  ?>
										
									
										<?php //echo (isset($fname) ? $fname : '').' '.(isset($lname) ? $lname : '') ?><br>
        <?php //echo (isset($email) ? $email : '') ; ?><br>
        <?php $shipping_add = strtoupper($order->shipping_firstname.','.$order->shipping_lastname.','.$order->shipping_address.','.$order->shipping_city.','.$order->shipping_kecamatan.','.$order->shipping_postcode) ?>
                                      <input type="hidden" name="sender_shipping_address" value="<?php echo $shipping_add ?>" /></p>
                                     
                                      
                                      
                                      
                                  </div>
                                    <div class="col-md-6">
                                        <h5><?php //$this->lang->line("sender") ?> Billing Address </h5>
                                        <p class="sender-reciver"><?php //echo (isset($fname) ? $fname : '').' '.(isset($lname) ? $lname : '') ?>
                                            <?php // echo (isset($email) ? $email : '') ; ?>
                                             <?php
									  $senaddress = "";
									if($order->payment_firstname != "" && $order->payment_lastname != "")
									{$senaddress .= ucfirst($order->payment_firstname)." ".ucfirst($order->payment_lastname);}
									if($order->payment_address_1 != "")
									{$senaddress .= "<br>".$order->payment_address_1;}
									if($order->payment_city != "")
									{$senaddress .= ",".$order->payment_city;}
									if($order->payment_address_2 != "")
									{$senaddress .= "<br>".$order->payment_address_2;}
									if($order->payment_postcode != "")
									{$senaddress .= ",".$order->payment_postcode;}
									if($order->payment_country != "")
									{$senaddress .= "<br>".$order->payment_country;}
									echo $senaddress;
									
									  ?>
                                            
                                            <br>
                                            
                                            
                                            
                                            
                                      <?php $sender_add = strtoupper($order->payment_firstname.','.$order->payment_lastname.','.$order->payment_address_1.','.$order->payment_address_2.','.$order->payment_city.','.$order->payment_postcode) ?>
                                      <input type="hidden" name="sender_address" value="<?php echo $sender_add ?>" />
                                       </p>
                                  </div>
                                    
                    </div>
                           
                            
                            
                            
                            
                                <div class="table table-condensed" id="basket">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2"> <?= $this->lang->line("product") ?> </th>
                                                <th><?= $this->lang->line("table_total") ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
											<?php 
											$sum = 0;
											if (!empty ($orderdetails)) {
                                            foreach($orderdetails as $allorder){ ?>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo $allorder->pslug ?>">
                                                        <img src="<?php echo $allorder->imgpurl ?>" alt="<?php echo $allorder->name?>" class="thumbnail">
                                                    </a>
                                                </td>
                                                <td class="itemdetails"><?php echo $allorder->item_description?>
                                                <?php if ($allorder->item_type == 'buy_gift'){?>
                                                <div><?= $this->lang->line("qty") ?> : <?php echo $allorder->quantity?></div>
                                                
                     
                                                <div><?= $this->lang->line("price") ?> : <?php echo $this->config->item('currency'); ?> <?php echo ($allorder->item_type == 'contributed_cash_gift') ? number_format($allorder->contribute_amt) : number_format($allorder->price)?></div>
                                                <?php } ?>
                                                </td>
                                                <td class="itemdetails"><?php echo $this->config->item('currency'); ?> 
												<?php if($allorder->item_type == 'buy_gift')
												{
													echo number_format(($allorder->price * $allorder->quantity)); 
												}elseif($allorder->item_type == 'contributed_cash_gift'){
													echo  number_format($allorder->contribute_amt);
												}else { echo number_format($allorder->price);} ?></td>
                                                
                                                <td class="itemdetailsstatus"><?php echo $allorder->p_status; ?></td>
                                            </tr>
                                            
                                        </tbody>
                                         <?php }}?>
                                        
                                       
                                    </table>
        
                                </div>
                                  <div class="hr-line-dashed"></div>

                            <h4>Product Transaction Status History</h4>

                            

                            <div class="hr-line-dashed"></div>

                            <?php if(!empty($orderdetailshistory)){?>

                            <div class="row">

                            <div class="col-md-4"><b>Date</b></div>

                             <div class="col-md-4"><b>Remark</b></div>

                              <div class="col-md-4"><b>Status</b></div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php foreach($orderdetailshistory as $Hval){?>

                            

                            <div class="row">

                            <div class="col-md-4"><?php echo $Hval->orderchange_date?></div>

                             <div class="col-md-4"><?php echo $Hval->remark?></div>

                              <div class="col-md-4"><?php echo $Hval->status?></div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php }}else { echo '<div class="alert alert-warning"> No Transaction Status</div>';}?>
                                
                                <!-- /.table-responsive -->
                                
                                <?php /*?><div class="ordertransaction">
                                <h4>Order Transaction Status History</h4>
                            
                                <div class="hr-line-dashed"></div>
                                
                                <div class="row">
                                
                                <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>Remark</th>
                                        <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!empty($orderdetailshistory)){?>
                                    <?php foreach($orderdetailshistory as $Hval){?>
                                    
                                      <tr>
                                        <td><?php echo $Hval->orderchange_date?></td>
                                        <td><?php echo $Hval->remark?></td>
                                        <td><?php echo $Hval->status?></td>
                                      </tr>
                                     <?php }}else{ ?>
									<tr class="active col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 start_expression">
                                        <td> No Record Found</td>
                                       
                                      </tr>
										
										 
										 <?php }?>
                                    </tbody>
                                  </table>
                                
                                
                                
                                
                                
                                
                                </div>
        
        
                                   </div><?php */?>
 

   


</div><!--eof infobox for Dashboard-->
</div><!--Eof Right Column-->
</div>
            
</div>


</div>


<!-- scripts -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>
    <?php /*?><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js">
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.debug.js">
    </script><?php */?>
    
    <script type="text/javascript">
	function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter','div');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#customers')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 800
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers 
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);
}
    </script>
    <script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<style>
.hr-line-dashed {
    background-color: #ffffff;
    border-top: 1px dashed #e7eaec;
    color: #ffffff;
    height: 1px;
    margin: 20px 0;
}
</style>