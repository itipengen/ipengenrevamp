<style>
.uploadImage {
	cursor: pointer;
	background: rgb(20, 154, 171) none repeat scroll 0% 0%;
	color: rgb(255, 255, 255);
	width: 50%;
	text-align: center;
	padding: 15px;
	font-size: 13px;
}
.loader {
	background: rgba(174, 168, 168, 0.36) none repeat scroll 0 0;
	border: 3px solid #1ec2e7;
	height: 100%;
	padding: 20%;
	position: fixed;
	text-align: center;
	width: 100%;
	z-index: 500;
}
.fnameerror {
	color: #C00;
}
</style>
<div class="loader" style="display:none"><Img src="<?php echo base_url().'assets/img/ipengen-loader.gif'; ?>" style="width: 100px;"/></div>
<div id="all">
  <div id="content">
    <div class="container white">
      <div class="row"> 
        <!--Side Bar-->
        <div class="col-md-3 nopadding"> 
          <!--Original sidebar-->
          <?php $this->load->view('common/leftsidebar')?>
          <!--Eof original sidebar--> 
        </div>
        <!-- Right Column-->
        <div class="col-md-9">
          <div class="dashboard-header"><?php echo $this->lang->line("pwd_change"); ?></div>
          <?php foreach($userdata as $userdatalist){ ?>
          <div class="col-sm-12 ">
            <div class="alert alert-success msg" style="display:none"> <strong><?= $this->lang->line("success") ?>!</strong><?= $this->lang->line("information_updated") ?> </div>
          </div>
          <?php
					$changepasswordform = array('name'=>'changepasswordform','id'=>'changepasswordform');
					echo form_open('user/login',$changepasswordform);
                ?>
          <div class="linerows">
            <div class="col-md-6">
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("current_pwd"); ?></label>
                <?php $data = array(
                                          'name'        => 'cpasssword',
                                          'id'          => 'cpasssword',
                                          'value'       => '',
										  'type'	    =>'password',
                                          'placeholder' => $this->lang->line("current_pwd"),
                                          'class'       => 'form-control',
                                        );
                          echo form_input($data); ?>
                <span class="fnameerror"></span> </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("new_pwd"); ?></label>
                <?php $newdata = array(
                                          'name'        => 'newpassword',
                                          'id'          => 'newpassword',
                                          'value'       => '',
										  'type'	    => 'password',
                                          'placeholder' => $this->lang->line("new_pwd"),
                                          'class'       => 'form-control',
                                        );
                          echo form_input($newdata); ?>
                <span class="lnameerror"></span> </div>
              <!--eof listform-->
              
              <div class="listform form-horizontal">
                <label for="Account_Name"><?php echo $this->lang->line("confirm_new_pwd"); ?></label>
                <?php $condata = array(
                                          'name'        => 'conpassword',
                                          'id'          => 'conpassword',
                                          'value'       => '',
										  'type'	    =>'password',
                                          'placeholder'   => $this->lang->line("confirm_new_pwd"),
                                          'class'        => 'form-control',
                                        );
                          echo form_input($condata); ?>
                <span class="lnameerror"></span> </div>
              <!--eof listform-->
              
              <div class="listform">
                <button type="submit" class="btn btn-primary changepwd" />
                <i class="fa fa-lock" aria-hidden="true"></i> <?php echo $this->lang->line("save_pwd"); ?>
                </button>
              </div>
            </div>
          </div>
          <?php
					echo form_close();
           ?>
          <?php } ?>
        </div>
        <!--eof infobox for Dashboard--> 
        
      </div>
      <!--Eof Right Column--> 
    </div>
  </div>
  <!--container White--> 
</div>
