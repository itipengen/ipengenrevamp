<!-- *** FOOTER ***
 _________________________________________________________ -->

<div id="footer" data-animate="fadeInUp">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <h4><?php echo $this->lang->line("wishlist"); ?></h4>
        <ul>
          <?php if($this->session->userdata('log_in')){ ?>
          <li><a href="<?php echo base_url().'dashboard'; ?>" ><?php echo $this->lang->line("dashboard"); ?></a></li>
          <li><a href="<?php echo base_url().'user/logout'; ?>" ><?php echo $this->lang->line("logout"); ?></a></li>
          <?php }else{ ?>
          <li><a href="#" data-toggle="modal" data-target="#signup-modal"><?php echo $this->lang->line("login"); ?></a></li>
          <li><a href="#" data-toggle="modal" data-target="#signup-modal"><?php echo $this->lang->line("register"); ?></a></li>
          <?php } ?>
          <li>Find Wishlist</li>
        </ul>
        <hr>
        <h4><?php echo $this->lang->line("askQuestion"); ?></h4>
        <ul>
          <li><a href="<?php echo base_url('help'); ?>"><?php echo $this->lang->line("help"); ?></a> </li>
          <li><a href="<?php echo base_url('track-order'); ?>"><?php echo $this->lang->line("track_orders"); ?></a> </li>
          <li><a href="<?php echo base_url('contact-us'); ?>"><?php echo $this->lang->line("contact_us"); ?></a></li>
          <li><a data-toggle="modal" data-target="#termsandaconditionModal" href="javascript:void(0)<?php //echo base_url("term-condition"); ?>"><?php echo $this->lang->line("terms_conditions"); ?></a> </li>
        </ul>
        <hr class="hidden-md hidden-lg hidden-sm">
      </div>
      <!-- /.col-md-3 -->
      
      <div class="col-md-3 col-sm-6"> 
        
        <!--<h4>Top categories</h4>

                        <h5>Men</h5>

                        <ul>
                            <li><a href="#">T-shirts</a>
                            </li>
                            <li><a href="#">Shirts</a>
                            </li>
                            <li><a href="#">Accessories</a>
                            </li>
                        </ul>

                        <h5>Ladies</h5>
                        <ul>
                            <li><a href="#">T-shirts</a>
                            </li>
                            <li><a href="#">Skirts</a>
                            </li>
                            <li><a href="#">Pants</a>
                            </li>
                            <li><a href="#">Accessories</a>
                            </li>
                        </ul>-->
        
        <hr class="hidden-md hidden-lg">
      </div>
      <!-- /.col-md-3 -->
      
      <div class="col-md-3 col-sm-6">
        <h4><?php echo $this->lang->line("company"); ?></h4>
        <ul>
          <li><a href="<?= base_url('about-us'); ?>" class=""><?php echo $this->lang->line("about_us"); ?></a> </li>
          <li><a href="<?= base_url('affiliate'); ?>"><?php echo $this->lang->line("affiliate"); ?> </a></li>
          <li><a href="<?= base_url('careers'); ?>"><?php echo $this->lang->line("careers"); ?></a> </li>
          <li><a  href="<?php echo base_url('privacy-security'); ?>"><?php echo $this->lang->line("privacy_security"); ?> </a></li>
          <li><a href="<?= base_url('contact-us'); ?>" class=""><?php echo $this->lang->line("contact_us"); ?></a></li>
        </ul>
        <a href="<?= base_url('contact-us'); ?>"><?php echo $this->lang->line("goto_contact"); ?></a>
        <hr class="hidden-md hidden-lg">
      </div>
      <!-- /.col-md-3 -->
      
      <div class="col-md-3 col-sm-6">
        <h4><?php echo $this->lang->line("get_the_news"); ?></h4>
        <p class="text-muted">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
        <div class="input-group">
          <input type="text" class="form-control" id="subscription_email" placeholder="E-Mail ID">
          <span class="input-group-btn">
          <button class="btn btn-default" type="button" id="subscription"><?php echo $this->lang->line("subscribe"); ?></button>
          </span> </div>
        <!-- /input-group -->
        
        <hr>
        <h4><?php echo $this->lang->line("stay_in_touch"); ?></h4>
        <p class="social"> <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a> <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a> <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a> <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a> <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a> </p>
      </div>
      <!-- /.col-md-3 --> 
      
    </div>
    <!-- /.row --> 
    
  </div>
  <!-- /.container --> 
</div>
<!-- /#footer --> 

<!-- *** FOOTER END *** --> 

<!-- *** COPYRIGHT ***
 _________________________________________________________ -->
<div id="copyright">
  <div class="container">
    <div class="col-md-6">
      <p class="pull-left">© 2016 iPengen.</p>
    </div>
    <div class="col-md-6">
      <p class="pull-right"><a href="<?php base_url();?>/create-a-merchant" style="color:#fff;" ><?php echo $this->lang->line("create_merchant"); ?></a> | <?php echo $this->lang->line("policy_privacy"); ?> | <a data-toggle="modal" data-target="#termsandaconditionModal" href="javascript:void(0);" style="color:#fff;" ><?php echo $this->lang->line("terms_conditions"); ?></a> | <?php echo $this->lang->line("feedback"); ?> </p>
    </div>
  </div>
</div>
<!-- *** COPYRIGHT END *** -->

</div>
<!-- /#all -->
<?php


  $sess_data = $this->session->userdata("site_lang");
  if(!empty($sess_data)){
    $this->lang_id = $sess_data['laguage_id'];
  }else{
    $this->lang_id = 1;
  }

  $this->db->select('*');
  $this->db->from('ig_static_block');
  $this->db->join('ig_static_block_text','ig_static_block.block_id = ig_static_block_text.block_id','inner');
  $this->db->where(array('ig_static_block.block_id' => 11,'ig_static_block_text.lang_id' => $this->lang_id ));
  $query = $this->db->get();
  $result = $query->row();

  $block = $result->block_description;
?>
<div class="modal fade" id="termsandaconditionModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $this->lang->line("terms_conditions"); ?></h4>
      </div>
      <div class="modal-body" style="overflow-y:scroll;max-height:465px;">
        <?php echo $block; ?>
    </div>
  </div>


<?php /*?>    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
 
		<script src="<?= base_url();?>assets/frontend/js/jquery-1.11.0.min.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/notify.min.js"></script>
        
        <script src="<?= base_url();?>assets/frontend/js/Moment.js"></script> 
       <script src="<?= base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	   <script src="<?= base_url();?>assets/frontend/js/bootstrap-datetimepicker.js"></script> 
	   <script src="<?= base_url();?>assets/frontend/js/bootstrap-datetimepicker.min.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/bootstrap.min.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/jquery.cookie.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/waypoints.min.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/modernizr.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/bootstrap-hover-dropdown.js"></script>
        <script src="<?= base_url();?>assets/frontend/js/owl.carousel.min.js"></script>
       
      	
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
       
        <script src="<?= base_url();?>assets/frontend/js/common.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
        <!-- Data picker -->
        <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

=======
</div><?php */?>

<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ --> 

<script src="<?= base_url();?>assets/frontend/js/jquery-1.11.0.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/notify.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/Moment.js"></script> 
<script src="<?= base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/bootstrap-datetimepicker.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/bootstrap-datetimepicker.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/bootstrap.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/jquery.cookie.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/waypoints.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/modernizr.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/bootstrap-hover-dropdown.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/owl.carousel.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/front.js"></script> 
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script> 
<script src="<?= base_url();?>assets/frontend/js/common.js"></script> 
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script> 





<!-- Data picker --> 
<script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script> 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

<!-- Latest compiled and minified JavaScript --> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script> 

<script src="<?php echo base_url();?>assets/frontend/js/bootstrap_confirm.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/bootbox.js"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo base_url();?>assets/frontend/js/short.js"></script> 



<script>

$(document).ready(function(){
	$('.filterbuttonclick').click(function(){

  $('.filtercategory').css('display','block');


});

	$('.filterbuttonclicknav').click(function(){

  $('.filtercategory').css('display','block');


});
$('.closecross ').click(function(){

  $('.filtercategory').css('display','none');


});
	
	
				//jQuery.noConflict();
 // $("area[rel^='prettyPhoto']").prettyPhoto();
				//$("#main a[rel^='prettyPhoto']").prettyPhoto();
				
				$('#mainImg').on('click',function(){
				//$("#main a[rel^='prettyPhoto']").prettyPhoto();
				//$.prettyPhoto.startSlideshow();
				//$.prettyPhoto.open();
				});
				
				//$('#footer').addClass('animated fadeInUp');
});

$(window).scroll(function() {
	//alert($(window).scrollTop());
   if($(window).scrollTop() + $(window).height()== $(document).height()) {
	   var baseUrl = window.location.href;
          $('#footer').addClass('animated fadeInUp');
	
   }
});
</script>

<script>
$(document).ready(function(){
				/*var categoryidcheck = 110;
		$("#sidemenuid ul li ul").find('#'+categoryidcheck).closest('ul').show();
		$("#sidemenuid ul li ul").find('#'+categoryidcheck).closest('ul').prev().find('.fa').removeClass('fa-plus-circle');
		//$("#sidemenuid ul li ul").find('#'+categoryidcheck).closest('ul').show();
		$("#sidemenuid ul li ul").find('#'+categoryidcheck).closest('ul').prev().find('.fa').addClass('fa-minus-circle');*/
		
		
		$(document).on('click','.parentchild>.fa.fa-plus-circle',function(){
		$('.parentchild i').removeClass('fa-minus-circle');
		$('.parentchild i').addClass('fa-plus-circle');
		$(this).removeClass('fa-plus-circle');
		$(this).addClass('fa-minus-circle');
		
		$('.parentclideshowhide').hide('slow');
		
		
		$(this).closest('li').find('.parentclideshowhide').slideDown( "slow" );
		
		
		});
		
		$(document).on('click','.parentchild>.fa.fa-minus-circle',function(){
		$('.parentchild i').removeClass('fa-minus-circle');
		$('.parentchild i').addClass('fa-plus-circle');
		$(this).removeClass('fa-minus-circle');
		$(this).addClass('fa-plus-circle');
		
		$('.parentclideshowhide').hide('slow');
		
		
		$(this).closest('li').find('.parentclideshowhide').slideUp("slow");
		
		
		});
		});

	$(document).ready(function(){
					//jQuery.noConflict();
	 // $("area[rel^='prettyPhoto']").prettyPhoto();
					//$("#main a[rel^='prettyPhoto']").prettyPhoto();
					
			$('#mainImg').on('click',function(){
			//$("#main a[rel^='prettyPhoto']").prettyPhoto();
			//$.prettyPhoto.startSlideshow();
			//$.prettyPhoto.open();
			});
			
			jQuery(document).on("click","#subscription",function(){
				var subsc_email = jQuery("#subscription_email").val();
				if(isValidEmailAddress(subsc_email))
				{
					jQuery.ajax({
							type:"POST",
							url:"<?php echo base_url("news-subscription"); ?>",
							data:{subscription:subsc_email},
							success: function(result){
									if(jQuery.trim(result) == 1)
									{
										jQuery("#subscription_email").notify("Subscription Successfully.","success");
										jQuery("#subscription_email").val("");
									}
									else
									{
										console.log(result);
										jQuery("#subscription_email").notify("E-Mail ID already Subscribe.","error");
									}
								}
						});
				}
				else
				{
					jQuery("#subscription_email").notify("Enter Valid E-Mail ID.","error");
				}
			});
	});
	
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		
		return pattern.test(emailAddress);
	};
</script>

</body></html>

