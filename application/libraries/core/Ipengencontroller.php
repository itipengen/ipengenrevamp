<?php
	/**
	* Codeigniter Controller extends in 'Ipengen_controller'
	* Author : Jayanta Rakshit
	* Year : (c)-2016
	*/
	class Ipengencontroller extends CI_controller
	{
		private $langId;
		private $lang_code;
		function __construct()
		{
			parent::__construct();
			$sess_data = $this->session->userdata("site_lang");
			if(empty($sess_data))
			{
				$lang = "english"; 
				$this->langId = 1; 
				$this->lang_code = "en";}
			else
			{
				$lang = $sess_data["laguage_name"]; 
				$this->langId = $sess_data["laguage_id"];
				$this->lang_code = $sess_data["code"];
			}
			$this->lang->load('home',$lang);

		}

		function index()
		{
			echo 'Opps..!!! Somwthing Wrong';
		}
		
		public function lang_code()
		{
			return $this->lang_code;
		}
		
		public function lang_id()
		{
			return $this->langId;
		}
	}
	
	
?>