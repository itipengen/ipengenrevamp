<?php
/**
 * General represents the common custom functions.
 */
class General
{
	public function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
	{
		if ($length == 0)
			return '';
		
		if (strlen($string) > $length) 
		{
			$length -= min($length, strlen($etc));
			if (!$break_words && !$middle) 
				$string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
			if(!$middle) 
			  return substr($string, 0, $length) . $etc;
			else 
				return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
		} 
		else 
			return $string;
	}
	
	public function getPagination($total_pages, $limit, $page, $targetpage, $adjacents)
	{
		/* Setup page vars for display. */
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		/*
			Now we apply our rules and draw the pagination object.
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		//echo $total_pages."+".$limit."+".$page."+".$targetpage."+".$adjacents;
	
		$pagination = "";
		if($lastpage > 1)
		{
			$pagination .= "<ul class=\"pagination\">";
			//previous button
			if ($page > 1)
				$pagination.= "<li><a href=\"$targetpage&page=$prev\"><</a></li>";
			//else
				//$pagination.= "<li><a class=\"javascript:void(0)\">< Vorige</a></li>";
	
			//pages
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><a href=\"javascript:void(0)\" >$counter</a></li>";
						else
							$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
					}
					$pagination.= "<li class=\"etc\"><a href=\"javascript:void(0)\">...</a></li>";
					$pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>";
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
					//$pagination.= "<a href=\"$targetpage&page=2\">2</a>";
					$pagination.= "<li class=\"etc\"><a href=\"javascript:void(0)\">...</a></li>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
						else
							$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
					}
					$pagination.= "<li class=\"etc\"><a href=\"javascript:void(0);\">...</a></li>";
					$pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>";
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
					$pagination.= "<li><a href=\"$targetpage&page=2\">2</a></li>";
					$pagination.= "<li class=\"etc\"><a href=\"javascript:void(0)\">...</a></li>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
						else
							$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
					}
				}
			}
	
			//next button
			if ($page < $counter - 1 && $lastpage!=$page)
				$pagination.= "<li><a href=\"$targetpage&page=$next\">></a></li>";
			//else
				//$pagination.= "<li><a class=\"disabled\" herf=\"javascript:void(0)\">Volgende></a></li>";
			$pagination.= "</ul>\n";
		}
		
		return $pagination;
	}
}