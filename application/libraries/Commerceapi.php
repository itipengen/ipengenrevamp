<?php
/**
 * Commerceapi represents all the functions of the api 8commerce.
 */
class Commerceapi
{
	public function authentication()
	{
		$host = "https://oms.8commerce.com/oms/index.php";
		$path = "?r=api/getInventory";
		$ch = curl_init();
		// endpoint url
		curl_setopt($ch, CURLOPT_URL, $host . $path);
		// set request as regular post
		curl_setopt($ch, CURLOPT_POST, true); 
		// set data to be send 
		curl_setopt($ch, CURLOPT_POSTFIELDS,MESSAGE);
		// set header 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'x-username:ipengen-api-test',
		 'x-password:5ue4djtkw4arae4s',
		 'x-companyid:ECIPN_TEST',
		 'x-apikey:43w9dtw378o4tdkwo3'
		));
		// return transfer as string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$json = curl_exec($ch);
		print_r($json);
		curl_close($ch);		
	}
	public function getInventory()
	{
		$host = "https://oms.8commerce.com/oms/index.php";
		$path = "?r=api/getInventory";
		$ch = curl_init();
		// endpoint url
		curl_setopt($ch, CURLOPT_URL, $host . $path);
		// set request as regular post
		curl_setopt($ch, CURLOPT_POST, true);
		// set header 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'x-username:ipengen-api-test',
		 'x-password:5ue4djtkw4arae4s',
		 'x-companyid:ECIPN_TEST',
		 'x-apikey:43w9dtw378o4tdkwo3'
		));
		// return transfer as string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$json = curl_exec($ch);
		curl_close($ch);
		print_r($json);
	}
	public function setOrder($orderHeaders)
	{	
		$host = "https://oms.8commerce.com/oms/index.php";
		$path = "?r=api/setSalesHeader";
		$ch = curl_init();
		// endpoint url
		curl_setopt($ch, CURLOPT_URL, $host . $path);
		// set request as regular post
		curl_setopt($ch, CURLOPT_POST, true);
		// set data to be send 
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($orderHeaders));
		// set header 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'x-username:ipengen-api-test',
		 'x-password:5ue4djtkw4arae4s',
		 'x-companyid:ECIPN_TEST',
		 'x-apikey:43w9dtw378o4tdkwo3'
		));
		// return transfer as string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$json = curl_exec($ch);
		curl_close($ch);
		return $json;
	}
	public function getStatus($order_id)
	{	
		$orderNo = array('x-orderno'=>$order_id);
		$host = "https://oms.8commerce.com/oms/index.php";
		$path = "?r=api/getOrderStatus";
		$ch = curl_init();
		// endpoint url
		curl_setopt($ch, CURLOPT_URL, $host . $path);
		// set request as regular post
		curl_setopt($ch, CURLOPT_POST, true);
		// set data to be send 
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($orderNo));
		// set header 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		 'x-username:ipengen-api-test',
		 'x-password:5ue4djtkw4arae4s',
		 'x-companyid:ECIPN_TEST',
		 'x-apikey:43w9dtw378o4tdkwo3'
		));
		// return transfer as string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$json = curl_exec($ch);
		return $json;
	}
	public function setDetails($orderDetails){
		if(isset($orderDetails['order_header_id'])) {
			$host = "https://oms.8commerce.com/oms/index.php";
			$path = "?r=api/setSalesDetail";
			$ch = curl_init();
			// construct your message and save in $orderDetails
			// endpoint url
			curl_setopt($ch, CURLOPT_URL, $host . $path);
			// set request as regular post
			curl_setopt($ch, CURLOPT_POST, true);
			// set data to be send
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($orderDetails));
			// set header
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			 'x-username:ipengen-api-test',
			 'x-password:5ue4djtkw4arae4s',
			 'x-companyid:ECIPN_TEST',
			 'x-apikey:43w9dtw378o4tdkwo3'
			));
			// return transfer as string
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$json = curl_exec($ch);
			curl_close($ch);
			print_r($json);
			return $json;
		}
	}
}