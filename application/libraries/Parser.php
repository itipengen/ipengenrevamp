<?php

class Parser {
    /*
    public function __construct(){
        if (!is_dir(BASEPATH.'temp/')) {
            mkdir(BASEPATH.'temp/');
        }
    }*/
    private $BASEPATH = '/system/';

    public function check_folder(){
        if (!is_dir($this->BASEPATH.'temp/')) {
            mkdir($this->BASEPATH.'temp/');
        }
    }

    public function canvastofile(){
        $img = $_POST['img'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $this->check_folder();
        $uniqid = uniqid();
        $file = $this->BASEPATH.'temp/'.$uniqid. '.png';
	    $success = file_put_contents($file, $data);

        $rst['file'] = $file;
        $rst['path'] = $this->BASEPATH.'temp/'.$uniqid. '.png';
        $rst['ext'] = 'png';
        $rst['raw_name'] = $uniqid;
        $rst['temp_name'] = $uniqid.'.png';
        $rst['success'] = $success;

        echo json_encode($rst);
        die();
    }
    
    public function formtofile(){
        $filename = $_FILES['file'];
        $this->check_folder();
        $uniqid = uniqid();
        
        $file = $this->BASEPATH.'temp/'.$uniqid. '_' . $filename['name'];
        $success = move_uploaded_file($filename['tmp_name'], $file);
        $rst['name'] = $filename['name'];
        $rst['file'] = $file;
        $rst['path'] = $this->BASEPATH.'temp/'.$uniqid. '_' . $filename['name'];

        $ext = explode('.', $filename['name']);
        $rst['ext'] = end($ext);
        $rst['raw_name'] = str_replace('.'.$rst['ext'],'',$filename['name']);
        $rst['temp_name'] = $uniqid. '_' . $filename['name'];
        $rst['success'] = $success;

        echo json_encode($rst);
        die();
    }
    
    public function longlattoname(){
        $result = $this->mdata->get_location($this->input->post('latitude'), $this->input->post('longitude'));
        foreach($result['address_components'] as $address){
            if(in_array('political', $address['types']) && !is_numeric($address['long_name'])){
                $rst['political'][] = $address;
            }else if(in_array('route', $address['types'])){
                $rst['street'] = $address;
            }else if(in_array('postal_code', $address['types'])){
                $rst['zipcode'] = $address;
            }
        }
        $data = array(
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'street' => $rst['street']['long_name'],
            'area' => $rst['political'][0]['long_name'],
            'subdistrict' => $rst['political'][1]['long_name'],
            'district' => $rst['political'][2]['long_name'],
            'province' => $rst['political'][3]['long_name'],
            'country' => $rst['political'][4]['long_name'],
            'zipcode' => $rst['zipcode']['long_name']
        );

        echo json_encode($data);
        die();
    }

    public function nametolonglat(){
        $result = $this->mdata->get_location_by_address($this->input->post('address'));

        foreach($result['address_components'] as $address){
            if(in_array('political', $address['types']) && !is_numeric($address['long_name'])){
                $rst['political'][] = $address;
            }else if(in_array('route', $address['types'])){
                $rst['street'] = $address;
            }else if(in_array('postal_code', $address['types'])){
                $rst['zipcode'] = $address;
            }
        }

        $data = array(
            'latitude' => $result['geometry']['location']['lat'],
            'longitude' => $result['geometry']['location']['lng'],
            'street' => $rst['street']['long_name'],
            'area' => $rst['political'][0]['long_name'],
            'subdistrict' => $rst['political'][1]['long_name'],
            'district' => $rst['political'][2]['long_name'],
            'province' => $rst['political'][3]['long_name'],
            'country' => $rst['political'][4]['long_name'],
            'zipcode' => $rst['zipcode']['long_name']
        );

        echo json_encode($data);
        die();
    }

    public function distance(){

        $param = array(
            'lat1' => $this->input->post('lat1'),
            'lon1' => $this->input->post('long1'),
            'lat2' => $this->input->post('lat2'),
            'lon2' => $this->input->post('long2'),
            'unit' => strtoupper($this->input->post('unit'))
        );

        $this->load->model('mdata');
        $result['distance'] = $this->mdata->parsetodistance($param);
        echo json_encode($result);
        die();
    }

    public function urltofeeds(){
        //fetching url data via curl 23 
        $html = file_get_contents($_POST['url']); 
        $doc = new DOMDocument();
        @$doc->loadHTML($html);
        $nodes = $doc->getElementsByTagName('title');
        $title = $nodes->item(0)->nodeValue;
        $metas = $doc->getElementsByTagName('meta');
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            if($meta->getAttribute('name') == 'description'){
                $description = $meta->getAttribute('content');
            }
        }
        
        $image_regex = '/]*'.'src=[\"|\'](.*)[\"|\']/Ui';
        preg_match_all($image_regex, $html, $img, PREG_PATTERN_ORDER); 
        if($title == null){
            $title = '';
        }
        $rst = array('title' => $title, 'description' => $description);
        echo json_encode($rst);
        die();
    }
    
    public function urltovideo($url){
        if(isset($_POST['url'])){
            $url = $_POST['url'];
        }

        $rst = array('type' => 'url', 'url' => '');
        if (strpos($url, 'youtube') !== false ){
            preg_match(
                    '/[\\?\\&]v=([^\\?\\&]+)/',
                    $url,
                    $matches
                );
            $id = $matches[1];

            $src = '//www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0';
            $rst['type'] = 'video';
        }
        else if (strpos($url, 'dailymotion') !== false ){
            $url = str_replace('https://', '' , $url);
            $string = explode('/', $url);
            $embed = explode('_', $string[4]);
            $src = '//www.dailymotion.com/embed/video/'.$embed[0];
            $rst['type'] = 'video';
        }
        else if (strpos($url, 'vimeo') !== false ){
            $url = str_replace('https://', '' , $url);
            $embed = explode('/', $url);
            $src = '//player.vimeo.com/video/'.$embed[1];
            $rst['type'] = 'video';
        }
        else{
            $src = $url;
        }

        if(isset($_POST['url'])) {
            $rst['url'] = $src;
            echo json_encode($rst);
            die();
        }else{
            return $src;
        }
    }
    
    public function compress_image() {
        $file = $_POST['file'];
        $quality = $_POST['quality'];
        
        $info = getimagesize($file);

        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file);

        //save it
        imagejpeg($image, $file, 80);
        
        $rst = array('file' => $file, 'quality' => $quality, 'condition' => 1);
        echo json_encode($rst);
        die();
    }
    
    public function delete_file(){
        $filepath = $_POST['file'];
        unlink($filepath);
        $rst = array('success' => 1);
        echo json_encode($rst);
        die();
    }
    
    public function check_file_size(){
        $arBytes =  array(
                        'TB' => pow(1024, 4),
                        'GB' => pow(1024, 3),
                        'MB' => pow(1024, 2),
                        'KB' => 1024,
                        'B' => 1
                    );
        
        $file_size = filesize($_POST['file']);
        
        $max_size = $_POST['max_size'] * $arBytes[$_POST['max_size_type']];
        
        if($file_size < $max_size){
            $error = 0;
        }else{
            $error = 1;
        }
        
        $rst = array('file_size' => $file_size, 'condition' => $error);
        echo json_encode($rst);
        die();
    }
    
    public function resize_image($filename){
        // Set a maximum height and width
        $width = 200;
        $height = 0;

        // Get new dimensions
        list($width_orig, $height_orig) = getimagesize($filename);

        $ratio_orig = $width_orig/$height_orig;

        if ($width/$height > $ratio_orig) {
           $width = $height*$ratio_orig;
        } else {
           $height = $width/$ratio_orig;
        }

        // Resample
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output
        imagejpeg($image_p, null, 100);
    }
    
    public function crop_image(){
        $imgUrl = $_POST['imgUrl'];
        //$imgW = $_POST['imgW'];
        //$imgH = $_POST['imgH'];
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        $contW = $_POST['contW'];
        $contH = $_POST['contH'];
        
        list($imgInitW, $imgInitH) = getimagesize($imgUrl);
        
        $imgW = $imgInitW;
        $imgH = $imgInitH;
        //print_r('Width Init : '.$imgInitW);
        //print_r('Height Init : '.$imgInitH);
        
        if($contW != $imgInitW){                
            $cropW = $cropW * (1 / ($contW / $imgInitW));
            $imgX1 = $imgX1 * (1 / ($contW / $imgInitW));
        }

        if($contH != $imgInitH){
            $cropH = $cropH * (1 / ($contH / $imgInitH));
            $imgY1 = $imgY1 * (1 / ($contH / $imgInitH));
        }
        $this->check_folder();
        $imgFile = $this->BASEPATH.'temp/'.basename($imgUrl);
        //print_r($imgFile);die();
        $jpeg_quality = 100;

        //$output_filename = BASEPATH.'/temp/'.basename($imgUrl);

        $what = getimagesize($imgFile);
        list($dummy, $ext) = explode("/", $what['mime'],2);
        $imgCreateFunc = "imagecreatefrom$ext";
        $imgSaveFunc = "image$ext";

        if (!function_exists($imgCreateFunc) || !function_exists($imgSaveFunc)) {
            die('image type not supported');
        }

        $source_image = call_user_func($imgCreateFunc, $imgFile);
        
        $resizedImage = imagecreatetruecolor($imgW, $imgH);
        imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW,$imgH, $imgInitW, $imgInitH);


        $dest_image = imagecreatetruecolor($cropW, $cropH);
        imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW,$cropH, $cropW, $cropH);

        //unlink($imgFile);
        call_user_func($imgSaveFunc, $dest_image, $imgFile);


       // $outputFile = basename($output_filename);

        $response = array(
            "status" => 'success',
            "url" => $this->BASEPATH.'temp/'.basename($imgUrl),
            "type" => $what['mime'],
            //"filename" => $output_filename
        );
        print json_encode($response);
        die();
    }

    public function strenght_password(){

        $password = $this->input->post('password');

        $strength = 0;

        /*** get the length of the password ***/
        $length = strlen($password);

        /*** check if password is not all lower case ***/
        if(strtolower($password) != $password)
        {
            $strength += 1;
        }

        /*** check if password is not all upper case ***/
        if(strtoupper($password) == $password)
        {
            $strength += 1;
        }

        /*** check string length is 8 -15 chars ***/
        if($length >= 8 && $length <= 15)
        {
            $strength += 1;
        }

        /*** check if lenth is 16 - 35 chars ***/
        if($length >= 16 && $length <=35)
        {
            $strength += 2;
        }

        /*** check if length greater than 35 chars ***/
        if($length > 35)
        {
            $strength += 3;
        }

        /*** get the numbers in the password ***/
        preg_match_all('/[0-9]/', $password, $numbers);
        $strength += count($numbers[0]);

        /*** check for special chars ***/
        preg_match_all('/[|!@#$%&*\/=?,;.:\-_+~^\\\]/', $password, $specialchars);
        $strength += sizeof($specialchars[0]);

        /*** get the number of unique chars ***/
        $chars = str_split($password);
        $num_unique_chars = sizeof( array_unique($chars) );
        $strength += $num_unique_chars * 2;

        /*** strength is a number 1-10; ***/
        $strength = $strength > 99 ? 99 : $strength;
        $strength = floor($strength / 10 + 1);

        $rst = array('password' => $password , 'strenght' =>  $strength);
        echo json_encode($rst);
        die();
    }

    public function content_download(){
        switch($this->input->post('type')){
            case 'ppt' : $url = 'learning/download_html5_ppt/';break;
            case 'zip' : $url = 'learning/download_html5_ppt_zip/';break;
            default : $url = 'learning/download/';break;
        }

        $result['url'] = $this->mbase->url_API().$url.$this->input->post('contentid').'/'.$this->input->post('pageid');

        echo json_encode($result);
        die();
    }

}
?>