<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct(){
							parent::__construct();
							$this->load->database();
    }
	/*Method Name: userregister
	  Tableaname : ig_user
	  Parameter: fname, lname, email, password
	*/

	public function userregister(){
							
							$length = 6;
							$str = "";
							$characters = array_merge(range('A','Z'), range('0','9'));
							$max = count($characters) - 1;
							for ($i = 0; $i < $length; $i++) {
												$rand = mt_rand(0, $max);
												$str .= $characters[$rand];
							}
							$uniqueid = $this->input->post('regfname').$str;
							$querycheck = $this->db->get_where('ig_user', array('user_id' => $uniqueid));
							$count = $querycheck->num_rows(); //counting result from query
							if ($count === 0) {
								$generateusername = $this->input->post('regfname').$str;
								}
							else{
								$generateusername = $this->input->post('regfname').$str.'1';
								}
							
                          
							$data = array(
										'user_id'		  =>$generateusername,
										'fname'			=>$this->input->post('regfname'),
										'lname'			=>$this->input->post('reglname'),
										'email'			=>$this->input->post('regemail'),
										'password' 		=>md5($this->input->post('regpass')),
                                        'created_date'	=>date('Y-m-d H:i:s'),
										'modification_date'	=>date('Y-m-d H:i:s'),
                                        'status' 		=> 'yes',
                      					'is_block'		=> 'no',
										//'ip_address'	=>$_SERVER['HTTP_X_FORWARDED_FOR']
										);
							
							$query['user_details']=$this->db->insert('ig_user', $data); 
							$lastinserted_id= $this->db->insert_id();
							//$dataaddress = array(
												//'uid'	=>$lastinserted_id,
												//'name'   =>$this->input->post('regfname').' '.$this->input->post('reglname'),
												//);
							//$query['user_address_details']=$this->db->insert('ig_address_book', $dataaddress);
							
							if($this->db->affected_rows() > 0){
							return $lastinserted_id;
							}
							else
								{
								return FALSE;
								}

                       } 
					   
	public function getUsername($uid)
	{
		$this->db->select('user_id');
		$this->db->from('ig_user');
 		$this->db->where('id', $uid); 
		return $this->db->get();
	}
   	/*Method Name: userlogin 
	  Tableaname : ig_user
	  Parameter: email, password
	*/

   public function userlogin(){

			$data = array(
						   'email' => $this->input->post('logemail'),
						   'password' => md5($this->input->post('logpass')),
						   'status' => 'yes',
						   'is_block' => 'no'
						   );
            $datablock = array(
						   'email' => $this->input->post('logemail'),
						   'password' => md5($this->input->post('logpass')),
						   'is_block' => 'yes'
						   );
			$queryblock =$this->db->get_where('ig_user', $datablock);

			$data = array(
						   'email' => $this->input->post('logemail'),
						   'password' => md5($this->input->post('logpass')),
						   //'is_block' => 'no',
						   );

		    $query =$this->db->get_where('ig_user', $data);

			if($query->num_rows() == 1)
					{	
						$mdata['msg'] = array('msg'=>'success');
						$mdata['result'] = $query->result();
						//return $query->result();
						return $mdata;
					}
			if($queryblock->num_rows() == 1)
					{
                        $mdata['msg'] = array('msg'=>'blocked');
						return $mdata;
					}		
		

	}
 
    /*Method Name: checkemail
	  Tableaname : ig_user
	  Parameter: email, password
	*/

   public function checkemail(){ 
		$data = array('email' => $this->input->post('email'));
       
		    $query =$this->db->get_where('ig_user',$data);
			  if($query->num_rows() > 0)
										{
										return $query->result();
										}
            
	}
	/*Method Name: fbemailinfoDetails
	  Tableaname : ig_user
	  Parameter: email
	*/
    public function fbemailinfoDetails($email){
								$query = $this->db->get_where('ig_user', array('email' => $email));
								return $query->result();
								}
    
   /*Method Name: fbinsert
	  Tableaname : ig_user
	  Parameter: email
	*/
    public function fbinsert($data,$imagefb){
							$query=$this->db->insert('ig_user', $data); 
							if($query){
							   $lasrinsertedid= $this->db->insert_id();
							   return $lasrinsertedid;
							}
							else{
							  	 return FALSE;
								}
						   }

    /*Method Name: fbinsert
	  Tableaname : ig_user
	  Parameter: email
	*/
	   public function userinfoidd($user_id){
	   							$result= array();
		     					$query = $this->db->get_where('ig_user', array('id' => $user_id)); 
			 			 		if($query->num_rows() > 0 )
								{ 
								$result = $query->result();
								}
								return $result;

	}
	
	
   public function userinfoid($user_id){
	   			$result= array();
				$query = $this->db->get_where('ig_user', array('user_id' => $user_id)); 
				if($query->num_rows() > 0 )
				{ $result = $query->result(); }
				else
				{
					$query = $this->db->get_where('ig_user', array('id' => $user_id)); 
					if($query->num_rows() > 0 )
					{ $result = $query->result(); }
				}
				return $result;

	}

	/*Method Name: userdeatails
	  Tableaname : ig_user
	  Parameter: email
	*/
   public function userdeatails($user_id){
								$query = $this->db->get_where('ig_user',array('id' => $user_id));
			 			 		return $query->result();

	}
	
	 public function userdeatails_address($user_id){
		 						$query = $this->db->get_where('ig_address_book',array('uid' => $user_id));
			 			 		$result= $query->result();
								if($query->num_rows >0){
								return $result;
								}else{
								return $result;
								
								}


	}
	
	/*Method Name: updateprofile
	  Tableaname : ig_user
	  Parameter: user_id
	*/
   public function updateprofile($user_id,$filename){
						
								/*$length = 6;
								$str = "";
								$characters = array_merge(range('A','Z'), range('0','9'));
								$max = count($characters) - 1;
								for ($i = 0; $i < $length; $i++) {
													$rand = mt_rand(0, $max);
													$str .= $characters[$rand];
								}*/
								
								if($filename == ""){
								   $ig_user_data= array(
															//'user_id'		=> $str,
															'fname'		  	=> $this->input->post('fname'),
															'lname'			=> $this->input->post('lname'),
															'email'			=> $this->input->post('email'),
															'dob'	   		=> $this->input->post('dob'),
															'mobile'		  => $this->input->post('mobile'),
															'facebook'		=> 'https://www.facebook.com/'.$this->input->post('fb'),
															'instagram'	    => 'https://www.instagram.com/'.$this->input->post('ig'),
															'twitter'		=> 'https://twitter.com/'.$this->input->post('tw'),
															'created_date'	=>date('Y-m-d H:i:s'),
															'modification_date' =>date('Y-m-d H:i:s'));
								   $ig_address_data = array(
															'name'      => $this->input->post('fname').' '.$this->input->post('lname'),
															'ph_no'		=> $this->input->post('mobile'),
															'street_address' => $this->input->post('address1'),
															'land_mark'	=> $this->input->post('address2'),
															'district'=> $this->input->post('kecamatan'),
															'city'		=> $this->input->post('city'),
															'zip'	=> $this->input->post('postcode'));
								   $ig_address_data_insert = array(
								   							'uid'			=> $user_id,
															'name'           => $this->input->post('fname').' '.$this->input->post('lname'),
															'ph_no'			 => $this->input->post('mobile'),
															'street_address' => $this->input->post('address1'),
															'land_mark'	     => $this->input->post('address2'),
															'district'       => $this->input->post('kecamatan'),
															'city'		     => $this->input->post('city'),
															'zip'	         => $this->input->post('postcode'));
									}
								else{
									$ig_user_data= array(
															//'user_id'		=> $str,
															'fname'		  	=> $this->input->post('fname'),
															'lname'			=> $this->input->post('lname'),
															'email'			=> $this->input->post('email'),
															'dob'	   		=> $this->input->post('dob'),
															'profile_pic' 	=>$filename,
															'mobile'		  => $this->input->post('mobile'),
															'facebook'		=> 'https://www.facebook.com/'.$this->input->post('fb'),
															'instagram'	    => 'https://www.instagram.com/'.$this->input->post('ig'),
															'twitter'		=> 'https://twitter.com/'.$this->input->post('tw'),
															'created_date'	=>date('Y-m-d H:i:s'),
															'modification_date' =>date('Y-m-d H:i:s'));

								   $ig_address_data = array(
															'name'           => $this->input->post('fname').' '.$this->input->post('lname'),
															'ph_no'			 => $this->input->post('mobile'),
															'street_address' => $this->input->post('address1'),
															'land_mark'	     => $this->input->post('address2'),
															'district'       => $this->input->post('kecamatan'),
															'city'		     => $this->input->post('city'),
															'zip'	         => $this->input->post('postcode'));
															
									$ig_address_data_insert = array(
															'uid'			=>$user_id,
															'name'           => $this->input->post('fname').' '.$this->input->post('lname'),
															'ph_no'			 => $this->input->post('mobile'),
															'street_address' => $this->input->post('address1'),
															'land_mark'	     => $this->input->post('address2'),
															'district'       => $this->input->post('kecamatan'),
															'city'		     => $this->input->post('city'),
															'zip'	         => $this->input->post('postcode'));
									}								
								
								
		     					$query['userdetails'] = $this->db->where('id', $user_id)
												  			     ->update('ig_user', $ig_user_data);
																 
								$userdetails = $this->db->get_where('ig_address_book',array('uid'=>$user_id));
								if($userdetails->num_rows() > 0){
												  			     

								$query['usershippingdetails'] = $this->db->where('uid', $user_id)
																		 ->update('ig_address_book', $ig_address_data);
								}
								else{
									$query['usershippingdetails'] = $this->db->insert('ig_address_book',$ig_address_data_insert);
									
									}
			 			 		if($query){
                                        return true;
										}

	}
	
  public function updateuserid($user_id){
						        $data = array('user_id'=>$this->input->post('user_id'));
		     					$userdetails = $this->db->where('id', $user_id)
												  			     ->update('ig_user', $data);
								if($userdetails){
									return true;
									}
			 			 		else{
                                        return false;
										}

	}

	public function existpasswordcheck($curentpassword,$userid){
						$data = array('id'=>$userid,'password'=>$curentpassword); 
						$query =$this->db->get_where('ig_user',$data);
			 				 if($query->num_rows() > 0)
										{
										return true;
										}


						}
	public function passwordupdate($password,$userid){
						$data = array('password'=>$password); 
						$result = $this->db->where('id', $userid)
								 		  ->update('ig_user', $data);
						if($result){
                                        return true;
										}



				}

    public function allusers(){
     		$query = $this->db->get('ig_user');
					return $query->result();

	}
	
	public function lastFiveRecordWishlistInfo($userid)
	{
		$query = $this->db->select("ig_wishlist.*")
							->from("ig_user")
							->where(array("user_id"=>$userid))
							//->join("ig_wishlist","ig_wishlist.uid = ig_user.id","left")
							->join("ig_wishlist","ig_wishlist.uid = ig_user.id")
							->order_by("ig_wishlist.created", "DESC")
							->limit("5","0")
							->get();
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	public function get_support_data()
	{
		$query = $this->db->get("ig_setting");
		return $query->num_rows() > 0 ? $query->result() : array();
	}
	
	public function getuserUrl($uid)
	{
		$query = $this->db->get_where("ig_user",array('id'=>$uid))->row();
		$url = site_url('u/'.$query->user_id); 
		return $url;
	}
	public function lastSIxRecordWishlistGift($userID)
	{
		$wishlist_query = $this->db->select("ig_wishlist.*")
									->from("ig_user")
									->join("ig_wishlist","ig_wishlist.uid = ig_user.id")
									->where(array("ig_user.user_id" => $userID))
									->order_by("ig_wishlist.created","desc")
									->limit("1","0")
									->get();
		$wishlist_query_data = $wishlist_query->result();
		if(!empty($wishlist_query_data))
		{
			$wishlistId = $wishlist_query_data[0]->id;
			$query = $this->db->select("ig_order_items.*, ig_products.product_image as product_image")
						  ->from("ig_order_items")
						  ->where(array("ig_order_items.item_type" => "buy_gift", "ig_order_items.wishlist_id " => $wishlistId))
						  ->join("ig_products","ig_products.product_id = ig_order_items.product_id")
						  ->limit("6","0")
						  ->get();
			return $query->num_rows() > 0 ? $query->result() : array();
		}
		else
		{ return array();}
		
	}

}
