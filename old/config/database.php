<?php
/**
 * Created by PhpStorm.
 * User: irfandipta
 * Date: 25/01/2017
 * Time: 20:27
 */


class DBConnect{
    private $SERVERNAME = "localhost";
    private $USERNAME = "root";
    private $PASSWORD = "";
    private $DBNAME = "ipengen";

    private $conn = NULL;

    public function CONNECT(){
        $this->conn = mysqli_connect($this->SERVERNAME, $this->USERNAME, $this->PASSWORD, $this->DBNAME);
        // Check connection
        if (!$this->conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
    }

    public function SELECT_EXECUTE($query){
        $result = mysqli_query($this->conn, $query);

        $rst = array();
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            $rst = mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
        return $rst;
    }

    public function CLOSE(){
        mysqli_close($this->conn);
    }
}




?>