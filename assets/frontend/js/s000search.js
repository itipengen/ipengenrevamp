//	Execute on document ready
var optionArr = new Array("cat","brand", "limit", "page","sort_by","priceRange");
var HOSTNAME;
var category_url;
var cat="",sort_by="",limit="",priceRange="";
var brand= new Array();
var pagename="pr";
var search= new Array();
jQuery(window).load(function(){

	/** Price **/

	$( "#slider-range" ).slider({
		range: true,
		min: 100000,
		max: 100000000,
		values: [ 100000,100000000 ],
		slide: function( event, ui ) {
		$( "#amount111" ).html( "RP" + ui.values[ 0 ]);
		$( "#amount222" ).html( "RP" + ui.values[ 1] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
		},
		stop: function( event, ui ) {
			$(document).scrollTop(0);
			var amt1 = $("#amount1").val();
			var amt2 = $("#amount2").val();
			if((amt1!= undefined) && (amt2!= undefined)){
				priceRange = amt1+'-'+amt2;
			}
			if ((priceRange.indexOf('undefined')) == -1) {
				if(category_url == undefined || category_url == ""){ category_url="price";}
			    changeSearchParam('price', priceRange);
			}
		}
	});

	/** Pagination **/

	$(document).on('click','.pagination a',function(e){
		  e.preventDefault();
		  $(document).scrollTop(0);
		  if(jQuery(this).parent().hasClass('active') || jQuery(this).parent().hasClass('etc'))
		  return false;
		  var urlVal = decodeURIComponent( jQuery(this).attr('href') );
		  urlVal = urlVal.replace("id=","#"); 
		  redirect(urlVal);
		  urlParam = urlVal.split('#');
		  goSearch(urlParam[1]);
 	});
	
	/** Category **/
	
	$(document).on("click", ".categoryAtribute", function(e) {
        e.preventDefault();
        category_url =$(this).attr("data-href");
        cat =$(this).attr("id");
        changeSearchParam('cat', cat);
    });
	
	/** Brand **/

	$(".checkbox :checkbox").each(function(e){
		$(this).click(function(){
			var ischecked = $(this).is(':checked');
			var brandVal = $(this).val(); 
			if(ischecked){
				if(category_url == undefined || category_url == ""){ category_url="brand";}
				brand.push(brandVal);
				changeSearchParam('brand',brandVal);
			}
			else{
				var index = brand.indexOf(brandVal);
				if (index > -1) {
				    brand.splice(index, 1);
				}
				changeSearchParam('brand',brandVal);
			}			
		});
	});

	/** Sorting **/

	$(document).on("change", "#sort-by", function(e) {
        e.preventDefault();
        sort_by = $(this).val();
        if(sort_by!=''){
        	if(category_url == undefined || category_url == ""){ category_url="sort_by";}
        	changeSearchParam('sort_by', sort_by); 
        }
    });

    /** Limit **/

    $(document).on("click", "#product-show a", function(e) {
        e.preventDefault();
        limit = $(this).attr("id");
        $("#count-limit").val(limit);
        $(this).parent().find('.btn-primary').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        if(category_url == undefined || category_url == ""){ category_url="limit";}
        changeSearchParam('limit', limit);
    });


	/** On Load **/
	
	HOSTNAME = base_url;
	var hashVal = window.location.hash;
	hashVal = hashVal.replace("#", "");
	getSelectedParameters(hashVal);
	goSearch(hashVal);
		
});
	
/** Make Search **/
function goSearch(paramVal) 
{
	post_paramVal=paramVal;
	var dObj = new Date();
	var loader = HOSTNAME +'assets/frontend/img/ripple.gif';
   	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="100px"></div></div></div>';
	jQuery.ajax({
	  url: HOSTNAME+"psearch/gosearch?"+paramVal,
	  type: "POST",
	  data: post_paramVal,
	  async: true,
	  //cache:false,
	  dataType: "json",
	  beforeSend: function(){
	    $("body").append(lhtml);
	   },
	  // callback handler that will be called on success
		success: function(result, textStatus, jqXHR){
				$('#loaderBg2').remove();
				var i=0;
				var j=1;
				var co=0;
				var count=0;
				var count_limit = 0;
				hml="";
				pagination="";
				var imgUrl; 
				if(result.no=='no'){
					$('.prod-msg').show().delay(5000).fadeOut('slow');
					$(".pages").html("");
				}else{
				$('.prod-msg').hide()
				$.each(result,function(){
				$(".pages").html("");
				co++;
				count = result[i].total_count;
				count_limit = result[i].count_limit;
				if(result[i].pagination!=""){
					$(".pages").html(result[i].pagination);
				}
				hml+='<div class="col-sm-4 product-cat"><div class="item-img"><a href="'+result[i].detailsUrl+'"><img src="'+result[i].imgUrl+'"  height="250px"></a></div><div  class="product-cat-desc"><div class="product-cat-name"><a href="'+result[i].detailsUrl+'">'+result[i].product_name+'</a></div><div class="product-cat-price">';
				if((result[i].sale_price > 0) && (result[i].price > result[i].sale_price)){
					hml+='<span class="price-new">'+currency+' '+result[i].sale_price+'</span>';
				    hml+='<span class="price-old">'+currency+' '+result[i].price+'</span>';
				}else{
					hml+='<span class="price-new">'+currency+' '+result[i].price+'</span>';
				}
				hml+='</div></div></a>';
				if(result[i].disview==1)
				hml+='<div class="ribbon sale"><div class="theribbon">SALE</div><div class="ribbon-background"></div></div>';
				
				if(result[i].isnew==1)
				hml+='<div class="ribbon new"><div class="theribbon">NEW</div><div class="ribbon-background"></div></div>';
				
				if(result[i].ishot==1)
				hml+='<div class="ribbon gift"><div class="theribbon">HOT</div><div class="ribbon-background"></div></div>';
				hml+='</div>';
					i++;
					j++;
					if(j==4){
						hml+='<div class="clearfix"></div>';
						j=1;
					}
				
				});
				
				}
				
			$('#loaderBg2').remove();
			var lmt=$('#count-limit').val();
				lmt=parseInt(lmt)
				

			console.log(i);
			console.log(co);
			if(i < lmt)
			{				
				$("#disp-count").html(count);
			}else{
				if(count_limit > 0)
					$("#disp-count").html(count_limit);
				else
					$("#disp-count").html(i);
			} 
			
			$("#total-count").html(count);
            $("#allproductid").html(hml);
		},
		// callback handler that will be called on error
		error: function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR.responseText);
			returnReloadResults("The following error occured: "+	textStatus, paramVal);
		}
	});
}
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function changeSearchParam(type, val)
{
    var hashVal = getSearchParameters(type, val);
    if (cat && cat != ''){
        hashVal += "&search[category]=" + cat;
    }
    if (brand && brand != ''){
        hashVal += "&search[brand]=" + brand;
    }
    if (priceRange && priceRange != ''){ 
    	if ((priceRange.indexOf('undefined')) == -1) {
			    var priceV = priceRange.split('-');
        		hashVal += "&search[priceFrom]=" + priceV[0] + "&search[priceTo]=" + priceV[1];
		}
    }
    if (sort_by && sort_by != ''){
        hashVal += "&search[sort_by]=" + sort_by;
    }
    if (limit && limit != ''){
        hashVal += "&search[limit]=" + limit;
    }else{
    	if($(document).find("#product-show a").hasClass("btn-primary")){
		  var defaultLimit = $("#product-show a").attr("id");
		  hashVal += "&search[limit]=" + defaultLimit;
		}
    }
    var rand_num =  parseInt((Math.random() * 100000000), 10); 
	var Url_ =HOSTNAME+category_url+"/"+pagename+"?rn="+rand_num+"&#"+hashVal;
    redirect(Url_);
    goSearch(hashVal);
}
function getSearchParameters(typeVal, val)
{
    var allVal = '';
    var hashVal_rightwindo = window.location.hash;
    var newHashvalue = '';
    if (hashVal_rightwindo.length > 1)
    {
        var urlPartArr = getURLParameters(hashVal_rightwindo);
        for (var tempurlpart in urlPartArr)
        {
            if (isInOptionArr(tempurlpart, optionArr) === true && tempurlpart != 'page')
            {
                allVal += tempurlpart + "=" + urlPartArr[tempurlpart] + '&';
            }
        }
    }
    return allVal;
}
function redirect(hash_val) {
	history.pushState({}, null, hash_val);
   // window.location = hash_val;
}
//	chech needla is exist in haystack or not
function isInOptionArr(needle, haystack)
{
    for (var i = 0; i < haystack.length; i++)
    {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}
//	convert hash into an array and return
function getURLParameters(paramName) 
{
	if (paramName.indexOf("#") >= 0)
	{
		var arrParams = paramName.split("#");         
		var arrURLParams = arrParams[1].split("&");      
		var arrParamValues = new Array(arrURLParams.length);     
		var i = 0;
		for (i=0;i<arrURLParams.length;i++)
		{
			
			if (arrURLParams[i].indexOf("=") > 0)
			{
				var sParam =  arrURLParams[i].split("=");
				
				if (sParam[1] != "")
				{
					arrParamValues[sParam[0]] = unescape(sParam[1]);
				}
				else
				{
					if(sParam[0]!="country_id")
					arrParamValues[sParam[0]] = "NA";
				}
			}
		}
		//console.log(arrParamValues);
		return arrParamValues;
	}
	return new Array();
}
/** Make Parameters Selected **/
function getSelectedParameters(paramName){
	var amount1, amount2;
	var splitParam = paramName.split('&');	
	$.each(splitParam,function(key,value){
		var str1= "search[category]=";
		var str2= "search[brand]=";
		var str3= "search[priceFrom]=";
		var str4= "search[priceTo]=";
		var str5= "search[sort_by]=";
		var str6= "search[limit]=";
		if(value.indexOf(str1) != -1){
		   var cat_id = value.replace(str1,'');
		   if(cat.length == 0){
 				cat = cat_id;
 				var href = $('#'+cat_id+'.categoryAtribute').attr('data-href');
 				category_url = href;
 			}
		}
		if(value.indexOf(str2) != -1){
		   var bids = value.replace(str2,'');
		   if(bids.indexOf(",") != -1){
     			if(category_url == undefined || category_url == ""){ category_url="brand";}
         		var splitIds = bids.split(',');
         		//if(brand.length == 0){
	         		$.each(splitIds,function(i,j){
	         			brand.push(j);
	         			var checked = $("input[value="+j+"]").is(':checked');
	         			if(!checked){
	         				$("input[value="+j+"]").prop('checked','checked');
	         			}

	         		});
	         	//}
		   }else{
		   		var checked = $("input[value="+bids+"]").is(':checked');
     			if(!checked){
     				$("input[value="+bids+"]").prop('checked','checked');
     			}
		   }
		}
		if(value.indexOf(str3) != -1){
		   amount1 = value.replace(str3,'');
		   $( "#amount1" ).val(amount1);
		   $( "#amount111" ).html( "RP" + amount1);
		}
		if(value.indexOf(str4) != -1){
		   amount2 = value.replace(str4,'');
		   $( "#amount2" ).val(amount2);
		   $( "#amount222" ).html( "RP" + amount2);
		}
		if(amount1!='' && amount2!=''){
			priceRange = amount1+'-'+amount2;
			if(category_url == undefined || category_url == ""){ category_url="price";}
		}
		if(value.indexOf(str5) != -1){
		   var sort_str = value.replace(str5,'');
		   if(sort_str!=''){
 				sort_by = sort_str;
 				var selected = $("#sort-by option:selected").val();
    			if(selected != sort_str){
    				if(category_url == undefined || category_url == ""){ category_url="sort_by";}
    				$("#sort-by option[value='"+sort_str+"']").prop('selected',true);
    			}
 			}
		}
		if(value.indexOf(str6) != -1){
		   var lim = value.replace(str6,'');
		   if(lim!=''){ 
 				limit = lim;
 				if(category_url == undefined || category_url == ""){ category_url="limit";}
 				$("#product-show a").parent().find('.btn-primary').removeClass('btn-primary');
        		$("#product-show a#"+lim).addClass('btn-primary');
 			}
		}
	});

}
