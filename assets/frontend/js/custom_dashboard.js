$(document).ready(function(){

	var hiddenInput_Tab1 = $("#hidTab1").val();
	var hiddenInput_Tab2 = $("#hidTab2").val();
	var hiddenInput_Tab3 = $("#hidTab3").val();

	if(hiddenInput_Tab1 == 0 || hiddenInput_Tab1 == 1){
		$(document).find('#tab_1_1 input').filter(function(){
			var empty = $(this).val();
			  if(empty == ''){
				$(this).removeAttr('readonly');
			  }
		});
	}

	if(hiddenInput_Tab2 == 0 || hiddenInput_Tab2 == 1){
		$(document).find('#tab_1_2 input').filter(function(){
			var empty = $(this).val();
			  if(empty == ''){
				$(this).removeAttr('readonly');
			  }
		});
	}

	if(hiddenInput_Tab3 == 0 || hiddenInput_Tab3 == 1){
		$(document).find('#tab_1_3 input').filter(function(){
			var empty = $(this).val();
			  if(empty == ''){
				$(this).removeAttr('readonly');
			  }
		});
	}

	var base_url = window.location.origin;



	/**** Business Tab Starts ****/



	$("#businessSubmit").on("click",function(){

		var cname = $("#cname").val();

		var cdescription = $("#cdescription").val();

		var cemail = $("#cemail").val();

		var cphone = $("#cphone").val();

		var url = base_url + '/merchant/index/business_registration';

		if(cname!=''){

			$.ajax({

					type:"POST",

					url:url,

					data:{"cname":cname,"cdescription":cdescription,"cemail":cemail,"cphone":cphone},

					dataType:'json',

					success: function(result){

							if(result){

								if(result.business_id!=0 && result.status=='success'){

									$('.successDiv').html(result.message);

									$('.successDiv').show();

									window.setTimeout(function() {

			    						$(".successDiv").slideUp(500, function(){

			        						$('.successDiv').html(''); 

				    					   });

			  						}, 3000);

								}else{

									if(result.status == 'error'){

										$('.errorDiv').html(result.message);

										$('.errorDiv').show();

										window.setTimeout(function() {

				    						$(".errorDiv").slideUp(500, function(){

				        						$('.errorDiv').html('');

					    					   });

				  						}, 3000);

									}

								}

							}

					}

			});

		}

	});



	$("#businessCancel").on("click",function(){

		$("#cname").val('');

		$("#cdescription").val('');

		$("#cemail").val('');

		$("#cphone").val('');

	});



	/**** Business Tab Ends ****/



	/**** Bank Tab Starts ****/



	$("#bankSubmit").on("click",function(){

		var acc_name = $("#acc_name").val();

		var acc_number = $("#acc_number").val();

		var bank_name = $("#bank_name").val();

		var branch_name = $("#branch_name").val();

		var state = $("#state").val();

		var city = $("#city").val();

		var url = base_url + '/merchant/index/bank_registration';



		if(acc_name!='' && acc_number!='' && bank_name!=''){

			$.ajax({

					type:"POST",

					url:url,

					data:{"acc_name":acc_name,"acc_number":acc_number,"bank_name":bank_name,"branch_name":branch_name,"state":state,"city":city},

					dataType:'json',

					success: function(result){

							if(result){

								if(result.bank_id!=0 && result.status=='success'){

									$('.successDiv').html(result.message);

									$('.successDiv').show();

									window.setTimeout(function() {

			    						$(".successDiv").slideUp(500, function(){

			        						$('.successDiv').html('');

				    					   });

			  						}, 3000);

								}else{

									if(result.status == 'error'){



										$('.errorDiv').html(result.message);

										$('.errorDiv').show();

										window.setTimeout(function() {

				    						$(".errorDiv").slideUp(500, function(){

				        						$('.errorDiv').html(''); 

					    					   });

				  						}, 3000);

									}

								}

							}

					}

			});

		}

	});



	$("#bankCancel").on("click",function(){

		$("#acc_name").val('');

		$("#acc_number").val('');

		$("#bank_name").val('');

		$("#branch_name").val('');

		$("#state").val('');

		$("#city").val('');

	});





	/**** Bank Tab Ends ****/



	/**** Store Tab Starts ****/



	$("#storeSubmit").on("click",function(){

		var store_name = $("#store_name").val();

		var sdescription = $("#sdescription").val();

		var brands = $("#brands").val();

		var url = base_url + '/merchant/index/store_registration';



		if(store_name!='' && brands!=''){

			$.ajax({

					type:"POST",

					url:url,

					data:{"store_name":store_name,"sdescription":sdescription,"brands":brands},

					dataType:'json',

					success: function(result){

							if(result){

								if(result.store_id!=0 && result.status=='success'){

									$('.successDiv').html(result.message);

									$('.successDiv').show();

									window.setTimeout(function() {

			    						$(".successDiv").slideUp(500, function(){

			        						$(".successDiv").html('');

				    					   });

			  						}, 3000);

								}else{

									if(result.status == 'error'){

										$('.errorDiv').html(result.message);

										$('.errorDiv').show();

										window.setTimeout(function() {

				    						$(".errorDiv").slideUp(500, function(){

				        						$('.errorDiv').html('');

					    					   });

				  						}, 3000);

									}

								}

							}

					}

			});

		}

	});



	$("#storeCancel").on("click",function(){

		$("#brands").val('');

		$("#store_name").val('');

		$("#sdescription").val('');

	});



	/**** Store Tab Ends ****/



	$('.fa-pencil').click(function(){

 		var input = $(this).parent().prev().find('input').val();

 		$(this).parent().prev().find('input').removeAttr('readonly');

	})
  var base_url = window.location.origin;
  var loader = base_url +'assets/img/ajax-loader.gif';
  var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';	
  $('#orderlist').DataTable();
  $('#activeorder').DataTable();
  $('#cancelorder').DataTable();
  $('#shipped').DataTable();
  $('#delivered').DataTable();
  $(".dropdown").change(function () {
    	var parentDiv=$(this).parent();
    	var orderId = parentDiv.find('#hid').val();
        var productId = parentDiv.find('#pid').val();
    	var otid = parentDiv.find('#otid').val();
    	var order_date = parentDiv.find('#order_created').val();
    	var due_date = '2016-12-29';
    	var courier_id='8commerce';
		var cod = 0;
		var order_source ='online store';
		var fulfillment_center_id = 'CIBITUNG';
		var dest_name = parentDiv.find('#shipping_name').val();
		var dest_address1 = parentDiv.find('#shipping_address').val();
		var dest_country = 'Indonesia';
		var dest_province = parentDiv.find('#shipping_kecamatan').val();
		var dest_city = parentDiv.find('#shipping_city').val();
		var dest_postal_code = parentDiv.find('#shipping_postcode').val();
    	var status = $(this).val();
    	var dataString = 'orderid='+ orderId + '&productid='+ productId + '&status='+ status + '&otid='+ otid;
    	var data = 'order_no='+orderId+'&order_date='+ order_date+'&due_date='+due_date+'&courier_id='+courier_id+'&cod='+cod+'&order_source='+ order_source+'&dest_name='+dest_name+'&dest_address1='+dest_address1+'&dest_country='+dest_country+'&dest_province='+dest_province+'&dest_city='+dest_city+'&dest_postal_code='+dest_postal_code+'&fulfillment_center_id='+fulfillment_center_id;
    	var ajaxStr = 'order_id='+orderId;
    	if (confirm('Are you sure to change this status?')) {
      			$.ajax({
      				type: "POST",
      				url: base_url+'/merchant/order/status_change',
      				data: dataString,
      				cache: false,
              		dataType: 'json',
      				beforeSend: function(){
                  		$("body").append(lhtml);
                  	},
      				success: function(result){
      				setTimeout(function(){
      					$('html, body').animate({ scrollTop: 0 }, 0);
      					$('#loaderBg2').remove();
                        if(result.msg == 'success'){
          					if(status == '1'){
          					   $( ".msg" ).html( "<div class='alert alert-success'>Your product approved successfully in order "+orderId+", please check on approved section</div>" );
          					   $.ajax({
					      				type: "POST",
					      				url: base_url+'/merchant/Commerceapi/setOrder',
					      				data: data,
					      				cache: false,
					              		dataType: 'json',
					      				beforeSend: function(){
					                  		$("body").append(lhtml);
					                  	},
					      				success: function(result){
				      					var response = result;
				      					console.log(response);
				      					var order_header_id = response.order_header_id;
				      					console.log(order_header_id);
				      					var ajaxString =  ajaxStr+'&order_header_id='+order_header_id;
				      					if(order_header_id > 0){
				      						$.ajax({
							      				type: "POST",
							      				url: base_url+'/merchant/order/updateOrderHeaderId',
							      				data: ajaxString,
							      				cache: false,
							              		dataType: 'json',
							      				beforeSend: function(){
							                  		$("body").append(lhtml);
							                  	},
							      				success: function(res){
							      					console.log(res);			
							      				}	
								      		});
				      					}				
				      				}
					      		});
          					}
          					if(status == '2'){
          					   $( ".errmsg" ).append( "<div class='alert alert-danger'>Your product cancelled successfully in order "+orderId+",please check on cancelled section</div>" );
          					}
                      	}
                        if(result.msg == 'error'){
                         }
      				}, 1000);      				
      				}	
      			});
      	}
      	return false;
  });

});