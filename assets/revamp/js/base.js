/**
 * Created by irfandipta on 10/12/2016.
 */



    jQuery.fn.outerHTML = function() {
        return jQuery('<div />').append(this.eq(0).clone()).html();
    };

    $(window).resize(function(){
        populate_height_box();
        populate_height_mobile_navigation();
    });

    $(document).ready(function () {
        numeral.defaultFormat('0,0[.]00');

        populate_height_box();
        populate_height_mobile_navigation();
        $('.open-navigation').hover(open_navigation, close_navigation);
        $('.popup-navigation').hover(open_navigation, close_navigation);
        $('.category-item').hover(open_category, close_category);

        $('.nav-action').click(navigation_action);

        $('.link-action').click(link_action);
        $("#back-popup-general").click(closepopupall);

        $("#registrationform").validate({
            // Specify validation rules
            rules: {
                fname:  "required",
                lname : "required",
                regemail: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 5
                }
            },
            // Specify validation error messages
            messages: {
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                regemail: "Please enter a valid email address"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                return false;
            }
        });
        $("#loginform").validate({
            // Specify validation rules
            rules: {
                logemail: {
                    required: true,
                    email: true
                },
                logpass: {
                    required: true,
                    minlength: 4
                }
            },
            // Specify validation error messages
            messages: {
                logpass: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                logemail: "Please enter a valid email address"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                return false;

            }
        });
        $("#forgetpwdform").validate({
            // Specify validation rules
            rules: {
                forgetemail: {
                    required: true,
                    email: true
                }
            },
            // Specify validation error messages
            messages: {
                forgetemail: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                }
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                return false;

            }
        });
    });

    function open_navigation(){
        var id = $(this).data('id');
        var elem = $('#menu-'+id).find('.icon-navigation');

        elem.removeClass('icon-nav-arrow-hide');
        elem.addClass('icon-nav-arrow-open');
        $('#popup-navigation-'+id).height('80%');
        $('#popup-navigation-'+id).css('margin-top', $('.navigation-bar').height());
    }

    function close_navigation(){
        var id = $(this).data('id');
        var elem = $('#menu-'+id).find('.icon-navigation');

        elem.removeClass('icon-nav-arrow-open');
        elem.addClass('icon-nav-arrow-hide');
        $('#popup-navigation-'+id).height(0);
        $('#popup-navigation-'+id).css('margin-top', 0);
    }
    
    function open_category(id){
        var id = $(this).data('id');
        $('.subcategory-empty').hide();
        $('.subcategory-item').hide();
        $('#category-'+id).show();

        $('.category-item').removeClass('font-bold');
        $('.category-item').removeClass('font-base');
        $('.category-item').addClass('font-light');
        $('.category-item').addClass('font-grey');

        $('#category-menu-'+id).addClass('font-bold');
        $('#category-menu-'+id).addClass('font-base');
        $('#category-menu-'+id).removeClass('font-light');
        $('#category-menu-'+id).removeClass('font-grey');

        $('.popup-navigation-content').data('active', id);
    }

    function close_category(id){
        return false;
    }
    
    function populate_height_box(){
        $('.adjust-height').each(function(){
            var source = $(this).data('source');
            var width = $('#'+source).width();
            $(this).height(width);
        });

    }

    function populate_height_mobile_navigation(){
        var height = $(window).height() - $('.navigation-bar').height();
        $('.mobile-navigation').css('margin-top', $('.navigation-bar').height());
        $('.mobile-navigation').height(height);
    }

    function navigation_action(){
        var action = $(this).data('action');
        
        switch(action){
            case 'nav_mobile' : {
                var open = $('.mobile-navigation').data('open');
                background_popup(open, 'mobile-navigation');
                if(open){
                    $('.mobile-navigation').css('right', '-100%');
                    $('.mobile-navigation').data('open', 0);
                }else{
                    $('.mobile-navigation').css('right', 0);
                    $('.mobile-navigation').data('open', 1);

                }
                break;
            }
            case 'nav-page' : {
                var id = $(this).data('id');
                $('.mobile-navigation-menu').fadeOut("normal");
                setTimeout(function(){
                    $('#mobile-navigation-menu-'+id).fadeIn(0001);
                }, 500);
                break;
            }
            case 'nav-submenu' : {
                var id = $(this).data('id');
                $('.mobile-navigation-submenu').height(0);
                $('#mobile-navigation-submenu-'+id).height('auto');
                break;
            }
        }
    }

    function background_popup(open, id){
        if(open){
            $("#back-popup-"+id).fadeOut("normal");
        }else{
            $("#back-popup-"+id).css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#back-popup-"+id).fadeIn(0001);
        }
    }

    function openpopup(open, id){
        // var open 0 = Close, 1 = Open
        if(!open){
            $("#popup-"+id).css("top", "-100%");
            $("#back-popup-general").fadeOut("normal");
        }else{
            $("#back-popup-general").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#back-popup-general").fadeIn(0001);
            $("#popup-"+id).css("top", margin_top_formula($(window), $("#popup-"+id)));
        }
    }

    function closepopupall(){
        var popup = ['.popup-large' , '.popup-small' , '.popup-xsmall', '.popup-medium']

        for(var i = 0; i< popup.length; i++){
            $(popup[i]).css("top", "-100%");
            $("#back-popup-general").fadeOut("normal");
        }
    }

    function margin_top_formula(container, element){
        var margin  = ($(container).height() - $(element).height()) / 2;
        return margin;
    }

    function open_sub_menu(id){
        var open = $('#navigation-sub-'+id).data('open');
        var elem = $('#menu-'+id).find('.icon-navigation');
        if(open){
            elem.removeClass('icon-nav-arrow-open');
            elem.addClass('icon-nav-arrow-hide');

            $('#navigation-sub-'+id).height(0);
            $('#navigation-sub-'+id).data('open', 0);
        }else{
            elem.removeClass('icon-nav-arrow-hide');
            elem.addClass('icon-nav-arrow-open');
            var wrapper_height = $('#navigation-sub-'+id+' .navigation-link-sub-wrapper').height();
            $('#navigation-sub-'+id).height(wrapper_height);
            $('#navigation-sub-'+id).data('open', 1);
        }
    }

    function link_action(){
        var action = $(this).data('action');
        switch(action){
            case 'opensub' : {
                var id = $(this).data('id');
                open_sub_menu(id);
                break;
            }
            case 'register' : {
                openpopup(1, 'register');
                break;
            }
            case 'register-submit' : {
                register_user(event);
                break;
            }
            case 'login' : {
                openpopup(1, 'login');
                break;
            }
            case 'login-submit' : {
                login_user(event);
                break;
            }
            case 'notyetregister' : {
                openpopup(0, 'login');
                openpopup(1, 'register');
                break;
            }
            case 'alereadyregister' : {
                openpopup(0, 'register');
                openpopup(1, 'login');
                break;
            }
            case 'forgotpassword' : {
                openpopup(0, 'login');
                openpopup(1, 'forgotpassword');
                break;
            }
            case 'cancelforgot' : {
                openpopup(0, 'forgotpassword');
                openpopup(1, 'login');
                break;
            }
            case 'submitforgot' : {
                forgot_password(event)
                break;
            }
            case 'terms' : {
                openpopup(1, 'terms');
                break;
            }
            case 'privacy' : {
                openpopup(1, 'privacy');
                break;
            }
            case 'removeitemcart' : {
                var rowid = $(this).data('id');
                remove_cart_item(rowid);
                break;
            }
            case 'addtowishlist' : {
                var product_id = $(this).data('id');
                $('#home-product-id').val(product_id);
                openpopup(1, 'addtowishlist');
                break;
            }
            case 'submitaddtowishlist' : {
                var wishlist_id = $('#home-wishlist-id').val();
                var product_id = $('#home-product-id').val();
                add_to_wishlist(wishlist_id, product_id);
                break;
            }
            case 'canceladdtowishlist' : {
                openpopup(0, 'addtowishlist');
                break;
            }
        }
    }

    /* AJAX REQUEST */

    function register_user(event) {
        if($("#registrationform").valid() == true){
            event.preventDefault();

            var regemail = $('#regemail').val();
            var regpass = $('#regpassword').val();
            var regfname = $('#fname').val();
            var reglname = $('#lname').val();

            var dataString = 'regemail='+ regemail + '&regpass='+ regpass + '&regfname='+ regfname + '&reglname='+ reglname;

            $('.img1loader').css('display','block');
            // AJAX Code To Submit Form.
            $.ajax({
                type: "POST",
                url: base_url+"user/signup",
                data: dataString,
                cache: false,
                dataType: 'json',
                success: function(result){
                    console.log(result);
                    setTimeout(function(){
                        $('.img1loader').css('display','none');
                        if(result.msg == 'unique'){
                            //$('#regemail-error').append(result.email.message);
                            $('#regemail-error12').css('display','block');

                        }
                        if(result.msg == 'success'){
                            window.location.href = base_url+'wishlist/create';

                        }
                        if(result.msg == 'error'){
                            // $('#regemail-error').text('Already Exists');
                            // $('#regemail-error').css('display','block');
                        }
                    }, 1000);

                }
            });

            return false;
        }else{
            return false;
        }
    }
    
    function login_user(event){
        var gethidurl = $("#setcat").val();

        if($("#loginform").valid() == true)
        {
            event.preventDefault();
            var logemail = $('#logemail').val();
            var logpass = $('#logpass').val();
            var currenturl = window.location.href;

            var dataString = 'logemail='+ logemail + '&logpass='+ logpass + '&currenturl='+ currenturl;
            var lhtmt='<div class="waitprocess" id="loaderBg"><div class="loaderwait"><div></div><div style="background:#fffff;width:102px;height:100px"><img alt="loading..." src="'+base_url+'/assets/frontend/img/ripple.gif"></div></div></div>';
            $("body").append(lhtmt);
            //$('.img1loader').css('display','block');
            // AJAX Code To Submit Form.
            $.ajax({
                type: "POST",
                url: base_url+"user/login",
                data: dataString,
                cache: false,
                dataType: 'json',
                success: function(result){
                    console.log(result);
                    $('#loaderBg').remove();
                    setTimeout(function(){
                        //$('.img1loader').css('display','none');
                        if(result.msg == 'alreadylogedin'){
                            //window.location.href = base_url;
                        }
                        if(result.msg == 'success'){
                            //window.location.href = base_url+'dashboard';
                            window.location.href= base_url+'create-a-wishlist';
                        }
                        if(result.msg == 'success' && gethidurl !=''){
                            window.location.href = currenturl;
                        }
                        if(result.msg == 'blocked'){

                            $('.autherror').css('display','block');
                            $('.autherror').text('Access Denied!');
                            setTimeout(function(){
                                $('.autherror').css('display','none');
                            }, 3000);

                        }
                        if(result.msg == 'error'){

                            $('.autherror').css({'display':'block','border':'none'});
                            $('.autherror').text('Username or Password is incorrect');
                            setTimeout(function(){
                                $('.autherror').css('display','none');
                            }, 3000);
                        }
                    }, 1000);
                }
            });

            return false;
        }
        else{

            return false;
        }
    }

    function forgot_password(event){
        var email = $('#forgetemail').val();
        if($("#forgetpwdform").valid() == true){
            event.preventDefault();
            $('.loader').css('display','block');
            // AJAX Code To Submit Form.
            var dataString = 'email='+ email;
            $.ajax({
                type: "POST",
                url: base_url+"user/forgetpassword",
                data: dataString,
                cache: false,
                dataType: 'json',
                success: function(result){
                    $('.loader').css('display','none');
                    if(result.msg == 'notmatched'){
                        $(".fnameerror").text('Password not matched');
                    }
                    if(result.msg == 'success'){
                        $(".forgetpwdmsg").show();
                        setTimeout(function(){
                            $(".forgetpwdmsg").hide();
                            openpopup(0, 'forgotpassword');
                        },8000);// trigger validation test
                    }
                }
            });
            return false;
        }
        else{
            return false;
        }
    }

    function remove_cart_item(rowid){
        if(rowid){
            $.ajax({
                url: base_url+"cart/removeItem/"+rowid,
                success: function(result){
                    //console.log(result);
                    $('#sub-item-'+rowid).remove();
                    $('#sub-item-tablet'+rowid).remove();

                    if($('#navigation-sub-shoppingcart').data('open')){
                        var wrapper_height = $('#navigation-sub-shoppingcart .navigation-link-sub-wrapper').height();
                        $('#navigation-sub-shoppingcart').height(wrapper_height);
                    }

                    if($('#navigation-sub-shoppingcarttablet').data('open')){
                        var wrapper_height = $('#navigation-sub-shoppingcarttablet .navigation-link-sub-wrapper').height();
                        $('#navigation-sub-shoppingcarttablet').height(wrapper_height);
                    }
                    
                    if(result == 0){
                        $('#cart-item').html('');
                        $('#cart-item-tablet').html('');
                         setTimeout(function () {
                             $.ajax({
                                 type: "POST",
                                 url: base_url+"cart/destroy",
                                 dataType: 'json',
                                 success: function(flag){
                                     var total = 0;
                                     $('#cart-subtotal').html(numeral(total).format());
                                     $('#cart-subtotal-tablet').html(numeral(total).format());
                                 ;}
                             });
                         }, 100);
                    }else{
                        $('#cart-item').html('&nbsp;&nbsp;'+result);
                        $('#cart-item-tablet').html('&nbsp;&nbsp;'+result);
                        $.ajax({
                            type: "POST",
                             url: base_url+"cart/info",
                             dataType: 'json',
                             success: function(res){
                                 //console.log(res);
                                 var total = 0;
                                 $.each(res,function(i,k){
                                     total += k.subtotal;
                                 });

                                 $('#cart-subtotal').html(numeral(total).format());
                                 $('#cart-subtotal-tablet').html(numeral(total).format());
                             }
                         });
                     }
                }
            });
        }else{
            return false;
        }
    }

    function add_to_wishlist(wishlist_id, product_id){
        $.ajax({
            type: "POST",
            url: base_url+'general/get_user_id',
            data: {},
            cache: false,
            success: function(result){
                if(result!='' && product_id!='' && wishlist_id!=''){
                    var dataString = 'wid='+wishlist_id+'&tag="addProductToWishlist"&uid='+result+'&pid='+product_id;
                    $.ajax({
                        type: "POST",
                        url: base_url+'product/addProductToWishlistAjax',
                        data: dataString,
                        cache: false,
                        success: function(result){
                            $('#home-product-id').val('');
                            $('.wishlistmsg').show();
                            setTimeout(function(){
                                $('.wishlistmsg').hide();
                                openpopup(0,'addtowishlist');
                            }, 3000);
                        }
                    });
                }
            }
        });

    }