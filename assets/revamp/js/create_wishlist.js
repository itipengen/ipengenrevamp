/**
 * Created by irfandipta on 20/02/2017.
 */

$(document).ready(function(){
    adjustImageFileBox();
})

function adjustImageFileBox(){
    var margin = ($('#img-file').height() / 2) - 20;
    $('#button-browse-image').css('margin-top', margin);

    $('#textarea-personalized').height($('#box-img-file').height());
}