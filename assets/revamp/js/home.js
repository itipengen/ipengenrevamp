/**
 * Created by irfandipta on 10/12/2016.
 */



    $(window).resize(function(){
        adjustSlide();
    });

    $(document).ready(function(){
        adjustSlide()

        setInterval(slideShow, 5000);

        $('.product-options').hover(popthover, poptout);
        $('.product-highlight-options-items').hover(popthover, poptout);
        $('.link-home-action').click(link_action);
    })

    function adjustSlide() {
        $('.slider-item').each(function(){
           $(this).width($('.slider-box').width());
        });

        var counter_items = $('.slider-item').length;
        var width = counter_items * $('.slider-item').width();
        $('.slider-container').width(width)
    }

    function slideShow(){
        var box_size = $('.slider-box').width();
        var max_items = $('.slider-item').length;
        var counter = $('.slider-container').data('counter');

        if(counter < max_items){
            var margin = box_size * (-1) * counter;
            $('.slider-container').css('margin-left', margin);

            $('#slider-page-'+counter).addClass('bg-base');
            $('#slider-page-'+counter).removeClass('bg-grey');

            $('#slider-page-'+(counter-1)).removeClass('bg-base');
            $('#slider-page-'+(counter-1)).addClass('bg-grey');

            counter++;
            $('.slider-container').data('counter', counter);
        }else{
            $('#slider-page-'+(counter-1)).removeClass('bg-base');
            $('#slider-page-'+(counter-1)).addClass('bg-grey');

            counter = 0;
            var margin = box_size * (-1) * counter;
            $('.slider-container').css('margin-left', margin);
            $('#slider-page-'+counter).addClass('bg-base');
            $('#slider-page-'+counter).removeClass('bg-grey');
            counter++;
            $('.slider-container').data('counter', counter);
        }

    }

    function popthover(){
        var counter = $(this).data('counter');
        var size_height = $('#product-options-item-'+counter).height() / $('#product-options-item-'+counter+' .product-highlight-options-item').length;
        $('#product-options-item-'+counter+' .product-highlight-options-item').each(function () {
            $(this).height(size_height);
        });
    }
    
    function poptout() {
        var counter = $(this).data('counter');
        var size_height = 0;
        $('#product-options-item-'+counter+' .product-highlight-options-item').each(function () {
            $(this).height(size_height);
        });
    }