$(document).ready(function(){



	var base_url = window.location.origin;



	$("#singleCat").on("change",function(){

		var catid = $(this).val();

		var url = base_url + '/merchant/product/subcategory';

		var html;

		if(catid!=''){

			$.ajax({

					type:"POST",

					url:url,

					data:{"catid":catid},

					dataType:'json',

					success: function(result){

							if(result){ 

								$.each(result, function (index, value) {

								   html += "<option></option><option value='"+value.sub_id+"'>"+value.name+"</option>";

								});



								$("#singleSub").html(html);



							}

					}

			});

		}

	});
	
	$("#singleCatadmin").on("change",function(){

		var catid = $(this).val();

		var url = base_url + '/admin/merchant/subcategory';

		var html;

		if(catid!=''){

			$.ajax({

					type:"POST",

					url:url,

					data:{"catid":catid},

					dataType:'json',

					success: function(result){

							if(result){ 

								$.each(result, function (index, value) {

								   html += "<option></option><option value='"+value.sub_id+"'>"+value.name+"</option>";

								});



								$("#singleSub").html(html);



							}

					}

			});

		}

	});



	$("#sample_csv_file").click(function() {

    	window.location = base_url+'/files/sample/Product-Sheets.xlsx';

	});

$("#sample_image_folder").click(function() {

    	window.location = base_url+'/files/imagezip/images.zip';

	});

	var loader = "<?php echo base_url('assets/img/ajax-loader.gif'); ?>";

	var lhtml='<div class="waitprocess" id="loaderBg2"><div class="loaderwait"><div></div><div><img alt="loading..." src="'+ loader +'" width="140px"></div></div></div>';



	/* Init DataTables */

	$('#productTable').DataTable({

		order:[[0,"desc"]],

		pageLength: 25,

	}); /*{"pageLength": 5}*/

	$(".imageZoom").imageTooltip();



	  /****Switcher****/

  // For Single Checkbox

  // var elem = document.querySelector('.js-switch');

  // Switchery(elem, { color: '#1AB394' });

  // For multiple Checkbox

  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch')); //console.log();

  elems.forEach(function(data) {

    var switchery = new Switchery(data,{ color: '#1AB394' });

  });

  /***after Click on Pagination Link*******/

$(document).on('click', '.paginate_button a', function(){

	$( "#admintable tbody tr" ).each(function() {

		 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){

			var elem = this.querySelector('.js-switch');

		 	Switchery(elem, { color: '#1AB394' });

			var elem_new = this.querySelector('.js_switch_feature');

		 	Switchery(elem_new, { color: '#1AB394' });	

			$(".imageZoom").imageTooltip();

		 }

	});

});



$(document).on("keyup",".dataTables_filter input", function(){

	$( "#admintable tbody tr" ).each(function() {

		 if(!$(this).find('td:eq(6) span').hasClass('switchery') && !$(this).find('td:eq(7) span').hasClass('switchery')){

			var elem = this.querySelector('.js-switch');

		 	Switchery(elem, { color: '#1AB394' });

			var elem_new = this.querySelector('.js_switch_feature');

		 	Switchery(elem_new, { color: '#1AB394' });	

			$(".imageZoom").imageTooltip();

		 }

	});

});





	



  /*****Product Delete******/

$(document).on('click','.product_delete',function(){

    var product_id = $(this).attr('id');

         if(product_id!=""){

      bootbox.confirm("Are you sure want delete ?", function(result) {

        if(result){
			

          window.location.href = base_url + '/merchant/product/delete/'+product_id ; 

        }

      }); 

     }

  });

 /*****Product Status Change*****/

$(document).on("change",".js-switch",function(e){

	  var productId = $(this).attr("value");

	  var ProName = $(this).closest("tr").find("td:eq(1)").html();

	  if (this.checked) {

		  $.ajax({

			  url:"<?php echo base_url('merchant/product/status/'); ?>" + productId + "/1",

			  beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).attr("checked", "checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" activated.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).removeAttr("checked");}

				  }

			  });

	  }else{

		  $.ajax({

			  url:"<?php echo base_url('merchant/product/status/'); ?>" + productId + "/0",

			   beforeSend: function(){

					  	$("body").append(lhtml);

					  },

			  success: function(result){

				  	$('#loaderBg2').remove();

					if($.trim(result) != 0)

					{

						$(this).removeAttr("checked");

						$(".admin-list-msg").css("display","");

						$(".admin-flash-msg").html(ProName +" deactivated.");

						$(".admin-list-msg").delay(2000).fadeTo(4000).slideUp("slow");

					}

					else

					{$(this).attr("checked", "checked");}

				  }

			  });

	  };

});







});